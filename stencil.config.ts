import { promises as fs } from 'fs';
import { Config } from '@stencil/core';
import { postcss } from '@stencil/postcss';
import autoprefixer from 'autoprefixer';
import { JsonDocs } from '@stencil/core/internal';

async function generateCustomElementsJson(docsData: JsonDocs) {
  const jsonData = {
    version: "1.0.0",
    modules: docsData.components.map((component) => ({
      kind: "javascript-module",
      path: component.filePath,
      declarations: [
        {
          kind: "class",
          name: component.fileName,
          tagName: component.tag,
          description: component.docs,

          attributes: component.props
            .filter((prop) => prop.attr)
            .map((prop) => ({
              name: prop.attr,
              type: {
                text: prop.type,
              },
              description: prop.docs,
              default: prop.default,
              required: prop.required,
            })),

          members: [
            ...component.props
            .filter((prop) => !prop.attr)
            .map((prop) => ({
              kind: "field",
              name: prop.name,
              type: {
                text: prop.type
              },
              description: prop.docs,
              default: prop.default,
              required: prop.required,
            })),

            ...component.methods.map((method) => ({
              kind: "method",
              name: method.name,
              description: method.docs,
              signature: method.signature,
            })),
          ],

          events: component.events.map((event) => ({
            name: event.event,
            type: {
              text: event.detail,
            },
            description: event.docs,
          })),

          slots: component.slots.map((slot) => ({
            name: slot.name === '' ? " " : slot.name,
            description: slot.docs,
          })),

          cssProperties: component.styles
            .filter((style) => style.annotation === 'prop')
            .map((style) => ({
              name: style.name,
              description: style.docs,
            })),

          cssParts: component.parts.map((part) => ({
            name: part.name,
            description: part.docs,
          })),
        },
      ]
    })),
  };

  await fs.writeFile(
    './custom-elements.json',
    JSON.stringify(jsonData, null, 2),
  );
}

export const config: Config = {
  namespace: 'roadtrip',
  plugins: [
    postcss({
      plugins: [
        autoprefixer()
      ]
    })
  ],
  globalStyle: 'src/global/app.css',
  globalScript: 'src/global/app.ts',
  extras: {
    enableImportInjection: true,
  },
  outputTargets: [
    {
      type: 'docs-vscode',
      file: 'dist/html.html-data.json',
      sourceCodeBaseUrl: 'https://gitlab.com/mobivia-design/roadtrip/components/-/blob/master/',
    },
    {
      type: 'dist',
      dir: 'dist',
      copy: [
        { src: 'fonts', warn: true },
        { src: '../icons' },
        { src: '../illustrations' },
        { src: '../assets' }
      ]
    },
    {
      type: 'docs-readme',
      strict: true
    },
    {
      type: 'docs-custom',
      generator: generateCustomElementsJson
    },
    {
      type: 'www',
      serviceWorker: null
    },
    {
      type: 'dist-hydrate-script',
    }
  ],
  testing: {
    allowableMismatchedPixels: 200,
    pixelmatchThreshold: 0.05,
    waitBeforeScreenshot: 20,
    emulate: [
      {
        viewport: {
          width: 360,
          height: 640,
          deviceScaleFactor: 3,
          isMobile: true,
          hasTouch: true,
          isLandscape: false,
        },
      },
      {
        viewport: {
          width: 768,
          height: 1024,
          deviceScaleFactor: 2,
          isMobile: true,
          hasTouch: true,
          isLandscape: false,
        },
      },
      {
        viewport: {
          width: 1920,
          height: 1080,
          isMobile: false,
          hasTouch: false,
          isLandscape: true
        }
      },
    ]
  },
};
