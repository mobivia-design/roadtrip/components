# Contributing

We follow the [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) development model. Which means that we have a `develop` branch and `master` branch. All development is done under feature branches, which are (when finished) merged into the development branch. When a new version is released we merge the `develop` branch into the `master` branch.


## Git flow

If you would like to use [git flow tools](http://danielkummer.github.io/git-flow-cheatsheet/) you are more than welcome to. We use it and it's pretty nifty. If you see a `feature\` prefix on a comment then that is git flow automating branch creation.


## Submit a pull request:

* If your push triggered a 'you just pushed...' message from GitLab then click on the button provided by that pop up to create a pull request.
* If not, then create a pull request and point it to your branch.
* Make sure that you're attempting to merge into `develop` and not `master`.
* Get your code reviewed by another contributor.

NOTE: Tests *must* pass in order for the code to be merged.

NOTE: Always do a `git pull` on `develop` before you start working to capture the latest changes.


## Commit Message Guidelines

We have very precise rules over how our git commit messages can be formatted.  This leads to **more
readable messages** that are easy to follow when looking through the **project history**.  But also,
we use the git commit messages to **generate the change log**.

### Commit Message Format

Each commit message consists of a **header**, a **body** and a **footer**.
The header has a specific format that includes a **type**, a **scope** and a **subject**:

```text
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer than 100 characters! This allows the message to be easier
to read on Gitlab as well as in various git tools.

The footer should contain a [closing reference to an issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically) if any.

Samples:

```text
chore(gitlab-ci): setup the E2E test
```

```text
feat(logging): add logging middlewares

- Use a specific middleware
- And to add a correlation ID to this logger

Closes #54
```

### Revert

If the commit reverts a previous commit, it should begin with `revert:`, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

### Type

Must be one of the following:

- **docs**: Documentation only changes
- **feat**: A new feature
- **fix**: A bug fix
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **test**: Adding missing tests or correcting existing tests
- **chore**: Updating grunt tasks or CI configuration; no production code change

### Scope

The scope should be the name of the resources or others parts of the application (as perceived by the person reading the changelog generated from commit messages).

The following is the list of possible scopes:

component name
- **input**
- **toast**
- **button**


And also

- **deployment**
- **build**
- **test**

### Subject

The subject contains a succinct description of the change:

- use the imperative, present tense: "change" not "changed" nor "changes"
- don't capitalize the first letter
- no dot (.) at the end

### Body

Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

### Footer

The footer should contain any information about **Breaking Changes** and is also the place to
reference GitHub issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.


## Versioning

This project adhere to [semver](http://semver.org/) i.e, a codified guide to versioning software. When a new feature is developed or a bug is fixed the version will need to be bumped to signify the change. 

The semver string is built like this:

Major.Minor.Patch

A major version bump means that a massive change took place and that application will probably have to be redeployed because a *backwards incompatible* version was released. Example: A library => model relationship change which requires previous configuration options to become invalid.

A minor version is a *backwards compatible* addition or change to the core software. Most development activity will be this type of version bump. Example: A new feature or model.

A patch version is a *backwards compatible* bug fix or application configuration change.

Documentation doesn't require a version bump.