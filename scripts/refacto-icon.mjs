// refacto-icon.mjs

import { promises as fs } from 'fs';
import { parse, serialize } from 'parse5';
import path from 'path';

async function modifyIndexJS() {
    const indexJSPath = './icons/index.js';
    let content = await fs.readFile(indexJSPath, 'utf-8');

    console.log('[INFO] Starting to modify icons...');

    // Supprimer width, height et focusable
    console.log('[INFO] Removing width, height, and focusable attributes...');
    content = content.replace(/(width|height|focusable)="[^"]*"\s?/g, '');

    // Supprimer la class de l'icône
    console.log('[INFO] Removing class attributes...');
    content = content.replace(/class="[^"]*"\s?/g, '');

    // Ajouter "data:image/svg+xml;utf8," devant chaque <svg> 
    console.log('[INFO] Adding "data:image/svg+xml;utf8," prefix...');
    content = content.replace(/export const ([a-zA-Z0-9_]+) = '<svg/g, 'export const $1 = \'data:image/svg+xml;utf8,<svg');

    // Si le nom de l'icône se termine par Colors, remplacez les valeurs hexadécimales par leur équivalent rgb
    console.log('[INFO] Replacing hex colors with RGB for icons ending with Colors...');
    content = content.replace(/export const ([a-zA-Z0-9_]+Colors) = .*fill="#([a-fA-F0-9]{6})"/g, (match, iconName, hexValue) => {
        const rgb = hexToRGB(hexValue);
        return match.replace(`fill="#${hexValue}"`, `fill="${rgb}"`);
    });

    // Supprimer fill="#22293A"
    console.log('[INFO] Removing fill="#22293A"...');
    content = content.replace(/fill="#22293A"/gi, '');

    // Convertir chaque valeur hexadécimale en rgb
    console.log('[INFO] Converting all hex color values to RGB...');
    content = content.replace(/fill="#([a-fA-F0-9]{3,6})"/gi, replaceHexWithRGB);


    await fs.writeFile(indexJSPath, content, 'utf-8');
    console.log('[SUCCESS] Modification complete.');
}

function hexToRGB(hex) {
    if (hex.length === 3) {
        hex = hex.split('').map(char => char + char).join('');
    }
    const bigint = parseInt(hex, 16);
    const r = (bigint >> 16) & 255;
    const g = (bigint >> 8) & 255;
    const b = bigint & 255;

    return `rgb(${r}, ${g}, ${b})`;
}

function replaceHexWithRGB(match, hexValue) {
    return `fill="${hexToRGB(hexValue)}"`;
}


async function main() {
    console.log('[INFO] Script execution started.');
    await modifyIndexJS();
    console.log('[INFO] Script execution finished.');
}

main().catch(error => {
    console.error('[ERROR] An error occurred during script execution:', error);
});