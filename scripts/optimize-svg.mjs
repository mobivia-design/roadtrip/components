// optimize-svg.mjs

import { promises as fs } from 'fs';
import path from 'path';

async function processSVGs(dir) {
    console.log('[INFO] Starting to process SVGs...');

    const svgFiles = await fs.readdir(dir);
    console.log(`[INFO] Found ${svgFiles.length} SVG files.`);

    for (let file of svgFiles) {
        console.log(`[INFO] Processing ${file}...`);
        const filePath = path.join(dir, file);
        let svgData = await fs.readFile(filePath, 'utf-8');

        // Supprimer strictement les propriétés width et height
        console.log('[INFO] Removing width and height attributes...');
        svgData = svgData.replace(/<svg ([^>]*?)?\s*width="[^"]*"\s*([^>]*?)?>/g, '<svg $1$2>');
        svgData = svgData.replace(/<svg ([^>]*?)?\s*height="[^"]*"\s*([^>]*?)?>/g, '<svg $1$2>');


        // Ajouter l'attribut viewBox si non présent
        console.log('[INFO] Adding viewBox attribute if not present...');
        if (!svgData.includes('viewBox')) {
            svgData = svgData.replace(/(<svg[^>]*\sxmlns="http:\/\/www\.w3\.org\/2000\/svg")/g, '$1 viewBox="0 0 64 64"');
        }

        // Remplacer et supprimer certaines couleurs spécifiées
        console.log('[INFO] Replacing and removing specific colors...');
        svgData = svgData.replace(/fill="#E99B29"/gi, 'fill="var(--road-icon-variant, rgb(252, 183, 49))"');
        svgData = svgData.replace(/fill="none"/gi, '');
        svgData = svgData.replace(/fill="#22293A"/gi, '');

        await fs.writeFile(filePath, svgData, 'utf-8');
        console.log(`[SUCCESS] Processed ${file} successfully.`);
    }

    console.log('[SUCCESS] All SVGs processed.');
}

async function main() {
    console.log('[INFO] Script execution started.');
    await processSVGs('./src/components/icon/svg');
    console.log('[INFO] Script execution finished.');
}

main().catch(error => {
    console.error('[ERROR] An error occurred during script execution:', error);
});