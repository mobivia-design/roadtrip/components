import svgtojs from '@nrk/svg-to-js';

const options = {
  input: 'src/components/icon/svg',  // Required. Folder with SVG files
  esm: 'icons/index.js',     // ES module for bundlers exposing `export const iconName = '<svg...'`
  dts: 'icons/index.d.ts',       // Exposes typescript definitions with `export declare const`
}

svgtojs(options);