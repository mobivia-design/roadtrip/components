import { join, resolve } from 'path'
import pkg from 'fs-extra';
const { readdir, readFileSync, writeFileSync } = pkg;
import SVGSpriter from 'svg-sprite'
import chalk from 'chalk'

const __dirname = resolve();
const ROADTRIP_SVG_DIR = join(__dirname, './src/components/icon/svg/')
const ASSETS_DIR = join(__dirname, './icons/')
const SPRITES = [
  {
    name: 'icons',
    spriter: generateSpriter('icons'),
    match: '.*',
  },
]

/** COMMAND */
const runSpriteIcons = async () => {
  try {
    readdir(ROADTRIP_SVG_DIR).then(async (files) => {
      console.info('🔵 Starting SVG Sprite generation !')
      // push SVG files in Spriters
      files.map(pushInSpriter)
      // compile Spriters
      await Promise.all(SPRITES.map(compileSpriter))
    })
  } catch {
    console.error('⚠️  An error occured while generating sprites of SVG icons')
  }
}

runSpriteIcons();

/** Generate a configured Spriter */
function generateSpriter(name) {
  return new SVGSpriter({
    dest: ASSETS_DIR,
    shape: { id: { generator: (filename) => filename.replace('.svg', '') }, transform: ['svgo'] },
    mode: { symbol: { dest: '.', sprite: `${name}.svg` } },
  })
}

/** Return the good Spriter (or default one) based on the SVG filename */
function getSpriter(filename) {
  return SPRITES.find((s) => s.match && filename.match(s.match)) || SPRITES[0]
}

/** Select the right Spriter, then add the SVG icon into it */
function pushInSpriter(filename) {
  const source = resolve(ROADTRIP_SVG_DIR, filename)
  const sprite = getSpriter(filename)
  if (sprite && sprite.spriter) {
    console.info(`...adding ${chalk.bold.blueBright(filename)} in the ${chalk.bold.greenBright(sprite.name)} sprite`)
    sprite.spriter.add(source, filename, readFileSync(source, { encoding: 'utf-8' }))
  }
}

/** Compile a Spriter, to output the SVG sprite file */
function compileSpriter(sprite) {
  return new Promise((resolve) => {
    sprite.spriter.compile(function (error, result) {
      for (const mode in result) {
        for (const resource in result[mode]) {
          console.info(
            `...writing SVG sprite in ${chalk.bold.yellowBright(result[mode][resource].path)} (${Math.round(
              result[mode][resource].contents.length / 1024
            )} Kb)`
          )
          writeFileSync(result[mode][resource].path, result[mode][resource].contents)
        }
      }
      resolve()
    })
  })
}