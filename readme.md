<div align="center">
  <img width="124" height="124" align="center" src="illustration/roadtrip-components.png">

  <h1 align="center">Roadtrip Components</h1>

  <p align="center">Web Component library for Roadtrip Design System</p>
</div>
​

## :blue_book: Documentation
​
Our documentation site lives at [design.mobivia.com](https://design.mobivia.com/03cde1f3f/p/574fb6-using-web-component-library/b/32da36). You'll be able to find detailed documentation on getting started, all of the components, our themes, our guidelines, and more.
​

## :package: Installation
​
Before moving further, please make sure you have [Node.js](https://nodejs.org) installed on your machine. You can install the latest version through their website.

If you’re planning on using web components in a project that doesn’t yet use [Node Package Manager](https://www.npmjs.com/), you’ll have to first create a [package.json](https://docs.npmjs.com/cli/v7/configuring-npm/package-json) file. To do this, run `npm init` and follow the steps provided.

Run in your project or website root directory (where package.json lives):
​
```bash
npm install @roadtrip/components --save
```
​
Now that the Web Component library is installed, you'll find information about [framework integration](https://design.mobivia.com/03cde1f3f/p/574fb6-using-web-component-library/t/706c81) in our documentation.
​

## :rocket: Usage
​
Our Web Components are loaded by the loader your installed in your application, so now you can just put tags and properties of components your want to use.
​
```html
<road-button color="secondary">click me!</road-button>
```
​
You can find the [list of all our components](https://design.mobivia.com/03cde1f3f/p/97f8bd-overview) already available in our documentation.

## Linter CSS avec Stylelint

Pour utiliser Stylelint avec roadtrip, installez d'abord **Stylelint** :

```bash
npm install --save-dev stylelint stylelint-config-standard stylelint-order postcss-html

Configuration de Stylelint

Ajoutez ou mettez à jour votre fichier .stylelintrc.json avec la configuration suivante :

{
  "extends": ["stylelint-config-standard", "stylelint-order"],
  "overrides": [
    {
      "files": ["*.vue"],
      "customSyntax": "postcss-html"
    }
  ],
  "rules": {
      "declaration-property-value-disallowed-list": [
        {
          "/.*/": [
            "var(--road-primary-100)",
            "var(--road-primary-200)",
            "var(--road-primary-300)",
            "var(--road-primary-400)",
            "var(--road-primary-500)",
            "var(--road-primary-600)",
            "var(--road-primary-700)",
            "var(--road-primary-800)",
            "var(--road-primary-900)",
            "var(--road-primary-contrast)",
            "var(--road-secondary-100)",
            "var(--road-secondary-200)",
            "var(--road-secondary-300)",
            "var(--road-secondary-400)",
            "var(--road-secondary-500)",
            "var(--road-secondary-600)",
            "var(--road-secondary-700)",
            "var(--road-secondary-800)",
            "var(--road-secondary-900)",
            "var(--road-secondary-contrast)",
            "var(--primary)",
            "var(--primary-hover)",
            "var(--primary-darken)",
            "var(--primary-lighten)",
            "var(--primary-contrast)",
            "var(--secondary)",
            "var(--secondary-hover)",
            "var(--secondary-darken)",
            "var(--secondary-lighten)",
            "var(--secondary-contrast)",
            "var(--road-grey-000)",
            "var(--road-grey-100)",
            "var(--road-grey-200)",
            "var(--road-grey-300)",
            "var(--road-grey-400)",
            "var(--road-grey-500)",
            "var(--road-grey-600)",
            "var(--road-grey-700)",
            "var(--road-grey-800)",
            "var(--road-grey-900)",
            "var(--road-grey-999)",
            "var(--road-disabled)",
            "var(--accent)",
            "var(--accent-hover)",
            "var(--accent-darken)",
            "var(--accent-lighten)",
            "var(--accent-contrast)",
            "var(--info)",
            "var(--info-hover)",
            "var(--info-text)",
            "var(--info-background)",
            "var(--info-contrast)",
            "var(--success)",
            "var(--success-hover)",
            "var(--success-text)",
            "var(--success-background)",
            "var(--success-contrast)",
            "var(--warning)",
            "var(--warning-hover)",
            "var(--warning-text)",
            "var(--warning-background)",
            "var(--warning-contrast)",
            "var(--danger)",
            "var(--danger-hover)",
            "var(--danger-text)",
            "var(--danger-background)",
            "var(--danger-contrast)",
            "var(--icon)",
            "var(--text)",
            "var(--text-second)",
            "var(--text-disabled)",
            "var(--border-form)",
            "var(--border)",
            "var(--background)",
            "var(--background-second)",
            "var(--white)"
          ]
        },
        {
          "message": "L'utilisation de cette variable est interdite. Elle sera supprimée dans une prochaine release majeur. Merci d'utiliser une autre valeur définie dans Roadtrip en fonction du contexte dans laquelle elle est utilisée."
        }
      ],
      "declaration-property-value-allowed-list": {
      "background": [
        "var(--road-surface)",
        "var(--road-surface-inverse)",
        "var(--road-surface-disabled)",
        "var(--road-header-surface)",
        "var(--road-header-surface-disabled)",
        "var(--road-input-surface)",
        "var(--road-input-surface-variant)",
        "var(--road-decorative-surface)",
        "var(--road-decorative-surface-variant)",
        "var(--road-info-surface)",
        "var(--road-info-surface-inverse)",
        "var(--road-success-surface)",
        "var(--road-success-surface-inverse)",
        "var(--road-warning-surface)",
        "var(--road-warning-surface-inverse)",
        "var(--road-danger-surface)",
        "var(--road-danger-surface-inverse)"
      ],
      "background-color": [
        "var(--road-surface)",
        "var(--road-surface-inverse)",
        "var(--road-surface-disabled)",
        "var(--road-header-surface)",
        "var(--road-header-surface-disabled)",
        "var(--road-input-surface)",
        "var(--road-input-surface-variant)",
        "var(--road-decorative-surface)",
        "var(--road-decorative-surface-variant)",
        "var(--road-info-surface)",
        "var(--road-info-surface-inverse)",
        "var(--road-success-surface)",
        "var(--road-success-surface-inverse)",
        "var(--road-warning-surface)",
        "var(--road-warning-surface-inverse)",
        "var(--road-danger-surface)",
        "var(--road-danger-surface-inverse)"
      ],
      "border-color": [
        "var(--road-outline)",
        "var(--road-outline-weak)",
        "var(--road-outline-weak-variant)",
        "var(--road-button-tertiary-outline)",
        "var(--road-button-destructive-outline)",
        "var(--road-input-outline)",
        "var(--road-input-outline-variant)",
        "var(--road-info-outline)",
        "var(--road-success-outline)",
        "var(--road-warning-outline)",
        "var(--road-danger-outline)"
      ],
      "color": [
        "var(--road-on-surface)",
        "var(--road-on-surface-weak)",
        "var(--road-on-surface-extra-weak)",
        "var(--road-on-surface-disabled)",
        "var(--road-info-text)",
        "var(--road-info-contrast)",
        "var(--road-success-text)",
        "var(--road-success-contrast)",
        "var(--road-warning-text)",
        "var(--road-warning-contrast)",
        "var(--road-danger-text)",
        "var(--road-danger-contrast)"
      ],
      {
        "message": "L'utilisation de cette variable n'est pas correcte. Veuillez choisir une variable conforme"
      }
    }
}


Lance Stylelint pour vérifier tes fichiers :

npx stylelint "**/*.{css,vue}"


Pourquoi cette règle ?
Cette configuration empêche l'utilisation de certaines variables CSS qui ne sont plus recommandées dans le Design System Roadtrip.

Si une variable interdite est utilisée, une erreur sera affichée avec un message explicatif.


## :computer: Contributing
Please take a look at our [contributing](CONTRIBUTING.md) guidelines if you're interested in helping!
​

## :newspaper: Changelog
Find all the latest changes in our [changelog](CHANGELOG.md).


## :page_with_curl: License
​
Copyright © 2021 [Mobivia](https://www.mobivia.com/).

Distributed under [Apache-2.0](LICENSE) license.