import { html } from 'lit';

export default {
  title: 'Indicators/Skeleton',
  component: 'road-skeleton',
  argTypes: {
    '--background-light': {
      description: "base light grey color",
      table: {
        defaultValue: { summary: 'var(--road-disabled)' },
      },
      control: {
        type: null,
      },
    },
    '--background-dark': {
      description: "second grey color to see the animation",
      table: {
        defaultValue: { summary: 'rgb(137, 143, 160, 0.4)' },
      },
      control: {
        type: null,
      },
    },
  },
};

export const List = () => html`
  <road-grid>
  <road-row>
    <road-col class="col-12 col-sm-3 mb-16">
      <road-skeleton style="height: 156px"></road-skeleton>
    </road-col>
    <road-col class="col-12 col-sm-9">
      <p class="h2" style="margin-bottom: 12px">
        <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
    </road-col>
  </road-row>

  <road-row class="mt-16">
    <road-col class="col-12 col-sm-3 mb-16">
      <road-skeleton style="height: 156px"></road-skeleton>
    </road-col>
    <road-col class="col-12 col-sm-9">
      <p class="h2" style="margin-bottom: 12px">
        <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
    </road-col>
  </road-row>

  <road-row class="mt-16">
    <road-col class="col-12 col-sm-3 mb-16">
      <road-skeleton style="height: 156px"></road-skeleton>
    </road-col>
    <road-col class="col-12 col-sm-9">
      <p class="h2" style="margin-bottom: 12px">
        <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
    </road-col>
  </road-row>

  <road-row class="mt-16">
    <road-col class="col-12 col-sm-3 mb-16">
      <road-skeleton style="height: 156px"></road-skeleton>
    </road-col>
    <road-col class="col-12 col-sm-9">
      <p class="h2" style="margin-bottom: 12px">
        <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
    </road-col>
  </road-row>
</road-grid>
`;

export const Half = () => html`
<road-grid>
<road-row>
  <road-col class="col-6">
    <road-skeleton style="height: 156px"></road-skeleton>
  </road-col>
  <road-col class="col-6">
    <p class="h2" style="margin-bottom: 12px">
      <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
  </road-col>
</road-row>

<road-row class="mt-16">
  <road-col class="col-6">
    <road-skeleton style="height: 156px"></road-skeleton>
  </road-col>
  <road-col class="col-6">
    <p class="h2" style="margin-bottom: 12px">
      <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
  </road-col>
</road-row>

<road-row class="mt-16">
  <road-col class="col-6">
    <road-skeleton style="height: 156px"></road-skeleton>
  </road-col>
  <road-col class="col-6">
    <p class="h2" style="margin-bottom: 12px">
      <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
  </road-col>
</road-row>

<road-row class="mt-16">
  <road-col class="col-6">
    <road-skeleton style="height: 156px"></road-skeleton>
  </road-col>
  <road-col class="col-6">
    <p class="h2" style="margin-bottom: 12px">
      <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
    <p class="text-content" style="margin-bottom: 12px">
      <road-skeleton style="height: 1.875rem"></road-skeleton>
    </p>
  </road-col>
</road-row>
</road-grid>
`;

export const Grid = () => html`
  <road-grid>
  <road-row>
    <road-col class="col-12 col-sm-6 col-md-3">
      <road-skeleton style="height: 156px"></road-skeleton>
      <p class="h2 mt-16" style="margin-bottom: 12px">
        <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
    </road-col>

    <road-col class="col-12 col-sm-6 col-md-3">
      <road-skeleton style="height: 156px"></road-skeleton>
      <p class="h2 mt-16" style="margin-bottom: 12px">
        <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
    </road-col>



    <road-col class="col-12 col-sm-6 col-md-3">
      <road-skeleton style="height: 156px"></road-skeleton>
      <p class="h2 mt-16" style="margin-bottom: 12px">
        <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
    </road-col>



    <road-col class="col-12 col-sm-6 col-md-3">
      <road-skeleton style="height: 156px"></road-skeleton>
      <p class="h2 mt-16" style="margin-bottom: 12px">
        <road-skeleton style="width: 60%; height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
      <p class="text-content" style="margin-bottom: 12px">
        <road-skeleton style="height: 1.875rem"></road-skeleton>
      </p>
    </road-col>
  </road-row>
</road-grid>
`;