import { Component, h } from '@stencil/core';

@Component({
  tag: 'road-skeleton',
  styleUrl: 'skeleton.css',
  shadow: true,
})
export class Skeleton {

  render() {
    return (
      <span>&nbsp;</span>
    );
  }

}
