# road-skeleton



<!-- Auto Generated Below -->


## CSS Custom Properties

| Name                 | Description                            |
| -------------------- | -------------------------------------- |
| `--background-dark`  | second grey color to see the animation |
| `--background-light` | base light grey color                  |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
