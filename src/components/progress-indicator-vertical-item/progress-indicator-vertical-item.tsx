import { Component, Host, h } from '@stencil/core';

/**
 * @slot - Content of the progress indicator vertical item
 */

@Component({
  tag: 'road-progress-indicator-vertical-item',
  styleUrl: 'progress-indicator-vertical-item.css',
})
export class ProgressIndicatorVerticalItem {

  render() {
    return (
      <Host>
        <slot/>
      </Host>
    );
  }
}
