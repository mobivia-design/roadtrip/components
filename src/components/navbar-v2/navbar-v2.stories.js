import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Navigation/NavBarV2',
  component: 'road-navbar-v2',
  subcomponents: {
    'road-navbar-item-v2': 'road-navbar-item-v2',
    'road-tooltip': 'road-tooltip',
  },
  parameters: {
    actions: {
      handles: ['roadnavbaritemclick'],
    },
    backgrounds: {
      default: 'grey',
    },
    layout: 'fullscreen',
  },
  argTypes: {
    ' ': {
      description: "Content of the navbar, it should be road-navbar-item elements. Max 5 items on Mobile",
      control: 'text',
    },
    'selected-tab': {
      description: "The selected tab component",
      control: 'text',
    },
    compact: {
      description: "Set to `true` for a compact navbar.",
      control: 'boolean',
    },
    '--z-index': {
      description: "The z-index of the Navbar.",
      table: {
        defaultValue: { summary: '10' },
      },
      control: {
        type: null,
      },
    },
  },
  args: {
    compact: null,
    ' ': `
    <div class="d-flex d-xl-block flex-grow-2 compact-container">
        <a href="#" class="align-self-auto d-none mb-0 py-12 w-full d-xl-flex compact-logo">
          <road-icon name="pass-maintain-logo-solid-color"></road-icon> <road-label class="font-weight-bold h6 mb-0 ml-8 d-xl-compact-none">App Name</road-label>
        </a>
        <hr class="w-full d-none d-xl-block mb-16"/>

        <div class="text-left d-none d-xl-flex align-items-center mb-12">
          <road-avatar slot="start" class="mr-16 mr-xl-compact-none">  
            <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
          </road-avatar>
            <road-label class="d-xl-compact-none">
            <p class="h8 mb-4">First and last name</p>
            <p class="text-medium text-truncate m-0 text-gray">Poste</p>
          </road-label>
        </div>
        <road-button expand color="destructive" outline="true" size="sm" class="mb-0 d-none d-xl-flex m-auto-xl-compact w-full w-xl-revert-compact" icon-only>
         <road-icon name="log-out-outline" aria-hidden="true"></road-icon>
          <road-label class="d-xl-compact-none ml-xl-8">Se déconnecter</road-label>
        </road-button>

    <hr class="w-full d-none d-xl-block my-16"/>
    

    <road-drawer is-open="false" position="left" drawer-width="480" drawer-title="Edit profil" class="drawer-profil" has-close-icon="true">
        <road-avatar class="mx-auto mb-16 mt-24" size="lg">  
          <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
        </road-avatar>
          <road-label class="mb-24">
          <p class="h8 mb-4">First and last name</p>
          <p class="text-medium text-truncate m-0">Email</p>
        </road-label>
    
        <div class="p-16" style="position: absolute; bottom: 0; width: 100%">
          <road-button color="secondary" expand="" class="mb-0">Save changes</road-button>
        </div>
      </road-drawer>
        
        <script>
        document.querySelector('.edit-profil').addEventListener('click', () => {
          document.querySelector('.drawer-profil').setAttribute('is-open', 'true');
        });
        </script>

    
        <road-navbar-item-v2 tab="tab-home" role="tabpanel">
        <road-tooltip content="Home" position="right" trigger="hover" contentalign="center" class="d-block d-xl-flex align-items-center">
          <road-icon name="navigation-home-outline" role="img" class="mr-xl-12"></road-icon>
          <road-label class="font-weight-bold d-xl-compact-none">Home</road-label>
        </road-tooltip>
        </road-navbar-item-v2>

      <road-navbar-item-v2 tab="tab-search" role="tabpanel">
        <road-tooltip content="Search" position="right" trigger="hover" contentalign="center" class="d-block d-xl-flex align-items-center">
          <road-icon name="search" role="img" class="mr-xl-12"></road-icon>
          <road-label class="font-weight-bold d-xl-compact-none">Search</road-label>
        </road-tooltip>
      </road-navbar-item-v2>

      <road-navbar-item-v2 tab="tab-catalog" role="tabpanel">
        <road-tooltip content="Print" position="right" trigger="hover" contentalign="center" class="d-block d-xl-flex align-items-center">
          <road-icon name="print-outline" role="img" class="mr-xl-12"></road-icon>
          <road-label class="font-weight-bold d-xl-compact-none">Print</road-label>
        </road-tooltip>
      </road-navbar-item-v2>

      <road-navbar-item-v2 tab="tab-notification" class="d-none d-xl-flex" role="tabpanel">
        <road-tooltip content="Notifications" position="right" trigger="hover" contentalign="center" class="d-block d-xl-flex align-items-center">
          <road-icon name="alert-notification-outline" role="img" class="mr-xl-12"></road-icon>
          <road-label class="font-weight-bold d-xl-compact-none">Notifications</road-label>
        </road-tooltip>
      </road-navbar-item-v2>

      <road-navbar-item-v2 tab="tab-scan" class="d-none d-xl-flex" role="tabpanel">
        <road-tooltip content="Scan" position="right" trigger="hover" contentalign="center" class="d-block d-xl-flex align-items-center">
          <road-icon name="scan" role="img" class="mr-xl-12"></road-icon>
          <road-label class="font-weight-bold d-xl-compact-none">Scan</road-label>
        </road-tooltip>
      </road-navbar-item-v2>

      <road-navbar-item-v2 tab="tab-catalogue" class="d-none d-xl-flex" role="tabpanel">
        <road-tooltip content="Catalogue" position="right" trigger="hover" contentalign="center" class="d-block d-xl-flex align-items-center">
          <road-icon name="file-catalog" role="img" class="mr-xl-12"></road-icon>
          <road-label class="font-weight-bold d-xl-compact-none">Catalogue</road-label>
        </road-tooltip>
      </road-navbar-item-v2>


      <road-navbar-item-v2 tab="tab-diag" class="d-none d-xl-flex" role="tabpanel">
        <road-tooltip content="Diagnostic" position="right" trigger="hover" contentalign="center" class="d-block d-xl-flex align-items-center">
          <road-icon name="Diagnostic" role="img" class="mr-xl-12"></road-icon>
          <road-label class="font-weight-bold d-xl-compact-none">Diagnostic</road-label>
        </road-toolip>
      </road-navbar-item-v2>

      </div>


      <div class="text-gray">

      <road-navbar-item-v2 tab="tab-help" class="d-none d-xl-flex" role="tabpanel">
        <road-tooltip content="Help" position="right" trigger="hover" contentalign="center" class="d-none d-xl-flex align-items-center">
          <road-icon name="alert-question-outline" role="img" class="mr-xl-12"></road-icon><road-label class="font-weight-bold d-xl-compact-none">Help</road-label>
        </road-toolip>
      </road-navbar-item-v2>

      <road-navbar-item-v2 tab="tab-feedback" class="d-none d-xl-flex" role="tabpanel">
        <road-tooltip content="Feedback" position="right" trigger="hover" contentalign="center" class="d-none d-xl-flex align-items-center">
          <road-icon name="speak-advice-outline" role="img" class="mr-xl-12"></road-icon></road-icon><road-label class="font-weight-bold d-xl-compact-none">Feedback</road-label>
        </road-toolip>
      </road-navbar-item-v2>



      
  </div>

  <road-navbar-item-v2 tab="tab-menu" class="d-block d-xl-none tab-menu">
        <road-icon name="navigation-menu" role="img"></road-icon>
        <road-label>Menu</road-label>
      </road-navbar-item-v2>
  
  <road-drawer is-open="false" position="right" drawer-width="480" class="d-xl-none drawer-menu">
  <div class="p-16 pt-0">
 <div class="text-left d-flex align-items-center">
    <road-avatar slot="start" class="mr-16">  
      <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
    </road-avatar>
      <road-label>
      <p class="h8 mb-4">First and last name</p>
      <p class="text-medium text-truncate m-0 text-gray">Poste</p>
    </road-label>
  </div>

  <road-button expand color="destructive" outline="true" size="sm" class="d-flex  mb-16 mt-12">
    <road-icon slot="start" name="log-out-outline" aria-hidden="true"></road-icon>
    Se déconnecter
  </road-button>

  <road-item class="text-left bg-light profil" tabindex="0">
  <road-label>
    <span class="text-content mb-8 d-flex align-items-center">
      <road-icon size="sm" class="mr-8" name="shop-outline" role="img"></road-icon>
      Centre
    </span>
    <p class="h8 mb-4">054 Norauto Chambéry
    <p class="text-content mb-0">Code collaborateur 18</p>
  
  </road-label>
    <road-icon slot="end" name="navigation-chevron" size="lg" class="ml-16" role="img"></road-icon>
  </road-item>

  <road-input-group slot="secondary" class="d-block d-xl-none my-12">
    <road-input input-id="cardNumber" sizes="lg" label="Rechercher dans le catalogue" class="mb-0"></road-input>
    <label slot="append" for="cardNumber">
      <road-icon name="search" aria-hidden="true" role="img"></road-icon>
    </label>
  </road-input-group>

  <hr class="my-16">

  <road-item class="bg-white border-0" button="true">
    <road-icon name="scan"></road-icon>
    Scan
  </road-item>

  <road-item class="bg-white border-0" button="true">
    <road-icon name="file-catalog"></road-icon>
    Catalogue
  </road-item>

  <road-item class="bg-white border-0" button="true">
    <road-icon name="Diagnostic"></road-icon>
    Diagnostic
  </road-item>

  <road-item class="bg-white border-0" button="true">
    <road-icon name="log-out"></road-icon>
    Log out
  </road-item>
  <road-item class="bg-white border-left-0 border-right-0 border-bottom-0 border-top" button="true">
    <road-icon name="alert-question-outline" role="img"></road-icon>Help
  </road-item>
</div>
  </road-drawer>


  <road-drawer is-open="false" position="right" drawer-width="480" drawer-title="Menu" class="d-xl-none drawer-menu2" has-back-icon="true" has-close-icon="true" back-text="back">
    <road-avatar class="mx-auto mb-16 mt-24" size="lg">  
      <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
    </road-avatar>
      <road-label class="mb-24">
      <p class="h8 mb-4">First and last name</p>
      <p class="text-medium text-truncate m-0">Email</p>
    </road-label>

    <div class="p-16" style="position: absolute; bottom: 0; width: 100%">
      <road-button color="secondary" expand="" class="mb-0">Save changes</road-button>
    </div>
  </road-drawer>
</road-navbar-v2>

  
  <script>
document.querySelector('.tab-menu').addEventListener('click', () => {
  document.querySelector('.drawer-menu').setAttribute('is-open', 'true');
});

document.querySelector('.profil').addEventListener('click', () => {
  document.querySelector('.drawer-menu2').setAttribute('is-open', 'true');
});

</script>`,
  },
};

const Template = (args) => html`
<road-navbar-v2 selected-tab="${ifDefined(args['selected-tab'])}" compact="${ifDefined(args.compact)}">
  ${unsafeHTML(args[' '])}
</road-navbar-v2>
`;

export const Playground = Template.bind({});

export const Selected = Template.bind({});
Selected.args = {
  ' ': `
  <road-profil-dropdown is-open="false" class="m-24 d-none d-xl-block">
    <road-list slot="list">
      <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
        <road-icon slot="start" name="edit-outline" size="md" aria-hidden="true"></road-icon>
        <road-label style="font-size: 0.75rem">
          Edit profile
        </road-label>
      </road-item>
      <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
        <road-icon slot="start" name="log-out-outline" size="md" aria-hidden="true"></road-icon>
        <road-label style="font-size: 0.75rem">
          Log out
        </road-label>
      </road-item>
    </road-list>
    <road-img slot="avatar" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
    <road-img slot="avatarItem" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
  </road-profil-dropdown>
  
  <road-navbar-item-v2 tab="tab-home" selected="true" aria-label="Home" role="menuitem">
    <road-tooltip content="Home" position="right" trigger="hover" contentalign="center">
      <road-icon name="navigation-home-outline" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Home</road-label>
    </road-tooltip>
  </road-navbar-item-v2>

  <road-navbar-item-v2 tab="tab-search" aria-label="Search" role="menuitem">
    <road-tooltip content="Search" position="right" trigger="hover" contentalign="center">
      <road-icon name="search" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Search</road-label>
    </road-tooltip>
  </road-navbar-item-v2>

  <road-navbar-item-v2 tab="tab-print" role="menuitem">
    <road-tooltip content="Print" position="right" trigger="hover" contentalign="center">
      <road-icon name="print-outline" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Print</road-label>
    </road-tooltip>
  </road-navbar-item-v2>

  <road-navbar-item-v2 tab="tab-catalogue" class="d-none d-xl-flex" role="menuitem">
    <road-tooltip content="Catalogue" position="right" trigger="hover" contentalign="center">
      <road-icon name="file-catalog" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Catalogue</road-label>
    </road-tooltip>
  </road-navbar-item-v2>
  
  <road-navbar-item-v2 tab="tab-scan" class="d-none d-xl-flex" aria-label="Scan" role="menuitem">
    <road-tooltip content="Scan" position="right" trigger="hover" contentalign="center">
      <road-icon name="scan" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Scan</road-label>
    </road-tooltip>
  </road-navbar-item-v2>`,
};

export const withNotifications = Template.bind({});
withNotifications.args = {
  ' ': `
  <road-profil-dropdown is-open="false" class="m-24 d-none d-xl-block">
    <road-list slot="list">
      <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
        <road-icon slot="start" name="edit-outline" size="md" aria-hidden="true"></road-icon>
        <road-label style="font-size: 0.75rem">
          Edit profile
        </road-label>
      </road-item>
      <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
        <road-icon slot="start" name="log-out-outline" size="md" aria-hidden="true"></road-icon>
        <road-label style="font-size: 0.75rem">
          Log out
        </road-label>
      </road-item>
    </road-list>
    <road-img slot="avatar" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
    <road-img slot="avatarItem" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
  </road-profil-dropdown>

  <road-navbar-item-v2 tab="tab-home" selected="true" aria-label="Home" role="menuitem">
    <road-tooltip content="Home" position="right" trigger="hover" contentalign="center">
      <road-icon name="navigation-home-outline" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Home</road-label>
    </road-tooltip>
  </road-navbar-item-v2>

  <road-navbar-item-v2 tab="tab-search" aria-label="Search" role="menuitem">
    <road-tooltip content="Search" position="right" trigger="hover" contentalign="center">
      <road-icon name="search" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Search</road-label>
    </road-tooltip>
  </road-navbar-item-v2>

  <road-navbar-item-v2 tab="tab-catalog" aria-label="Catalog" role="menuitem">
    <road-tooltip content="Scan" position="right" trigger="hover" contentalign="center">
      <road-icon name="print-outline" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Scan</road-label>
    </road-tooltip>
    <road-badge>3</road-badge>
  </road-navbar-item-v2>

  <road-navbar-item-v2 tab="tab-catalogue" class="d-none d-xl-flex" aria-label="Catalog" role="menuitem">
    <road-tooltip content="Catalog" position="right" trigger="hover" contentalign="center">
      <road-icon name="file-catalog" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Catalog</road-label>
    </road-tooltip>
  </road-navbar-item-v2>
  
  <road-navbar-item-v2 tab="tab-scan" class="d-none d-xl-flex" aria-label="Scan" role="menuitem">
    <road-tooltip content="Scan" position="right" trigger="hover" contentalign="center">
      <road-icon name="scan" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Scan</road-label>
    </road-tooltip>
  </road-navbar-item-v2>`,
};
