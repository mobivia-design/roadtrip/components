import { Component, Host, h, Prop, Watch, Event, EventEmitter, Listen } from '@stencil/core';
import './../../utils/polyfill';

/**
 * @slot  - Content of the navbar, it should be road-navbar-item elements. Max 5 items on Mobile
 */

@Component({
  tag: 'road-navbar-v2',
  styleUrl: 'navbar-v2.css',
  shadow: true,
})
export class NavbarV2 {

  /**
   * Set to `true` for a compact navbar.
   */
  @Prop({ reflect: true }) compact: boolean = false;

  /**
   * The selected tab component
   */
  @Prop() selectedTab?: string;
  @Watch('selectedTab')
  selectedTabChanged() {
    if (this.selectedTab !== undefined) {
      this.roadnavbarchanged.emit({
        tab: this.selectedTab,
      });
      this.roadNavbarChanged.emit({
        tab: this.selectedTab,
      });
    }
  }

  /** @internal */
  @Event() roadnavbarchanged!: EventEmitter;
  /** @internal */
  @Event() roadNavbarChanged!: EventEmitter;

  @Listen('roadNavbarItemClick')
  @Listen('roadnavbaritemclick')
  onNavbarChanged(ev: CustomEvent) {
    this.selectedTab = ev.detail.tab;
  }

  componentWillLoad() {
    this.selectedTabChanged();
  }

  render() {

    const compactClass = this.compact ? 'compact' : '';

    return (
      <Host
        role="menubar"
        class={`${compactClass}`}
      >
        <slot/>
      </Host>
    );
  }

}
