# road-text

The text component is a simple component that can be used to style the text color of any element. The `road-text` element should wrap the element in order to change the text color of that element.

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description        | Type                                                                                                                                                          | Default     |
| -------- | --------- | ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `color`  | `color`   | Color of the text. | `"accent" \| "danger" \| "default" \| "default-second" \| "disabled" \| "info" \| "primary" \| "secondary" \| "success" \| "warning" \| "white" \| undefined` | `'default'` |


## Slots

| Slot | Description          |
| ---- | -------------------- |
|      | Content of the text. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
