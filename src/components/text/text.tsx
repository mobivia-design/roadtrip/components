import { Component, Host, h, Prop } from '@stencil/core';

/**
 * @slot  - Content of the text.
 */

@Component({
  tag: 'road-text',
  styleUrl: 'text.css',
  shadow: true,
})
export class Text {

  /**
   * Color of the text.
   */
  @Prop() color?: 'primary' | 'secondary' | 'accent' | 'info' | 'success' | 'warning' | 'danger' | 'default' | 'default-second' | 'disabled' | 'white' = 'default';

  render() {
    const colorClass = this.color !== undefined ? `text-${this.color}` : '';

    return (
      <Host class={`${colorClass}`}>
        <slot/>
      </Host>
    );
  }

}
