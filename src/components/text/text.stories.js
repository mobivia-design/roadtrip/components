import { html } from 'lit';

export default {
  title: 'Listing/Text',
  component: 'road-text',
};

export const Playground = (args) => html`
<road-text color="${args.color}">
  ${args[' ']}
</road-text>
`;
Playground.args = {
  color: 'info',
  ' ': 'Text',
};
Playground.argTypes = {
  color: {
    description: "Color of the text.",
    options: ['primary', 'secondary', 'accent', 'info', 'success', 'warning', 'danger', 'default', 'default-second', 'disabled', 'white'],
    control: {
      type: 'radio',
    },
  },
  ' ': {
    description: "Content of the text.",
    control: 'text',
  },
};