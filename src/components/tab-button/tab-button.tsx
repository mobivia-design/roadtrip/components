import { Component, Element, Event, EventEmitter, Host, Listen, Prop, h } from '@stencil/core';

/**
 * @slot  - Content of the tab-button, it should be road-icon and road-label elements.
 *
 * @part native - The native HTML anchor element that wraps all child elements.
 */

@Component({
  tag: 'road-tab-button',
  styleUrl: 'tab-button.css',
  shadow: true,
})
export class TabButton {

  @Element() el!: HTMLRoadTabButtonElement;

  /**
   * This attribute instructs browsers to download a URL instead of navigating to
   * it, so the user will be prompted to save it as a local file. If the attribute
   * has a value, it is used as the pre-filled file name in the Save prompt
   * (the user can still change the file name if they want).
   */
  @Prop() download: string | undefined;

  /**
   * Contains a URL or a URL fragment that the hyperlink points to.
   * If this property is set, an anchor tag will be rendered.
   */
  @Prop() href: string | undefined;

  /**
   * Specifies the relationship of the target object to the link object.
   * The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).
   */
  @Prop() rel: string | undefined;

  /**
   * Set the layout of the text and icon in the tab bar.
   * It defaults to `'icon-start'`.
   */
  @Prop() layout?: 'icon-start' | 'icon-top' = 'icon-start';

  /**
   * The selected tab component
   */
  @Prop({ mutable: true }) selected = false;

    /**
   * The disabled tab 
   */
    @Prop({ mutable: true }) disabled = false;

  /**
   * A tab id must be provided for each `road-tab`. It's used internally to reference
   */
  @Prop() tab?: string;

  /**
   * Specifies where to display the linked URL.
   * Only applies when an `href` is provided.
   * Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.
   */
  @Prop() target: string | undefined;

  /**
   * Emitted when the tab bar is clicked
   * @internal
   */
  @Event() roadtabbuttonclick!: EventEmitter;
  /** @internal */
  @Event() roadTabButtonClick!: EventEmitter;

  @Listen('roadTabBarChanged', { target: 'window' })
  @Listen('roadTabbarchanged', { target: 'window' })
  onTabBarChanged(ev: CustomEvent) {
    const dispatchedFrom = ev.target as HTMLElement;
    const parent = this.el.parentElement as EventTarget;

    if ((ev.composedPath && ev.composedPath().includes(parent)) || (dispatchedFrom && dispatchedFrom.contains(this.el))) {
      this.selected = this.tab === ev.detail.tab;
    }
  }

  private selectTab(ev: Event | KeyboardEvent) {
    if (this.tab !== undefined) {
      this.roadtabbuttonclick.emit({
        tab: this.tab,
        href: this.href,
        selected: this.selected,
      });
      this.roadTabButtonClick.emit({
        tab: this.tab,
        href: this.href,
        selected: this.selected,
      });

      ev.preventDefault();
    }
  }

  private get hasLabel() {
    return !!this.el.querySelector('road-label');
  }

  private get hasIcon() {
    return !!this.el.querySelector('road-icon');
  }

  private get tabIndex() {
    const hasTabIndex = this.el.hasAttribute('tabindex');

    if (hasTabIndex) {
      return this.el.getAttribute('tabindex');
    }

    return 0;
  }

  private onKeyUp = (ev: KeyboardEvent) => {
    if (ev.key === 'Enter' || ev.key === ' ') {
      this.selectTab(ev);
    }
  };

  private onClick = (ev: Event) => {
    this.selectTab(ev);
  };

  render() {
    const { hasIcon, hasLabel, tabIndex, href, rel, target, layout, selected, tab, disabled } = this;
    const attrs = {
      download: this.download,
      href: !disabled ? href : undefined, // Ne pas inclure le lien si désactivé
      rel,
      target,
    };
  
    return (
      <Host
        onClick={(ev: Event) => !disabled && this.onClick(ev)} // Bloquer les clics si désactivé
        onKeyup={(ev: KeyboardEvent) => !disabled && this.onKeyUp(ev)} // Bloquer les interactions clavier si désactivé
        role="tab"
        tabindex={disabled ? -1 : tabIndex} // Désactiver le focus si désactivé
        aria-selected={selected ? 'true' : 'false'}
        aria-disabled={disabled ? 'true' : null} // Ajout pour l'accessibilité
        id={tab !== undefined ? `tab-button-${tab}` : null}
        class={{
          'tab-selected': selected,
          'tab-has-label': hasLabel,
          'tab-has-icon': hasIcon,
          'tab-has-label-only': hasLabel && !hasIcon,
          'tab-has-icon-only': hasIcon && !hasLabel,
          [`tab-layout-${layout}`]: true,
          'tab-disabled': disabled, // Classe CSS pour les styles désactivés
        }}
      >
        <a {...attrs} tabIndex={-1} class="button-native" part="native">
          <span class="button-inner">
            <slot/>
          </span>
        </a>
      </Host>
    );
  }
}
