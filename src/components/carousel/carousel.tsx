import { Component, Element, Event, EventEmitter, Host, Method, Prop, Watch, h } from '@stencil/core';
import { navigationChevron } from '../../../icons';
import Swiper, { SwiperOptions, Autoplay, Pagination, Scrollbar, Keyboard, Zoom } from "swiper"

Swiper.use([Pagination, Scrollbar, Autoplay, Keyboard, Zoom]);

@Component({
  tag: 'road-carousel',
  styleUrl: 'carousel.css',
  shadow: true,
})
export class Carousel {
  private paginationEl?: HTMLElement;
  private swiperReady = false;
  private mutationO?: MutationObserver;
  private readySwiper!: (swiper: Swiper) => void;
  private swiper: Promise<Swiper> = new Promise(resolve => {
    this.readySwiper = resolve;
  });
  private syncSwiper?: Swiper;
  private didInit = false;

  @Element() el!: HTMLRoadCarouselElement;

  @Prop() options: any = {};

  @Watch('options')
  async optionsChanged() {
    if (this.swiperReady) {
      const swiper = await this.getSwiper();
      Object.assign(swiper.params, this.options);
      await this.update();
    }
  }

  @Prop() pager = true;
  @Prop() arrows = false;

  @Event() roadslidesdidload!: EventEmitter<void>;
  @Event() roadslidetap!: EventEmitter<void>;
  @Event() roadslidedoubletap!: EventEmitter<void>;
  @Event() roadslidewillchange!: EventEmitter<void>;
  @Event() roadslidedidchange!: EventEmitter<void>;
  @Event() roadslidenextstart!: EventEmitter<void>;
  @Event() roadslideprevstart!: EventEmitter<void>;
  @Event() roadslidenextend!: EventEmitter<void>;
  @Event() roadslideprevend!: EventEmitter<void>;
  @Event() roadslidetransitionstart!: EventEmitter<void>;
  @Event() roadslidetransitionend!: EventEmitter<void>;
  @Event() roadslidedrag!: EventEmitter<void>;
  @Event() roadslidereachstart!: EventEmitter<void>;
  @Event() roadslidereachend!: EventEmitter<void>;
  @Event() roadslidetouchstart!: EventEmitter<void>;
  @Event() roadslidetouchend!: EventEmitter<void>;

  connectedCallback() {
    if (typeof MutationObserver !== 'undefined') {
      const mut = this.mutationO = new MutationObserver(() => {
        if (this.swiperReady) {
          this.update();
        }
      });
      mut.observe(this.el, {
        childList: true,
        subtree: true,
      });
    }
  }

  componentDidLoad() {
    if (!this.didInit) {
      this.didInit = true;
      this.initSwiper();
    }
    this.setupKeyboardNavigation();
  }

  disconnectedCallback() {
    if (this.mutationO) {
      this.mutationO.disconnect();
      this.mutationO = undefined;
    }

    const swiper = this.syncSwiper;
    if (swiper !== undefined) {
      swiper.destroy(true, true);
      this.swiper = new Promise(resolve => {
        this.readySwiper = resolve;
      });
      this.swiperReady = false;
      this.syncSwiper = undefined;
    }

    this.didInit = false;
  }

  @Method()
  async update() {
    const [swiper] = await Promise.all([
      this.getSwiper(),
      waitForSlides(this.el)
    ]);
    swiper.update();
  }

  @Method()
  async updateAutoHeight(speed?: number) {
    const swiper = await this.getSwiper();
    swiper.updateAutoHeight(speed);
  }

  @Method()
  async slideTo(index: number, speed?: number, runCallbacks?: boolean) {
    const swiper = await this.getSwiper();
    swiper.slideTo(index, speed, runCallbacks);
  }

  @Method()
  async slideNext(speed?: number, runCallbacks?: boolean) {
    const swiper = await this.getSwiper();
    swiper.slideNext(speed!, runCallbacks!);
  }

  @Method()
  async slidePrev(speed?: number, runCallbacks?: boolean) {
    const swiper = await this.getSwiper();
    swiper.slidePrev(speed, runCallbacks);
  }

  @Method()
  async getActiveIndex(): Promise<number> {
    const swiper = await this.getSwiper();
    return swiper.activeIndex;
  }

  @Method()
  async getPreviousIndex(): Promise<number> {
    const swiper = await this.getSwiper();
    return swiper.previousIndex;
  }

  @Method()
  async length(): Promise<number> {
    const swiper = await this.getSwiper();
    return swiper.slides.length;
  }

  @Method()
  async isEnd(): Promise<boolean> {
    const swiper = await this.getSwiper();
    return swiper.isEnd;
  }

  @Method()
  async isBeginning(): Promise<boolean> {
    const swiper = await this.getSwiper();
    return swiper.isBeginning;
  }

  @Method()
  async startAutoplay() {
    const swiper = await this.getSwiper();
    if (swiper.autoplay) {
      swiper.autoplay.start();
    }
  }

  @Method()
  async stopAutoplay() {
    const swiper = await this.getSwiper();
    if (swiper.autoplay) {
      swiper.autoplay.stop();
    }
  }

  @Method()
  async lockSwipeToNext(lock: boolean) {
    const swiper = await this.getSwiper();
    swiper.allowSlideNext = !lock;
  }

  @Method()
  async lockSwipeToPrev(lock: boolean) {
    const swiper = await this.getSwiper();
    swiper.allowSlidePrev = !lock;
  }

  @Method()
  async lockSwipes(lock: boolean) {
    const swiper = await this.getSwiper();
    swiper.allowSlideNext = !lock;
    swiper.allowSlidePrev = !lock;
    swiper.allowTouchMove = !lock;
  }

  @Method()
  async getSwiper(): Promise<any> {
    return this.swiper;
  }

  private async initSwiper() {
    const finalOptions = this.normalizeOptions();

    await waitForSlides(this.el);
    const swiper = new Swiper(this.el, finalOptions);
    this.swiperReady = true;
    this.syncSwiper = swiper;
    this.readySwiper(swiper);
  }

  private normalizeOptions(): SwiperOptions {
    const swiperOptions: SwiperOptions = {
      effect: undefined,
      direction: 'horizontal',
      initialSlide: 0,
      loop: false,
      parallax: false,
      slidesPerView: 1,
      spaceBetween: 0,
      speed: 300,
      grid: {
        rows: 1,
        fill: 'column',
      },
      slidesPerGroup: 1,
      centeredSlides: false,
      slidesOffsetBefore: 0,
      slidesOffsetAfter: 0,
      touchEventsTarget: 'container',
      autoplay: false,
      freeMode: {
        enabled: false,
        momentum: true,
        momentumRatio: 1,
        momentumBounce: true,
        momentumBounceRatio: 1,
        momentumVelocityRatio: 1,
        sticky: false,
        minimumVelocity: 0.02,
      },
      autoHeight: false,
      setWrapperSize: false,
      zoom: {
        maxRatio: 3,
        minRatio: 1,
        toggle: false,
      },
      touchRatio: 1,
      touchAngle: 45,
      simulateTouch: true,
      touchStartPreventDefault: false,
      shortSwipes: true,
      longSwipes: true,
      longSwipesRatio: 0.5,
      longSwipesMs: 300,
      followFinger: true,
      threshold: 0,
      touchMoveStopPropagation: true,
      touchReleaseOnEdges: false,
      resistance: true,
      resistanceRatio: 0.85,
      watchSlidesProgress: false,
      preventClicks: true,
      preventClicksPropagation: true,
      slideToClickedSlide: false,
      loopAdditionalSlides: 0,
      noSwiping: true,
      runCallbacksOnInit: true,
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows: true,
      },
      flipEffect: {
        slideShadows: true,
        limitRotation: true,
      },
      cubeEffect: {
        slideShadows: true,
        shadow: true,
        shadowOffset: 20,
        shadowScale: 0.94,
      },
      fadeEffect: {
        crossFade: false,
      },
      a11y: {
        prevSlideMessage: 'Previous slide',
        nextSlideMessage: 'Next slide',
        firstSlideMessage: 'This is the first slide',
        lastSlideMessage: 'This is the last slide',
      },
    };

    if (this.pager) {
      swiperOptions.pagination = {
        el: this.paginationEl!,
        type: 'bullets',
        clickable: true,
        hideOnClick: false,
      };
    }

    const eventOptions: SwiperOptions = {
      on: {
        init: () => {
          setTimeout(() => {
            this.roadslidesdidload.emit();
          }, 20);
        },
        slideChangeTransitionStart: () => this.roadslidewillchange.emit(),
        slideChangeTransitionEnd: () => this.roadslidedidchange.emit(),
        slideNextTransitionStart: () => this.roadslidenextstart.emit(),
        slidePrevTransitionStart: () => this.roadslideprevstart.emit(),
        slideNextTransitionEnd: () => this.roadslidenextend.emit(),
        slidePrevTransitionEnd: () => this.roadslideprevend.emit(),
        transitionStart: () => this.roadslidetransitionstart.emit(),
        transitionEnd: () => this.roadslidetransitionend.emit(),
        sliderMove: () => this.roadslidedrag.emit(),
        reachBeginning: () => this.roadslidereachstart.emit(),
        reachEnd: () => this.roadslidereachend.emit(),
        touchStart: () => this.roadslidetouchstart.emit(),
        touchEnd: () => this.roadslidetouchend.emit(),
        tap: () => this.roadslidetap.emit(),
        doubleTap: () => this.roadslidedoubletap.emit(),
      },
    };

    const customEvents = (!!this.options && !!this.options.on) ? this.options.on : {};
    const mergedEventOptions = { on: { ...customEvents, ...eventOptions.on } };

    return { ...swiperOptions, ...this.options, ...mergedEventOptions };
  }

  private setupKeyboardNavigation() {
    this.el.addEventListener('keydown', (event: KeyboardEvent) => {
      switch (event.key) {
        case 'ArrowLeft':
          this.slidePrev();
          break;
        case 'ArrowRight':
          this.slideNext();
          break;
      }
    });
  }

  render() {
    return (
      <Host class="swiper-container" tabindex="0">
        <div class="swiper-wrapper">
          <slot />
        </div>
        {this.pager && <div class="swiper-pagination" ref={el => this.paginationEl = el}></div>}
        {this.arrows && (
          <div role="button" class="swiper-button-prev" tabindex="0" aria-label="Previous slide" onClick={() => this.slidePrev()}>
            <road-icon icon={navigationChevron} rotate="180"></road-icon>
          </div>
        )}
        {this.arrows && (
          <div role="button" class="swiper-button-next" tabindex="0" aria-label="Next slide" onClick={() => this.slideNext()}>
            <road-icon icon={navigationChevron}></road-icon>
          </div>
        )}
      </Host>
    );
  }
}

const waitForSlides = (el: HTMLElement) => {
  return Promise.all(
    Array.from(el.querySelectorAll('road-carousel-item')).map(s => s.componentOnReady())
  );
};
