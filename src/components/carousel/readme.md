# road-carousel



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type      | Default |
| --------- | --------- | ----------- | --------- | ------- |
| `arrows`  | `arrows`  |             | `boolean` | `false` |
| `options` | `options` |             | `any`     | `{}`    |
| `pager`   | `pager`   |             | `boolean` | `true`  |


## Events

| Event                      | Description | Type                |
| -------------------------- | ----------- | ------------------- |
| `roadslidedidchange`       |             | `CustomEvent<void>` |
| `roadslidedoubletap`       |             | `CustomEvent<void>` |
| `roadslidedrag`            |             | `CustomEvent<void>` |
| `roadslidenextend`         |             | `CustomEvent<void>` |
| `roadslidenextstart`       |             | `CustomEvent<void>` |
| `roadslideprevend`         |             | `CustomEvent<void>` |
| `roadslideprevstart`       |             | `CustomEvent<void>` |
| `roadslidereachend`        |             | `CustomEvent<void>` |
| `roadslidereachstart`      |             | `CustomEvent<void>` |
| `roadslidesdidload`        |             | `CustomEvent<void>` |
| `roadslidetap`             |             | `CustomEvent<void>` |
| `roadslidetouchend`        |             | `CustomEvent<void>` |
| `roadslidetouchstart`      |             | `CustomEvent<void>` |
| `roadslidetransitionend`   |             | `CustomEvent<void>` |
| `roadslidetransitionstart` |             | `CustomEvent<void>` |
| `roadslidewillchange`      |             | `CustomEvent<void>` |


## Methods

### `getActiveIndex() => Promise<number>`



#### Returns

Type: `Promise<number>`



### `getPreviousIndex() => Promise<number>`



#### Returns

Type: `Promise<number>`



### `getSwiper() => Promise<any>`



#### Returns

Type: `Promise<any>`



### `isBeginning() => Promise<boolean>`



#### Returns

Type: `Promise<boolean>`



### `isEnd() => Promise<boolean>`



#### Returns

Type: `Promise<boolean>`



### `length() => Promise<number>`



#### Returns

Type: `Promise<number>`



### `lockSwipeToNext(lock: boolean) => Promise<void>`



#### Parameters

| Name   | Type      | Description |
| ------ | --------- | ----------- |
| `lock` | `boolean` |             |

#### Returns

Type: `Promise<void>`



### `lockSwipeToPrev(lock: boolean) => Promise<void>`



#### Parameters

| Name   | Type      | Description |
| ------ | --------- | ----------- |
| `lock` | `boolean` |             |

#### Returns

Type: `Promise<void>`



### `lockSwipes(lock: boolean) => Promise<void>`



#### Parameters

| Name   | Type      | Description |
| ------ | --------- | ----------- |
| `lock` | `boolean` |             |

#### Returns

Type: `Promise<void>`



### `slideNext(speed?: number, runCallbacks?: boolean) => Promise<void>`



#### Parameters

| Name           | Type                   | Description |
| -------------- | ---------------------- | ----------- |
| `speed`        | `number \| undefined`  |             |
| `runCallbacks` | `boolean \| undefined` |             |

#### Returns

Type: `Promise<void>`



### `slidePrev(speed?: number, runCallbacks?: boolean) => Promise<void>`



#### Parameters

| Name           | Type                   | Description |
| -------------- | ---------------------- | ----------- |
| `speed`        | `number \| undefined`  |             |
| `runCallbacks` | `boolean \| undefined` |             |

#### Returns

Type: `Promise<void>`



### `slideTo(index: number, speed?: number, runCallbacks?: boolean) => Promise<void>`



#### Parameters

| Name           | Type                   | Description |
| -------------- | ---------------------- | ----------- |
| `index`        | `number`               |             |
| `speed`        | `number \| undefined`  |             |
| `runCallbacks` | `boolean \| undefined` |             |

#### Returns

Type: `Promise<void>`



### `startAutoplay() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `stopAutoplay() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `update() => Promise<void>`



#### Returns

Type: `Promise<void>`



### `updateAutoHeight(speed?: number) => Promise<void>`



#### Parameters

| Name    | Type                  | Description |
| ------- | --------------------- | ----------- |
| `speed` | `number \| undefined` |             |

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-carousel --> road-icon
  style road-carousel fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
