import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Listing/Carousel',
  component: 'road-carousel',
  subcomponents: {
    'road-carousel-item': 'road-carousel-item',
  },
  parameters: {
    actions: {
      handles: [
        'roadslidesdidload',
        'roadslidetap',
        'roadslidedoubletap',
        'roadslidewillchange',
        'roadslidedidchange',
        'roadslidenextstart',
        'roadslideprevstart',
        'roadslidenextend',
        'roadslideprevend',
        'roadslidetransitionstart',
        'roadslidetransitionend',
        'roadslidedrag',
        'roadslidereachstart',
        'roadslidereachend',
        'roadslidetouchstart',
        'roadslidetouchend',
      ],
    },
    docs: {
      description: {
        component: `The Carousel component uses child components named \`road-carousel-item\` to create different slides, so the content of each slide needs to be inside a \`road-carousel-item\` component.`,
      },
    },
  },
  argTypes: {
    pager: {
      control: 'boolean',
      description: 'Shows pagination bullets below the carousel.',
    },
    arrows: {
      control: 'boolean',
      description: 'Displays navigation arrows for manual slide transitions.',
    },
    options: {
      control: 'object',
      description: 'Configuration object for the Swiper instance.',
    },
    ' ': {
      control: 'text',
      description: 'Content inside the carousel, usually multiple `<road-carousel-item>` elements.',
    },
    // 🔹 Méthodes accessibles depuis Storybook
    getActiveIndex: {
      description: 'Returns the index of the active slide.',
      table: { category: 'Methods', type: { summary: '() => Promise<number>' } },
    },
    getPreviousIndex: {
      description: 'Returns the index of the previous slide.',
      table: { category: 'Methods', type: { summary: '() => Promise<number>' } },
    },
    getSwiper: {
      description: 'Returns the Swiper instance. See Swiper API for more details.',
      table: { category: 'Methods', type: { summary: '() => Promise<any>' } },
    },
    isBeginning: {
      description: 'Checks if the current slide is the first slide.',
      table: { category: 'Methods', type: { summary: '() => Promise<boolean>' } },
    },
    isEnd: {
      description: 'Checks if the current slide is the last slide.',
      table: { category: 'Methods', type: { summary: '() => Promise<boolean>' } },
    },
    length: {
      description: 'Returns the total number of slides.',
      table: { category: 'Methods', type: { summary: '() => Promise<number>' } },
    },
    lockSwipeToNext: {
      description: 'Locks the ability to slide to the next slide.',
      table: { category: 'Methods', type: { summary: '(lock: boolean) => Promise<void>' } },
    },
    lockSwipeToPrev: {
      description: 'Locks the ability to slide to the previous slide.',
      table: { category: 'Methods', type: { summary: '(lock: boolean) => Promise<void>' } },
    },
    lockSwipes: {
      description: 'Locks the ability to swipe in either direction.',
      table: { category: 'Methods', type: { summary: '(lock: boolean) => Promise<void>' } },
    },
    slideNext: {
      description: 'Transitions to the next slide.',
      table: { category: 'Methods', type: { summary: '(speed?: number, runCallbacks?: boolean) => Promise<void>' } },
    },
    slidePrev: {
      description: 'Transitions to the previous slide.',
      table: { category: 'Methods', type: { summary: '(speed?: number, runCallbacks?: boolean) => Promise<void>' } },
    },
    slideTo: {
      description: 'Transitions to a specified slide index.',
      table: { category: 'Methods', type: { summary: '(index: number, speed?: number, runCallbacks?: boolean) => Promise<void>' } },
    },
    startAutoplay: {
      description: 'Starts automatic slide transition.',
      table: { category: 'Methods', type: { summary: '() => Promise<void>' } },
    },
    stopAutoplay: {
      description: 'Stops automatic slide transition.',
      table: { category: 'Methods', type: { summary: '() => Promise<void>' } },
    },
    update: {
      description: 'Updates the carousel when slides are added or removed.',
      table: { category: 'Methods', type: { summary: '() => Promise<void>' } },
    },
    updateAutoHeight: {
      description: 'Updates carousel height when `autoHeight` is enabled.',
      table: { category: 'Methods', type: { summary: '(speed?: number) => Promise<void>' } },
    },
  },
  args: {
    pager: false,
    arrows: false,
    ' ': `
      <road-carousel-item>
        <road-img src="https://via.placeholder.com/800x400?text=Slide+1" alt="Slide 1"></road-img>
      </road-carousel-item>
      <road-carousel-item>
        <road-img src="https://via.placeholder.com/800x400?text=Slide+2" alt="Slide 2"></road-img>
      </road-carousel-item>
      <road-carousel-item>
        <road-img src="https://via.placeholder.com/800x400?text=Slide+3" alt="Slide 3"></road-img>
      </road-carousel-item>
    `,
  },
};

const Template = (args) => html`
<road-carousel 
  ?pager="${args.pager}" 
  ?arrows="${args.arrows}" 
  .options="${args.options}">
  ${unsafeHTML(args[' '])}
</road-carousel>
`;

export const Playground = Template.bind({});

export const Arrows = Template.bind({});
Arrows.args = {
  arrows: true,
};
