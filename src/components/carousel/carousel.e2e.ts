import { newE2EPage } from '@stencil/core/testing';

describe('road-carousel', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<road-carousel></road-carousel>');

    const element = await page.find('road-carousel');
    expect(element).toHaveClass('hydrated');
  });
});
