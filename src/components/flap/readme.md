# road-flap



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                     | Type                                                         | Default   |
| -------- | --------- | ------------------------------- | ------------------------------------------------------------ | --------- |
| `color`  | `color`   | Set the color of the flap.      | `"blackfriday" \| "ecology" \| "exclu" \| "info" \| "promo"` | `'promo'` |
| `filled` | `filled`  | Set to `true` for a filled flap | `boolean`                                                    | `false`   |
| `size`   | `size`    | The button size.                | `"md" \| "sm"`                                               | `'sm'`    |


## Slots

| Slot | Description          |
| ---- | -------------------- |
|      | Content of the flap. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
