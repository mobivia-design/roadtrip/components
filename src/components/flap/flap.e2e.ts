import { newE2EPage } from '@stencil/core/testing';

describe('road-flap', () => {
  it('it should render', async () => {
    const page = await newE2EPage();
    await page.setContent('<road-flap>Promo</road-flap>');

    const element = await page.find('road-flap');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
