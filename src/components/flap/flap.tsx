import { Component, Host, h, Prop } from '@stencil/core';

/**
 * @slot  - Content of the flap.
 */

@Component({
  tag: 'road-flap',
  styleUrl: 'flap.css',
  shadow: true,
})
export class Flap {

  /**
   * Set the color of the flap.
   */
  @Prop() color: 'promo' | 'exclu' | 'info' | 'ecology' | 'blackfriday' = 'promo';

  /**
   * Set to `true` for a filled flap
   */
    @Prop() filled: boolean = false;

  /**
    * The button size.
    */
  @Prop({ reflect: true }) size: 'sm' | 'md' = 'sm';

  render() {
    const colorClass = this.filled ? `flap-${this.color}-filled` : `flap-${this.color}`;

    return (
      <Host class={`${colorClass} flap-${this.size}`}>
        <slot/>
      </Host>
    );
  }

}
