import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Indicators/Flap',
  component: 'road-flap',
  argTypes: {
    color: {
      options: ['promo', 'exclu', 'info', 'ecology', 'blackfriday'],
      control: { type: 'select' },
      description: 'Defines the color theme of the flap.',
      table: {
        defaultValue: { summary: 'promo' },
      },
    },
    size: {
      options: ['sm', 'md'],
      control: { type: 'select' },
      description: 'Sets the size of the flap.',
      table: {
        defaultValue: { summary: 'md' },
      },
    },
    filled: {
      control: 'boolean',
      description: 'Determines if the flap has a filled background.',
    },
    ' ': {
      control: 'text',
      description: 'Content displayed inside the flap.',
    },
  },
  args: {
    color: 'promo',
    size: 'md',
    ' ': 'Promo',
    filled: false,
  },
};

const Template = (args) => html`
  <road-flap 
    color="${ifDefined(args.color)}" 
    size="${ifDefined(args.size)}" 
    ?filled="${args.filled}"
  >
    ${unsafeHTML(args[' '])}
  </road-flap>
`;

export const Playground = Template.bind({});

export const Exclu = Template.bind({});
Exclu.args = {
  color: 'exclu',
  ' ': 'Web Only',
};

export const Info = Template.bind({});
Info.args = {
  color: 'info',
  ' ': 'New',
};

export const Ecology = Template.bind({});
Ecology.args = {
  color: 'ecology',
  ' ': 'Ecology',
};

export const BlackFriday = Template.bind({});
BlackFriday.args = {
  color: 'blackfriday',
  ' ': 'Black Friday',
};

export const Sizes = Template.bind({});
Sizes.args = {
  size: 'sm',
  ' ': 'Small Size',
};

export const Filled = Template.bind({});
Filled.args = {
  color: 'promo',
  filled: true,
};
