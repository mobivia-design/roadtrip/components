import { newE2EPage } from '@stencil/core/testing';

describe('road-avatar', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <road-avatar>
        <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80"></road-img>
      </road-avatar>

      <road-avatar>
        <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80"></road-img>
        <road-badge>1</road-badge>
      </road-avatar>

      <road-avatar>
        <road-icon name="people"></road-icon>
      </road-avatar>
    `);

    const element = await page.find('road-avatar');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
