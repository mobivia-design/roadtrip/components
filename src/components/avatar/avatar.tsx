import { Component, h, Prop, Host } from '@stencil/core';

/**
 * @slot  - Image of the avatar.
 */
@Component({
  tag: 'road-avatar',
  styleUrl: 'avatar.css',
  shadow: true,
})
export class Avatar {

  /**
    * The Avatar size.
    */
   @Prop({ reflect: true }) size?: 'sm' | 'md' | 'lg' = 'md';

  render() {

    const sizeClass = this.size !== undefined ? `avatar-${this.size}` : '';


    return (

      <Host
        class={`${sizeClass}`}
      >
        <slot/>
      </Host>
      
    );
  }

}
