import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Media/Avatar',
  component: 'road-avatar',
  argTypes: {
    ' ': {
      description: "Image of the avatar.",
      control: 'text',
    },
    '--avatar-background': {
      description: "background color of the avatar",
      table: {
        defaultValue: { summary: 'var(--road-primary-500)' },
      },
      control: {
        type: null,
      },
    },
    size: {
      description: "The Avatar size.",
      options: ['sm', 'md', 'lg'],
      control: {
        type: 'select',
      },
      table: {
        defaultValue: { summary: 'md' },
      },
    },
  },
  args: {
    size: null,
    ' ': `<road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>`,
  },
};

const Template = (args) => html`
<road-avatar
size="${ifDefined(args.size)}">
  ${unsafeHTML(args[' '])}
</road-avatar>
`;

export const Playground = Template.bind({});

export const withNotification = Template.bind({});
withNotification.args = {
  ' ': `<road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
  <road-badge>1</road-badge>`,
};

export const Icon = Template.bind({});
Icon.args = {
  ' ': `<road-icon name="people-group" role="img"></road-icon>`,
};
