# road-avatar

Avatars are circular components that usually wrap an image or icon. They can be used to represent a person or an object.

Avatars can be used by themselves or inside of any element.

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description      | Type                                | Default |
| -------- | --------- | ---------------- | ----------------------------------- | ------- |
| `size`   | `size`    | The Avatar size. | `"lg" \| "md" \| "sm" \| undefined` | `'md'`  |


## Slots

| Slot | Description          |
| ---- | -------------------- |
|      | Image of the avatar. |


## CSS Custom Properties

| Name                  | Description                    |
| --------------------- | ------------------------------ |
| `--avatar-background` | background color of the avatar |
| `--avatar-width`      | width & height of the avatar   |


## Dependencies

### Used by

 - [road-profil-dropdown](../profil-dropdown)

### Graph
```mermaid
graph TD;
  road-profil-dropdown --> road-avatar
  style road-avatar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
