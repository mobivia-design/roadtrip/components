# plate-number



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description                                         | Type                                                           | Default     |
| ------------- | ------------- | --------------------------------------------------- | -------------------------------------------------------------- | ----------- |
| `country`     | `country`     | country of the plate                                | `"AT" \| "BE" \| "DE" \| "ES" \| "FR" \| "IT" \| "PL" \| "PT"` | `'FR'`      |
| `disabled`    | `disabled`    | If `true`, the user cannot interact with the input. | `boolean`                                                      | `false`     |
| `motorbike`   | `motorbike`   | Enable motorbike display                            | `boolean \| undefined`                                         | `undefined` |
| `placeholder` | `placeholder` | overwrite the default placeholder                   | `string \| undefined`                                          | `undefined` |
| `readonly`    | `readonly`    | If `true`, the user cannot modify the value.        | `boolean`                                                      | `false`     |
| `value`       | `value`       | The value of the input.                             | `null \| number \| string \| undefined`                        | `''`        |


## Events

| Event        | Description                             | Type                                                   |
| ------------ | --------------------------------------- | ------------------------------------------------------ |
| `roadblur`   | Emitted when the input loses focus.     | `CustomEvent<void>`                                    |
| `roadchange` | Emitted when the value has changed.     | `CustomEvent<{ value: string \| null \| undefined; }>` |
| `roadfocus`  | Emitted when the input has focus.       | `CustomEvent<void>`                                    |
| `roadinput`  | Emitted when a keyboard input occurred. | `CustomEvent<KeyboardEvent>`                           |


## Dependencies

### Depends on

- [road-input-group](../input-group)
- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-plate-number --> road-input-group
  road-plate-number --> road-icon
  style road-plate-number fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
