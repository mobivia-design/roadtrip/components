import { Component, h, Prop, Event, EventEmitter, Watch } from '@stencil/core';
import { licensePlateStarEu } from '../../../icons';

@Component({
  tag: 'road-plate-number',
  styleUrl: 'plate-number.css',
  scoped: true,
})
export class PlateNumber {

  private countries = [
    {
      country: 'FR',
      letter: 'F',
      placeholder: 'AT-857-YY',
    },
    {
      country: 'BE',
      letter: 'B',
      placeholder: '1-AAA-001',
    },
    {
      country: 'IT',
      letter: 'I',
      placeholder: 'AT 814TX',
    },
    {
      country: 'ES',
      letter: 'E',
      placeholder: '9512 HVY',
    },
    {
      country: 'PT',
      letter: 'P',
      placeholder: '13 24 PZ',
    },
    {
      country: 'PL',
      letter: 'PL',
      placeholder: 'GD 921KF',
    },
    {
      country: 'AT',
      letter: 'A',
      placeholder: 'FF 10 FF',
    },
    {
      country: 'DE',
      letter: 'D',
      placeholder: 'RA KL 8136',
    }
  ];

  /**
   * country of the plate
   */
  @Prop() country: 'FR' | 'BE' | 'IT' | 'ES' | 'PT' | 'PL' | 'AT' | 'DE' = 'FR';

  /**
   * If `true`, the user cannot interact with the input.
   */
  @Prop() disabled = false;

  /**
   * overwrite the default placeholder
   */
  @Prop() placeholder?: string;

  /**
   * If `true`, the user cannot modify the value.
   */
  @Prop() readonly = false;

  /**
   * The value of the input.
   */
  @Prop({ mutable: true }) value?: string | number | null = '';

  /**
   * Enable motorbike display
   */
   @Prop() motorbike?: boolean;

  /**
   * Update the native input element when the value changes
   */
  @Watch('value')
   protected valueChanged() {
     this.roadchange.emit({ value: this.value == null ? this.value : this.value.toString() });
     this.roadChange.emit({ value: this.value == null ? this.value : this.value.toString() });
   }

  /**
   * Emitted when a keyboard input occurred.
   */
  @Event() roadinput!: EventEmitter<KeyboardEvent>;
  /** @internal */
  @Event() roadInput!: EventEmitter<KeyboardEvent>;

  /**
   * Emitted when the value has changed.
   */
  @Event() roadchange!: EventEmitter<{
    value: string | undefined | null;
  }>;
  /** @internal */
  @Event() roadChange!: EventEmitter<{
    value: string | undefined | null;
  }>;

  /**
   * Emitted when the input loses focus.
   */
  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;

  /**
   * Emitted when the input has focus.
   */
  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  private getValue(): string {
    return typeof this.value === 'number'
      ? this.value.toString()
      : (this.value || '').toString();
  }

  private onInput = (ev: Event) => {
    const input = ev.target as HTMLInputElement | null;
    if (input) {
      input.value = input.value.toUpperCase() || "";
    }
    this.roadinput.emit(ev as KeyboardEvent);
    this.roadInput.emit(ev as KeyboardEvent);
  };

  private onBlur = () => {
    this.roadblur.emit();
    this.roadBlur.emit();
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  render() {
    const value = this.getValue();
    const motorbikeClass = this.motorbike ? 'motorbike-plate' : '';

    return (
      <road-input-group class={`plate-number plate-number-${this.country.toLowerCase()} ${motorbikeClass}`}>
        <label slot="prepend" class="input-group-prepend">
          <div class="plate-number-start">
            <road-icon class="plate-number-icon" icon={licensePlateStarEu}></road-icon>
            <div class="plate-number-location" aria-label={this.country} role="img">
              {this.countries
                .filter(item => item.country === this.country)[0].letter}
            </div>
          </div>
        </label>
        {this.motorbike
          ? (
            <textarea
              class="form-control plate-number-input mb-0"
              disabled={this.disabled}
              placeholder={this.placeholder != null ? this.placeholder :  this.countries.filter(item => item.country === this.country)[0].placeholder}
              readOnly={this.readonly}
              maxlength="9"
              rows={2}
              onInput={this.onInput}
              onBlur={this.onBlur}
              onFocus={this.onFocus}
            >
            </textarea>
          )
          : (
            <input
              class="plate-number-input mb-0"
              disabled={this.disabled}
              placeholder={this.placeholder != null ? this.placeholder :  this.countries.filter(item => item.country === this.country)[0].placeholder}
              readOnly={this.readonly}
              value={value}
              onInput={this.onInput}
              onBlur={this.onBlur}
              onFocus={this.onFocus}
            />
          )}
        <label slot="append" class="input-group-append">
          <div class="plate-number-end"></div>
        </label>
      </road-input-group>
    );
  }

}