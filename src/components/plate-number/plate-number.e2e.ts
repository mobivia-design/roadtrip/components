import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-plate-number', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-plate-number></road-plate-number>
      `,
    });
  });

  it('should render', async () => {
    const element = await page.find('road-plate-number');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should add a value', async () => {
    const roadinput = await page.spyOnEvent('roadinput');
    const roadchange = await page.spyOnEvent('roadchange');
    const roadblur = await page.spyOnEvent('roadblur');
    const roadfocus = await page.spyOnEvent('roadfocus');

    await page.focus('road-plate-number input');
    await page.type('road-plate-number input', 'AA-123-AA');
    await page.$eval('road-plate-number input', e => e.blur());

    expect(roadfocus).toHaveReceivedEvent();
    expect(roadblur).toHaveReceivedEvent();
    expect(roadinput).toHaveReceivedEvent();

    expect(roadchange).toHaveReceivedEventDetail({
      value: 'AA-123-AA',
    });

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should be disabled', async () => {
    const element = await page.find('road-plate-number');
    expect(element).toHaveClass('hydrated');

    element.setProperty('disabled', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
