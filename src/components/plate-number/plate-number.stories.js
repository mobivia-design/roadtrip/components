import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Forms/PlateNumber',
  component: 'road-plate-number',
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus'],
    },
  },
  argTypes: {
    country: {
      description: "country of the plate",
      options: ['FR', 'BE', 'IT', 'ES', 'PT', 'PL', 'AT', 'DE'],
      control: {
        type: 'select',
      },
      table: {
        defaultValue: { summary: 'FR' },
      },
    },
    disabled: {
      description: "If `true`, the user cannot interact with the input.",
      control: 'boolean',
    },
    readonly: {
      description: "If `true`, the user cannot modify the value.",
      control: 'boolean',
    },
    placeholder: {
      description: "overwrite the default placeholder",
      control: 'text',
    },
    motorbike: {
      description: "Enable motorbike display",
      control: 'boolean',
    },
    value: {
      description: "The value of the input.",
      control: 'text',
    },
    roadblur: {
      description: "Emitted when the input loses focus.",
      control: {
        type: null,
      },
    },
    roadfocus: {
      description: "Emitted when the input has focus.",
      control: {
        type: null,
      },
    },
    roadchange: {
      description: "Emitted when the value has changed.",
      action: 'roadchange',
      control: {
        type: null,
      },
    },
    roadinput: {
      description: "Emitted when a keyboard input occurred.",
      action: 'roadinput',
      control: {
        type: null,
      },
    },
  },
  args: {
    country: 'FR',
    disabled: null,
    readonly: null,
  },
};

const Template = (args) => html`
<road-plate-number
  country="${ifDefined(args.country)}"
  motorbike="${ifDefined(args.motorbike)}"
  disabled="${ifDefined(args.disabled)}"
  placeholder="${ifDefined(args.placeholder)}"
  readonly="${ifDefined(args.readonly)}"
  value="${ifDefined(args.value)}"
  @roadchange=${event => args.roadchange(event.detail.value)}
  @roadinput=${event => args.roadinput(event.target.value)}
></road-plate-number>
`;

export const Playground = Template.bind({});

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
  value: "AA-123-AA",
};

export const ReadOnly = Template.bind({});
ReadOnly.args = {
  readonly: true,
  value: "AA-123-AA",
};

export const MotorBike = () => html`
<road-plate-number
  country="FR"
  motorbike="true"
  style="width: 190px;"
></road-plate-number>
`;