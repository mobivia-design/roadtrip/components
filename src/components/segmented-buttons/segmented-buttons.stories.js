import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Navigation/Segmented buttons',
  component: 'road-segmented-buttons',
  subcomponents: {
    'road-segmented-button-bar': 'road-segmented-button-bar',
    'road-segmented-button': 'road-segmented-button',
    'road-tab': 'road-tab',
  },
  parameters: {
    actions: {
      handles: ['roadsegmentedbuttonclick'],
    },
    backgrounds: {
      default: 'grey',
    },
  },
  argTypes: {
    top: {
      description: "Content is placed at the top of the screen.",
      control: 'text',
    },
    ' ': {
      description: "Content is placed between the named slots if provided without a slot.",
      control: 'text',
    },
    roadtabsdidchange: {
      control: {
        type: null,
      },
    },
    roadtabswillchange: {
      control: {
        type: null,
      },
    },
    getSelected : {
      description: 'Get the currently selected tab.',
      table: {
        category: 'methods',
        type: {
          summary: 'getSelected() => Promise<string | undefined>',
        },
      },
      defaultValue: {
        summary: null,
      },
    },
    getTab : {
      description: 'Get a specific tab by the value of its `tab` property or an element reference.',
      table: {
        category: 'methods',
        type: {
          summary: 'getTab(tab: string | HTMLRoadTabElement) => Promise<HTMLRoadTabElement | undefined>',
        },
      },
      defaultValue: {
        summary: null,
      },
    },
    select : {
      description: 'Select a tab by the value of its `tab` property or an element reference.',
      table: {
        category: 'methods',
        type: {
          summary: 'select(tab: string | HTMLRoadTabElement) => Promise<boolean>',
        },
      },
      defaultValue: {
        summary: null,
      },
    },
  },
};

const Template = (args) => html`
<road-segmented-buttons>
  ${unsafeHTML(args.top)}
  ${unsafeHTML(args[' '])}
</road-segmented-buttons>
`;

export const Playground = Template.bind({});
Playground.args = {
  top: `<road-segmented-button-bar slot="top" selected-tab="tab-description">
    <road-segmented-button tab="tab-description">
      Description
    </road-segmented-button>

    <road-segmented-button tab="tab-services">
      Services
    </road-segmented-button>
    </road-segmented-button-bar>`,
  ' ': `<road-tab tab="tab-description">
    Description
  </road-tab>

  <road-tab tab="tab-services">
    Services
  </road-tab>`,
};

export const Small = Template.bind({});
Small.args = {
  top: `<road-segmented-button-bar slot="top" selected-tab="tab-description">
    <road-segmented-button tab="tab-description" size="sm">
      Description
    </road-segmented-button>

    <road-segmented-button tab="tab-services" size="sm">
      Services
    </road-segmented-button>
    </road-segmented-button-bar>`,
  ' ': `<road-tab tab="tab-description">
    Description
  </road-tab>

  <road-tab tab="tab-services">
    Services
  </road-tab>`,
};


