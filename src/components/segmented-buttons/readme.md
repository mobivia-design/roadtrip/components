# road-tabs



<!-- Auto Generated Below -->


## Events

| Event                | Description                                                                | Type                            |
| -------------------- | -------------------------------------------------------------------------- | ------------------------------- |
| `roadtabsdidchange`  | Emitted when the navigation has finished transitioning to a new component. | `CustomEvent<{ tab: string; }>` |
| `roadtabswillchange` | Emitted when the navigation is about to transition to a new component.     | `CustomEvent<{ tab: string; }>` |


## Methods

### `getSelected() => Promise<string | undefined>`

Get the currently selected tab.

#### Returns

Type: `Promise<string | undefined>`



### `getTab(tab: string | HTMLRoadTabElement) => Promise<HTMLRoadTabElement | undefined>`

Get a specific tab by the value of its `tab` property or an element reference.

#### Parameters

| Name  | Type                           | Description                                                                                         |
| ----- | ------------------------------ | --------------------------------------------------------------------------------------------------- |
| `tab` | `string \| HTMLRoadTabElement` | The tab instance to select. If passed a string, it should be the value of the tab's `tab` property. |

#### Returns

Type: `Promise<HTMLRoadTabElement | undefined>`



### `select(tab: string | HTMLRoadTabElement) => Promise<boolean>`

Select a tab by the value of its `tab` property or an element reference.

#### Parameters

| Name  | Type                           | Description                                                                                         |
| ----- | ------------------------------ | --------------------------------------------------------------------------------------------------- |
| `tab` | `string \| HTMLRoadTabElement` | The tab instance to select. If passed a string, it should be the value of the tab's `tab` property. |

#### Returns

Type: `Promise<boolean>`




## Slots

| Slot    | Description                                                           |
| ------- | --------------------------------------------------------------------- |
|         | Content is placed between the named slots if provided without a slot. |
| `"top"` | Content is placed at the top of the screen.                           |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
