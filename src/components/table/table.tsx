import { Component, h } from '@stencil/core';

/**
 * @slot  - Content of table, it should be an html table (https://developer.mozilla.org/fr/docs/Web/HTML/Element/table).
 */
@Component({
  tag: 'road-table',
  styleUrl: 'table.css',
  scoped: true,
})
export class Table {

  render() {
    return (
      <slot/>
    );
  }

}
