# road-table



<!-- Auto Generated Below -->


## Slots

| Slot | Description                                                                                                  |
| ---- | ------------------------------------------------------------------------------------------------------------ |
|      | Content of table, it should be an html table (https://developer.mozilla.org/fr/docs/Web/HTML/Element/table). |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
