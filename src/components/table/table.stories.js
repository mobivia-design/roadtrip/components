import { html } from 'lit';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Layout/Table',
  component: 'road-table',
  argTypes: {
    ' ': {
      description: "Content of table, it should be an html table (https://developer.mozilla.org/fr/docs/Web/HTML/Element/table).",
      control: 'text',
    },
  },
  args: {
    ' ': `<table class="table">
    <thead>
      <tr>
        <th scope="col">Title</th>
        <th scope="col">Title</th>
        <th scope="col">Title</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
      <tr>
        <td>Cell</td>
        <td>Cell</td>
        <td>Cell</td>
      </tr>
    </tbody>
  </table>`,
  },
};

export const Playground = (args) => html`
  <road-table>
  ${unsafeHTML(args[' '])}
</road-table>
`;
