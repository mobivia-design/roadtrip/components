# road-spinner



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description        | Type                                                  | Default     |
| -------- | --------- | ------------------ | ----------------------------------------------------- | ----------- |
| `color`  | `color`   | The color spinner. | `"dark" \| "default" \| "light" \| undefined`         | `'default'` |
| `size`   | `size`    | The button size.   | `"large" \| "medium" \| "small" \| "xl" \| undefined` | `'large'`   |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
