import { Component, h, Prop } from '@stencil/core';

@Component({
  tag: 'road-spinner',
  styleUrl: 'spinner.css',
  shadow: true,
})
export class Spinner {

  /**
    * The button size.
    */
   @Prop({ reflect: true }) size?: 'small' | 'medium' | 'large' | 'xl' = 'large';

   /**
    * The color spinner.
    */
    @Prop({ reflect: true }) color?: 'default' | 'light' | 'dark' = 'default';

    render() {
      const sizeClass = this.size !== undefined ? `spinner spinner--${this.size}` : 'spinner';
      const colorClass = this.size !== undefined ? `spinner-circle spinner-circle--${this.color}` : 'spinner-circle';


      return (
        <svg class={`${sizeClass}`} viewBox="25 25 50 50">
          <circle class={`${colorClass}`} cx="50" cy="50" r="20"></circle>
        </svg>
      );
    }

}
