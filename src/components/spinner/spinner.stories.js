import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';


export default {
  title: 'Indicators/Spinner',
  component: 'road-spinner',
  argTypes: {
    size: {
      description: "The button size.",
      options: ['small', 'medium', 'large', 'xl'],
      control: {
        type: 'select',
      },
    },
    color: {
      description: "The color spinner.",
      options: ['default', 'light', 'dark'],
      control: {
        type: 'select',
      },
    },
  },
  args: {
    size: null,
  },
};

export const Default = (args) => html`
  <road-spinner size="${ifDefined(args.size)}" color="${ifDefined(args.color)}"></road-spinner>
`;