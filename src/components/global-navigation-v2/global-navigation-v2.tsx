import { Component, Host, h, Prop, Watch, Event, EventEmitter, Listen, Element } from '@stencil/core';
import './../../utils/polyfill';

/**
 * @slot navbar - it should be road-navbar-item elements. Max 5 items on Mobile or add them to the drawer
 */
@Component({
  tag: 'road-global-navigation-v2',
  styleUrl: 'global-navigation-v2.css',
  shadow: true,
})
export class GlobalNavigationV2 {
  /**
   * The selected tab component
   */
  @Prop() selectedTab?: string;
  @Watch('selectedTab')
  selectedTabChanged() {
    if (this.selectedTab !== undefined) {
      this.roadnavbarchanged.emit({
        tab: this.selectedTab,
      });
      this.roadNavbarChanged.emit({
        tab: this.selectedTab,
      });
    }
  }

  /** @internal */
  @Event() roadnavbarchanged!: EventEmitter;
  /** @internal */
  @Event() roadNavbarChanged!: EventEmitter;

  /** Root element of the component */
  @Element() host!: HTMLElement;

  @Listen('roadNavbarItemClick')
  @Listen('roadnavbaritemclick')
  onNavbarChanged(ev: CustomEvent) {
    this.selectedTab = ev.detail.tab;
  }

  /** Toggles the compact attribute on <road-navbar-v2> */
  private toggleCompactMode() {
    const navbar = this.host.querySelector('road-navbar-v2');
    const toolbar = this.host.querySelector('road-toolbar-v2');

    // Toggle "compact" attribute on <road-navbar-v2>
    if (navbar) {
      if (navbar.hasAttribute('compact')) {
        navbar.removeAttribute('compact');
      } else {
        navbar.setAttribute('compact', 'true');
      }
    } else {
      console.warn('<road-navbar-v2> not found inside <road-global-navigation-v2>');
    }

    // Replace class "toolbar" with "toolbar-compact" on <road-toolbar-v2>
    if (toolbar) {
      if (toolbar.classList.contains('toolbar-compact')) {
        toolbar.classList.remove('toolbar-compact');
        toolbar.classList.add('toolbar');
      } else {
        toolbar.classList.remove('toolbar');
        toolbar.classList.add('toolbar-compact');
      }
    } else {
      console.warn('<road-toolbar-v2> not found inside <road-global-navigation-v2>');
    }
  }

  /** Toggles the "justify-content-center" class on the element with "compact-logo" */
  private toggleCompactLogoClass() {
    const logoElement = this.host.querySelector('.compact-logo');
    if (logoElement) {
      if (logoElement.classList.contains('justify-content-center')) {
        logoElement.classList.remove('justify-content-center');
      } else {
        logoElement.classList.add('justify-content-center');
      }
    } else {
      console.warn('Element with class "compact-logo" not found inside <road-global-navigation-v2>');
    }
  }

  /** Toggles the "rotate" attribute on the <road-icon> inside the <road-button> */
  private toggleIconRotation(button: HTMLElement) {
    const icon = button.querySelector('road-icon');
    if (icon) {
      if (icon.getAttribute('rotate') === '180') {
        icon.removeAttribute('rotate');
      } else {
        icon.setAttribute('rotate', '180');
      }
    } else {
      console.warn('<road-icon> not found inside <road-button>');
    }
  }

/** Toggles the "trigger" attribute on <road-tooltip> elements based on the presence of the "compact" attribute on <road-navbar-v2> */
private toggleTooltips() {
  const navbar = this.host.querySelector('road-navbar-v2');
  if (navbar) {
    const hasCompactAttribute = navbar.hasAttribute('compact');
    const tooltips = navbar.querySelectorAll('road-tooltip');

    if (tooltips.length > 0) {
      // Mettre à jour l'attribut "trigger" en fonction de l'attribut "compact"
      tooltips.forEach((tooltip) => {
        if (hasCompactAttribute) {
          tooltip.setAttribute('trigger', 'hover'); // Définir sur "hover" si "compact" est présent
        } else {
          tooltip.setAttribute('trigger', 'focus'); // Définir sur "focus" si "compact" est absent
        }
      });
    } else {
      console.warn('<road-tooltip> elements not found inside <road-navbar-v2>.');
    }
  } else {
    console.warn('<road-navbar-v2> not found inside <road-global-navigation-v2>.');
  }
}







  
    /** Toggles the compact mode and performs all related updates */

  /** Handles click events on road-button or its children */
  @Listen('click', { target: 'window' })
  onButtonClick(event: MouseEvent) {
    let target = event.target as HTMLElement;

    // Traverse up the DOM tree to find the parent road-button
    while (target && target.tagName !== 'ROAD-BUTTON' && target !== document.body) {
      target = target.parentElement as HTMLElement;
    }

    if (target?.tagName === 'ROAD-BUTTON' && target.classList.contains('compact-button')) {
      this.toggleCompactMode();
      this.toggleIconRotation(target);
      this.toggleCompactLogoClass();
      this.toggleTooltips();
    }
  }

  componentWillLoad() {
    this.selectedTabChanged();
  }

  render() {
    return (
      <Host role="application">
        <slot />
      </Host>
    );
  }
}
