import { newE2EPage } from '@stencil/core/testing';

describe('road-select', () => {
  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-select label="Label" select-id="selectId"></road-select>
      `,
    });

    await page.$eval('road-select', (elm: any) => {
      elm.options = [
        { value: 'link1', label: 'link1' },
        { value: 'link2', label: 'link2' },
        { value: 'link3', label: 'link3' }
      ];
    });

    await page.waitForChanges();
  });

  it('should render', async () => {
    const element = await page.find('road-select');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should select an option', async () => {
    const roadchange = await page.spyOnEvent('roadchange');
    const roadblur = await page.spyOnEvent('roadblur');
    const roadfocus = await page.spyOnEvent('roadfocus');

    await page.focus('road-select select');
    await page.select('road-select select', 'link2');
    await page.$eval('road-select select', e => e.blur());

    expect(roadfocus).toHaveReceivedEvent();
    expect(roadblur).toHaveReceivedEvent();

    expect(roadchange).toHaveReceivedEventDetail({
      value: 'link2',
    });

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render disabled', async () => {
    const element = await page.find('road-select');
    expect(element).toHaveClass('hydrated');

    element.setProperty('disabled', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with error', async () => {
    const element = await page.find('road-select');
    expect(element).toHaveClass('hydrated');

    element.setProperty('error', 'Select an item');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
