import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Forms/Select',
  component: 'road-select',
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus'],
    },
  },
  argTypes: {
    disabled: {
      description: "If `true`, the user cannot interact with the select.",
      control: 'boolean',
    },
    required: {
      description: "If `true`, the user must fill in a value before submitting a form.",
      control: 'boolean',
    },
    autofocus: {
      description: "This Boolean attribute lets you specify that a form control should have input focus when the page loads.",
      control: 'boolean',
    },
    error: {
      description: "Error message for the field",
      control: 'text',
    },
    label: {
      description: "Label for the field",
      control: 'text',
    },
    name: {
      description: "The name of the control, which is submitted with the form data.",
      control: 'text',
    },
    size: {
      description: "The sizes of the input.",
      control: 'number',
    },
    sizes: {
      description: "If the control is presented as a scrolling list box (e.g. when multiple is specified),\nthis attribute represents the number of rows in the list that should be visible at one time.",
      options: ['md', 'lg', 'xl'],
      control: {
        type: 'select',
      },
    },
    'select-id': {
      description: "The id of select",
      control: 'text',
    },
    value: {
      description: "the value of the select.",
      control: 'text',
    },
    roadblur: {
      description: "Emitted when the select loses focus.",
      control: {
        type: null,
      },
    },
    roadfocus: {
      description: "Emitted when the select has focus.",
      control: {
        type: null,
      },
    },
    roadchange: {
      description: "Emitted when the value has changed.",
      action: 'roadchange',
      control: {
        type: null,
      },
    },
  },
  args: {
    options: [
      { value: 'value1', label: 'Value1'},
      { value: 'value2', label: 'Value2' },
      { value: 'value3', label: 'Value3' }
    ],
    sizes: 'xl',
    disabled: null,
    required: null,
    label: 'Label',
    autofocus: null,
  },
};

const Template = (args) => html`
  <road-select 
    .options=${args.options}
    label="${ifDefined(args.label)}"
    disabled="${ifDefined(args.disabled)}"
    required="${ifDefined(args.required)}"
    autofocus="${ifDefined(args.autofocus)}"
    error="${ifDefined(args.error)}"
    size="${ifDefined(args.size)}"
    sizes="${ifDefined(args.sizes)}"
    select-id="${ifDefined(args['select-id'])}"
    value="${ifDefined(args.value)}"
    @roadchange=${event => args.roadchange(event.detail.value)}
  ></road-select>
`;

export const Playground = Template.bind({});

export const WithValue = Template.bind({});
WithValue.args = {
  options: [
    { value: 'value1', label: 'Value1', selected: true },
    { value: 'value2', label: 'Value2' },
    { value: 'value3', label: 'Value3' }
  ],
  value: 'value2',
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
};

export const Error = Template.bind({});
Error.args = {
  error: 'Select an item',
};
