# road-select



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description                                                                                                                                                                         | Type                                                                             | Default                            |
| ----------- | ----------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- | ---------------------------------- |
| `autofocus` | `autofocus` | This Boolean attribute lets you specify that a form control should have input focus when the page loads.                                                                            | `boolean`                                                                        | `false`                            |
| `disabled`  | `disabled`  | If `true`, the user cannot interact with the select.                                                                                                                                | `boolean`                                                                        | `false`                            |
| `error`     | `error`     | Error message for the field                                                                                                                                                         | `string \| undefined`                                                            | `undefined`                        |
| `label`     | `label`     | Label for the field                                                                                                                                                                 | `string`                                                                         | `` `${this.selectId}-label` ``     |
| `name`      | `name`      | The name of the control, which is submitted with the form data.                                                                                                                     | `string`                                                                         | `this.selectId`                    |
| `options`   | --          | List of options of the select                                                                                                                                                       | `{ value: string \| number; label: string; selected?: boolean \| undefined; }[]` | `[]`                               |
| `required`  | `required`  | If `true`, the user must fill in a value before submitting a form.                                                                                                                  | `boolean`                                                                        | `false`                            |
| `selectId`  | `select-id` | The id of select                                                                                                                                                                    | `string`                                                                         | `` `road-select-${selectIds++}` `` |
| `size`      | `size`      | If the control is presented as a scrolling list box (e.g. when multiple is specified), this attribute represents the number of rows in the list that should be visible at one time. | `number`                                                                         | `0`                                |
| `sizes`     | `sizes`     | The sizes of the input.                                                                                                                                                             | `"lg" \| "md" \| "xl"`                                                           | `'xl'`                             |
| `value`     | `value`     | the value of the select.                                                                                                                                                            | `any`                                                                            | `undefined`                        |


## Events

| Event        | Description                          | Type                                                   |
| ------------ | ------------------------------------ | ------------------------------------------------------ |
| `roadblur`   | Emitted when the select loses focus. | `CustomEvent<void>`                                    |
| `roadchange` | Emitted when the value has changed.  | `CustomEvent<{ value: string \| null \| undefined; }>` |
| `roadfocus`  | Emitted when the select has focus.   | `CustomEvent<void>`                                    |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
