import { Component, Event, EventEmitter, Host, Prop, Watch, h } from '@stencil/core';

@Component({
  tag: 'road-select',
  styleUrl: 'select.css',
  scoped: true,
})
export class Select {

  /**
   * The id of select
   */
  @Prop() selectId: string = `road-select-${selectIds++}`;

  /**
   * List of options of the select
   */
  @Prop() options: Array<{
    value: string | number;
    label: string;
    selected?: boolean;
  }> = [];

  /**
   * This Boolean attribute lets you specify that a form control should have input focus when the page loads.
   */
  @Prop() autofocus: boolean = false;

  /**
   * If `true`, the user cannot interact with the select.
   */
  @Prop() disabled: boolean = false;

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.selectId;

  /**
   * If `true`, the user must fill in a value before submitting a form.
   */
  @Prop() required: boolean = false;

  /**
   * If the control is presented as a scrolling list box (e.g. when multiple is specified),
   * this attribute represents the number of rows in the list that should be visible at one time.
   */
  @Prop() size: number = 0;

  /**
   * The sizes of the input.
   */
  @Prop() sizes: 'md' | 'lg' | 'xl' = 'xl';

  /**
   * Label for the field
   */
  @Prop() label: string = `${this.selectId}-label`;

  /**
   * Error message for the field
   */
  @Prop() error?: string;

  /**
   * the value of the select.
   */
  @Prop({ mutable: true }) value?: any | null;

  /**
   * Emitted when the value has changed.
   */
  @Event() roadchange!: EventEmitter<{
    value: string | undefined | null;
  }>;
  /** @internal */
  @Event() roadChange!: EventEmitter<{
    value: string | undefined | null;
  }>;

  /**
   * Emitted when the select has focus.
   */
  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  /**
   * Emitted when the select loses focus.
   */
  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;

  @Watch('value')
  valueChanged() {
    if (this.value === null) {
      this.resetSelection();
    } else {
      this.addSelected();
    }

    this.roadchange.emit({
      value: this.value,
    });
    this.roadChange.emit({
      value: this.value,
    });
  }

  componentWillLoad() {
    if (this.value === null) {
      this.resetSelection();
    } else {
      this.addSelected();
    }
  }

  addSelected() {
    // Désélection de toutes les options
    this.options.forEach(option => option.selected = false);

    // Sélection de la nouvelle option si elle correspond à la valeur actuelle
    const selectedOption = this.options.find(option => option.value == this.value);
    if (selectedOption) {
      selectedOption.selected = true;
    }
  }

  resetSelection() {
    this.value = null;
    this.options.forEach(option => option.selected = false);
  }

  private onChange = (ev: Event) => {
    const select = ev.target as HTMLSelectElement | null;
    if (select) {
      this.value = select.value || null;
    }
  };

  private onBlur = () => {
    this.roadblur.emit();
    this.roadBlur.emit();
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  render() {
    const labelId = this.selectId + '-label';
    const hasValueClass = this.value && this.value !== '' ? 'has-value' : '';
    const isInvalidClass = this.error !== undefined && this.error !== '' ? 'is-invalid' : '';

    return (
      <Host class={this.sizes && `select-${this.sizes}`}>
        <select
        class={`form-select ${hasValueClass} ${isInvalidClass}`}
        id={this.selectId}
        aria-disabled={this.disabled ? 'true' : null}
        autoFocus={this.autofocus}
        disabled={this.disabled}
        name={this.name}
        required={this.required}
        size={this.size}
        onChange={this.onChange}
        onFocus={this.onFocus}
        onBlur={this.onBlur}
      >
        <option
          selected={!this.value} // sélectionné si this.value est null ou vide
          disabled
          hidden
          style={{ display: 'none' }}
          value=""
        >
        </option>
        {this.options && this.options.map(option => (
          <option
            value={option.value}
            selected={this.value !== null && option.value == this.value}
          >
            {option.label}
          </option>
        ))}
      </select>
        <label class="form-select-label" id={labelId} htmlFor={this.selectId}>{this.label}</label>
        {this.error && this.error !== '' && <p class="invalid-feedback">{this.error}</p>}
      </Host>
    );
  }

}

let selectIds = 0;
