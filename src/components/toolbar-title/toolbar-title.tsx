import { Component, h } from '@stencil/core';

/**
 * @slot - Text of the title.
 */

@Component({
  tag: 'road-toolbar-title',
  styleUrl: 'toolbar-title.css',
  shadow: true,
})
export class ToolbarTitle {

  render() {
    return (
      <div class="toolbar-title">
        <slot/>
      </div>
    );
  }

}
