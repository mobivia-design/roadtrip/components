# road-toolbar-title

`road-title` is a component that sets the title of the `Toolbar`.

<!-- Auto Generated Below -->


## Slots

| Slot | Description        |
| ---- | ------------------ |
|      | Text of the title. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
