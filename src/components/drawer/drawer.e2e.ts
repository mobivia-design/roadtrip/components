import { newE2EPage } from '@stencil/core/testing';

describe('road-drawer', () => {
  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
      <road-drawer position="left" drawer-width="360"></road-drawer>
      `,
    });
  });

  it('should render closed by default', async () => {
    const open = await page.spyOnEvent('open');
    const close = await page.spyOnEvent('close');

    const element = await page.find('road-drawer');

    const isOpen = await element.getProperty('isOpen');
    expect(isOpen).toBeFalsy();

    await element.setProperty('isOpen', true);

    await page.waitForChanges();

    const isOpened = await element.getProperty('isOpen');
    expect(isOpened).toBeTruthy();

    expect(open).toHaveReceivedEvent();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();

    await element.callMethod('close');

    await page.waitForChanges();

    const isStillOpen = await element.getProperty('isOpen');
    expect(isStillOpen).toBeFalsy();

    await page.waitForTimeout(500);

    expect(close).toHaveReceivedEvent();
  });
});
