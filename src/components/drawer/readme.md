# road-drawer



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description                                                          | Type                  | Default     |
| ------------------ | -------------------- | -------------------------------------------------------------------- | --------------------- | ----------- |
| `ariaLabel`        | `aria-label`         | Aria label of the drawer                                             | `string \| undefined` | `undefined` |
| `ariaLabelBack`    | `aria-label-back`    | Aria label of the drawer                                             | `string \| undefined` | `undefined` |
| `ariaLabelClose`   | `aria-label-close`   | Aria label of the drawer                                             | `string \| undefined` | `undefined` |
| `backText`         | `back-text`          | Show / hide back icon                                                | `string \| undefined` | `undefined` |
| `drawerTitle`      | `drawer-title`       | Title of the drawer in the header bar                                | `string \| undefined` | `undefined` |
| `drawerWidth`      | `drawer-width`       | Width of the drawer                                                  | `number`              | `480`       |
| `hasBackIcon`      | `has-back-icon`      | Show / hide back icon                                                | `boolean`             | `false`     |
| `hasCloseIcon`     | `has-close-icon`     | Show / hide close icon                                               | `boolean`             | `true`      |
| `hasInverseHeader` | `has-inverse-header` | Inverse header colors                                                | `boolean`             | `false`     |
| `isOpen`           | `is-open`            | Set isOpen property to true to open the drawer                       | `boolean`             | `false`     |
| `position`         | `position`           | position of the drawer. e.g. left, right, bottom                     | `string`              | `'left'`    |
| `removePadding`    | `remove-padding`     | Set removePadding property to true to remove padding for drawer body | `boolean`             | `true`      |


## Events

| Event   | Description                                                  | Type                |
| ------- | ------------------------------------------------------------ | ------------------- |
| `back`  | Indicate when return to previous state of the drawer content | `CustomEvent<void>` |
| `close` | Indicate when closing the drawer                             | `CustomEvent<void>` |
| `open`  | Indicate when opening the drawer                             | `CustomEvent<void>` |


## Methods

### `back() => Promise<void>`

Return to previous state of the drawer content

#### Returns

Type: `Promise<void>`



### `close() => Promise<void>`

Close the drawer

#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`

Open the drawer

#### Returns

Type: `Promise<void>`




## Slots

| Slot       | Description                                                                                                                                                                                                            |
| ---------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|            | Content of the drawer.                                                                                                                                                                                                 |
| `"footer"` | footer of the drawer `<div slot="footer">` `<road-button color="primary" outline="true" expand="true">Annuler</road-button>` `<road-button color="primary" expand="true" class="mb-0">Valider</road-button>` ` </div>` |
| `"title"`  | replace the title with a custom title section when `'drawerTitle'` is not set                                                                                                                                          |


## CSS Custom Properties

| Name                   | Description                                                      |
| ---------------------- | ---------------------------------------------------------------- |
| `--back-chevron-color` | color of back chevron                                            |
| `--background`         | background color of the content of the drawer.                   |
| `--color`              | text color of the content of the drawer.                         |
| `--header-background`  | color of the header background.                                  |
| `--header-color`       | color of the header text.                                        |
| `--header-delimiter`   | size of the border delimiter (in px) at the bottom of the header |
| `--header-icon`        | color of the header icons.                                       |
| `--max-height`         | max-height of the drawer for bottom position                     |
| `--z-index`            | The z-index of the Drawer.                                       |


## Dependencies

### Used by

 - [road-duration](../duration)

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-drawer --> road-icon
  road-duration --> road-drawer
  style road-drawer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
