import { Component, Element, Event, EventEmitter, Listen, Method, Prop, h, Host, Watch } from '@stencil/core';
import { navigationClose, navigationChevron } from '../../../icons';

/**
 * @slot  - Content of the drawer.
 * @slot title - replace the title with a custom title section when `'drawerTitle'` is not set
 * @slot footer - footer of the drawer
 * `<div slot="footer">`
   `<road-button color="primary" outline="true" expand="true">Annuler</road-button>`
   `<road-button color="primary" expand="true" class="mb-0">Valider</road-button>`
   ` </div>`
 */


@Component({
  tag: 'road-drawer',
  styleUrl: 'drawer.css',
  shadow: true,
})
export class Drawer {

  /**
   * Current reference of the drawer
   */
  @Element() el!: HTMLRoadDrawerElement;

  /**
   * Set isOpen property to true to open the drawer
   */
  @Prop({ mutable: true }) isOpen: boolean = false;

    /**
   * Set removePadding property to true to remove padding for drawer body
   */
    @Prop({ mutable: true }) removePadding: boolean = false;

  /**
   * position of the drawer. e.g. left, right, bottom
   */
  @Prop() position: string = 'left';

  /**
   * Width of the drawer
   */
  @Prop() drawerWidth: number = 480;

  /**
   * Inverse header colors
   */
  @Prop() hasInverseHeader: boolean = false;

  /**
   * Show / hide back icon
   */
  @Prop() hasBackIcon: boolean = false;

  /**
   * Show / hide back icon
   */
  @Prop() backText?: string;

  /**
   * Title of the drawer in the header bar
   */
  @Prop() drawerTitle?: string;

   /**
   * Aria label of the drawer
   */
   @Prop() ariaLabel?: string;

  /**
   * Aria label of the drawer
   */
    @Prop() ariaLabelBack?: string;

  /**
   * Aria label of the drawer
   */
   @Prop() ariaLabelClose?: string;

  /**
   * Show / hide close icon
   */
  @Prop() hasCloseIcon: boolean = true;

  /**
   * Indicate when opening the drawer
   */
  @Event({ eventName: 'open' }) onOpen!: EventEmitter<void>;

  /**
   * Indicate when closing the drawer
   */
  @Event({ eventName: 'close' }) onClose!: EventEmitter<void>;

  /**
   * Indicate when return to previous state of the drawer content
   */
  @Event({ eventName: 'back' }) onBack!: EventEmitter<void>;

  /**
   * Open the drawer
   */
  @Method()
  async open() {
    this.isOpen = true;
  }

  /**
   * Close the drawer
   */
  @Method()
  async close() {
    this.isOpen = false;
  }

  /**
   * Return to previous state of the drawer content
   */
  @Method()
  async back() {
    this.onBack.emit();
  }

  // @Watch('isOpen')
  // handleOpen(openValue: boolean) {
  //   if(openValue === true) {
  //     this.onOpen.emit();
  //   } else {
  //     this.el.addEventListener('transitionend', () => {
  //       this.onClose.emit();
  //       this.el.shadowRoot && ((this.el.shadowRoot.querySelector('.drawer-body') as HTMLElement).scrollTop = 0);
  //     }, { once: true});
  //   }
  // }


  @Watch('isOpen')
  handleOpen(openValue: boolean) {
    if(openValue === true) {
      this.onOpen.emit();
      // Focus the first button element after the drawer is opened
      setTimeout(() => this.focusFirstButton(), 50); // Add a slight delay to ensure the element is rendered
    } else {
      this.el.addEventListener('transitionend', () => {
        this.onClose.emit();
        this.el.shadowRoot && ((this.el.shadowRoot.querySelector('.drawer-body') as HTMLElement).scrollTop = 0);
      }, { once: true });
    }
  }
  
  /**
   * Find and focus the first button element in the drawer
   */
  private focusFirstButton() {
    const buttonElement = this.el.shadowRoot?.querySelector('button') as HTMLElement;
  
    if (buttonElement) {
      buttonElement.focus(); // Focus the first button element
    }
  }
  

  /**
   * Close the dialog when clicking on the cross or layer
   */
  private onClick = (ev: UIEvent) => {
    ev.stopPropagation();
    ev.preventDefault();

    this.close();
  };

  /**
   * Close the dialog when clicking on the cross or layer
   */
  private onClickBack = (event: MouseEvent) => {
    event.stopPropagation();
    event.preventDefault();

    this.back();
  };

  /**
   * Close the dialog when press Escape key
   */
  @Listen('keyup', { target: 'document' })
  onEscape(event: KeyboardEvent) {
    if (event.key === 'Escape' || event.key === "Esc") {
      this.close();
    }
  }

  /**
   * Call close function when clicking an element with data-dismiss="modal" attribute
   */
  componentDidLoad() {
    this.el.querySelectorAll('[data-dismiss="modal"]').forEach(item => {
      item.addEventListener('click', () => this.close());
    });
  }

  render() {
    const drawerIsOpenClass = this.isOpen ? 'drawer-open' : '';
    const removePaddingClass = this.removePadding ? 'remove-padding' : '';
    const inverseHeaderClass = this.hasInverseHeader ? 'drawer-header-inverse' : '';
    const ariaLabel = this.ariaLabel ?? 'drawer';
    const ariaLabelBack = this.ariaLabelBack ?? 'Back';
    const ariaLabelClose = this.ariaLabelClose ?? 'Close';
    const drawerDelimiterClass = this.drawerTitle && !this.hasInverseHeader ? 'drawer-delimiter' : '';
    const backIconElement = this.hasBackIcon ? (
      <button type="button" class="drawer-action" aria-label={ariaLabelBack} onClick={this.onClickBack}>
        <road-icon icon={navigationChevron} rotate="180"></road-icon>
        {this.backText}
      </button>
    ) : null;
    const closeIconElement = this.hasCloseIcon ? (
      <button type="button" class="drawer-close" onClick={this.onClick} aria-label={ariaLabelClose}>
        <road-icon icon={navigationClose} aria-hidden="true"></road-icon>
      </button>
    ) : null;
    const drawerWidthValue = this.position === 'bottom' ? '100%' : `${this.drawerWidth}px`;
  
    return (
      <Host class={`${drawerIsOpenClass}  drawer-${this.position}`} tabindex="0" role="dialog" aria-label={ariaLabel}>
        <div class="drawer-overlay" onClick={this.onClick} tabindex="-1"></div>
        <div class="drawer-dialog" style={{ maxWidth: drawerWidthValue }} role="document" tabindex="0">
          <div class="drawer-content">
            <header class={`drawer-header ${inverseHeaderClass} ${drawerDelimiterClass}`}>
              {backIconElement}
              {this.drawerTitle ? (
                <h2 class="drawer-title">{this.drawerTitle}</h2>
              ) : (
                <div class="drawer-title">
                  <slot name="title" />
                </div>
              )}
              {closeIconElement}
            </header>
            <div class={`drawer-body ${removePaddingClass}`}>
              <slot />
            </div>
            <footer class="drawer-footer">
              <slot name="footer" />
            </footer>
          </div>
        </div>
      </Host>
    );
  }
  
}