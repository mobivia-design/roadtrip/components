import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Modals/Drawer',
  component: 'road-drawer',
  parameters: {
    layout: 'fullscreen',
    backgrounds: {
      default: 'grey',
    },
    actions: {
      handles: ['open', 'close', 'back'],
    },
  },
  argTypes: {
    'is-open': {
      control: 'boolean',
      description: 'Defines whether the drawer is open or closed.',
    },
    'remove-padding': {
      control: 'boolean',
      description: 'If true add padding for drawer body',
    },
    'has-back-icon': {
      control: 'boolean',
      description: 'Displays a back arrow icon in the header.',
    },
    'back-text': {
      control: 'text',
      description: 'Text label for the back button.',
    },
    'has-close-icon': {
      control: 'boolean',
      description: 'Displays a close (X) icon in the header.',
    },
    'has-inverse-header': {
      control: 'boolean',
      description: 'Uses an inverse color scheme for the header.',
    },
    position: {
      options: ['left', 'bottom', 'right'],
      control: { type: 'radio' },
      description: 'Defines the drawer position.',
    },
    'drawer-width': {
      control: 'number',
      description: 'Sets the width of the drawer (only applicable for side drawers).',
      table: {
        defaultValue: { summary: 480 },
      },
    },
    'drawer-title': {
      control: 'text',
      description: 'Defines the title displayed in the drawer header.',
    },
    footer: {
      control: 'text',
      description: ' `<div slot="footer"><road-button color="primary" outline="true" expand="true">Annuler</road-button>`\n` <road-button color="primary" expand="true">Envoyer</road-button></div>`',
    },
    title: {
      control: 'text',
      description: 'Custom title slot for advanced customization.',
    },
    'aria-label': {
      control: 'text',
      description: 'ARIA label for accessibility.',
    },
    'aria-label-back': {
      control: 'text',
      description: 'ARIA label for the back button.',
    },
    'aria-label-close': {
      control: 'text',
      description: 'ARIA label for the close button.',
    },
    ' ': {
      control: 'text',
      description: 'Main content inside the drawer.',
    },
    '--z-index': {
      table: { defaultValue: { summary: '10' } },
      control: 'text',
      description: 'Defines the z-index of the drawer.',
    },
    '--background': {
      table: { defaultValue: { summary: 'var(--road-grey-000)' } },
      control: 'text',
      description: 'Sets the background color of the drawer.',
    },
  },
  args: {
    'is-open': true,
    'remove-padding': false,
    position: 'right',
    'drawer-width': 480,
    'has-inverse-header': false,
    'has-back-icon': false,
    'has-close-icon': true,
    'drawer-title': 'Drawer title',
    ' ': `<div>
      <p>This is the drawer content.</p>
      <road-button color="primary" expand>Confirm</road-button>
    </div>`,
  },
};

const Template = (args) => html`
  <road-drawer
    ?is-open="${args['is-open']}"
    ?remove-padding="${args['remove-padding']}"
    ?has-back-icon="${args['has-back-icon']}"
    ?has-close-icon="${args['has-close-icon']}"
    ?has-inverse-header="${args['has-inverse-header']}"
    position="${ifDefined(args.position)}"
    drawer-width="${ifDefined(args['drawer-width'])}"
    drawer-title="${ifDefined(args['drawer-title'])}"
    back-text="${ifDefined(args['back-text'])}"
    aria-label="${ifDefined(args['aria-label'])}"
    aria-label-back="${ifDefined(args['aria-label-back'])}"
    aria-label-close="${ifDefined(args['aria-label-close'])}"
    style="
      --z-index: ${ifDefined(args['--z-index'])};
      --background: ${ifDefined(args['--background'])};
    "
  >
    ${unsafeHTML(args.title)}
    ${unsafeHTML(args[' '])}
    ${unsafeHTML(args.footer)}
  </road-drawer>
`;

export const Playground = Template.bind({});

export const HeaderInverse = Template.bind({});
HeaderInverse.args = {
  'has-inverse-header': true,
};

export const WithBackButton = Template.bind({});
WithBackButton.args = {
  'has-back-icon': true,
  'back-text': 'Back',
};

export const CustomTitle = Template.bind({});
CustomTitle.args = {
  title: `<div slot="title" class="d-flex align-items-center">
    <road-icon name="key" color="secondary" class="mr-16" role="img"></road-icon>
    <road-text color="secondary">618</road-text>
    <road-icon name="multi-service-outline" color="secondary" class="ml-16" role="img"></road-icon>
  </div>`,
};

export const BottomDrawer = Template.bind({});
BottomDrawer.args = {
  position: 'bottom',
  title: `<strong slot="title">Contact by SMS</strong>`,
  ' ': `<div class="bg-light p-16">
    <road-input label="Phone Number*" type="tel"></road-input>
    <road-button color="secondary" expand class="mb-8">Confirm</road-button>
    <p class="text-content mb-0">*required fields</p>
  </div>`,
};
