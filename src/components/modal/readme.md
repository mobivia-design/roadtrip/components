# road-modal



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description                                    | Type                  | Default     |
| ------------------ | -------------------- | ---------------------------------------------- | --------------------- | ----------- |
| `hasCloseIcon`     | `has-close-icon`     | Show / hide close icon                         | `boolean`             | `true`      |
| `hasInverseHeader` | `has-inverse-header` | inverse header colors                          | `boolean`             | `false`     |
| `isOpen`           | `is-open`            | Set isOpen propertie to true to show the modal | `boolean`             | `false`     |
| `maxWidth`         | `max-width`          | Max width of the modal on desktop              | `number`              | `696`       |
| `modalTitle`       | `modal-title`        | Title of the modal in the header bar           | `string \| undefined` | `undefined` |


## Events

| Event   | Description                     | Type                |
| ------- | ------------------------------- | ------------------- |
| `close` | Indicate when closing the modal | `CustomEvent<void>` |


## Methods

### `close() => Promise<void>`

Close the modal

#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`

Open the modal

#### Returns

Type: `Promise<void>`




## Slots

| Slot | Description           |
| ---- | --------------------- |
|      | Content of the modal. |


## CSS Custom Properties

| Name        | Description               |
| ----------- | ------------------------- |
| `--z-index` | The z-index of the Modal. |


## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-modal --> road-icon
  style road-modal fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
