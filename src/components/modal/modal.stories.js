import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Modals/Modal',
  component: 'road-modal',
  parameters: {
    layout: 'fullscreen',
    actions: {
      handles: ['close'],
    },
  },
  argTypes: {
    'is-open': {
      description: "Set isOpen propertie to true to show the modal",
      control: 'boolean',
    },
    'has-close-icon': {
      description: "Show / hide close icon",
      control: 'boolean',
      table: {
        defaultValue: { summary: 'true' },
      },
    },
    'has-inverse-header': {
      description: "inverse header colors",
      control: 'boolean',
    },
    'max-width': {
      description: "Max width of the modal on desktop",
      control: 'number',
      min: 0,
      table: {
        defaultValue: { summary: '696' },
      },
    },
    'modal-title': {
      description: "Title of the modal in the header bar",
      control: 'text',
    },
    ' ': {
      description: "Content of the modal.",
      control: 'text',
    },
    close: {
      description: "Indicate when closing the modal",
      control: {
        type: null,
      },
    },
    '--z-index': {
      description: "The z-index of the Modal.",
      table: {
        defaultValue: { summary: '3' },
      },
      control: {
        type: null,
      },
    },
  },
};

export const Playground = (args) => html`
<road-modal 
  is-open="${ifDefined(args['is-open'])}"
  has-back-icon="${ifDefined(args['has-back-icon'])}" 
  has-close-icon="${ifDefined(args['has-close-icon'])}" 
  has-inverse-header="${ifDefined(args['has-inverse-header'])}" 
  max-width="${ifDefined(args['max-width'])}"
  modal-title="${ifDefined(args['modal-title'])}"
>
  ${unsafeHTML(args[' '])}
</road-modal>
`;
Playground.args = {
  'has-inverse-header': true,
  'is-open': true,
  'has-close-icon': null,
  'max-width': 480,
};
