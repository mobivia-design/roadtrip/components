import { Component, Element, Event, EventEmitter, Listen, Method, Prop, h, Host } from '@stencil/core';
import { navigationClose } from '../../../icons';

/**
 * @slot  - Content of the modal.
 */

@Component({
  tag: 'road-modal',
  styleUrl: 'modal.css',
  shadow: true,
})
export class Modal {

  /**
   * Current reference of the modal
   */
  @Element() el!: HTMLRoadModalElement;

  /**
   * Max width of the modal on desktop
   */
  @Prop() maxWidth: number = 696;

  /**
   * Set isOpen propertie to true to show the modal
   */
  @Prop({ mutable: true }) isOpen: boolean = false;

  /**
   * inverse header colors
   */
  @Prop() hasInverseHeader: boolean = false;

  /**
   * Title of the modal in the header bar
   */
  @Prop() modalTitle?: string;

  /**
   * Show / hide close icon
   */
  @Prop() hasCloseIcon: boolean = true;

  /**
   * Indicate when closing the modal
   */
  @Event({ eventName: 'close' }) onClose!: EventEmitter<void>;

  /**
   * Open the modal
   */
  @Method()
  async open() {
    this.isOpen = true;
  }

  /**
   * Close the modal
   */
  @Method()
  async close() {
    this.isOpen = false;
    this.el.addEventListener('transitionend', () => {
      this.onClose.emit();
    }, { once: true});
  }

  /**
   * Close the dialog when clicking on the cross or layer
   */
  private onClick = (ev: UIEvent) => {
    ev.stopPropagation();
    ev.preventDefault();

    this.close();
  };

  /**
   * Close the dialog when press Escape key
   */
  @Listen('keyup', { target: 'document' })
  onEscape(event: KeyboardEvent) {
    if (event.key === 'Escape' || event.key === "Esc") {
      this.close();
    }
  }

  /**
   * Call close function when clicking an element with data-dismiss="modal" attribute
   */
  componentDidLoad() {
    this.el.querySelectorAll('[data-dismiss="modal"]').forEach(item => {
      item.addEventListener('click', () => this.close());
    });
  }

  render() {
    const modalIsOpenClass = this.isOpen ? 'modal-open' : '';
    const inverseHeaderClass = this.hasInverseHeader ? 'modal-header-inverse' : '';
    const closeIconElement = this.hasCloseIcon ? <button type="button" class="modal-close" onClick={this.onClick} aria-label="Close"><road-icon icon={navigationClose} aria-hidden="true"></road-icon></button> : '';

    return (
      <Host class={`modal ${modalIsOpenClass}`} tabindex="-1" role="dialog" aria-label="modal">
        <div class="modal-overlay" onClick={this.onClick} tabindex="-1"></div>
        <div class="modal-dialog" style={{ maxWidth: `${this.maxWidth}px` }} role="document" tabindex="0">
          <div class="modal-content">
            <header class={`modal-header ${inverseHeaderClass}`}>
              <h2 class="modal-title">{this.modalTitle}</h2>
              {closeIconElement}
            </header>
            <div class="modal-body">
              <slot/>
            </div>
          </div>
        </div>
      </Host>
    );
  }
}