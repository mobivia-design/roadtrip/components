# road-dialog



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute        | Description                                                              | Type                                                        | Default     |
| -------------- | ---------------- | ------------------------------------------------------------------------ | ----------------------------------------------------------- | ----------- |
| `color`        | `color`          | Set the color of information dialog. e.g. info, success, warning, danger | `"danger" \| "info" \| "success" \| "warning" \| undefined` | `undefined` |
| `description`  | `description`    | Content description of the dialog                                        | `string \| undefined`                                       | `undefined` |
| `hasCloseIcon` | `has-close-icon` | Show / hide the close icon                                               | `boolean`                                                   | `true`      |
| `icon`         | `icon`           | override default icon                                                    | `string \| undefined`                                       | `undefined` |
| `isOpen`       | `is-open`        | Set isOpen property to true to open the dialog                           | `boolean`                                                   | `false`     |
| `label`        | `label`          | Text to the top                                                          | `string \| undefined`                                       | `undefined` |


## Events

| Event   | Description                      | Type                |
| ------- | -------------------------------- | ------------------- |
| `close` | Indicate when closing the dialog | `CustomEvent<void>` |


## Methods

### `close() => Promise<void>`

Close the dialog

#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`

Open the dialog

#### Returns

Type: `Promise<void>`




## Slots

| Slot | Description                                                                    |
| ---- | ------------------------------------------------------------------------------ |
|      | Content of the footer dialog if it's an action dialog add action buttons here. |


## Shadow Parts

| Part            | Description             |
| --------------- | ----------------------- |
| `"dialog-icon"` | main icon of the dialog |


## CSS Custom Properties

| Name        | Description                |
| ----------- | -------------------------- |
| `--z-index` | The z-index of the Dialog. |


## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-dialog --> road-icon
  style road-dialog fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
