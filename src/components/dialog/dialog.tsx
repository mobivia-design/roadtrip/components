import { Component, Element, Event, EventEmitter, Listen, Method, Prop, h, Host } from '@stencil/core';
import { FeedbackColors } from '../../interface';
import { navigationClose, alertDangerOutline, alertInfoOutline, alertSuccessOutline, alertWarningOutline } from '../../../icons';

/**
 * @slot  - Content of the footer dialog if it's an action dialog add action buttons here.
 *
 * @part dialog-icon - main icon of the dialog
 */

@Component({
  tag: 'road-dialog',
  styleUrl: 'dialog.css',
  shadow: true,
})
export class Dialog {

  /**
   * Current reference of the dialog
   */
  @Element() el!: HTMLRoadDialogElement;

  /**
   * Set isOpen property to true to open the dialog
   */
  @Prop({ mutable: true }) isOpen: boolean = false;

  /**
   * Show / hide the close icon
   */
  @Prop() hasCloseIcon: boolean = true;

  /**
   * Set the color of information dialog. e.g. info, success, warning, danger
   */
  @Prop() color?: FeedbackColors;

  /**
   * override default icon
   */
   @Prop() icon?: string;

  /**
   * Text to the top
   */
  @Prop() label?: string;

  /**
   * Content description of the dialog
   */
  @Prop() description?: string;

  /**
   * Indicate when closing the dialog
   */
  @Event({ eventName: 'close' }) onClose!: EventEmitter<void>;

  /**
   * Open the dialog
   */
  @Method()
  async open() {
    this.isOpen = true;
  }

  /**
   * Close the dialog
   */
  @Method()
  async close() {
    this.isOpen = false;
    this.el.addEventListener('transitionend', () => {
      this.onClose.emit();
    }, { once: true});
  }

  /**
   * Close the dialog when clicking on the cross or layer
   */
  private onClick = (ev: UIEvent) => {
    ev.stopPropagation();
    ev.preventDefault();

    this.close();
  };

  /**
   * Close the dialog when press Escape key
   */
  @Listen('keyup', { target: 'document' })
  onEscape(event: KeyboardEvent) {
    if ((event.key === 'Escape' || event.key === "Esc") && this.isOpen && this.hasCloseIcon) {
      this.close();
    }
  }

  /**
   * Call close function when clicking an element with data-dismiss="modal" attribute
   */
  componentDidLoad() {
    this.el.querySelectorAll('[data-dismiss="modal"]').forEach(item => {
      item.addEventListener('click', () => this.close());
    });
  }

  render() {
    const modalIsOpenClass = this.isOpen ? 'dialog-open' : '';
    const modalhasClose = this.hasCloseIcon ? 'dialog-has-close' : '';

    let icon;

    if(this.icon) {
      icon = this.icon;
    } else {
      switch(this.color) {
      case 'info':
        icon = alertInfoOutline;
        break;
      case 'success':
        icon = alertSuccessOutline;
        break;
      case 'warning':
        icon = alertWarningOutline;
        break;
      case 'danger':
        icon = alertDangerOutline;
        break;
      default:
        icon = alertInfoOutline;
        break;
      }
    }

    return (
      <Host class={`dialog ${modalIsOpenClass} ${modalhasClose}`} role="alertdialog" aria-modal="true" tabindex="-1" aria-label="dialogLabel">
        <div class="dialog-overlay" onClick={this.hasCloseIcon === true ? this.onClick : undefined} tabindex="-1"></div>
        <div class="dialog-modal" role="document" tabindex="0">
          <div class="dialog-content">
            <header class="dialog-header">
              {this.hasCloseIcon
                ? <button type="button" class="dialog-close" onClick={this.onClick} aria-label="Close"><road-icon icon={navigationClose} aria-hidden="true"></road-icon></button>
                : ''}
            </header>
            <div class="dialog-body">
              {this.color !== undefined
                ? <road-icon class="dialog-icon" part="dialog-icon" color={this.color} icon={icon} aria-hidden="true"></road-icon>
                : ''}
              <h2 class="dialog-title">{this.label}</h2>
              <p class="dialog-description" id="dialogDesc">{this.description}</p>
            </div>
            <footer class="dialog-footer">
              <slot/>
            </footer>
          </div>
        </div>
      </Host>
    );
  }
}