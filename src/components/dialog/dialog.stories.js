import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';
import { list } from '../../../.storybook/list';

export default {
  title: 'Modals/Dialog',
  component: 'road-dialog',
  parameters: {
    layout: 'fullscreen',
    backgrounds: {
      default: 'grey',
    },
    actions: {
      handles: ['close'],
    },
  },
  argTypes: {
    'is-open': {
      control: 'boolean',
      description: 'Defines whether the dialog is open or closed.',
    },
    'has-close-icon': {
      control: 'boolean',
      description: 'Displays a close icon in the dialog header.',
    },
    color: {
      options: ['info', 'success', 'warning', 'danger'],
      control: { type: 'radio' },
      description: 'Sets the color theme of the dialog.',
    },
    icon: {
      options: list,
      control: { type: 'select' },
      description: 'Defines the icon displayed in the dialog.',
    },
    label: {
      control: 'text',
      description: 'Sets the dialog title.',
    },
    description: {
      control: 'text',
      description: 'Defines the main text content of the dialog.',
    },
    ' ': {
      control: 'text',
      description: 'Slot for additional content (buttons, links, etc.).',
    },
    '--z-index': {
      table: { defaultValue: { summary: '3' } },
      control: 'text',
      description: 'Defines the z-index of the dialog.',
    },
  },
  args: {
    'is-open': true,
    'has-close-icon': true,
    color: 'info',
    label: 'Dialog title',
    description: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.',
    ' ': `<div class="road-button-group">
      <road-button class="mb-0" color="primary" expand>Done</road-button>
    </div>`,
  },
};

const Template = (args) => html`
  <road-dialog 
    ?is-open="${args['is-open']}" 
    ?has-close-icon="${args['has-close-icon']}" 
    color="${ifDefined(args.color)}" 
    .icon="${ifDefined(args.icon)}" 
    label="${ifDefined(args.label)}" 
    description="${ifDefined(args.description)}"
    style="--z-index: ${ifDefined(args['--z-index'])};"
  >
    ${unsafeHTML(args[' '])}
  </road-dialog>
`;

export const Playground = Template.bind({});

export const SideActions = Template.bind({});
SideActions.args = {
  ' ': `<div class="road-button-group">
    <road-button class="mb-0" outline expand data-dismiss="modal">Cancel</road-button>
    <road-button class="mb-0" color="primary" expand style="margin-left: 1rem;">Done</road-button>
  </div>`,
};

export const Info = Template.bind({});
Info.args = {
  color: 'info',
  label: 'Information',
};

export const Success = Template.bind({});
Success.args = {
  color: 'success',
  label: 'Success',
};

export const Warning = Template.bind({});
Warning.args = {
  color: 'warning',
  label: 'Warning',
};

export const Danger = Template.bind({});
Danger.args = {
  color: 'danger',
  label: 'Danger',
};
