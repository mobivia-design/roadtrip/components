# road-duration



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                                                           | Type      | Default      |
| -------- | --------- | ----------------------------------------------------------------------------------------------------- | --------- | ------------ |
| `header` | `header`  | title of the widget                                                                                   | `string`  | `'duration'` |
| `isOpen` | `is-open` | Set isOpen property to true to open the duration widget                                               | `boolean` | `false`      |
| `max`    | `max`     | The maximum value, which must not be less than its minimum (min attribute) value in minutes.          | `number`  | `300`        |
| `min`    | `min`     | The minimum value, which must not be greater than its maximum (max attribute) value in minutes.       | `number`  | `15`         |
| `step`   | `step`    | Works with the min and max attributes to limit the increments at which a value can be set in minutes. | `number`  | `15`         |


## Events

| Event          | Description                                       | Type                                                                  |
| -------------- | ------------------------------------------------- | --------------------------------------------------------------------- |
| `roadselected` | Emitt the value and label of the selected option. | `CustomEvent<{ value: string \| null \| undefined; label: string; }>` |


## Methods

### `close() => Promise<void>`

Close the widget

#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`

Open the widget

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [road-drawer](../drawer)
- [road-grid](../grid)
- [road-row](../row)
- [road-col](../col)
- [road-card](../card)
- [road-label](../label)

### Graph
```mermaid
graph TD;
  road-duration --> road-drawer
  road-duration --> road-grid
  road-duration --> road-row
  road-duration --> road-col
  road-duration --> road-card
  road-duration --> road-label
  road-drawer --> road-icon
  style road-duration fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
