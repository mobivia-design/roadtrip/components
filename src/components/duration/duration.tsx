import { Component, Element, Event, EventEmitter, h, Listen, Method, Prop } from '@stencil/core';

@Component({
  tag: 'road-duration',
  styleUrl: 'duration.css',
  shadow: true,
})
export class Duration {

  /**
   * Current reference of the duration element
   */
   @Element() el!: HTMLRoadDurationElement;

  /**
   * Set isOpen property to true to open the duration widget
   */
  @Prop({ mutable: true }) isOpen: boolean = false;

  /**
   * title of the widget
   */
  @Prop() header: string = 'duration';

  /**
   * The minimum value, which must not be greater than its maximum (max attribute) value in minutes.
   */
  @Prop() min: number = 15;

  /**
   * The maximum value, which must not be less than its minimum (min attribute) value in minutes.
   */
  @Prop() max: number = 300;

  /**
   * Works with the min and max attributes to limit the increments at which a value can be set in minutes.
   */
  @Prop() step: number = 15;

  @Listen('roadcardclick')
  handleClick(event: CustomEvent) {
    this.roadselected.emit({
      value: event.detail.value.toString(),
      label: event.detail.label,
    });
    this.close();
  }

  /**
   * Emitt the value and label of the selected option.
   */
  @Event() roadselected!: EventEmitter<{
    value: string | undefined | null,
    label: string
  }>;

  /**
   * Open the widget
   */
  @Method()
  async open() {
    this.isOpen = true;
  }

  /**
   * Close the widget
   */
  @Method()
  async close() {
    this.isOpen = false;
  }

  render() {
    return (
      <road-drawer is-open={this.isOpen} position="bottom" drawer-title={this.header}>
        <road-grid>
          <road-row class="duration-values">
            {Array(Math.floor((this.max - this.min) / this.step) + 1).fill(this.min).map(((item, index) => (item + index * this.step))).map(item =>
              <road-col class="col-3 col-md-2">
                <road-card button value={item}>
                  <road-label>
                    {Math.floor(item / 60).toLocaleString("en-GB", {minimumIntegerDigits: 2, useGrouping: false})}:{(item % 60).toLocaleString("en-GB", {minimumIntegerDigits: 2, useGrouping: false})}
                  </road-label>
                </road-card>
              </road-col>
            )}
          </road-row>
        </road-grid>
      </road-drawer>
    );
  }

}
