import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Forms/Duration',
  component: 'road-duration',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
    layout: 'fullscreen',
    actions: {
      handles: ['open', 'close', 'roadselected'],
    },
  },
  argTypes: {
    'is-open': {
      control: 'boolean',
      description: 'Defines whether the duration picker is open or closed.',
    },
    header: {
      control: 'text',
      description: 'Title displayed in the duration picker.',
    },
    min: {
      control: 'number',
      description: 'Minimum selectable duration in minutes.',
    },
    max: {
      control: 'number',
      description: 'Maximum selectable duration in minutes.',
    },
    step: {
      control: 'number',
      min: 15,
      description: 'Step interval for selecting the duration (in minutes).',
      table: {
        defaultValue: { summary: 15 },
      },
    },
    roadselected: {
      action: 'roadselected',
      description: 'Triggered when a duration is selected.',
      table: {
        category: 'Events',
        type: { summary: 'CustomEvent<{ value: number, label: string }>' },
      },
    },
    open: {
      description: 'Opens the duration picker.',
      table: {
        category: 'Methods',
        type: { summary: 'void' },
      },
    },
    close: {
      description: 'Closes the duration picker.',
      table: {
        category: 'Methods',
        type: { summary: 'void' },
      },
    },
  },
  args: {
    'is-open': true,
    header: 'Select a duration',
    min: 15,
    max: 240,
    step: 15,
  },
};

const Template = (args) => html`
  <road-duration 
    ?is-open="${args['is-open']}"
    header="${ifDefined(args.header)}"
    min="${ifDefined(args.min)}"
    max="${ifDefined(args.max)}"
    step="${ifDefined(args.step)}"
    @roadselected=${event => args.roadselected(event.detail)}
  ></road-duration>
`;

export const Playground = Template.bind({});

export const Example = () => html`
  <road-label class="mb-8">Duration of the service</road-label>
  <div class="d-flex align-items-center">
    <road-input 
      class="input"
      aria-label="Duration of the service" 
      style="width: 100px" 
      placeholder="00:15"
      @roadfocus=${() => document.querySelector('.duration').open()}
    ></road-input>
    <div class="text-content ml-8">hour(s)</div>
  </div>

  <road-duration 
    class="duration"
    header="Duration of the service"
    @roadselected=${event => document.querySelector('.input').value = event.detail.label}
  ></road-duration>
`;
Example.parameters = {
  docs: {
    source: {
      type: 'code',
    },
  },
};
