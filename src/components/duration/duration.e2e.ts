import { newE2EPage } from '@stencil/core/testing';

describe('road-duration', () => {
  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
      <road-duration></road-duration>
      `,
    });
  });

  it('should render closed by default', async () => {
    const open = await page.spyOnEvent('open');
    const close = await page.spyOnEvent('close');

    const element = await page.find('road-duration');

    const isOpen = await element.getProperty('isOpen');
    expect(isOpen).toBeFalsy();

    await element.setProperty('isOpen', true);

    await page.waitForChanges();

    const isOpened = await element.getProperty('isOpen');
    expect(isOpened).toBeTruthy();

    expect(open).toHaveReceivedEvent();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();

    await element.callMethod('close');

    await page.waitForChanges();

    const isStillOpen = await element.getProperty('isOpen');
    expect(isStillOpen).toBeFalsy();

    await page.waitForTimeout(500);

    expect(close).toHaveReceivedEvent();
  });

  it('should send event when a value is clicked', async () => {
    const roadselected = await page.spyOnEvent('roadselected');

    const element = await page.find('road-duration');

    await element.setProperty('isOpen', true);

    await page.waitForChanges();


    await page.waitForChanges();

    const collapseBtn = await page.evaluateHandle(`document.querySelector('road-duration').shadowRoot.querySelector('road-card')`);
    await collapseBtn.click();

    await page.waitForTimeout(500);

    expect(roadselected).toHaveReceivedEventDetail({
      value: '15',
      label: '00:15'
    });
  });
});
