# road-badge



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                               | Type                                                                                                             | Default       |
| -------- | --------- | --------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- | ------------- |
| `bubble` | `bubble`  | if `true` the badge will be displayed has a little bubble | `boolean \| undefined`                                                                                           | `false`       |
| `color`  | `color`   | Color of the badge                                        | `"accent" \| "danger" \| "default" \| "info" \| "primary" \| "secondary" \| "success" \| "warning" \| undefined` | `'secondary'` |


## Slots

| Slot | Description                                           |
| ---- | ----------------------------------------------------- |
|      | Content of the badge, it could be a number or a text. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
