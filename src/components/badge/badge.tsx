import { Component, Host, h, Prop } from '@stencil/core';

import { Color } from '../../interface';

/**
 * @slot - Content of the badge, it could be a number or a text.
 */

@Component({
  tag: 'road-badge',
  styleUrl: 'badge.css',
  shadow: true,
})
export class Badge {

  /**
   * Color of the badge
   */
  @Prop() color?: Color = 'secondary';

  /**
   * if `true` the badge will be displayed has a little bubble
   */
  @Prop() bubble?: boolean = false;

  render() {
    const colorClass = this.color !== undefined ? `badge-${this.color}` : '';
    const bubbleClass = this.bubble ? 'badge-bubble' : '';

    return (
      <Host class={`${colorClass} ${bubbleClass}`}>
        <slot/>
      </Host>
    );
  }

}
