import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Indicators/Badge',
  component: 'road-badge',
  argTypes: {
    color: {
      description: "Color of the badge",
      options: ['primary', 'secondary', 'accent', 'info', 'success', 'warning', 'danger', 'inverse'],
      control: {
        type: 'select',
      },
      table: {
        defaultValue: { summary: 'secondary' },
      },
    },
    bubble: {
      description: "if `true` the badge will be displayed has a little bubble",
      control: 'boolean',
    },
    ' ': {
      description: "Content of the badge, it could be a number or a text.",
      control: 'text',
    },
  },
  args: {
    color: 'accent',
    bubble: null,
    ' ': `1`,
  },
};

const Template = (args) => html`
<a class="text-content d-inline-flex">
  <road-icon name="alert-notification-outline" role="button"></road-icon>
  <sup style="margin-left: -1.125rem;margin-top: 0.25rem;">
    <road-badge color="${ifDefined(args.color)}" bubble="${ifDefined(args.bubble)}">${unsafeHTML(args[' '])}</road-badge>
  </sup>
</a>
`;

export const Playground = Template.bind({});

export const Notification = Template.bind({});
Notification.args = {
  ' ': `10`,
};

export const Status = (args) => html`
<a class="text-content d-inline-flex">
  <road-icon name="people-outline" role="button"></road-icon>
  <sup style="margin-left: -0.25rem;margin-top: 0.375rem;">
    <road-badge color="${ifDefined(args.color)}" bubble="${ifDefined(args.bubble)}">${unsafeHTML(args[' '])}</road-badge>
  </sup>
</a>
`;
Status.args = {
  bubble: true,
  ' ': ``,
};
