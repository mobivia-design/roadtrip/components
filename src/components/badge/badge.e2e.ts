import { newE2EPage } from '@stencil/core/testing';

describe('should render a badge', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<road-badge></road-badge>');

    const element = await page.find('road-badge');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});

describe('should render a badge with content', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<road-badge>1</road-badge>');

    const element = await page.find('road-badge');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
