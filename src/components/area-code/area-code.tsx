import { Component, Event, EventEmitter, Host, Prop, Watch, h, State } from '@stencil/core';

@Component({
  tag: 'road-area-code',
  styleUrl: 'area-code.css',  // Mettez à jour le nom du fichier de style si nécessaire
  scoped: true,
})
export class RoadAreaCode {

  /**
   * The id of select
   */

  @Prop() selectId: string = `road-select-${selectIds++}`;
  @State() selectedValue: string = '';

/**
   * List of options of the area code
   */
  @Prop() options: Array<{
    value: string;
    label: string;
    selected?: boolean;
  }> = [];

  /**
   * This Boolean attribute lets you specify that a form control should have input focus when the page loads.
   */

  @Prop() autofocus: boolean = false;

  /**
   * If `true`, the user cannot interact with the select.
   */

  @Prop() disabled: boolean = false;

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.selectId;

  /**
 * If `true`, the user must fill in a value before submitting a form.
 */
  @Prop() required: boolean = false;

  /**
 * The size of the areacode
 */
  @Prop() size: number = 0;

  /**
 * The size of the areacode
 */
  @Prop() sizes: 'lg' | 'xl' = 'lg';

  /**
 * The label of the areacode
 */
  @Prop() label: string = 'Country';

  /**
   * Error message for the field
   */
  @Prop() error?: string;

  /**
   * the value of the select.
   */
  @Prop({ mutable: true }) value: string = '';

  /**
   * Emitted when the value has changed.
   */
  @Event() roadchange!: EventEmitter<{
    value: string | undefined | null
  }>;
  /** @internal */
  @Event() roadChange!: EventEmitter<{
    value: string | undefined | null
  }>;

  /**
* Emitted when the select has focus.
*/

  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  /**
* Emitted when the select loses focus.
*/

  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;

  @Watch('value')
  valueChanged() {
    this.roadchange.emit({
      value: this.value,
    });
    this.roadChange.emit({
      value: this.value,
    });
    this.selectedValue = this.getMatchedOptionValue();
  }

  /**
   * Trigger number.
   */
  @Prop({ mutable: true }) triggerRender: number = 0;

  componentWillLoad() {
    if (!this.value && this.options.length > 0) {
      this.selectedValue = this.options[0].value;
    } else {
      this.selectedValue = this.getMatchedOptionValue();
    }
  }

  getMatchedOptionValue() {
    return this.options.find(option => option.value === this.value)?.value ?? '';
  }

  private onChange = (ev: Event) => {
    const select = ev.target as HTMLSelectElement | null;
    if (select) {
      this.value = select.value || '';
      this.selectedValue = this.getMatchedOptionValue();
    }
  };

  private onBlur = () => {
    this.roadblur.emit();
    this.roadBlur.emit();
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  render() {
    const labelId = this.selectId + '-label';
    const valueId = this.selectId + '-value';

    const isInvalidClass = this.error !== undefined && this.error !== '' ? 'is-invalid' : '';

    return (
      <Host class={this.sizes && `select-${this.sizes}`}>
        <select
          class={`form-select-area has-value ${isInvalidClass}`}  // Appliquez la classe has-value par défaut ici
          id={this.selectId}
          aria-disabled={this.disabled ? 'true' : null}
          autoFocus={this.autofocus}
          disabled={this.disabled}
          name={this.name}
          required={this.required}
          size={this.size}
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        >
          {this.options && this.options.map(option => (
            <option value={option.value} selected={option.selected}>{option.label}</option>
          ))}
        </select>
        <label class="form-select-area-label" id={labelId} htmlFor={this.selectId}>{this.label}</label>
        <label class="form-select-area-value" id={valueId} htmlFor={this.selectId}>+{this.selectedValue}</label>
        {this.error && this.error !== '' && <p class="invalid-feedback">{this.error}</p>}
      </Host>
    );
  }
}

let selectIds = 0;
