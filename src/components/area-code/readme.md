# road-area-code



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description                                                                                              | Type                                                                   | Default                            |
| --------------- | ---------------- | -------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------- | ---------------------------------- |
| `autofocus`     | `autofocus`      | This Boolean attribute lets you specify that a form control should have input focus when the page loads. | `boolean`                                                              | `false`                            |
| `disabled`      | `disabled`       | If `true`, the user cannot interact with the select.                                                     | `boolean`                                                              | `false`                            |
| `error`         | `error`          | Error message for the field                                                                              | `string \| undefined`                                                  | `undefined`                        |
| `label`         | `label`          | The label of the areacode                                                                                | `string`                                                               | `'Country'`                        |
| `name`          | `name`           | The name of the control, which is submitted with the form data.                                          | `string`                                                               | `this.selectId`                    |
| `options`       | --               | List of options of the area code                                                                         | `{ value: string; label: string; selected?: boolean \| undefined; }[]` | `[]`                               |
| `required`      | `required`       | If `true`, the user must fill in a value before submitting a form.                                       | `boolean`                                                              | `false`                            |
| `selectId`      | `select-id`      | The id of select                                                                                         | `string`                                                               | `` `road-select-${selectIds++}` `` |
| `size`          | `size`           | The size of the areacode                                                                                 | `number`                                                               | `0`                                |
| `sizes`         | `sizes`          | The size of the areacode                                                                                 | `"lg" \| "xl"`                                                         | `'lg'`                             |
| `triggerRender` | `trigger-render` | Trigger number.                                                                                          | `number`                                                               | `0`                                |
| `value`         | `value`          | the value of the select.                                                                                 | `string`                                                               | `''`                               |


## Events

| Event        | Description                          | Type                                                   |
| ------------ | ------------------------------------ | ------------------------------------------------------ |
| `roadblur`   | Emitted when the select loses focus. | `CustomEvent<void>`                                    |
| `roadchange` | Emitted when the value has changed.  | `CustomEvent<{ value: string \| null \| undefined; }>` |
| `roadfocus`  | Emitted when the select has focus.   | `CustomEvent<void>`                                    |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
