import { Component, Element, Event, EventEmitter, Host, Prop, State, h, Listen } from '@stencil/core';
import './../../utils/polyfill';

@Component({
  tag: 'road-radio',
  styleUrl: 'radio.css',
  scoped: true,
})
export class Radio {

  private radioGroup: HTMLRoadRadioGroupElement | null = null;

  @Element() el!: HTMLRoadRadioElement;

  /**
   * The id of radio
   */
  @Prop() radioId: string = `road-radio-${radioIds++}`;

  /**
   * If `true`, the radio is selected.
   */
  @State() checked: boolean = false;

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.radioId;

  /**
   * If `true`, the user must fill in a value before submitting a form.
   */
  @Prop() required: boolean = false;

  /**
   * If `true`, the user cannot interact with the radio.
   */
  @Prop() disabled: boolean = false;

  /**
   * Value the form will get
   */
  @Prop() value?: any | null;

  /**
   * Label for the field
   */
  @Prop() label: string = `${this.radioId}-label`;

  /**
  * Secondary Label for the field
  */
  @Prop() secondaryLabel?: string;

  /**
   * If `true`, the label and the radio are inverse and spaced
   */
  @Prop() inverse: boolean = false;

  /**
   * Error message for the field
   */
  @Prop() error?: boolean;

  /**
   * Helper message for the field
   */
  @Prop() helper?: string;

  /**
   * Inline multiple radio
   */
  @Prop() inline: boolean = true;

  /**
   * Emitted when the radio button has focus.
   */
  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  /**
   * Emitted when the radio button loses focus.
   */
  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;

  connectedCallback() {
    if (this.value === undefined) {
      this.value = this.radioId;
    }
    const radioGroup = this.radioGroup = this.el.closest('road-radio-group');
    if (radioGroup) {
      this.updateState();
    }
  }

  disconnectedCallback() {
    const radioGroup = this.radioGroup;
    if (radioGroup) {
      this.radioGroup = null;
    }
  }

  @Listen('roadChange', {target: 'window'})
  @Listen('roadchange', {target: 'window'})
  onRoadChangedChanged() {
    this.updateState();
  }

  private updateState = () => {
    if (this.radioGroup) {
      this.checked = this.radioGroup.value === this.value;
    }
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  private onBlur = () => {
    this.roadblur.emit();
    this.roadBlur.emit();
  };

  render() {
    const labelId = this.radioId + '-label';
    const inverseClass = this.inverse && 'form-radio-inverse';
    const isInvalidClass = this.error ? 'is-invalid' : '';
    const inlineClass = this.inline ? 'form-radio-inline' : '';

    return (
      <Host class={`form-radio ${inlineClass}`}>
        <input
          class={`form-radio-input ${isInvalidClass}`}
          type="radio"
          id={this.radioId}
          name={this.name}
          required={this.required}
          disabled={this.disabled}
          aria-disabled={this.disabled ? 'true' : null}
          checked={this.checked}
          aria-checked={`${this.checked}`}
          aria-labelledby={labelId}
          value={this.value}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
        <label class={`form-radio-label ${inverseClass}`} id={labelId} htmlFor={this.radioId}>{this.label} <span class="form-radio-label-span">{this.secondaryLabel}</span></label>
      </Host>
    );
  }
}

let radioIds = 0;
