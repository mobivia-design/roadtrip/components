import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-radio', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
      <road-radio-group name="Labels" label="Labels">
        <road-radio name="Labels" label="Label1" value="1"></road-radio>
        <road-radio name="Labels" label="Label2" value="2"></road-radio>
        <road-radio name="Labels" label="Label3" value="3"></road-radio>
      </road-radio-group>
      `,
    });
  });

  it('should render', async () => {
    const roadchange = await page.spyOnEvent('roadchange');
    const roadblur = await page.spyOnEvent('roadblur');
    const roadfocus = await page.spyOnEvent('roadfocus');

    const element = await page.find('road-radio-group');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();

    await page.waitForChanges();

    await page.focus('road-radio input');
    await page.click('road-radio input');
    await page.$eval('road-radio input', e => e.blur());

    await page.waitForChanges();

    const isChecked = await element.getProperty('value');
    expect(isChecked).toEqual('1');

    expect(roadfocus).toHaveReceivedEvent();
    expect(roadblur).toHaveReceivedEvent();

    expect(roadchange).toHaveReceivedEventDetail({
      value: '1',
    });
  });

  it('should render checked', async () => {
    const element = await page.find('road-radio-group');
    expect(element).toHaveClass('hydrated');

    element.setProperty('value', '1');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render disabled', async () => {
    const element = await page.find('road-radio-group');
    expect(element).toHaveClass('hydrated');

    await page.$$eval('road-radio', element => element.disabled = true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with error', async () => {
    const element = await page.find('road-radio-group');
    expect(element).toHaveClass('hydrated');

    element.setProperty('error', 'Check the radio to continue');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
