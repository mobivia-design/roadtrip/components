# road-radio

A radio is used in a [radio-group](../radio-group). It allows
a user to select at most one radio button from a set. Checking one radio
button that belongs to a radio group unchecks any previous checked
radio button within the same group.

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute         | Description                                                        | Type                   | Default                          |
| ---------------- | ----------------- | ------------------------------------------------------------------ | ---------------------- | -------------------------------- |
| `disabled`       | `disabled`        | If `true`, the user cannot interact with the radio.                | `boolean`              | `false`                          |
| `error`          | `error`           | Error message for the field                                        | `boolean \| undefined` | `undefined`                      |
| `helper`         | `helper`          | Helper message for the field                                       | `string \| undefined`  | `undefined`                      |
| `inline`         | `inline`          | Inline multiple radio                                              | `boolean`              | `true`                           |
| `inverse`        | `inverse`         | If `true`, the label and the radio are inverse and spaced          | `boolean`              | `false`                          |
| `label`          | `label`           | Label for the field                                                | `string`               | `` `${this.radioId}-label` ``    |
| `name`           | `name`            | The name of the control, which is submitted with the form data.    | `string`               | `this.radioId`                   |
| `radioId`        | `radio-id`        | The id of radio                                                    | `string`               | `` `road-radio-${radioIds++}` `` |
| `required`       | `required`        | If `true`, the user must fill in a value before submitting a form. | `boolean`              | `false`                          |
| `secondaryLabel` | `secondary-label` | Secondary Label for the field                                      | `string \| undefined`  | `undefined`                      |
| `value`          | `value`           | Value the form will get                                            | `any`                  | `undefined`                      |


## Events

| Event       | Description                                | Type                |
| ----------- | ------------------------------------------ | ------------------- |
| `roadblur`  | Emitted when the radio button loses focus. | `CustomEvent<void>` |
| `roadfocus` | Emitted when the radio button has focus.   | `CustomEvent<void>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
