import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Forms/Radio',
  component: 'road-radio',
  subcomponents: {
    'road-radio-group': 'road-radio-group',
  },
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus'],
    },
    docs: {
      description: {
        component: 'A radio is used in a `radio-group`. It allows a user to select at most one radio button from a set. Checking one radio button that belongs to a radio group unchecks any previous checked radio button within the same group.',
      },
    },
  },
};

export const Playground = (args) => html`
<road-radio-group 
  name="${ifDefined(args.name)}"
  label="Label of radio group" 
  error="${ifDefined(args.error)}" 
  helper="${ifDefined(args.helper)}"
  @roadchange=${event => args.roadchange(event.detail.value)}
  asterisk="${ifDefined(args.asterisk)}"
>
  <road-radio
    disabled="${ifDefined(args.disabled)}"
    required="${ifDefined(args.required)}"
    label="${ifDefined(args.label)}"
    secondary-label="${ifDefined(args['secondary-label'])}"
    inverse="${ifDefined(args.inverse)}"
    inline="${ifDefined(args.inline)}"
    value="${ifDefined(args.value)}"
    radio-id="${ifDefined(args['radio-id'])}"
  ></road-radio>
</road-radio-group>
`;
Playground.args = {
  disabled: null,
  required: null,
  inverse: null,
  inline: undefined,
  label: 'Label',
  'secondary-label': undefined,
  value: 'on',
  asterisk: null,
};
Playground.argTypes = {
  disabled: {
    description: "If `true`, the user cannot interact with the radio.",
    control: 'boolean',
  },
  required: {
    description: "If `true`, the user must fill in a value before submitting a form.",
    control: 'boolean',
  },
  inverse: {
    description: "If `true`, the label and the radio are inverse and spaced",
    control: 'boolean',
  },
  inline: {
    description: "Inline multiple radio",
    control: 'boolean',
  },
  error: {
    description: "Error message for the field",
    control: 'text',
  },
  helper: {
    description: "Helper message for the field",
    control: 'text',
  },
  label: {
    description: "Label for the field",
    control: 'text',
  },
  'secondary-label': {
    description: "Secondary Label for the field",
    control: 'text',
  },
  asterisk: {
    control: 'boolean',
  },
  name: {
    description: "The name of the control, which is submitted with the form data.",
    control: 'text',
  },
  value: {
    description: "Value the form will get",
    control: 'text',
  },
  'radio-id': {
    description: "The id of radio",
    control: 'text',
  },
  roadblur: {
    description: "Emitted when the radio button loses focus.",
    control: {
      type: null,
    },
  },
  roadfocus: {
    description: "Emitted when the radio button has focus.",
    control: {
      type: null,
    },
  },
  roadchange: {
    action: 'roadchange',
    control: {
      type: null,
    },
  },
};

export const Inline = () => html`
<road-radio-group name="NameOfTheField" value="1">
  <road-radio label="Label1" value="1"></road-radio>
  <road-radio label="Label2" value="2"></road-radio>
  <road-radio label="Label3" value="3"></road-radio>
</road-radio-group>
`;

export const Stacked = () => html`
<road-radio-group name="NameOfTheField">
  <road-radio label="Label1" value="1" inline="false"></road-radio>
  <road-radio label="Label2" value="2" inline="false"></road-radio>
  <road-radio label="Label3" value="3" inline="false"></road-radio>
</road-radio-group>
`;

export const Inverse = () => html`
<road-radio-group name="NameOfTheField">
  <road-radio inverse label="Label1" value="1" inline="false"></road-radio>
  <road-radio inverse label="Label2" value="2" inline="false"></road-radio>
  <road-radio inverse label="Label3" value="3" inline="false"></road-radio>
</road-radio-group>
`;

export const Disabled = () => html`
<road-radio-group name="NameOfTheField">
  <road-radio label="Label1" value="1" disabled></road-radio>
  <road-radio label="Label2" value="2" disabled></road-radio>
  <road-radio label="Label3" value="3" disabled></road-radio>
</road-radio-group>
`;

export const Error = () => html`
<road-radio-group name="NameOfTheField" error="Check the radio to continue">
  <road-radio label="Label1" value="1"></road-radio>
  <road-radio label="Label2" value="2"></road-radio>
  <road-radio label="Label3" value="3"></road-radio>
</road-radio-group>
`;
