import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Expand/Dropdown',
  component: 'road-dropdown',
  argTypes: {
    'is-open': {
      control: 'boolean',
      description: 'Defines whether the dropdown is open or closed.',
    },
    'is-light': {
      control: 'boolean',
      description: 'Applies a light style to the dropdown.',
    },
    'is-medium': {
      control: 'boolean',
      description: 'Applies a medium-sized style to the dropdown.',
    },
    position: {
      options: ['left', 'right'],
      control: { type: 'radio' },
      description: 'Sets the horizontal position of the dropdown.',
      table: {
        defaultValue: { summary: 'left' },
      },
    },
    direction: {
      options: ['top', 'bottom'],
      control: { type: 'radio' },
      description: 'Sets the vertical direction in which the dropdown expands.',
      table: {
        defaultValue: { summary: 'bottom' },
      },
    },
    list: {
      control: 'text',
      description: 'Defines the list of items inside the dropdown.',
    },
    '--margin-top': {
      table: { defaultValue: { summary: '0.5rem' } },
      control: 'text',
      description: 'Sets the margin-top of the dropdown.',
    },
  },
  args: {
    'is-open': true,
    'is-light': false,
    'is-medium': false,
    position: 'left',
    direction: 'bottom',
    list: `<road-list slot="list">
      <road-item button class="border-0">
        <road-icon slot="start" name="picture" size="md" aria-hidden="true"></road-icon>
        <road-label>Label</road-label>
      </road-item>
      <road-item button class="border-0">
        <road-icon slot="start" name="picture" size="md" aria-hidden="true"></road-icon>
        <road-label>Label</road-label>
      </road-item>
      <road-item button class="border-0">
        <road-icon slot="start" name="picture" size="md" aria-hidden="true"></road-icon>
        <road-label>Label</road-label>
      </road-item>
      <road-item button class="border-0">
        <road-icon slot="start" name="picture" size="md" aria-hidden="true"></road-icon>
        <road-label>Label</road-label>
      </road-item>
    </road-list>`,
  },
};

const Template = (args) => html`
  <road-dropdown
    ?is-open="${args['is-open']}"
    ?is-light="${args['is-light']}"
    ?is-medium="${args['is-medium']}"
    position="${ifDefined(args.position)}"
    direction="${ifDefined(args.direction)}"
    style="--margin-top: ${ifDefined(args['--margin-top'])};"
  >
    ${unsafeHTML(args.list)}
  </road-dropdown>
`;

export const Playground = Template.bind({});

export const RightPosition = Template.bind({});
RightPosition.args = {
  position: 'right',
};

export const TopDirection = Template.bind({});
TopDirection.args = {
  direction: 'top',
};

export const LightDropdown = Template.bind({});
LightDropdown.args = {
  'is-light': true,
};

export const MediumDropdown = Template.bind({});
MediumDropdown.args = {
  'is-medium': true,
};
