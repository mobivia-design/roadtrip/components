import { newE2EPage } from '@stencil/core/testing';

describe('road-dropdown', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <road-dropdown is-open="true">
        <div class="d-flex align-items-center justify-content-end pt-16 pb-16">
          <road-button color="default" size="sm" class="mb-0">
            <road-icon name="navigation-more"></road-icon>
          </road-button>
        </div>
        <road-list slot="list">
          <road-item button>
            <road-icon slot="start" name="picture" size="md"></road-icon>
            <road-label>
              Label
            </road-label>
          </road-item>
          <road-item button>
            <road-icon slot="start" name="picture" size="md"></road-icon>
            <road-label>
            Label
            </road-label>
          </road-item>
          <road-item button>
            <road-icon slot="start" name="picture" size="md"></road-icon>
            <road-label>
            Label
            </road-label>
          </road-item>
          <road-item button>
            <road-icon slot="start" name="picture" size="md"></road-icon>
            <road-label>
              Label
            </road-label>
          </road-item>
        </road-list>
      <road-dropdown>
    `);

    const element = await page.find('road-dropdown');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
