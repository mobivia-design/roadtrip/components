# road-dropdown



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description | Type                             | Default     |
| ----------- | ----------- | ----------- | -------------------------------- | ----------- |
| `direction` | `direction` |             | `"bottom" \| "top" \| undefined` | `'bottom'`  |
| `isLight`   | `is-light`  |             | `boolean`                        | `false`     |
| `isMedium`  | `is-medium` |             | `boolean`                        | `false`     |
| `isOpen`    | `is-open`   |             | `boolean`                        | `false`     |
| `position`  | `position`  |             | `"left" \| "right" \| undefined` | `undefined` |


## CSS Custom Properties

| Name           | Description            |
| -------------- | ---------------------- |
| `--margin-top` | Top margin of the menu |


## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-dropdown --> road-icon
  style road-dropdown fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
