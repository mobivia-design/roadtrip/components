import { Component, h, Element, Prop, Listen } from '@stencil/core';

@Component({
  tag: 'road-dropdown',
  styleUrl: 'dropdown.css',
  shadow: true,
})
export class Dropdown {

  @Element() el!: HTMLRoadDropdownElement;


  @Prop({ mutable: true }) isOpen: boolean = false;
  @Prop() isLight: boolean = false;
  @Prop() isMedium: boolean = false;
  @Prop({ reflect: true }) position?: 'left' | 'right';
  @Prop({ reflect: true }) direction?: 'top' | 'bottom' = 'bottom';

  private onClick = () => {
    this.isOpen = !this.isOpen;
  };

  @Listen('click', { target: 'document' })
  handleDocumentClick(ev: MouseEvent) {
    // Check if the clicked element is the dropdown button
    if ((ev.target as HTMLElement).closest('road-dropdown') === this.el) {
      return; // Do nothing if clicked element is the dropdown button
    }
    // Close dropdown if click is outside the dropdown
    this.isOpen = false;
  }

  render() {
    const isLightButtonDropDown = this.isLight ? 'dropdown-button' : 'dropdown-button';
    const isMediumButtonDropDown = this.isMedium ? 'dropdown-medium-button dropdown-button' : 'dropdown-button';
    const positionClass = this.position ? `position-${this.position}` : '';
    const dropdownClass = this.position ? `dropdown-${this.position}` : '';
    const directionClass = this.direction ? `direction-${this.direction}` : '';

    return (
      <details class="dropdown" open={this.isOpen}>
        <summary aria-expanded={this.isOpen.toString()} tabindex="0" role="button" onClick={this.onClick} tab-index="0">
          <div class={`d-flex ${positionClass}`}>
            <div class={`${isLightButtonDropDown} ${isMediumButtonDropDown}`}>
              <road-icon name="navigation-more" class="dropdown-button-icon" aria-hidden="true"></road-icon>
            </div>
          </div>
        </summary>
        <div class={`dropdown-menu ${directionClass} ${dropdownClass}`}>
          <slot name="list" />
        </div>
      </details>
    );
  }
}
