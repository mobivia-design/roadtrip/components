import { newE2EPage } from '@stencil/core/testing';

describe('road-accordion', () => {
  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-accordion>
          <div slot="header">Accordion link</div>
          Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
        </road-accordion>
      `,
    });
  });

  it('should render closed by default', async () => {
    const element = await page.find('road-accordion');
    expect(element).toHaveClass('hydrated');

    const isOpen = await element.getProperty('is-open');
    expect(isOpen).toBeFalsy();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render opened', async () => {
    const element = await page.find('road-accordion');
    element.setProperty('isOpen', true);

    await page.waitForChanges();

    const isOpen = await element.getProperty('isOpen');
    expect(isOpen).toBeTruthy();
  });

  it('should open when clicking the header', async () => {
    const element = await page.find('road-accordion');

    const header = await page.evaluateHandle(`document.querySelector('road-accordion').shadowRoot.querySelector('.accordion-trigger')`);
    await header.click();

    await page.waitForChanges();

    const isOpen = await element.getProperty('isOpen');
    expect(isOpen).toBeTruthy();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});