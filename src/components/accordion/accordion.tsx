import { Component, Prop, h } from '@stencil/core';
import { navigationChevron } from '../../../icons';
import './../../utils/polyfill';

/**
 * @slot header - Content of the header.
 * @slot - Content hidden in the accordion.
 * @slot icon-left - Icon of the alert, it should be a road-icon element.
 * `<road-icon name="alert-info-outline" class="mr-16"></road-icon>`

 */

@Component({
  tag: 'road-accordion',
  styleUrl: 'accordion.css',
  shadow: true,
})
export class Accordion {

  /**
   * Set to `true` to open the accordion and to `false` to close it.
   */
  @Prop({ mutable: true }) isOpen : boolean = false;

  /**
   * Set to `true` to remove border the accordion and to `false` to add border it.
   */
   @Prop() isLight : boolean = false;

   /**
   * Set to `true` to add a border in the header and the content only for the light accordion.
   */
    @Prop() isLightSeparator : boolean = false;

   /**
   * Set to `true` to add the small version only for the light accordion.
   */
    @Prop() isSmall : boolean = false;

  /**
   * Toggle the display when clicking header
   */
  private onClick = () => {
    this.isOpen = !this.isOpen;
  };

  render() {

    const accordionLight = this.isLight ? 'accordion accordion-light' : 'accordion';
    const accordionLightHeader = this.isLight ? 'accordion-header accordion-light-header' : 'accordion-header';
    const accordionLightContent = this.isLight ? 'accordion-content accordion-light-content' : 'accordion-content';
    const accordionLightSeparator = this.isLightSeparator ? 'accordion accordion-light accordion-light--border' : 'accordion';
    const accordionSmall = this.isSmall ? 'accordion accordion-light accordion-light--small' : 'accordion';
    return (
      <details class={`${accordionLight} ${accordionSmall} ${accordionLightSeparator}`} open={this.isOpen}>
        <summary class="accordion-trigger" aria-expanded={`${this.isOpen}`} tabindex="0" role="button" onClick={this.onClick}>
          <div class={accordionLightHeader}>
            <slot name="icon-left"/>
            <slot name="header"/>
            <road-icon class="accordion-arrow" icon={navigationChevron}></road-icon>
          </div>
        </summary>
        <div class={accordionLightContent}>
          <slot/>
        </div>
      </details>
    );
  }
}