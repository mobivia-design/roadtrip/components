import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Expand/Accordion',
  component: 'road-accordion',
  argTypes: {
    'is-open': {
      control: 'boolean',
      description: 'Set to `true` to open the accordion and to `false` to close it.',
      table: {
        defaultValue: { summary: 'false' },
        type: { summary: null },
        required: { summary: false }
      },
    },
    'is-light': {
      control: 'boolean',
      description: 'Set to `true` to remove border the accordion and to `false` to add border it.',
      table: {
        defaultValue: { summary: 'false' },
        type: { summary: null },
        required: { summary: false }
      },
    },
    'is-light-separator': {
      control: 'boolean',
      description: 'Set to `true` to add a border in the header and the content only for the light accordion.',
      table: {
        defaultValue: { summary: 'false' },
        type: { summary: null },
        required: { summary: false }
      },
    },
    'is-small': {
      control: 'boolean',
      description: 'Set to `true` to add the small version only for the light accordion.',
      table: {
        defaultValue: { summary: 'false' },
        type: { summary: null },
        required: { summary: false }
      },
    },
    'icon-left': {
      control: 'text',
      description: 'Icon of the alert, it should be a road-icon element.\n`<road-icon name=\"alert-info-outline\" class=\"mr-16\"></road-icon>`',
    },
    header: {
      control: 'text',
      description: "Content of the header."
    },
    ' ': {
      control: 'text',
      description: "Content hidden in the accordion."
    },
    '--accordion-header-border': {
      description: "height of the border of the header",
      table: {
        defaultValue: { summary: '1px' },
      },
      control: {
        type: null,
      },
    },
    '--content-margin': {
      description: "margin of the accordion content",
      table: {
        defaultValue: { summary: '0 1rem 1rem' },
      },
      control: {
        type: null,
      },
    },
    '--content-padding': {
      description: "padding of the accordion content",
      table: {
        defaultValue: { summary: '1rem 0.5rem 0' },
      },
      control: {
        type: null,
      },
    },
    '--header-padding': {
      description: "padding horizontal and vertical of the accordion header",
      table: {
        defaultValue: { summary: 'var(--road-spacing-04) var(--road-spacing-05)' },
      },
      control: {
        type: null,
      },
    },
    '--header-padding-vertical': {
      description: "padding of the accordion header",
      table: {
        defaultValue: { summary: 'var(--road-spacing-04)' },
      },
      control: {
        type: null,
      },
    },
    '--icon-color': {
      description: "color of the chevron icon",
      table: {
        defaultValue: { summary: 'var(--road-icon)' },
      },
      control: {
        type: null,
      },
    },
    '--max-height': {
      description: "maximum height of the collapse content",
      table: {
        defaultValue: { summary: 'none' },
      },
      control: {
        type: null,
      },
    },
  },
  args: {
    'is-open': false,
    'is-light': false,
    'is-light-separator': false,
    'is-small': false,
    'icon-left': ``,
    header: `Accordion`,
    ' ': `Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.`,
  },
};

const Template = (args) => html`
<road-accordion is-open="${ifDefined(args['is-open'])}" is-light="${ifDefined(args['is-light'])}" is-light-separator="${ifDefined(args['is-light-separator'])}" is-small="${ifDefined(args['is-small'])}">
  <div slot="header">
    ${unsafeHTML(args['icon-left'])}
    ${unsafeHTML(args.header)}
  </div>
  ${unsafeHTML(args[' '])}
</road-accordion>
`;

export const Playground = Template.bind({});

export const Closed = Template.bind({});
Closed.args = {
  'is-open': false,
};

export const Open = Template.bind({});
Open.args = {
  'is-open': true,
};

export const IconLeft = Template.bind({});
IconLeft.args = {
  'icon-left': `<road-icon name="alert-info-outline" class="mr-16" aria-hidden="true"></road-icon>`,
  ' ': `Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.`,

};
