# road-accordion


<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description                                                                               | Type      | Default |
| ------------------ | -------------------- | ----------------------------------------------------------------------------------------- | --------- | ------- |
| `isLight`          | `is-light`           | Set to `true` to remove border the accordion and to `false` to add border it.             | `boolean` | `false` |
| `isLightSeparator` | `is-light-separator` | Set to `true` to add a border in the header and the content only for the light accordion. | `boolean` | `false` |
| `isOpen`           | `is-open`            | Set to `true` to open the accordion and to `false` to close it.                           | `boolean` | `false` |
| `isSmall`          | `is-small`           | Set to `true` to add the small version only for the light accordion.                      | `boolean` | `false` |


## Slots

| Slot          | Description                                                                                                            |
| ------------- | ---------------------------------------------------------------------------------------------------------------------- |
|               | Content hidden in the accordion.                                                                                       |
| `"header"`    | Content of the header.                                                                                                 |
| `"icon-left"` | Icon of the alert, it should be a road-icon element. `<road-icon name="alert-info-outline" class="mr-16"></road-icon>` |


## CSS Custom Properties

| Name                        | Description                                             |
| --------------------------- | ------------------------------------------------------- |
| `--accordion-header-border` | height of the border of the header                      |
| `--content-margin`          | margin of the accordion content                         |
| `--content-padding`         | padding of the accordion content                        |
| `--header-padding`          | padding horizontal and vertical of the accordion header |
| `--header-padding-vertical` | padding of the accordion header                         |
| `--icon-color`              | color of the chevron icon                               |
| `--max-height`              | maximum height of the collapse content                  |


## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-accordion --> road-icon
  style road-accordion fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
