import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Forms/Button Floating',
  component: 'road-button-floating',
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus'],
    },
  },
  argTypes: {
    href: {
      control: { type: null },
      description: 'Defines the hyperlink reference (only applicable if the button acts as a link).',
    },
    position: {
      options: ['left', 'right', 'center'],
      control: { type: 'radio' },
      description: 'Defines the floating button position.',
      table: {
        defaultValue: { summary: 'right' },
      },
    },
    rel: {
      control: { type: null },
      description: 'Specifies the relationship between the current document and the linked document.',
    },
    target: {
      control: { type: null },
      description: 'Specifies where to open the linked document (only applicable if `href` is provided).',
    },
    start: {
      control: 'text',
      description: 'Slot for an icon or content at the **start** of the button.',
    },
    ' ': {
      control: 'text',
      description: 'Label or main content inside the button.',
    },
    roadblur: {
      action: 'roadblur',
      description: 'Triggered when the button loses focus.',
      table: {
        category: 'Events',
        type: { summary: 'CustomEvent' },
      },
    },
    roadfocus: {
      action: 'roadfocus',
      description: 'Triggered when the button gains focus.',
      table: {
        category: 'Events',
        type: { summary: 'CustomEvent' },
      },
    },
    native: {
      control: { type: null },
      description: 'Internal native button element reference (if applicable).',
    },
  },
  args: {
    start: '',
    position: 'right',
    ' ': 'Label',
  },
};

const Template = (args) => html`
<road-button-floating 
  position="${ifDefined(args.position)}" 
  tabindex="0"
>
  ${unsafeHTML(args.start)}
  ${unsafeHTML(args[' '])}
</road-button-floating>
`;

export const Playground = Template.bind({});
Playground.args = {
  start: `<road-icon slot="start" name="shopping-cart-add" role="button"></road-icon>`,
  ' ': `Add to cart`,
};
