import { Component, Element, Event, EventEmitter, Host, Prop, h, Listen } from '@stencil/core';

import './../../utils/polyfill';

/**
 * @slot - Content of the button.
 * @slot start - Left content of the button text, usually for icon.
 *
 * @part native - The native HTML button or anchor element that wraps all child elements.
 */

@Component({
  tag: 'road-button-floating',
  styleUrl: 'button-floating.css',
  shadow: true,
})
export class Button {

  @Element() el!: HTMLRoadButtonElement;

  /**
  * position.
  */
     @Prop({ reflect: true }) position?: 'left' | 'center' | 'right' = 'right';


  /**
   * Contains a URL or a URL fragment that the hyperlink points to.
   * If this property is set, an anchor tag will be rendered.
   */
  @Prop() href?: string;

  /**
   * Specifies the relationship of the target object to the link object.
   * The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).
   */
  @Prop() rel?: string;

  /**
   * Specifies where to display the linked URL.
   * Only applies when an `href` is provided.
   * Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.
   */
  @Prop() target?: string;

  /**
   * Emitted when the button has focus.
   */
  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  /**
   * Emitted when the button loses focus.
   */
  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;


  @Listen('scroll', {target: 'window'})

    onScroll() {
      const positionScroll = window.scrollY;
      if (positionScroll < 300 ) {
          this.el.classList.add("scroll-down");
          this.el.classList.remove("scroll-up");
      }
      if (positionScroll > 300 ) {
          this.el.classList.add("scroll-up");
          this.el.classList.remove("scroll-down");
      }
    }




  private onClick = (ev: Event) => {
    if (this.el.shadowRoot && (this.el as any).attachShadow) {
      // this button wants to specifically submit a form
      // climb up the dom to see if we're in a <form>
      // and if so, then use JS to submit it
      const form = this.el.closest('form');
      if (form) {
        ev.preventDefault();

        const fakeButton = document.createElement('button');
        fakeButton.style.display = 'none';
        form.appendChild(fakeButton);
        fakeButton.click();
        fakeButton.remove();
      }
    }
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  private onBlur = () => {
    this.roadblur.emit();
    this.roadBlur.emit();
  };

  render() {
    const TagType = this.href === undefined ? 'button' : 'a' as any;
    const positionClass = this.position !== undefined ? `position-${this.position}` : '';

    return (
      <Host
        onClick={this.onClick}
        class={`${positionClass}`}
      >
        <TagType
          class="button-native"
          part="native"
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        >
          <slot name="start"/>
          <slot/>
        </TagType>
      </Host>
    );
  }
}