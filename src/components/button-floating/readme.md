# road-button



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                                                                                                                            | Type                                         | Default     |
| ---------- | ---------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- | ----------- |
| `href`     | `href`     | Contains a URL or a URL fragment that the hyperlink points to. If this property is set, an anchor tag will be rendered.                                                                | `string \| undefined`                        | `undefined` |
| `position` | `position` | position.                                                                                                                                                                              | `"center" \| "left" \| "right" \| undefined` | `'right'`   |
| `rel`      | `rel`      | Specifies the relationship of the target object to the link object. The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types). | `string \| undefined`                        | `undefined` |
| `target`   | `target`   | Specifies where to display the linked URL. Only applies when an `href` is provided. Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.                                    | `string \| undefined`                        | `undefined` |


## Events

| Event       | Description                          | Type                |
| ----------- | ------------------------------------ | ------------------- |
| `roadblur`  | Emitted when the button loses focus. | `CustomEvent<void>` |
| `roadfocus` | Emitted when the button has focus.   | `CustomEvent<void>` |


## Slots

| Slot      | Description                                        |
| --------- | -------------------------------------------------- |
|           | Content of the button.                             |
| `"start"` | Left content of the button text, usually for icon. |


## Shadow Parts

| Part       | Description                                                             |
| ---------- | ----------------------------------------------------------------------- |
| `"native"` | The native HTML button or anchor element that wraps all child elements. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
