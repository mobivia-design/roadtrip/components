# road-tab-button



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                      | Type                        | Default     |
| ---------- | ---------- | -------------------------------------------------------------------------------- | --------------------------- | ----------- |
| `selected` | `selected` | The selected tab component                                                       | `boolean`                   | `false`     |
| `size`     | `size`     | The Segmented buttons size.                                                      | `"md" \| "sm" \| undefined` | `'md'`      |
| `tab`      | `tab`      | A tab id must be provided for each `road-tab`. It's used internally to reference | `string \| undefined`       | `undefined` |


## Shadow Parts

| Part       | Description                                                   |
| ---------- | ------------------------------------------------------------- |
| `"native"` | The native HTML anchor element that wraps all child elements. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
