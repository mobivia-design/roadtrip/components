import { Component, Element, Event, EventEmitter, Host, Listen, Prop, h } from '@stencil/core';

/**
 *
 * @part native - The native HTML anchor element that wraps all child elements.
 */

@Component({
  tag: 'road-segmented-button',
  styleUrl: 'segmented-button.css',
  shadow: true,
})
export class SegmentedButton {

  @Element() el!: HTMLRoadSegmentedButtonElement;

  /**
    * The Segmented buttons size.
    */
   @Prop() size?: 'sm' | 'md' = 'md';

  /**
   * The selected tab component
   */
  @Prop({ mutable: true }) selected = false;

  /**
   * A tab id must be provided for each `road-tab`. It's used internally to reference
   */
  @Prop() tab?: string;

  /**
   * Emitted when the tab bar is clicked
   * @internal
   */
  @Event() roadsegmentedbuttonclick!: EventEmitter;
  /** @internal */
  @Event() roadSegmentedButtonClick!: EventEmitter;

  @Listen('roadSegmentedButtonBarChanged', { target: 'window' })
  @Listen('roadSegmentedButtonbarchanged', { target: 'window' })
  onButtonBarChanged(ev: CustomEvent) {
    const dispatchedFrom = ev.target as HTMLElement;
    const parent = this.el.parentElement as EventTarget;

    if ((ev.composedPath && ev.composedPath().includes(parent)) || (dispatchedFrom && dispatchedFrom.contains(this.el))) {
      this.selected = this.tab === ev.detail.tab;
    }
  }

  private selectTab(ev: Event | KeyboardEvent) {
    if (this.tab !== undefined) {
      this.roadsegmentedbuttonclick.emit({
        tab: this.tab,
        selected: this.selected,
      });
      this.roadSegmentedButtonClick.emit({
        tab: this.tab,
        selected: this.selected,
      });

      ev.preventDefault();
    }
  }

  private get tabIndex() {
    const hasTabIndex = this.el.hasAttribute('tabindex');

    if (hasTabIndex) {
      return this.el.getAttribute('tabindex');
    }

    return 0;
  }

  private onKeyUp = (ev: KeyboardEvent) => {
    if (ev.key === 'Enter' || ev.key === ' ') {
      this.selectTab(ev);
    }
  };

  private onClick = (ev: Event) => {
    this.selectTab(ev);
  };

  render() {
    const { tabIndex, selected, tab } = this;

    const sizeClass = this.size !== undefined ? `btn-${this.size}` : '';


    return (
      <Host
        onClick={this.onClick}
        onKeyup={this.onKeyUp}
        role="tab"
        tabindex={tabIndex}
        aria-selected={selected ? 'true' : null}
        id={tab !== undefined ? `tab-button-${tab}` : null}
        class={{
          'tab-selected': selected,
          [`${sizeClass}`]: true,
        }}
      >
        <span tabIndex={-1} class="button-native" part="native" aria-hidden="true">
            <slot/>
        </span>
      </Host>
    );
  }
}