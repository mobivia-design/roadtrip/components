# road-carousel-item

The Carousel Item component is a child component of [Carousel](../carousel). The template
should be written as `road-carousel-item`. Any slide content should be written
in this component and it should be used in conjunction with [Carousel](../carousel).

See the [Carousel API Docs](../carousel) for more usage information.

<!-- Auto Generated Below -->


## Slots

| Slot | Description                                                       |
| ---- | ----------------------------------------------------------------- |
|      | Content of the carousel item, it should be road-img or road-card. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
