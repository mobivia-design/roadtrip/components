import { Component, Host, h } from '@stencil/core';

/**
 * @slot - Content of the carousel item, it should be road-img or road-card.
 */

@Component({
  tag: 'road-carousel-item',
  styleUrl: 'carousel-item.css',
})
export class CarouselItem {

  render() {
    return (
      <Host
        class={{
          'swiper-slide': true,
          'swiper-zoom-container': true,
        }}
      >
        <slot/>
      </Host>
    );
  }
}
