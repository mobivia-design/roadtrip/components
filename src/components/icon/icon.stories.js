import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { list } from '../../../.storybook/list';

export default {
  title: 'Media/Icon',
  component: 'road-icon',
};

export const Playground = ({ color, name, size, rotate, lazy }) => html`
<road-icon 
  color="${ifDefined(color)}" 
  name="${ifDefined(name)}" 
  size="${ifDefined(size)}" 
  rotate="${ifDefined(rotate)}" 
  lazy="${ifDefined(lazy)}"
  role="img"
></road-icon>
`;
Playground.args = {
  color: 'default',
  name: 'alert-info-outline',
  size: null,
  lazy: null,
};
Playground.argTypes = {
  color: {
    options: ['primary', 'secondary', 'accent', 'info', 'success', 'warning', 'danger', 'default', 'white'],
    control: {
      type: 'select',
    },
  },
  name: {
    options: list,
    control: {
      type: 'select',
    },
  },
  size: {
    options: ['sm', 'md', 'lg', '3x', '4x'],
    control: {
      type: 'select',
    },
  },
  rotate: {
    options: ['0', '90', '180', '270'],
    control: {
      type: 'select',
    },
  },
  icon: {
    control: 'text',
  },
  lazy: {
    control: 'boolean',
  },
  'aria-label': {
    control: 'text',
  },
  'aria-hidden': {
    control: 'text',
  },
  src: {
    control: 'text',
  },
  sanitize: {
    control: 'boolean',
  },
};

export const Sizes = () => html`
<road-icon name="alert-success" size="sm" role="img"></road-icon>
<road-icon name="alert-success" size="md" role="img"></road-icon>
<road-icon name="alert-success" size="lg" role="img"></road-icon>
<road-icon name="alert-success" size="3x" role="img"></road-icon>
<road-icon name="alert-success" size="4x" role="img"></road-icon>
`;

export const Rotate = () => html`
<road-icon name="navigation-chevron" role="img"></road-icon>
<road-icon name="navigation-chevron" rotate="90" role="img"></road-icon>
<road-icon name="navigation-chevron" rotate="180" role="img"></road-icon>
<road-icon name="navigation-chevron" rotate="270" role="img"></road-icon>
`;

export const Colors = () => html`
<road-icon name="location-pin-garage-norauto" color="primary" role="img"></road-icon>
<road-icon name="location-pin-garage" color="secondary" role="img"></road-icon>
<road-icon name="location-pin-all" color="accent" role="img"></road-icon>

<br/>

<road-icon name="alert-info" color="info" role="img"></road-icon>
<road-icon name="alert-success" color="success" role="img"></road-icon>
<road-icon name="alert-warning" color="warning" role="img"></road-icon>
<road-icon name="alert-danger" color="danger" role="img"></road-icon>

<br/>

<road-icon name="navigation-back" role="img"></road-icon>
<road-icon name="navigation-back" color="white" class="bg-primary" role="img"></road-icon>
`;

export const MultipleColors = () => html`
<road-icon name="vehicle-3-doors-color" size="4x" role="img"></road-icon>
<road-icon name="vehicle-3-doors-hatchback-color" size="4x" role="img"></road-icon>

<br/>

<road-icon name="vehicle-5-doors-color" size="4x" role="img"></road-icon>
<road-icon name="vehicle-5-doors-hatchback-color" size="4x" role="img"></road-icon>

<br/>

<road-icon name="vehicle-tires-front-color" size="4x" role="img"></road-icon>
<road-icon name="vehicle-tires-back-color" size="4x" role="img"></road-icon>
<road-icon name="vehicle-tires-all-color" size="4x" role="img"></road-icon>

<br/>

<road-icon name="car-wiper-front-color" size="4x" role="img"></road-icon>
<road-icon name="car-wiper-back-color" size="4x" role="img"></road-icon>

<br/>

<road-icon name="vehicle-car-light-front-color" size="4x" role="img"></road-icon>
<road-icon name="vehicle-car-light-back-color" size="4x" role="img"></road-icon>
<road-icon name="vehicle-car-light-inside-color" size="4x" role="img"></road-icon>

<br/>

<road-icon name="brake-color" size="4x" role="img"></road-icon>
<road-icon name="brake" color="secondary" size="4x" role="img"></road-icon>

<br/>

<div class="bg-primary">
  <road-icon name="location-pin-outline-color" color="white" size="4x" role="img"></road-icon>
  <road-icon name="vehicle-car-add-outline-color" color="white" size="4x" role="img"></road-icon>
  <road-icon name="vehicle-car-checked-outline-color" color="white" size="4x" role="img"></road-icon>
</div>

<br/>

<road-icon name="weather-sun-snow-color" size="4x" role="img"></road-icon>
<road-icon name="weather-sun-color" size="4x" role="img"></road-icon>
<road-icon name="weather-snow-color" size="4x" role="img"></road-icon>

<br/>

<road-icon name="people-outline" color="primary" size="4x" role="img"></road-icon>
<road-icon name="people-vip-outline-color" size="4x" role="img"></road-icon>
<road-icon name="people-card-color" size="4x" role="img"></road-icon>
<road-icon name="people-car-fleet-color" size="4x" role="img"></road-icon>
<road-icon name="people-coworker-norauto-color" size="4x" role="img"></road-icon>
`;
