import { Build, Component, Element, Host, Prop, State, Watch, h } from '@stencil/core';
import { getSvgContent, roadiconContent } from './request';
import { getName, getUrl } from './utils';

import { Color } from '../../interface';

@Component({
  tag: 'road-icon',
  assetsDirs: ['svg'],
  styleUrl: 'icon.css',
  shadow: true,
})
export class Icon {
  private io?: IntersectionObserver;

  @Element() el!: HTMLRoadIconElement;

  @State() private svgContent?: string;

  @State() private isVisible = false;

  /**
   * Color of the icon
   */
  @Prop() color?: Color | 'white';

  /**
   * Specifies the label to use for accessibility. Defaults to the icon name.
   */
  @Prop({ mutable: true, reflect: true }) ariaLabel?: string;

  /**
   * Set the icon to hidden, respectively `true`, to remove it from the accessibility tree.
   */
  @Prop({ reflect: true }) ariaHidden?: string;

  /**
   * Specifies which icon to use from the built-in set of icons.
   */
  @Prop() name?: string;

  /**
   * Specifies the exact `src` of an SVG file to use.
   */
  @Prop() src?: string;

  /**
   * A combination of both `name` and `src`. If a `src` url is detected
   * it will set the `src` property. Otherwise it assumes it's a built-in named
   * SVG and set the `name` property.
   */
  @Prop() icon?: any;

  /**
   * The size of the icon.
   * Available options are: `"sm"`, `"md"`, `"lg"`, `"3x"` and `"4x"`.
   */
  @Prop() size?: 'sm' | 'md' | 'lg' | '3x' | '4x'  = "lg";

  /**
   * The rotation of the icon.
   * Available options are: `"90"`, `"180"`, `"270"`.
   */
  @Prop() rotate?: '90' | '180' | '270';

  /**
   * If enabled, road-icon will be loaded lazily when it's visible in the viewport.
   * Default, `false`.
   */
  @Prop() lazy: boolean = false;

  /**
   * When set to `false`, SVG content that is HTTP fetched will not be checked
   * if the response SVG content has any `<script>` elements, or any attributes
   * that start with `on`, such as `onclick`.
   */
  @Prop() sanitize = true;

  componentWillLoad() {

    this.waitUntilVisible(this.el, '50px', () => {
      this.isVisible = true;
      this.loadIcon();
    });
  }

  disconnectedCallback() {
    if (this.io) {
      this.io.disconnect();
      this.io = undefined;
    }
  }

  private waitUntilVisible(el: HTMLRoadIconElement, rootMargin: string, cb: () => void) {
    if (this.lazy && (window as any).IntersectionObserver) {
      const io = this.io = new (window as any).IntersectionObserver((data: IntersectionObserverEntry[]) => {
        if (data[0].isIntersecting) {
          io.disconnect();
          this.io = undefined;
          cb();
        }
      }, { rootMargin });

      io.observe(el);

    } else {
      // browser doesn't support IntersectionObserver
      // so just fallback to always show it
      cb();
    }
  }

  @Watch('name')
  @Watch('src')
  @Watch('icon')
  loadIcon() {
    if (Build.isBrowser && this.isVisible) {
      const url = getUrl(this);
      if (url) {
        if (roadiconContent.has(url)) {
          // sync if it's already loaded
          this.svgContent = roadiconContent.get(url);
        } else {
          // async if it hasn't been loaded
          getSvgContent(url, this.sanitize).then(() => (this.svgContent = roadiconContent.get(url)));
        }
      }
    }

    if (!this.ariaLabel && this.ariaHidden !== 'true') {
      const label = getName(this.name, this.icon);
      // user did not provide a label
      // come up with the label based on the icon name
      if (label) {
        this.ariaLabel = label.replace(/-/g, ' ');
      }
    }
  }

  render() {
    const colorClass = this.color ? `icon-${this.color}` : '';
    const sizeClass = this.size ? `icon-${this.size}` : '';
    const rotateClass = this.rotate ? `icon-rotate-${this.rotate}` : '';

    // Ajouter une classe "default" si aucune des valeurs n'est définie
    const classes = [colorClass, sizeClass, rotateClass].filter(Boolean).join(' ');

    return (
      <Host class={classes} aria-hidden="true">
        {this.svgContent !== ''
          ? <div class="icon-inner" innerHTML={this.svgContent}></div>
          : <div class="icon-inner"></div>
        }
      </Host>
    );

  }
}