# road-icon



<!-- Auto Generated Below -->


## Usage

### React

```javascript
import React from 'react';
import { tire, vehicleCar } from "@roadtrip/components/icons";

const MyComponent: React.FC = () => {
  return (
    <>
      <road-icon icon={tire}></road-icon>
      <road-icon icon={vehicleCar}></road-icon>
    </>
  );
};

export default MyComponent;
```


### Vue

```html
<template>
  <road-icon icon="tire"></road-icon>
  <road-icon icon="vehicle-car"></road-icon>
</template>

<script>
  import { tire, vehicleCar } from "@roadtrip/components/icons";
  import { addIcons } from "@roadtrip/components";
  addIcons({
    tire,
    "vehicle-car": vehicleCar
  });
</script>
```



## Properties

| Property     | Attribute     | Description                                                                                                                                                                                   | Type                                                                                                                        | Default     |
| ------------ | ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `ariaHidden` | `aria-hidden` | Set the icon to hidden, respectively `true`, to remove it from the accessibility tree.                                                                                                        | `string \| undefined`                                                                                                       | `undefined` |
| `ariaLabel`  | `aria-label`  | Specifies the label to use for accessibility. Defaults to the icon name.                                                                                                                      | `string \| undefined`                                                                                                       | `undefined` |
| `color`      | `color`       | Color of the icon                                                                                                                                                                             | `"accent" \| "danger" \| "default" \| "info" \| "primary" \| "secondary" \| "success" \| "warning" \| "white" \| undefined` | `undefined` |
| `icon`       | `icon`        | A combination of both `name` and `src`. If a `src` url is detected it will set the `src` property. Otherwise it assumes it's a built-in named SVG and set the `name` property.                | `any`                                                                                                                       | `undefined` |
| `lazy`       | `lazy`        | If enabled, road-icon will be loaded lazily when it's visible in the viewport. Default, `false`.                                                                                              | `boolean`                                                                                                                   | `false`     |
| `name`       | `name`        | Specifies which icon to use from the built-in set of icons.                                                                                                                                   | `string \| undefined`                                                                                                       | `undefined` |
| `rotate`     | `rotate`      | The rotation of the icon. Available options are: `"90"`, `"180"`, `"270"`.                                                                                                                    | `"180" \| "270" \| "90" \| undefined`                                                                                       | `undefined` |
| `sanitize`   | `sanitize`    | When set to `false`, SVG content that is HTTP fetched will not be checked if the response SVG content has any `<script>` elements, or any attributes that start with `on`, such as `onclick`. | `boolean`                                                                                                                   | `true`      |
| `size`       | `size`        | The size of the icon. Available options are: `"sm"`, `"md"`, `"lg"`, `"3x"` and `"4x"`.                                                                                                       | `"3x" \| "4x" \| "lg" \| "md" \| "sm" \| undefined`                                                                         | `"lg"`      |
| `src`        | `src`         | Specifies the exact `src` of an SVG file to use.                                                                                                                                              | `string \| undefined`                                                                                                       | `undefined` |


## Dependencies

### Used by

 - [road-accordion](../accordion)
 - [road-banner](../banner)
 - [road-carousel](../carousel)
 - [road-checkbox](../checkbox)
 - [road-chip](../chip)
 - [road-counter](../counter)
 - [road-dialog](../dialog)
 - [road-drawer](../drawer)
 - [road-dropdown](../dropdown)
 - [road-input](../input)
 - [road-item](../item)
 - [road-modal](../modal)
 - [road-phone-number-input](../phone-number-input)
 - [road-plate-number](../plate-number)
 - [road-range](../range)
 - [road-rating](../rating)
 - [road-toast](../toast)

### Graph
```mermaid
graph TD;
  road-accordion --> road-icon
  road-banner --> road-icon
  road-carousel --> road-icon
  road-checkbox --> road-icon
  road-chip --> road-icon
  road-counter --> road-icon
  road-dialog --> road-icon
  road-drawer --> road-icon
  road-dropdown --> road-icon
  road-input --> road-icon
  road-item --> road-icon
  road-modal --> road-icon
  road-phone-number-input --> road-icon
  road-plate-number --> road-icon
  road-range --> road-icon
  road-rating --> road-icon
  road-toast --> road-icon
  style road-icon fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
