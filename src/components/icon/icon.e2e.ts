import { newE2EPage } from '@stencil/core/testing';

describe('road-icon', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<road-icon name="alert-success"></road-icon>');

    const element = await page.find('road-icon');
    expect(element).toHaveClass('hydrated');
  });
});
