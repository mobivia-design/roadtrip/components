```javascript
import React from 'react';
import { tire, vehicleCar } from "@roadtrip/components/icons";

const MyComponent: React.FC = () => {
  return (
    <>
      <road-icon icon={tire}></road-icon>
      <road-icon icon={vehicleCar}></road-icon>
    </>
  );
};

export default MyComponent;
```