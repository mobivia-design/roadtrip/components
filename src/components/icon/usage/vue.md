```html
<template>
  <road-icon icon="tire"></road-icon>
  <road-icon icon="vehicle-car"></road-icon>
</template>

<script>
  import { tire, vehicleCar } from "@roadtrip/components/icons";
  import { addIcons } from "@roadtrip/components";
  addIcons({
    tire,
    "vehicle-car": vehicleCar
  });
</script>
```
