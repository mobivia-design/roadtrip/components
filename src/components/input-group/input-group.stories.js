import { html } from 'lit';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Forms/Input Group',
  component: 'road-input-group',
  argTypes: {
    prepend: {
      control: 'text',
    },
    ' ': {
      control: 'text',
    },
    append: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
  },
  args: {
    disabled: false, // Défaut : non désactivé
  },
};

const Template = (args) => html`
<road-input-group ?disabled=${args.disabled}>
  ${unsafeHTML(args.prepend || '')}
  ${unsafeHTML(args[' '] || '')}
  ${unsafeHTML(args.append || '')}
</road-input-group>
`;

export const Playground = Template.bind({});
Playground.args = {
  ' ': `<road-input input-id="cardNumber" sizes="xl" label="Card Number"></road-input>`,
  append: `<label slot="append" for="cardNumber">
    <road-icon name="payment-card" aria-hidden="true" role="img"></road-icon>
  </label>`,
};

export const Button = Template.bind({});
Button.args = {
  ' ': `<road-input input-id="newsletter" sizes="xl" label="Newsletter"></road-input>`,
  append: `<road-button slot="append" color="ghost">
    OK
  </road-button>`,
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true, // Désactive tout le groupe
  ' ': `<road-input input-id="inputGroupDisabled" sizes="xl" label="Label" disabled></road-input>`,
  prepend: `<road-button slot="prepend" outline="true" disabled>
    <road-icon color="secondary" name="alert-info-outline"></road-icon>
  </road-button>`,
  append: `<road-button slot="append" outline="true" disabled>
    <road-icon color="secondary" name="alert-info-outline"></road-icon>
  </road-button>`,
};

export const Error = Template.bind({});
Error.args = {
  ' ': `<road-input input-id="inputGroupError" sizes="xl" label="Label" error="error message here"></road-input>`,
  append: `<label slot="append">
    <road-icon name="payment-card" role="img"></road-icon>
  </label>`,
};

export const Password = Template.bind({});
Password.args = {
  ' ': `<road-input input-id="password" sizes="xl" label="password" type="password"></road-input>`,
  append: `<road-button slot="append" outline="true">
    <road-icon name="visibility-off-outline" role="button"></road-icon>
  </road-button>`,
};
