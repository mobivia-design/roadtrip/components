# road-input-group



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                           | Type      | Default |
| ---------- | ---------- | --------------------------------------------------------------------- | --------- | ------- |
| `disabled` | `disabled` | Disables the entire input group and propagates the state to children. | `boolean` | `false` |


## Slots

| Slot        | Description                             |
| ----------- | --------------------------------------- |
|             | Add the road-input or road-select here. |
| `"append"`  | Add icon or button after the field.     |
| `"prepend"` | Add icon or button before the field.    |


## Dependencies

### Used by

 - [road-counter](../counter)
 - [road-plate-number](../plate-number)

### Graph
```mermaid
graph TD;
  road-counter --> road-input-group
  road-plate-number --> road-input-group
  style road-input-group fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
