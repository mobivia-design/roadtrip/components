import { Component, Element, h, Prop, Watch } from '@stencil/core';

/**
 * @slot prepend - Add icon or button before the field.
 * @slot  - Add the road-input or road-select here.
 * @slot append - Add icon or button after the field.
 */

@Component({
  tag: 'road-input-group',
  styleUrl: 'input-group.css',
  shadow: true,
})
export class InputGroup {
  @Element() el!: HTMLRoadInputGroupElement;

  /** Disables the entire input group and propagates the state to children. */
  @Prop() disabled: boolean = false;

  /** Watches for changes in the `disabled` prop and updates child elements. */
  @Watch('disabled')
  handleDisabledChange(newValue: boolean) {
    this.updateDisabledState(newValue);
  }

  /** Applies or removes the `disabled` attribute to/from children. */
  private updateDisabledState(isDisabled: boolean) {
    const children = this.el.querySelectorAll('road-input, road-button, road-select');
    children.forEach(child => {
      if (isDisabled) {
        child.setAttribute('disabled', '');
      } else {
        child.removeAttribute('disabled');
      }
    });
  }

  componentWillLoad() {
    // Ensure the initial state of `disabled` is applied before render.
    this.updateDisabledState(this.disabled);
  }

  render() {
    const errorClass = this.el.querySelector('road-input[error]') ? 'is-invalid' : '';
    const sizeClass = this.el.querySelector('road-input[sizes]') 
      ? `size-${(this.el.querySelector('road-input') as HTMLRoadInputElement).sizes}` 
      : '';
    const disabledClass = this.disabled ? 'is-disabled' : '';

    return (
      <div class={`input-group ${errorClass} ${disabledClass} ${sizeClass}`}>
        <div class="input-group-prepend">
          <slot name="prepend" />
        </div>
        <slot />
        <div class="input-group-append">
          <slot name="append" />
        </div>
      </div>
    );
  }
}
