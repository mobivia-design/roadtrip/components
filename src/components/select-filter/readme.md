# road-autocomplete



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                                                                                       | Type                                            | Default                                                                             |
| ------------ | ------------- | ------------------------------------------------------------------------------------------------- | ----------------------------------------------- | ----------------------------------------------------------------------------------- |
| `isActive`   | `is-active`   | If `true`, add class Active                                                                       | `boolean`                                       | `false`                                                                             |
| `onlySelect` | `only-select` | If `false`, the user can submit custom value                                                      | `boolean`                                       | `true`                                                                              |
| `options`    | --            | List of options of the select                                                                     | `{ value: string \| number; label: string; }[]` | `[]`                                                                                |
| `parameters` | `parameters`  | Options to pass to the fuse.js instance. See https://fusejs.io/api/options.html for valid options | `any`                                           | `{     keys: ["value", "label"],     threshold: 0.3,     ignoreLocation: true,   }` |


## Events

| Event          | Description | Type                                                                  |
| -------------- | ----------- | --------------------------------------------------------------------- |
| `roadselected` |             | `CustomEvent<{ value: string \| null \| undefined; label: string; }>` |
| `roadSelected` |             | `CustomEvent<{ value: string \| null \| undefined; label: string; }>` |


## Slots

| Slot | Description                                                      |
| ---- | ---------------------------------------------------------------- |
|      | Input element for the widget, it should be a road-input element. |


## CSS Custom Properties

| Name           | Description                                                            |
| -------------- | ---------------------------------------------------------------------- |
| `--margin-top` | margin top of the select-filter (should equal the height of the field) |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
