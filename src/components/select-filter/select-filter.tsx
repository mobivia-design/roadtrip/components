import { Component, Element, Host, Listen, Prop, State, h, Event, EventEmitter } from '@stencil/core';
import Fuse from 'fuse.js';

/**
 * @slot  - Input element for the widget, it should be a road-input element.
 */

@Component({
  tag: 'road-select-filter',
  styleUrl: 'select-filter.css',
  scoped: true,
})
export class SelectFilter {

  @Element() el!: HTMLRoadSelectFilterElement;

  @State() isOpen: boolean = false;

  @State() currentValue: string = '';

  @State() activeIndex: number = -1;

  /**
   * List of options of the select
   */
  @Prop() options: Array<{
    value: string | number;
    label: string;
  }> = [];

  /**
   * Options to pass to the fuse.js instance.
   * See https://fusejs.io/api/options.html for valid options
   */
  @Prop() parameters: any = {
    keys: ["value", "label"],
    threshold: 0.3,
    ignoreLocation: true,
  };

  /**
   * If `true`, add class Active
   */
  @Prop() isActive: boolean = false;

  /**
   * If `false`, the user can submit custom value
   */
  @Prop() onlySelect: boolean = true;

  @Listen('roadFocus')
  @Listen('roadfocus')
  handleFocus() {
    this.isOpen = true;
    this.isActive = true;
  }

  @Listen('roadBlur')
  @Listen('roadblur')
  handleBlur() {
    this.isActive = false;
  }

  @Listen('roadChange')
  @Listen('roadchange')
  handleChange(event: CustomEvent) {
    this.currentValue = event.detail.value.toLowerCase();
  }

  @Listen('click', { target: 'document' })
  onClickOutside(event: MouseEvent) {
    if (this.isOpen && !this.el.contains(event.target as Node)) {
      this.isOpen = false;
      this.isActive = false;
      const value = (this.el.querySelector('road-input') as HTMLRoadInputElement).value;
      if (this.onlySelect && !this.options.some(option => option['label'] === value)) {
        (this.el.querySelector('road-input') as HTMLRoadInputElement).value = '';
      }
    }
  }

  @Listen('keydown', { target: 'window' })
  handleKeydown(event: KeyboardEvent) {
  if (this.isOpen) {
    if (event.key === 'ArrowDown') {
      event.preventDefault();
      this.activeIndex = (this.activeIndex + 1) % this.filteredOptions.length;
    } else if (event.key === 'ArrowUp') {
      event.preventDefault();
      this.activeIndex = (this.activeIndex - 1 + this.filteredOptions.length) % this.filteredOptions.length;
    } else if (event.key === 'Enter') {
      event.preventDefault();
      const activeOption = this.filteredOptions[this.activeIndex];
      if (activeOption) {
        this.onClick(activeOption.item.value, activeOption.item.label);
      }
    } else if (event.key === 'Escape') {
      event.preventDefault();
      this.isOpen = false;
      this.isActive = false;
    }
  }
}


  get filteredOptions() {
    const fuseSearch = new Fuse(this.options, this.parameters);
    return this.currentValue === '' ? this.options.map((doc, idx) => ({ item: doc, score: 1, refIndex: idx })) : fuseSearch.search(this.currentValue);
  }

  @Event() roadselected!: EventEmitter<{
    value: string | undefined | null,
    label: string
  }>;

  @Event() roadSelected!: EventEmitter<{
    value: string | undefined | null,
    label: string
  }>;

  private onClick = (value: string | number, label: string) => {
    (this.el.querySelector('road-input') as HTMLRoadInputElement).value = label;
    if (document.getElementsByClassName('phone-number-label-icon').length > 0) {
      (this.el.querySelector('road-icon') as HTMLRoadIconElement).name = 'flag-' + label;
    }
    this.isOpen = false;
    this.roadselected.emit({
      value: value.toString(),
      label: label,
    });
    this.roadSelected.emit({
      value: value.toString(),
      label: label,
    });
    this.activeIndex = -1;
  };

  private handleOptionMouseOver = (index: number) => {
    this.activeIndex = index;
  };

  private handleOptionClick = (value: string | number, label: string) => {
    this.onClick(value, label);
  };

  private handleMouseLeave = () => {
    this.activeIndex = -1;
  };

  render() {
    const options = this.filteredOptions;
    const isActive = this.isActive ? 'true' : 'false';
    const notActive = this.isActive ? '' : 'not-active';

    return (
      <Host is-active={isActive}>
        <slot />
        {this.isOpen &&
          <ul 
            class={`${notActive} select-filter-list`} 
            role="listbox"
            onMouseLeave={this.handleMouseLeave}
          >
            {options.map((option, idx) => (
              <li
                class={`select-filter-item ${this.activeIndex === idx ? 'active' : ''}`}
                role="option"
                aria-selected={this.activeIndex === idx ? 'true' : 'false'}
                onClick={() => this.handleOptionClick(option.item.value, option.item.label.replace(/(<([^>]+)>)/gi, ""))}
                onMouseOver={() => this.handleOptionMouseOver(idx)}
                innerHTML={option.item.label}
                tabindex={this.activeIndex === idx ? 0 : -1}
              >
              </li>
            ))}
          </ul>}
      </Host>
    );
  }
}
