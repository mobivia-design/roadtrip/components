import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Forms/Select Filter',
  component: 'road-select-filter',
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus'],
    },
  },
  argTypes: {
    ' ': {
      description: "Input element for the widget, it should be a road-input element.",
      control: 'text',
    },
    'only-select': {
      description: "If `false`, the user can submit custom value",
      control: 'boolean',
    },
    'is-active': {
      description: "If `true`, add class Active",
      control: 'boolean',
    },
    parameters: {
      description: "Options to pass to the fuse.js instance.\nSee https://fusejs.io/api/options.html for valid options",
      control: 'object',
    },
    roadselected: {
      action: 'roadselected',
      control: {
        type: null,
      },
    },
    '--margin-top': {
      description: "margin top of the select-filter (should equal the height of the field)",
      table: {
        defaultValue: { summary: '3rem' },
      },
      control: {
        type: null,
      },
    },
  },
  args: {
    options: [
      { value: 'audi', label: 'Audi' },
      { value: 'bmw', label: 'BMW' },
      { value: 'citroen', label: 'Citroën' },
      { value: 'mercedes-benz', label: 'Mercedes-benz' },
      { value: 'peugeot', label: 'Peugeot' },
      { value: 'renault', label: 'Renault' }
    ],
    'only-select': undefined,
    parameters: {
      keys: ["value", "label"],
      threshold: 0.3,
      ignoreLocation: true,
    },
    ' ': `<road-input-group>
    <road-input input-id="brand-input" sizes="xl" label="Brand"></road-input>
    <label slot="append" for="brand-label" aria-label="select">
      <road-icon name="arrow-drop" focusable="false" aria-hidden="true" role="button"></road-icon>
    </label>
  </road-input-group>`,
  },
};

const Template = (args) => html`
<road-select-filter style="--margin-top: 3.5rem;" .options=${args.options} only-select="${ifDefined(args['only-select'])}" is-active="${ifDefined(args['is-active'])}" @roadselected=${event => args.roadselected(event.detail)}>
  ${unsafeHTML(args[' '])}
</road-select-filter>
`;

export const Playground = Template.bind({});

export const WithValue = Template.bind({});
WithValue.args = {
  ' ': `<road-input-group>
    <road-input input-id="brand2" sizes="xl" label="Brand" value="Audi"></road-input>
    <label slot="append" for="brand2">
      <road-icon name="arrow-drop" focusable="false" role="button"></road-icon>
    </label>
  </road-input-group>`,
};

export const Disabled = Template.bind({});
Disabled.args = {
  ' ': `<road-input-group>
    <road-input input-id="brand3" sizes="xl" label="Brand" disabled></road-input>
    <label slot="append" for="brand3">
      <road-icon name="arrow-drop" focusable="false" role="button"></road-icon>
    </label>
  </road-input-group>`,
};

export const Error = Template.bind({});
Error.args = {
  ' ': `<road-input-group>
    <road-input input-id="brand4" sizes="xl" label="Brand" error="Select a Brand"></road-input>
    <label slot="append" for="brand4">
      <road-icon name="arrow-drop" focusable="false" role="button"></road-icon>
    </label>
  </road-input-group>`,
};

export const withHTML = Template.bind({});
withHTML.args = {
  options: [
    {
      "value": 31,
      "label": "<b>Original equipment electronics</b> > Cleaning Injection",
    },
    {
      "value": 4,
      "label": "<b>Exhaust / Pollution</b> > Replacement EGR",
    },
    {
      "value": 14,
      "label": "<b>Drain and filter</b> > Oil filter + fuel filter",
    }
  ],
  ' ': `<road-input-group>
    <road-input input-id="controls" sizes="xl" label="Controls" class="mb-0"></road-input>
    <label slot="append" for="brand">
      <road-icon name="arrow-drop" focusable="false" role="button"></road-icon>
    </label>
  </road-input-group>`,
};

export const PhoneNumber = Template.bind({});
PhoneNumber.args = {
  options: [
    {
      "value": "France",
      "label": "<road-icon name='flag-france' size='md' style='margin-right: 0.75rem'></road-icon>France",
    },
    {
      "value": "Spain",
      "label": "<road-icon name='flag-spain' size='md' style='margin-right: 0.75rem'></road-icon>Spain",
    },
    {
      "value": "Portugal",
      "label": "<road-icon name='flag-portugal' size='md' style='margin-right: 0.75rem'></road-icon>Portugal",
    }
  ],
  ' ': `<road-input-group class="phone-number-group">
    <road-input input-id="brand5" sizes="xl" label="Label" value="" class="phone-number" type="tel"></road-input>
    <label slot="prepend" for="brand5" class="pl-16 phone-number-label">
      <road-icon name="flag-france" size="md" class="phone-number-label-icon"></road-icon>
      <road-icon name="arrow-drop" focusable="false" role="button"></road-icon>
    </label>
  </road-input-group>`,
};
