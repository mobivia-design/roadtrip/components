import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-select-filter', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-select-filter>
          <road-input-group>
            <road-input label="Brand" class="mb-0"></road-input>
            <label slot="append" class="input-group-text" for="brand">
              <road-icon name="arrow-drop" focusable="false"></road-icon>
            </label>
          </road-input-group>
        </road-select-filter>
      `,
    });

    await page.$eval('road-select-filter', (elm: any) => {
      elm.options = [
        { value: 'audi', label: 'audi' },
        { value: 'bmw', label: 'bmw' },
        { value: 'citroen', label: 'citroën' },
        { value: 'mercedes-benz', label: 'mercedes-benz' },
        { value: 'peugeot', label: 'peugeot' },
        { value: 'renault', label: 'renault' }
      ];
    });

    await page.waitForChanges();
  });

  it('should render', async () => {

    const element = await page.find('road-select-filter');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with value', async () => {

    const element = await page.find('road-select-filter');
    expect(element).toHaveClass('hydrated');

    const input = await page.find('road-input');
    input.setProperty('value', 'audi');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render disabled', async () => {

    const element = await page.find('road-select-filter');
    expect(element).toHaveClass('hydrated');

    const input = await page.find('road-input');
    input.setProperty('disabled', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with error', async () => {

    const element = await page.find('road-select-filter');
    expect(element).toHaveClass('hydrated');

    const input = await page.find('road-input');
    input.setProperty('error', 'Select a Brand');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
