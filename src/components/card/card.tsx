import { Component, h, Prop, Event, EventEmitter, Element, Host } from '@stencil/core';

/**
 * @slot  - Content of the card.
 *
 * @part native - The native HTML button, anchor, or div element that wraps all child elements.
 */

@Component({
  tag: 'road-card',
  styleUrl: 'card.css',
  shadow: true,
})
export class Card {

  @Element() el!: HTMLRoadCardElement;

    /**
   * The elevation.
   */
    @Prop() elevation?: 'none' | 'lowest' | 'average' = 'none';

  /**
   * If `true`, a button tag will be rendered and the card will be tappable.
   */
  @Prop() button: boolean = false;

  /**
   * value of the card
   */
  @Prop() value?: string;

  /**
   * The type of the button. Only used when an `onclick` or `button` property is present.
   */
  @Prop() type: 'submit' | 'reset' | 'button' = 'button';

  /**
   * This attribute instructs browsers to download a URL instead of navigating to
   * it, so the user will be prompted to save it as a local file. If the attribute
   * has a value, it is used as the pre-filled file name in the Save prompt
   * (the user can still change the file name if they want).
   */
  @Prop() download?: string;

  /**
   * Contains a URL or a URL fragment that the hyperlink points to.
   * If this property is set, an anchor tag will be rendered.
   */
  @Prop() href?: string;

  /**
   * Specifies the relationship of the target object to the link object.
   * The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).
   */
  @Prop() rel?: string;

  /**
   * Specifies where to display the linked URL.
   * Only applies when an `href` is provided.
   * Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.
   */
  @Prop() target?: string;

  /**
   * Emitted when the card is clicked and send the `value` of the card
   */
  @Event() roadcardclick!: EventEmitter<{
    value: string | undefined | null;
    label: string | undefined | null;
  }>;
  /** @internal */
  @Event() roadCardClick!: EventEmitter<{
    value: string | undefined | null;
    label: string | undefined | null;
  }>;

  private isClickable(): boolean {
    return (this.href !== undefined || this.button);
  }

  private onClick = () => {
    if(this.value !== undefined) {
      this.roadcardclick.emit({
        value: this.value,
        label: this.el.querySelector('road-label')?.textContent,
      });
      this.roadCardClick.emit({
        value: this.value,
        label: this.el.querySelector('road-label')?.textContent,
      });
    }
  };

  private renderCard() {
    const clickable = this.isClickable();

    if (!clickable) {
     
      
      return [
        <slot/>
      ];
      
    }
    const { href } = this;
    const TagType = clickable ? (href === undefined ? 'button' : 'a') : 'div' as any;
    const attrs = (TagType === 'button')
      ? { type: this.type }
      : {
        download: this.download,
        href: this.href,
        rel: this.rel,
        target: this.target,
      };

    return (
      
      <TagType
        {...attrs}
        class="card-native"
        part="native"
        onClick={this.onClick}
      >
        <slot/>
      </TagType>
    );
  }

  render() {
    const { elevation } = this;
    const elevationClass = this.elevation ? `card-elevation-${elevation}` : ``;
    return (
      <Host class={`${elevationClass}`}>
      {this.renderCard()}
      </Host>
    );
  }

}
