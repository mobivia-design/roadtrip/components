# road-card



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute   | Description                                                                                                                                                                                                                                                                               | Type                                           | Default     |
| ----------- | ----------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- | ----------- |
| `button`    | `button`    | If `true`, a button tag will be rendered and the card will be tappable.                                                                                                                                                                                                                   | `boolean`                                      | `false`     |
| `download`  | `download`  | This attribute instructs browsers to download a URL instead of navigating to it, so the user will be prompted to save it as a local file. If the attribute has a value, it is used as the pre-filled file name in the Save prompt (the user can still change the file name if they want). | `string \| undefined`                          | `undefined` |
| `elevation` | `elevation` | The elevation.                                                                                                                                                                                                                                                                            | `"average" \| "lowest" \| "none" \| undefined` | `'none'`    |
| `href`      | `href`      | Contains a URL or a URL fragment that the hyperlink points to. If this property is set, an anchor tag will be rendered.                                                                                                                                                                   | `string \| undefined`                          | `undefined` |
| `rel`       | `rel`       | Specifies the relationship of the target object to the link object. The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).                                                                                                    | `string \| undefined`                          | `undefined` |
| `target`    | `target`    | Specifies where to display the linked URL. Only applies when an `href` is provided. Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.                                                                                                                                       | `string \| undefined`                          | `undefined` |
| `type`      | `type`      | The type of the button. Only used when an `onclick` or `button` property is present.                                                                                                                                                                                                      | `"button" \| "reset" \| "submit"`              | `'button'`  |
| `value`     | `value`     | value of the card                                                                                                                                                                                                                                                                         | `string \| undefined`                          | `undefined` |


## Events

| Event           | Description                                                       | Type                                                                                       |
| --------------- | ----------------------------------------------------------------- | ------------------------------------------------------------------------------------------ |
| `roadcardclick` | Emitted when the card is clicked and send the `value` of the card | `CustomEvent<{ value: string \| null \| undefined; label: string \| null \| undefined; }>` |


## Slots

| Slot | Description          |
| ---- | -------------------- |
|      | Content of the card. |


## Shadow Parts

| Part       | Description                                                                   |
| ---------- | ----------------------------------------------------------------------------- |
| `"native"` | The native HTML button, anchor, or div element that wraps all child elements. |


## Dependencies

### Used by

 - [road-duration](../duration)

### Graph
```mermaid
graph TD;
  road-duration --> road-card
  style road-card fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
