import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Listing/Card',
  component: 'road-card',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
    actions: {
      handles: ['roadcardclick'],
    },
  },
  argTypes: {
    elevation: {
      options: ['none', 'lowest', 'average'],
      control: { type: 'select' },
      description: 'Defines the elevation (shadow effect) of the card.',
      table: {
        defaultValue: { summary: 'none' },
      },
    },
  },
  args: {
    elevation: 'none',
  },
};

const Template = (args) => html`
<road-grid>
  <road-row>
    <road-col class="col-4 col-md-3 col-lg-2">
      <road-card button value="125" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-label>125</road-label>
      </road-card>
    </road-col>
    <road-col class="col-4 col-md-3 col-lg-2">
      <road-card button value="135" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-label>135</road-label>
      </road-card>
    </road-col>
    <road-col class="col-4 col-md-3 col-lg-2">
      <road-card button value="145" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-label>145</road-label>
      </road-card>
    </road-col>
    <road-col class="col-4 col-md-3 col-lg-2">
      <road-card button value="155" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-label>155</road-label>
      </road-card>
    </road-col>
    <road-col class="col-4 col-md-3 col-lg-2">
      <road-card button value="165" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-label>165</road-label>
      </road-card>
    </road-col>
    <road-col class="col-4 col-md-3 col-lg-2">
      <road-card button value="175" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-label>175</road-label>
      </road-card>
    </road-col>
  </road-row>
</road-grid>
`;

export const Value = Template.bind({});
Value.args = {
  elevation: 'average',
};

export const Vehicle = (args) => html`
<road-grid>
  <road-row>
    <road-col class="col-6 col-md-4 col-lg-3">
      <road-card button value="car" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-icon name="vehicle-car" size="3x" role="img"></road-icon>
        <road-label>Car</road-label>
      </road-card>
    </road-col>
    <road-col class="col-6 col-md-4 col-lg-3">
      <road-card button value="4x4" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-icon name="vehicle-suv" size="3x" role="img"></road-icon>
        <road-label>4x4</road-label>
      </road-card>
    </road-col>
    <road-col class="col-6 col-md-4 col-lg-3">
      <road-card button value="truck" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-icon name="vehicle-pickup-van" size="3x" role="img"></road-icon>
        <road-label>Truck</road-label>
      </road-card>
    </road-col>
    <road-col class="col-6 col-md-4 col-lg-3">
      <road-card button value="bike" elevation="${ifDefined(args.elevation)}" @roadcardclick=${args.roadcardclick}>
        <road-icon name="vehicle-moto" size="3x" role="img"></road-icon>
        <road-label>Bike</road-label>
      </road-card>
    </road-col>
  </road-row>
</road-grid>
`;
Vehicle.args = {
  elevation: 'average',
};
