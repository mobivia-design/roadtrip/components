import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-card', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-card button value="car">
          <road-icon name="vehicle-car" size="3x"></road-icon>
          <road-label>Car</road-label>
        </road-card>
      `,
    });
  });

  it('should render', async () => {

    const element = await page.find('road-card');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should send event when clicked', async () => {

    const roadcardclick = await page.spyOnEvent('roadcardclick');

    await page.click('road-card');

    await page.waitForChanges();

    expect(roadcardclick).toHaveReceivedEventDetail({
      value: 'car',
      label: 'Car'
    });
  });
});
