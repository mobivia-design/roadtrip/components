import { Component, h } from '@stencil/core';

/**
 * @slot  - Content to add row components.
 */

@Component({
  tag: 'road-grid',
  styleUrl: 'grid.css',
  shadow: true,
})
export class Grid {

  render() {
    return (
      <slot/>
    );
  }

}
