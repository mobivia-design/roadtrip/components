# road-grid

The grid is a powerful mobile-first flexbox system for building custom layouts.

It is composed of three units — a grid, [row(s)](../row) and [column(s)](../col). Columns will expand to fill the row, and will resize to fit additional columns. It is based on a 12 column layout with different breakpoints based on the screen size. The number of columns can be customized using CSS.

<!-- Auto Generated Below -->


## Slots

| Slot | Description                    |
| ---- | ------------------------------ |
|      | Content to add row components. |


## Dependencies

### Used by

 - [road-duration](../duration)

### Graph
```mermaid
graph TD;
  road-duration --> road-grid
  style road-grid fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
