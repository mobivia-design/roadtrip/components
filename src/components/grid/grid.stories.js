import { html } from 'lit';

export default {
  title: 'Layout/Grid',
  component: 'road-grid',
  subcomponents: {
    'road-row': 'road-row',
    'road-col': 'road-col',
  },
  parameters: {
    docs: {
      description: {
        component: 'The grid is a powerful mobile-first flexbox system for building custom layouts. It is composed of three units — a `grid`, `row(s)` and `column(s)`. Columns will expand to fill the row, and will resize to fit additional columns. It is based on a 12 column layout with different breakpoints based on the screen size. The number of columns can be customized using CSS.'
      },
    },
  },
};

export const Grid = () => html`
  <road-grid>
  <road-row>
    <road-col>
      <div>1 of 2</div>
    </road-col>
    <road-col>
      <div>2 of 2</div>
    </road-col>
  </road-row>
  <road-row>
    <road-col>
      <div>1 of 3</div>
    </road-col>
    <road-col>
      <div>2 of 3</div>
    </road-col>
    <road-col>
      <div>3 of 3</div>
    </road-col>
  </road-row>
  <road-row>
    <road-col>
      <div>1 of 3</div>
    </road-col>
    <road-col col-6>
      <div>2 of 3</div>
    </road-col>
    <road-col>
      <div>3 of 3</div>
    </road-col>
  </road-row>
  <road-row>
    <road-col col-6>
      <div>1 of 3</div>
    </road-col>
    <road-col>
      <div>2 of 3</div>
    </road-col>
    <road-col>
      <div>3 of 3</div>
    </road-col>
  </road-row>
  <road-row>
    <road-col>
      <div>1 of 2</div>
    </road-col>
    <road-col>
      <div>2 of 2</div>
    </road-col>
  </road-row>
  <road-row>
    <road-col>
      <div>1 of 2</div>
    </road-col>
    <road-col>
      <div>2 of 2</div>
    </road-col>
  </road-row>
  <road-row>
    <road-col>
      <div>1 of 4</div>
    </road-col>
    <road-col>
      <div>2 of 4</div>
    </road-col>
    <road-col>
      <div>3 of 4</div>
    </road-col>
    <road-col>
      <div>4 of 4</div>
    </road-col>
  </road-row>
</road-grid>
`;
