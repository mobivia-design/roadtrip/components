import { Component, Host, h, Element, Prop, Event, EventEmitter, Listen, Watch } from '@stencil/core';

/**
 * @slot  - Content of the item, it should be road-icon and road-label elements.
 *
 * @part native - The native HTML anchor element that wraps all child elements.
 */

@Component({
  tag: 'road-navbar-item',
  styleUrl: 'navbar-item.css',
  shadow: true,
})
export class NavbarItem {

  @Element() el!: HTMLRoadNavbarItemElement;

  /**
   * If `true`, the user cannot interact with the tab button.
   */
  @Prop() disabled = false;

  /**
   * This attribute instructs browsers to download a URL instead of navigating to
   * it, so the user will be prompted to save it as a local file. If the attribute
   * has a value, it is used as the pre-filled file name in the Save prompt
   * (the user can still change the file name if they want).
   */
  @Prop() download: string | undefined;

  /**
   * Contains a URL or a URL fragment that the hyperlink points to.
   * If this property is set, an anchor tag will be rendered.
   */
  @Prop() href: string | undefined;

  /**
   * Specifies the relationship of the target object to the link object.
   * The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).
   */
  @Prop() rel: string | undefined;

  /**
   * The selected tab component
   */
  @Prop({ mutable: true }) selected = false;

  /**
   * A tab id must be provided for each `road-tab`. It's used internally to reference
   * the selected tab.
   */
  @Prop() tab?: string;

  /**
   * Specifies where to display the linked URL.
   * Only applies when an `href` is provided.
   * Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.
   */
  @Prop() target: string | undefined;

  /**
   * Emitted when the tab bar is clicked
   * @internal
   */
  @Event() roadnavbaritemclick!: EventEmitter;
  /** @internal */
  @Event() roadNavbarItemClick!: EventEmitter;

  @Listen('roadNavbarChanged', { target: 'window' })
  @Listen('roadnavbarchanged', { target: 'window' })
  onNavbarChanged(ev: CustomEvent) {
    this.selected = this.tab === ev.detail.tab;
  }

  /**
   * Watch for changes to `selected` and move focus to this element if `selected` is true.
   */
  @Watch('selected')
  handleSelectedChange(newValue: boolean) {
    if (newValue) {
      this.el.focus();
    }
  }

  private selectTab(ev: Event | KeyboardEvent) {
    if (this.tab !== undefined) {
      if (!this.disabled) {
        this.roadnavbaritemclick.emit({
          tab: this.tab,
          href: this.href,
          selected: this.selected,
        });
        this.roadNavbarItemClick.emit({
          tab: this.tab,
          href: this.href,
          selected: this.selected,
        });
      }
      if(this.href === undefined) {
        ev.preventDefault();
      }
    }
  }

  private onKeyUp = (ev: KeyboardEvent) => {
    if (ev.key === 'Enter' || ev.key === ' ') {
      this.selectTab(ev);
    }
  };

  private onClick = (ev: Event) => {
    this.selectTab(ev);
  };

  render() {
    const { href, rel, target, selected, tab, disabled } = this;
    const attrs = {
      download: this.download,
      href,
      rel,
      target,
    };

    return (
      <Host
        onClick={this.onClick}
        onKeyup={this.onKeyUp}
        role="menuitem"
        tabindex="0"
        aria-selected={selected ? 'true' : null}
        id={tab !== undefined ? `navbar-item-${tab}` : null}
        disabled={disabled}
        class={{
          'navbar-item': true,
          'tab-selected': selected,
          'tab-disabled': disabled,
        }}
      >
        <a {...attrs} tabIndex={-1} class="button-native" part="native">
          <span class="button-inner">
            <slot/>
          </span>
        </a>
      </Host>
    );
  }

}
