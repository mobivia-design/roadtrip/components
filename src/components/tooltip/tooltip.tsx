import { Component, Host, h, Prop, Method, Element } from '@stencil/core';

/**
 * @slot tooltip-content - content of the tooltip, if you need to add html content, use this slot, otherwise use directly the content prop.
 *
 * @part tooltip - tooltip container
 */

@Component({
  tag: 'road-tooltip',
  styleUrl: 'tooltip.css',
  shadow: true,
})
export class Tooltip {

  @Element() el!: HTMLRoadTooltipElement;

  /**
   * The id of tooltip
   */
  @Prop() tooltipId: string = `road-tooltip-${++tooltipIds}`;

  /**
   * The tooltip's content.
   */
  @Prop() content = '';

  /**
   * The position of the tooltip.
   */
  @Prop() position:
    | 'top'
    | 'right'
    | 'bottom'
    | 'top-left'
    | 'top-right'
    | 'bottom-left'
    | 'bottom-right'
    | 'left' = 'top';

  /**
   * The content align of the tooltip.
   */
  @Prop() contentAlign:
    | 'center'
    | 'left' = 'center';

  /**
   * Indicates whether or not the tooltip is open. You can use this or the open/close methods.
   */
  @Prop({ mutable: true }) isOpen = false;

  /**
   * Controls how the tooltip is activated. Possible options include `click`, `hover`, `focus`. Multiple
   * options can be passed by separating them with a space. When manual is used, the tooltip must be activated
   * programmatically.
   */
  @Prop() trigger: string = 'hover';

  /**
   * Open the tooltip.
   */
  @Method()
  async open() {
    this.isOpen = true;
  }

  /**
   * Close the tooltip.
   */
  @Method()
  async close() {
    this.isOpen = false;
  }

  private onClick = () => {
    if (this.hasTrigger('click')) {
      this.isOpen ? this.close() : this.open();
    }
  };

  private onMouseOver = () => {
    if (this.hasTrigger('hover')) {
      this.open();
    }
  };

  private onMouseOut = () => {
    if (this.hasTrigger('hover')) {
      this.close();
    }
  };

private isKeyboardNavigation = false;

// Écoute les événements pour détecter le mode d'interaction
componentDidMount() {
  document.addEventListener('keydown', this.handleKeyDown);
  document.addEventListener('mousedown', this.handleMouseDown);
}

componentDidLoad() {
  document.addEventListener('keydown', this.handleKeyDown);
  document.addEventListener('mousedown', this.handleMouseDown);
}

componentWillUnmount() {
  document.removeEventListener('keydown', this.handleKeyDown);
  document.removeEventListener('mousedown', this.handleMouseDown);
}


private handleKeyDown = () => {
  this.isKeyboardNavigation = true;
};

private handleMouseDown = () => {
  this.isKeyboardNavigation = false;
};

private onFocus = () => {
  if (this.isKeyboardNavigation && this.hasTrigger('hover')) {
    this.open();
  }
};


  private onBlur = () => {
    if (this.isKeyboardNavigation && this.hasTrigger('hover')) {
      this.close();
    }
  };

  private hasTrigger(triggerType: string) {
    const triggers = this.trigger.split(' ');
    return triggers.includes(triggerType);
  }

  render() {
    return (
      <Host
        onMouseOver={this.onMouseOver}
        onMouseOut={this.onMouseOut}
        onClick={this.onClick}
        onFocus={this.onFocus}
        onBlur={this.onBlur}
        tabindex="0"  // Makes the element focusable
        data-tooltip-position={this.position}
      >
        <slot aria-describedby={this.tooltipId} />

        <div
          part="tooltip"
          id={this.tooltipId}
          class={{
            tooltip: true,
            'tooltip-open': this.isOpen,
          }}
          role="tooltip"
          aria-hidden={!this.isOpen ? 'true' : 'false'}
        >
          {this.content}
          <slot name="tooltip-content" />
        </div>
      </Host>
    );
  }
}

let tooltipIds = 0;
