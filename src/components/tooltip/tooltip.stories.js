import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Indicators/Tooltip',
  component: 'road-tooltip',
};

export const Playground = (args) => html`
<road-tooltip 
  is-open="${ifDefined(args['is-open'])}" 
  tooltip-id="${ifDefined(args['tooltip-id'])}" 
  content="${ifDefined(args.content)}" 
  position="${ifDefined(args.position)}" 
  trigger="${ifDefined(args.trigger)}"
  contentalign="${ifDefined(args.contentAlign)}">
  <road-icon name="alert-info" color="secondary" role="button"></road-icon>
  ${unsafeHTML(args['tooltip-content'])}
</road-tooltip>
`;
Playground.args = {
  content: 'Fit & small tooltip',
  'is-open': null,
  position: 'right',
  trigger: 'hover',
  contentAlign: 'center',
};
Playground.argTypes = {
  'is-open': {
    description: "Indicates whether or not the tooltip is open. You can use this or the open/close methods.",
    control: 'boolean',
  },
  position: {
    description: "The position of the tooltip.",
    options: ['top', 'left', 'bottom', 'right', 'top-left', 'top-right', 'bottom-left', 'bottom-right'],
    control: {
      type: 'radio',
    },
  },
  contentAlign: {
    description: "The content align of the tooltip.",
    options: ['center', 'left'],
    control: {
      type: 'radio',
    },
  },
  content: {
    description: "The tooltip's content.",
    control: 'text',
  },
  trigger: {
    description: "Controls how the tooltip is activated. Possible options include `click`, `hover`, `focus`. Multiple\noptions can be passed by separating them with a space. When manual is used, the tooltip must be activated\nprogrammatically.",
    options: ['hover', 'click', 'focus'],
    control: {
      type: 'select',
    },
  },
  'tooltip-id': {
    description: "The id of tooltip",
    control: 'text',
  },
  'tooltip-content': {
    description: "content of the tooltip, if you need to add html content, use this slot, otherwise use directly the content prop.",
    control: 'text',
  },
  open : {
    description: 'Open the tooltip.',
    table: {
      category: 'methods',
      type: {
        summary: 'void',
      },
    },
    defaultValue: {
      summary: null,
    },
  },
  close : {
    description: 'Close the tooltip.',
    table: {
      category: 'methods',
      type: {
        summary: 'void',
      },
      defaultValue: {
        summary: null,
      },
    },
  },
  '--max-width': {
    description: "The maximum width of the tooltip.",
    table: {
      defaultValue: { summary: 'auto' },
    },
    control: {
      type: null,
    },
  },
  '--z-index': {
    description: "The z-index of the tooltip.",
    table: {
      defaultValue: { summary: '1' },
    },
    control: {
      type: null,
    },
  },
  '--width': {
    description: "width of the tooltip.",
    table: {
      defaultValue: { summary: 'auto' },
    },
    control: {
      type: null,
    },
  },
  'tooltip': {
    description: "tooltip container",
    control: {
      type: null,
    },
  },
};

export const MouseOver = () => html`
<road-tooltip position="bottom" style="width: 100%;--width: 95%;--max-width: 100%;">
  <road-item class="bg-light mb-16">
    <road-avatar class="flex-shrink-0 mr-16">
      <road-icon lazy size="3x" name="brake" role="button"></road-icon>
    </road-avatar>
    <road-label>
      <p class="h9 mb-8">Braking</p>
      <p class="m-0">(6 skills)</p>
    </road-label>
  </road-item>
  <div class="text-content text-left mb-0" slot="tooltip-content">
    Attelage<br/>
    Vidange<br/>
    Filtres<br/>
  </div>
</road-tooltip>
`;

export const Click = () => html`
<road-tooltip content="Fit & small tooltip" position="right" trigger="click">
  <road-button class="mb-0">
    <road-icon name="alert-info" color="secondary" role="button"></road-icon>
  </road-button>
</road-tooltip>
`;
