import { newE2EPage } from '@stencil/core/testing';

describe('road-tooltip', () => {
  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-tooltip content="Fit & small tooltip" position="right" trigger="click">
          <road-button class="mb-0">
            <road-icon name="alert-info" color="secondary"></road-icon>
          </road-button>
        </road-tooltip>
      `,
    });
  });

  it('it should renders', async () => {
    const element = await page.find('road-tooltip');
    expect(element).toHaveClass('hydrated');
  });
});
