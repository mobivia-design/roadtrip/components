# road-tooltip



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                                                                                                                                                                                     | Type                                                                                                     | Default                              |
| -------------- | --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------- | ------------------------------------ |
| `content`      | `content`       | The tooltip's content.                                                                                                                                                                                                          | `string`                                                                                                 | `''`                                 |
| `contentAlign` | `content-align` | The content align of the tooltip.                                                                                                                                                                                               | `"center" \| "left"`                                                                                     | `'center'`                           |
| `isOpen`       | `is-open`       | Indicates whether or not the tooltip is open. You can use this or the open/close methods.                                                                                                                                       | `boolean`                                                                                                | `false`                              |
| `position`     | `position`      | The position of the tooltip.                                                                                                                                                                                                    | `"bottom" \| "bottom-left" \| "bottom-right" \| "left" \| "right" \| "top" \| "top-left" \| "top-right"` | `'top'`                              |
| `tooltipId`    | `tooltip-id`    | The id of tooltip                                                                                                                                                                                                               | `string`                                                                                                 | `` `road-tooltip-${++tooltipIds}` `` |
| `trigger`      | `trigger`       | Controls how the tooltip is activated. Possible options include `click`, `hover`, `focus`. Multiple options can be passed by separating them with a space. When manual is used, the tooltip must be activated programmatically. | `string`                                                                                                 | `'hover'`                            |


## Methods

### `close() => Promise<void>`

Close the tooltip.

#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`

Open the tooltip.

#### Returns

Type: `Promise<void>`




## Slots

| Slot                | Description                                                                                                      |
| ------------------- | ---------------------------------------------------------------------------------------------------------------- |
| `"tooltip-content"` | content of the tooltip, if you need to add html content, use this slot, otherwise use directly the content prop. |


## Shadow Parts

| Part        | Description       |
| ----------- | ----------------- |
| `"tooltip"` | tooltip container |


## CSS Custom Properties

| Name          | Description                       |
| ------------- | --------------------------------- |
| `--max-width` | The maximum width of the tooltip. |
| `--width`     | width of the tooltip.             |
| `--z-index`   | The z-index of the tooltip.       |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
