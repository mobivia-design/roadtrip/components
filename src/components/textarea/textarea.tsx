import { Component, Event, EventEmitter, Host, Prop, Watch, h } from '@stencil/core';

@Component({
  tag: 'road-textarea',
  styleUrl: 'textarea.css',
  scoped: true,
})
export class Textarea {

  /**
   * The id of textarea
   */
  @Prop() textareaId: string = `road-textarea-${textareaIds++}`;

  /**
   * Indicates whether and how the text value should be automatically capitalized as it is entered/edited by the user.
   */
  @Prop() autocapitalize: string = 'none';

  /**
   * This Boolean attribute lets you specify that a form control should have focus when the page loads.
   */
  @Prop() autofocus: boolean = false;

  /**
   * If `true`, the user cannot interact with the textarea.
   */
  @Prop() disabled: boolean = false;

  /**
   * A hint to the browser for which keyboard to display.
   * This attribute applies when the value of the type attribute is `"text"`, `"password"`, `"email"`, or `"url"`. Possible values are: `"verbatim"`, `"latin"`, `"latin-name"`, `"latin-prose"`, `"full-width-latin"`, `"kana"`, `"katakana"`, `"numeric"`, `"tel"`, `"email"`, `"url"`.
   */
  @Prop() inputmode?: 'none' | 'text' | 'tel' | 'url' | 'email' | 'numeric' | 'decimal' | 'search';

  /**
   * A hint to the browser for which enter key to display.
   * Possible values: `"enter"`, `"done"`, `"go"`, `"next"`,
   * `"previous"`, `"search"`, and `"send"`.
   */
  @Prop() enterkeyhint?: 'enter' | 'done' | 'go' | 'next' | 'previous' | 'search' | 'send';

  /**
   * If the value of the type attribute is `text`, `email`, `search`, `password`, `tel`, or `url`, this attribute specifies the maximum number of characters that the user can enter.
   */
  @Prop() maxlength?: number;

  /**
   * If the value of the type attribute is `text`, `email`, `search`, `password`, `tel`, or `url`, this attribute specifies the minimum number of characters that the user can enter.
   */
  @Prop() minlength?: number;

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.textareaId;

  /**
   * Instructional text that shows before the textarea has a value.
   */
  @Prop() placeholder?: string;

  /**
   * The sizes of the input.
   */
   @Prop() sizes: 'lg' | 'xl' = 'lg';

  /**
   * If `true`, the user cannot modify the value.
   */
  @Prop() readonly: boolean = false;

  /**
   * If `true`, the user must fill in a value before submitting a form.
   */
  @Prop() required: boolean = false;

  /**
   * If `true`, the element will have its spelling and grammar checked.
   */
  @Prop() spellcheck: boolean = false;

    /**
   * If `false`, to disabled resize.
   */
    @Prop() resize: boolean = true;

  /**
   * The visible width of the text control, in average character widths. If it is specified, it must be a positive integer.
   */
  @Prop() cols?: number;

  /**
   * The number of rows of the control.
   */
  @Prop() rows?: number;

  /**
   * Indicates how the control wraps text.
   */
  @Prop() wrap?: 'hard' | 'soft' | 'off';

  /**
   * The value of the textarea.
   */
  @Prop({ mutable: true }) value?: string | null = '';

  /**
   * Label for the field
   */
  @Prop() label: string = `${this.textareaId}-label`;

  /**
   * Error message for the field
   */
  @Prop() error?: string;

  /**
   * Helper message for the field
   */
  @Prop() helper?: string;

  /**
   * Update the native textarea element when the value changes
   */
  @Watch('value')
  protected valueChanged() {
    this.roadchange.emit({ value: this.value });
    this.roadChange.emit({ value: this.value });
  }

  /**
   * Emitted when a keyboard input occurred.
   */
  @Event() roadinput!: EventEmitter<KeyboardEvent>;
  /** @internal */
  @Event() roadInput!: EventEmitter<KeyboardEvent>;

  /**
   * Emitted when the value has changed.
   */
  @Event() roadchange!: EventEmitter<{
    value: string | undefined | null
  }>;
  /** @internal */
  @Event() roadChange!: EventEmitter<{
    value: string | undefined | null
  }>;

  /**
   * Emitted when the textarea loses focus.
   */
  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;

  /**
   * Emitted when the textarea has focus.
   */
  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  private getValue(): string {
    return this.value || '';
  }

  private onInput = (ev: Event) => {
    const input = ev.target as HTMLInputElement | null;
    if (input) {
      this.value = input.value || '';
    }
    this.roadinput.emit(ev as KeyboardEvent);
    this.roadInput.emit(ev as KeyboardEvent);
  };

  private onBlur = () => {
    this.roadblur.emit();
    this.roadBlur.emit();
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  render() {
    const value = this.getValue();
    const labelId = this.textareaId + '-label';
    const hasValueClass = this.value !== '' ? 'has-value' : '';
    const noResizeClass = this.resize == false ? 'no-resize' : '';
    const isInvalidClass = this.error !== undefined && this.error !== '' ? 'is-invalid' : '';

    return (
      <Host aria-disabled={this.disabled ? 'true' : null} class={this.sizes && `input-${this.sizes}`}>
        <textarea
          class={`form-control textarea-control ${hasValueClass} ${noResizeClass} ${isInvalidClass}`}
          id={this.textareaId}
          aria-disabled={this.disabled ? 'true' : null}
          aria-labelledby={labelId}
          disabled={this.disabled}
          autoCapitalize={this.autocapitalize}
          autoFocus={this.autofocus}
          enterKeyHint={this.enterkeyhint}
          inputMode={this.inputmode}
          maxLength={this.maxlength}
          minLength={this.minlength}
          name={this.name}
          placeholder={this.placeholder}
          readOnly={this.readonly}
          required={this.required}
          value={value}
          spellcheck={this.spellcheck}
          cols={this.cols}
          rows={this.rows}
          wrap={this.wrap}
          onInput={this.onInput}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
        >
        </textarea>
        <label class="form-label" id={labelId} htmlFor={this.textareaId}>{this.label}</label>
        {this.error && this.error !== '' && <p class="invalid-feedback">{this.error}</p>}
        {this.helper && this.helper !== '' && <p class="helper">{this.helper}</p>}
      </Host>
    );
  }

}

let textareaIds = 0;
