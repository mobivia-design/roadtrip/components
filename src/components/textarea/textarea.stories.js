import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Forms/Textarea',
  component: 'road-textarea',
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus'],
    },
  },
  argTypes: {
    disabled: {
      description: "If `true`, the user cannot interact with the textarea.",
      control: 'boolean',
    },
    readonly: {
      description: "If `true`, the user cannot modify the value.",
      control: 'boolean',
    },
    required: {
      description: "If `true`, the user must fill in a value before submitting a form.",
      control: 'boolean',
    },
    autofocus: {
      description: "This Boolean attribute lets you specify that a form control should have focus when the page loads.",
      control: 'boolean',
    },
    spellcheck: {
      description: "If `true`, the element will have its spelling and grammar checked.",
      control: 'boolean',
    },
    resize: {
      description: "If `false`, to disabled resize.",
      control: 'boolean',
    },
    error: {
      description: "Error message for the field",
      control: 'text',
    },
    helper: {
      description: "Helper message for the field",
      control: 'text',
    },
    label: {
      description: "Label for the field",
      control: 'text',
    },
    name: {
      description: "The name of the control, which is submitted with the form data.",
      control: 'text',
    },
    placeholder: {
      description: "Instructional text that shows before the textarea has a value.",
      control: 'text',
    },
    minlength: {
      description: "If the value of the type attribute is `text`, `email`, `search`, `password`, `tel`, or `url`, this attribute specifies the minimum number of characters that the user can enter.",
      control: 'number',
    },
    maxlength: {
      description: "If the value of the type attribute is `text`, `email`, `search`, `password`, `tel`, or `url`, this attribute specifies the maximum number of characters that the user can enter.",
      control: 'number',
    },
    value: {
      description: "The value of the textarea.",
      control: 'text',
    },
    sizes: {
      description: "The sizes of the input.",
      options: ['lg', 'xl'],
      control: {
        type: 'select',
      },
    },
    autocapitalize: {
      description: "Indicates whether and how the text value should be automatically capitalized as it is entered/edited by the user.",
      options: ['on', 'off'],
      control: {
        type: 'select',
      },
    },
    enterkeyhint: {
      description: "A hint to the browser for which enter key to display.\nPossible values: `\"enter\"`, `\"done\"`, `\"go\"`, `\"next\"`,\n`\"previous\"`, `\"search\"`, and `\"send\"`.",
      options: ['enter', 'done', 'go', 'next', 'previous', 'search', 'send'],
      control: {
        type: 'select',
      },
    },
    inputmode: {
      description: "A hint to the browser for which keyboard to display.\nThis attribute applies when the value of the type attribute is `\"text\"`, `\"password\"`, `\"email\"`, or `\"url\"`. Possible values are: `\"verbatim\"`, `\"latin\"`, `\"latin-name\"`, `\"latin-prose\"`, `\"full-width-latin\"`, `\"kana\"`, `\"katakana\"`, `\"numeric\"`, `\"tel\"`, `\"email\"`, `\"url\"`.",
      options: ['none', 'text', 'tel', 'url', 'email', 'numeric', 'decimal', 'search'],
      control: {
        type: 'select',
      },
    },
    rows: {
      description: "The number of rows of the control.",
      control: {
        type: 'number',
        min: 0,
      },
    },
    cols: {
      description: "The visible width of the text control, in average character widths. If it is specified, it must be a positive integer.",
      control: {
        type: 'number',
        min: 0,
      },
    },
    'textarea-id' : {
      description: "The id of textarea",
      control: 'text',
    },
    wrap: {
      description: "Indicates how the control wraps text.",
      options: ['hard', 'off', 'soft'],
      control: {
        type: 'select',
      },
    },
    roadblur: {
      description: "Emitted when the textarea loses focus.",
      control: {
        type: null,
      },
    },
    roadfocus: {
      description: "Emitted when the textarea has focus.",
      control: {
        type: null,
      },
    },
    roadchange: {
      description: "Emitted when the value has changed.",
      action: 'roadchange',
      control: {
        type: null,
      },
    },
    roadinput: {
      description: "Emitted when a keyboard input occurred.",
      action: 'roadinput',
      control: {
        type: null,
      },
    },
  },
  args: {
    label: 'Label',
    sizes: 'xl',
    disabled: null,
    readonly: null,
    required: null,
    autofocus: null,
    spellcheck: null,
    rows: 3,
  },
};

const Template = (args) => html`
  <road-textarea
    textarea-id="${ifDefined(args['textarea-id'])}"
    label="${ifDefined(args.label)}"
    disabled="${ifDefined(args.disabled)}"
    required="${ifDefined(args.required)}"
    autofocus="${ifDefined(args.autofocus)}"
    error="${ifDefined(args.error)}"
    helper="${ifDefined(args.helper)}"
    maxlength="${ifDefined(args.maxlength)}"
    minlength="${ifDefined(args.minlength)}"
    sizes="${ifDefined(args.sizes)}"
    value="${ifDefined(args.value)}"
    spellcheck="${ifDefined(args.spellcheck)}"
    resize="${ifDefined(args.resize)}"
    placeholder="${ifDefined(args.placeholder)}"
    autocapitalize="${ifDefined(args.autocapitalize)}"
    enterkeyhint="${ifDefined(args.enterkeyhint)}"
    inputmode="${ifDefined(args.inputmode)}"
    rows="${ifDefined(args.rows)}"
    cols="${ifDefined(args.cols)}"
    wrap="${ifDefined(args.wrap)}"
    readonly="${ifDefined(args.readonly)}"
    @roadchange=${event => args.roadchange(event.detail.value)}
    @roadinput=${event => args.roadinput(event.target.value)}
  ></road-textarea>
`;

export const Playground = Template.bind({});

export const WithValue = Template.bind({});
WithValue.args = {
  value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium pharetra semper. Fusce lacus metus, facilisis sit amet lacus ut, facilisis dapibus urna.',
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
};

export const noResize = Template.bind({});
noResize.args = {
  resize: false,
};

export const Error = Template.bind({});
Error.args = {
  error: 'This field is required',
  required: true,
};

export const Helper = Template.bind({});
Helper.args = {
  helper: 'This field is required',
};
