import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-textarea', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-textarea label="Label" rows="5"></textarea>
      `,
    });
  });

  it('should render', async () => {
    const element = await page.find('road-textarea');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should add a value', async () => {
    const roadinput = await page.spyOnEvent('roadinput');
    const roadchange = await page.spyOnEvent('roadchange');
    const roadblur = await page.spyOnEvent('roadblur');
    const roadfocus = await page.spyOnEvent('roadfocus');

    await page.focus('road-textarea textarea');
    await page.type('road-textarea textarea', 'Value');
    await page.$eval('road-textarea textarea', e => e.blur());

    expect(roadfocus).toHaveReceivedEvent();
    expect(roadblur).toHaveReceivedEvent();
    expect(roadinput).toHaveReceivedEvent();

    expect(roadchange).toHaveReceivedEventDetail({
      value: 'Value',
    });

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render disabled', async () => {
    const element = await page.find('road-textarea');
    expect(element).toHaveClass('hydrated');

    element.setProperty('disabled', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with error', async () => {
    const element = await page.find('road-textarea');
    expect(element).toHaveClass('hydrated');

    element.setProperty('error', 'This field is required');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
