# road-navbar-item



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                                                                                                                                                                                                                               | Type                  | Default     |
| ---------- | ---------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- | ----------- |
| `disabled` | `disabled` | If `true`, the user cannot interact with the tab button.                                                                                                                                                                                                                                  | `boolean`             | `false`     |
| `download` | `download` | This attribute instructs browsers to download a URL instead of navigating to it, so the user will be prompted to save it as a local file. If the attribute has a value, it is used as the pre-filled file name in the Save prompt (the user can still change the file name if they want). | `string \| undefined` | `undefined` |
| `href`     | `href`     | Contains a URL or a URL fragment that the hyperlink points to. If this property is set, an anchor tag will be rendered.                                                                                                                                                                   | `string \| undefined` | `undefined` |
| `rel`      | `rel`      | Specifies the relationship of the target object to the link object. The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).                                                                                                    | `string \| undefined` | `undefined` |
| `selected` | `selected` | The selected tab component                                                                                                                                                                                                                                                                | `boolean`             | `false`     |
| `tab`      | `tab`      | A tab id must be provided for each `road-tab`. It's used internally to reference the selected tab.                                                                                                                                                                                        | `string \| undefined` | `undefined` |
| `target`   | `target`   | Specifies where to display the linked URL. Only applies when an `href` is provided. Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.                                                                                                                                       | `string \| undefined` | `undefined` |


## Slots

| Slot | Description                                                          |
| ---- | -------------------------------------------------------------------- |
|      | Content of the item, it should be road-icon and road-label elements. |


## Shadow Parts

| Part       | Description                                                   |
| ---------- | ------------------------------------------------------------- |
| `"native"` | The native HTML anchor element that wraps all child elements. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
