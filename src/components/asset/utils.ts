import { getAssetPath } from '@stencil/core';
import { Asset } from './asset';


let CACHED_MAP: Map<string, string>;

export const getAssetMap = (): Map<string, string> => {
  if (typeof window === 'undefined') {
    return new Map();
  } else {
    if (!CACHED_MAP) {
      const win = window as any;
      win.Ionicons = win.Ionicons || {};
      CACHED_MAP = win.Ionicons.map = win.Ionicons.map || new Map();
    }
    return CACHED_MAP;
  }
};

export const addAssets = (assets: { [name: string]: string; }) => {
  const map = getAssetMap();
  Object.keys(assets).forEach(name => map.set(name, assets[name]));
};


export const getUrl = (i: Asset) => {
  let url = getSrc(i.src);
  if (url) {
    return url;
  }

  url = getName(i.name, i.asset);
  if (url) {
    return getNamedUrl(url);
  }

  if (i.asset) {
    url = getSrc(i.asset);
    if (url) {
      return url;
    }
  }

  return null;
};


const getNamedUrl = (assetName: string) => {
  const url = getAssetMap().get(assetName);
  if (url) {
    return url;
  }
  return getAssetPath(`svg/${assetName}.svg`);
};


export const getName = (
  assetName: string | undefined,
  asset: string | undefined
) => {

  if (!assetName && asset && !isSrc(asset)) {
    assetName = asset;
  }
  if (isStr(assetName)) {
    assetName = toLower(assetName);
  }

  if (!isStr(assetName) || assetName.trim() === '') {
    return null;
  }

  // only allow alpha characters and dash
  const invalidChars = assetName.replace(/[a-z]|-|\d/gi, '');
  if (invalidChars !== '') {
    return null;
  }

  return assetName;
};

export const getSrc = (src: string | undefined) => {
  if (isStr(src)) {
    src = src.trim();
    if (isSrc(src)) {
      return src;
    }
  }
  return null;
};

export const isSrc = (str: string) => str.length > 0 && /(\/|\.)/.test(str);

export const isStr = (val: any): val is string => typeof val === 'string';

export const toLower = (val: string) => val.toLowerCase();