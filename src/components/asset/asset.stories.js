import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { asset } from '../../../.storybook/asset';

export default {
  title: 'Media/Asset',
  component: 'road-asset',
};

export const Playground = ({ name, lazy }) => html`
<road-asset  
  name="${ifDefined(name)}" 
  lazy="${ifDefined(lazy)}"
></road-asset>
`;
Playground.args = {
  name: 'ecomobiliste-fr',
  lazy: null,
};
Playground.argTypes = {
  name: {
    description: "Specifies which illustration to use from the built-in set of illustrations.",
    options: asset,
    control: {
      type: 'select',
    },
  },
  lazy: {
    description: "If enabled, road-illustration will be loaded lazily when it's visible in the viewport.\nDefault, `false`.",
    control: 'boolean',
  },
  'aria-label': {
    description: "Specifies the label to use for accessibility. Defaults to the illustration name.",
    control: 'text',
  },
  'aria-hidden': {
    description: "Set the illustration to hidden, respectively `true`, to remove it from the accessibility tree.",
    control: 'text',
  },
  src: {
    description: "Specifies the exact `src` of an SVG file to use.",
    control: 'text',
  },
  sanitize: {
    description: "When set to `false`, SVG content that is HTTP fetched will not be checked\nif the response SVG content has any `<script>` elements, or any attributes\nthat start with `on`, such as `onclick`.",
    control: 'boolean',
  },
};