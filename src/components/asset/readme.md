# road-icon



<!-- Auto Generated Below -->


## Usage

### React

```javascript
import React from 'react';
import { ecomobilisteFr, ecomobilisteEs } from "@roadtrip/components/assets";

const MyComponent: React.FC = () => {
  return (
    <>
      <road-asset icon={ecomobilisteFr}></road-asset>
      <road-asset icon={ecomobilisteEs}></road-asset>
    </>
  );
};

export default MyComponent;
```


### Vue

```html
<template>
  <road-asset icon="ecomobilisteFr"></road-asset>
  <road-asset icon="ecomobilisteEs"></road-asset>
</template>

<script>
  import { ecomobilisteFr, ecomobilisteEs } from "@roadtrip/components/assets";
  import { addAssets } from "@roadtrip/assets";
  addAssets({
    ecomobilisteFr,
    ecomobilisteEs
  });
</script>
```



## Properties

| Property     | Attribute     | Description                                                                                                                                                                                   | Type                  | Default     |
| ------------ | ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- | ----------- |
| `ariaHidden` | `aria-hidden` | Set the illustration to hidden, respectively `true`, to remove it from the accessibility tree.                                                                                                | `string \| undefined` | `undefined` |
| `ariaLabel`  | `aria-label`  | Specifies the label to use for accessibility. Defaults to the illustration name.                                                                                                              | `string \| undefined` | `undefined` |
| `asset`      | `asset`       | A combination of both `name` and `src`. If a `src` url is detected it will set the `src` property. Otherwise it assumes it's a built-in named SVG and set the `name` property.                | `any`                 | `undefined` |
| `lazy`       | `lazy`        | If enabled, road-illustration will be loaded lazily when it's visible in the viewport. Default, `false`.                                                                                      | `boolean`             | `false`     |
| `name`       | `name`        | Specifies which illustration to use from the built-in set of illustrations.                                                                                                                   | `string \| undefined` | `undefined` |
| `sanitize`   | `sanitize`    | When set to `false`, SVG content that is HTTP fetched will not be checked if the response SVG content has any `<script>` elements, or any attributes that start with `on`, such as `onclick`. | `boolean`             | `true`      |
| `src`        | `src`         | Specifies the exact `src` of an SVG file to use.                                                                                                                                              | `string \| undefined` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
