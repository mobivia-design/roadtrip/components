import { Build, Component, Element, Host, Prop, State, Watch, h } from '@stencil/core';
import { getAssetSvgContent, roadassetContent } from './request';
import { getName, getUrl } from './utils';

@Component({
  tag: 'road-asset',
  assetsDirs: ['svg'],
  styleUrl: 'asset.css',
  shadow: true,
})
export class Asset {
  private io?: IntersectionObserver;

  @Element() el!: HTMLRoadAssetElement;

  @State() private assetSvgContent?: string;

  @State() private isVisible = false;



  /**
   * Specifies the label to use for accessibility. Defaults to the illustration name.
   */
  @Prop({ mutable: true, reflect: true }) ariaLabel?: string;

  /**
   * Set the illustration to hidden, respectively `true`, to remove it from the accessibility tree.
   */
  @Prop({ reflect: true }) ariaHidden?: string;

  /**
   * Specifies which illustration to use from the built-in set of illustrations.
   */
  @Prop() name?: string;

  /**
   * Specifies the exact `src` of an SVG file to use.
   */
  @Prop() src?: string;

  /**
   * A combination of both `name` and `src`. If a `src` url is detected
   * it will set the `src` property. Otherwise it assumes it's a built-in named
   * SVG and set the `name` property.
   */
  @Prop() asset?: any;

  /**
   * If enabled, road-illustration will be loaded lazily when it's visible in the viewport.
   * Default, `false`.
   */
  @Prop() lazy: boolean = false;

  /**
   * When set to `false`, SVG content that is HTTP fetched will not be checked
   * if the response SVG content has any `<script>` elements, or any attributes
   * that start with `on`, such as `onclick`.
   */
  @Prop() sanitize = true;

  componentWillLoad() {

    this.waitUntilVisible(this.el, '50px', () => {
      this.isVisible = true;
      this.loadAsset();
    });
  }

  disconnectedCallback() {
    if (this.io) {
      this.io.disconnect();
      this.io = undefined;
    }
  }

  private waitUntilVisible(el: HTMLRoadAssetElement, rootMargin: string, cb: () => void) {
    if (this.lazy && (window as any).IntersectionObserver) {
      const io = this.io = new (window as any).IntersectionObserver((data: IntersectionObserverEntry[]) => {
        if (data[0].isIntersecting) {
          io.disconnect();
          this.io = undefined;
          cb();
        }
      }, { rootMargin });

      io.observe(el);

    } else {
      // browser doesn't support IntersectionObserver
      // so just fallback to always show it
      cb();
    }
  }

  @Watch('name')
  @Watch('src')
  @Watch('asset')
  loadAsset() {
    if (Build.isBrowser && this.isVisible) {
      const url = getUrl(this);
      if (url) {
        if (roadassetContent.has(url)) {
          // sync if it's already loaded
          this.assetSvgContent = roadassetContent.get(url);
        } else {
          // async if it hasn't been loaded
          getAssetSvgContent(url, this.sanitize).then(() => (this.assetSvgContent = roadassetContent.get(url)));
        }
      }
    }

    if (!this.ariaLabel && this.ariaHidden !== 'true') {
      const label = getName(this.name, this.asset);
      // user did not provide a label
      // come up with the label based on the icon name
      if (label) {
        this.ariaLabel = label.replace(/-/g, ' ');
      }
    }
  }

  render() {

    return (
      <Host aria-hidden="true" role="img">
        {(
          (this.assetSvgContent !== '')
            ? <div class="icon-inner" innerHTML={this.assetSvgContent}></div>
            : <div class="icon-inner"></div>
        )}
      </Host>
    );
  }
}