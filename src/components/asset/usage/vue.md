```html
<template>
  <road-asset icon="ecomobilisteFr"></road-asset>
  <road-asset icon="ecomobilisteEs"></road-asset>
</template>

<script>
  import { ecomobilisteFr, ecomobilisteEs } from "@roadtrip/components/assets";
  import { addAssets } from "@roadtrip/assets";
  addAssets({
    ecomobilisteFr,
    ecomobilisteEs
  });
</script>
```
