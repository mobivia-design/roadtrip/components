```javascript
import React from 'react';
import { ecomobilisteFr, ecomobilisteEs } from "@roadtrip/components/assets";

const MyComponent: React.FC = () => {
  return (
    <>
      <road-asset icon={ecomobilisteFr}></road-asset>
      <road-asset icon={ecomobilisteEs}></road-asset>
    </>
  );
};

export default MyComponent;
```