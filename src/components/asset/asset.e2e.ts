import { newE2EPage } from '@stencil/core/testing';

describe('road-asset', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<road-asset name="apps"></road-asset>');

    const element = await page.find('road-asset');
    expect(element).toHaveClass('hydrated');
  });
});
