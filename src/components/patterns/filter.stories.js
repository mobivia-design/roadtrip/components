import { html } from 'lit';

export default {
  title: 'Patterns/Filter',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
    layout: 'fullscreen',
  },
};

export const Filters = () => html`
  <style>
    .category-accordion {
      --header-padding: 0.75rem 1.25rem 0.75rem 1rem;
      --accordion-header-border: 0;
      --content-padding: 0 0 0.5rem 0;
    }

    .filter-accordion {
      --header-padding: 0.75rem 1.25rem 0.75rem 1rem;
      --accordion-header-border: 0;
      --content-padding: 0 0.75rem 0.5rem 0;
    }

    .filter-collapse {
      --btn-padding-start: 0;
      --btn-padding-end: 0;
    }

    .category-link {
      --min-height: 2.25rem;
      --inner-padding: 0.5rem 0.5rem 0.5rem 0;
      --padding-left: 0.5rem;
      --detail-color: var(--road-secondary-500);
    }
  </style>
  <road-grid>
      <road-row>
        <road-col class="col-12 col-md-8 col-lg-3">
          <road-accordion is-open class="category-accordion mb-8 mt-8">
            <div slot="header" class="h7 mb-0">
              Catégories
            </div>
            <road-list>
              <road-item button detail class="category-link">
                GPS voiture <road-text color="disabled" class="ml-4">(32)</road-text>
              </road-item>
              <road-item button detail class="category-link">
                GPS camping-car <road-text color="disabled" class="ml-4">(6)</road-text>
              </road-item>
              <road-item button detail class="category-link">
                GPS poids lourds <road-text color="disabled" class="ml-4">(24)</road-text>
              </road-item>
              <road-item button detail class="category-link">
                GPS moto <road-text color="disabled" class="ml-4">(8)</road-text>
              </road-item>
              <road-item button detail class="category-link">
                Accessoires GPS <road-text color="disabled" class="ml-4">(128)</road-text>
              </road-item>
            </road-list>
          </road-accordion>
          <road-accordion  is-open class="filter-accordion mb-8">
            <div slot="header" class="h7 d-flex align-items-center mb-0">
              Marque <road-badge class="ml-8">3</road-badge>
            </div>
            <road-collapse class="filter-collapse mb-0" show-more="Plus de choix" show-less="Moins de choix">

              <road-checkbox checked inverse class="pt-8 pb-8 mb-4" color="secondary" label="Norauto" value="Norauto">
                <road-text color="disabled" class="ml-4">(3)</road-text>
              </road-checkbox>

              <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Norauto Sound" value="Norauto Sound">
                <road-text color="disabled" class="ml-4">(118)</road-text>
              </road-checkbox>

              <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Phonocar" value="Phonocar">
                <road-text color="disabled" class="ml-4">(314)</road-text>
              </road-checkbox>

              <road-checkbox checked inverse class="pt-8 pb-8 mb-4" color="secondary" label="TNB" value="TNB">
                <road-text color="disabled" class="ml-4">(83)</road-text>
              </road-checkbox>

              <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Focal" value="Focal">
                <road-text color="disabled" class="ml-4">(50)</road-text>
              </road-checkbox>

              <road-checkbox checked inverse class="pt-8 pb-8 mb-4" color="secondary" label="Pioneer" value="Pioneer">
                <road-text color="disabled" class="ml-4">(44)</road-text>
              </road-checkbox>

              <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="MTX" value="MTX">
                <road-text color="disabled" class="ml-4">(41)</road-text>
              </road-checkbox>

              <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Alpine" value="Alpine">
                <road-text color="disabled" class="ml-4">(21)</road-text>
              </road-checkbox>
      
              <div slot="collapsed-content">
                <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Norauto" value="Norauto">
                  <road-text color="disabled" class="ml-4">(3)</road-text>
                </road-checkbox>

                <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Norauto Sound" value="Norauto Sound">
                  <road-text color="disabled" class="ml-4">(118)</road-text>
                </road-checkbox>

                <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Phonocar" value="Phonocar">
                  <road-text color="disabled" class="ml-4">(314)</road-text>
                </road-checkbox>

                <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="TNB" value="TNB">
                  <road-text color="disabled" class="ml-4">(83)</road-text>
                </road-checkbox>

                <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Focal" value="Focal">
                  <road-text color="disabled" class="ml-4">(50)</road-text>
                </road-checkbox>

                <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Pioneer" value="Pioneer">
                  <road-text color="disabled" class="ml-4">(44)</road-text>
                </road-checkbox>

                <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="MTX" value="MTX">
                  <road-text color="disabled" class="ml-4">(41)</road-text>
                </road-checkbox>

                <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Alpine" value="Alpine">
                  <road-text color="disabled" class="ml-4">(21)</road-text>
                </road-checkbox>
              </div>
            </road-collapse>
          </road-accordion>
          <road-accordion is-open class="filter-accordion mb-8">
            <div slot="header" class="h7 d-flex align-items-center mb-0">
              Sort by
            </div>
            <road-radio-group name="sort" value="popularity" aria-label="sort by">
              <road-radio inverse class="pt-8 pb-8 mb-4" label="Popularity" value="popularity" inline="false"></road-radio>
              <road-radio inverse class="pt-8 pb-8 mb-4" label="Price: high - low" value="high" inline="false"></road-radio>
              <road-radio inverse class="pt-8 pb-8 mb-4" label="Price: low - high" value="low" inline="false"></road-radio>
            </road-radio-group>
          </road-accordion>
        </road-col>
    </road-row>
  </road-grid>
`;

export const ModalFilters = () => html`
  <style>
    .category-accordion {
      --header-padding: 0.75rem 1.25rem 0.75rem 1rem;
      --accordion-header-border: 0;
      --content-padding: 0 0 0.5rem 0;
    }

    .filter-accordion {
      --header-padding: 0.75rem 1.25rem 0.75rem 1rem;
      --accordion-header-border: 0;
      --content-padding: 0 0.75rem 0.5rem 0;
    }

    .category-link {
      --min-height: 2.25rem;
      --inner-padding: 0.5rem 0.5rem 0.5rem 0;
      --padding-left: 0.5rem;
      --detail-color: var(--road-secondary-500);
    }
  </style>
  <road-drawer is-open drawer-title="Refine">
    <div class="bg-light p-8">
      <road-accordion is-open class="category-accordion mb-8 mt-8">
        <div slot="header" class="h7 mb-0">
          Catégories
        </div>
        <road-list>
          <road-item button detail class="category-link">
            GPS voiture <road-text color="disabled" class="ml-4">(32)</road-text>
          </road-item>
          <road-item button detail class="category-link">
            GPS camping-car <road-text color="disabled" class="ml-4">(6)</road-text>
          </road-item>
          <road-item button detail class="category-link">
            GPS poids lourds <road-text color="disabled" class="ml-4">(24)</road-text>
          </road-item>
          <road-item button detail class="category-link">
            GPS moto <road-text color="disabled" class="ml-4">(8)</road-text>
          </road-item>
          <road-item button detail class="category-link">
            Accessoires GPS <road-text color="disabled" class="ml-4">(128)</road-text>
          </road-item>
        </road-list>
      </road-accordion>
      <road-accordion  is-open class="filter-accordion mb-8">
        <div slot="header" class="h7 d-flex align-items-center mb-0">
          Marque <road-badge class="ml-8">3</road-badge>
        </div>
        <road-collapse class="mb-0" show-more="Plus de choix" show-less="Moins de choix">

          <road-checkbox checked inverse class="pt-8 pb-8 mb-4" color="secondary" label="Norauto" value="Norauto">
            <road-text color="disabled" class="ml-4">(3)</road-text>
          </road-checkbox>

          <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Norauto Sound" value="Norauto Sound">
            <road-text color="disabled" class="ml-4">(118)</road-text>
          </road-checkbox>

          <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Phonocar" value="Phonocar">
            <road-text color="disabled" class="ml-4">(314)</road-text>
          </road-checkbox>

          <road-checkbox checked inverse class="pt-8 pb-8 mb-4" color="secondary" label="TNB" value="TNB">
            <road-text color="disabled" class="ml-4">(83)</road-text>
          </road-checkbox>

          <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Focal" value="Focal">
            <road-text color="disabled" class="ml-4">(50)</road-text>
          </road-checkbox>

          <road-checkbox checked inverse class="pt-8 pb-8 mb-4" color="secondary" label="Pioneer" value="Pioneer">
            <road-text color="disabled" class="ml-4">(44)</road-text>
          </road-checkbox>

          <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="MTX" value="MTX">
            <road-text color="disabled" class="ml-4">(41)</road-text>
          </road-checkbox>

          <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Alpine" value="Alpine">
            <road-text color="disabled" class="ml-4">(21)</road-text>
          </road-checkbox>

          <div slot="collapsed-content">
            <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Norauto" value="Norauto">
              <road-text color="disabled" class="ml-4">(3)</road-text>
            </road-checkbox>

            <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Norauto Sound" value="Norauto Sound">
              <road-text color="disabled" class="ml-4">(118)</road-text>
            </road-checkbox>

            <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Phonocar" value="Phonocar">
              <road-text color="disabled" class="ml-4">(314)</road-text>
            </road-checkbox>

            <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="TNB" value="TNB">
              <road-text color="disabled" class="ml-4">(83)</road-text>
            </road-checkbox>

            <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Focal" value="Focal">
              <road-text color="disabled" class="ml-4">(50)</road-text>
            </road-checkbox>

            <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Pioneer" value="Pioneer">
              <road-text color="disabled" class="ml-4">(44)</road-text>
            </road-checkbox>

            <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="MTX" value="MTX">
              <road-text color="disabled" class="ml-4">(41)</road-text>
            </road-checkbox>

            <road-checkbox inverse class="pt-8 pb-8 mb-4" color="secondary" label="Alpine" value="Alpine">
              <road-text color="disabled" class="ml-4">(21)</road-text>
            </road-checkbox>
          </div>
        </road-collapse>
      </road-accordion>
      <road-accordion is-open class="filter-accordion mb-8">
        <div slot="header" class="h7 d-flex align-items-center mb-0">
          Sort by
        </div>
        <road-radio-group name="sort" value="popularity" aria-label="sort by">
          <road-radio inverse class="pt-8 pb-8 mb-4" label="Popularity" value="popularity" inline="false"></road-radio>
          <road-radio inverse class="pt-8 pb-8 mb-4" label="Price: high - low" value="high" inline="false"></road-radio>
          <road-radio inverse class="pt-8 pb-8 mb-4" label="Price: low - high" value="low" inline="false"></road-radio>
        </road-radio-group>
      </road-accordion>
    </div>
  </road-drawer>
`;
ModalFilters.parameters = {
  docs: {
    inlineStories: false,
    iframeHeight: '600px',
  },
};
