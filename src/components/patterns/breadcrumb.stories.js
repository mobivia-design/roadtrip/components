import { html } from 'lit';

export default {
  title: 'Navigation/Breadcrumb',
};

export const Playground = () => html`
<div class="d-flex align-items-center">
  <a class="link link-default d-inline-flex align-items-center" href="#">
    Home
  </a>
  <span class="mx-8">/</span>
  <a class="link link-default d-inline-flex align-items-center" href="#">
    link 1
  </a>
  <span class="mx-8">/</span>
  <a class="link link-default d-inline-flex align-items-center" href="#">
    link 2
  </a>
  <span class="mx-8">/</span>
  <span class="text-secondary">
    Page title
  </span>
</div>
`;

export const Levels = () => html`
<!-- 1 level -->
<div class="d-flex align-items-center">
  <a class="link link-default d-inline-flex align-items-center" href="#">
    Home
  </a>
  <span class="mx-8">/</span>
  <span class="link">
    Page title
  </span>
</div>

<!--  2 levels -->
<div class="d-flex align-items-center">
  <a class="link link-default d-inline-flex align-items-center" href="#">
    Home
  </a>
  <span class="mx-8">/</span>
  <a class="link link-default d-inline-flex align-items-center" href="#">
    link 1
  </a>
  <span class="mx-8">/</span>
  <span class="text-secondary">
    Page title
  </span>
</div>

<!--  3 levels -->
<div class="d-flex align-items-center">
  <a class="link link-default d-inline-flex align-items-center" href="#">
    Home
  </a>
  <span class="mx-8">/</span>
  <a class="link link-default d-inline-flex align-items-center" href="#">
    link 1
  </a>
  <span class="mx-8">/</span>
  <a class="link link-default d-inline-flex align-items-center" href="#">
    link 2
  </a>
  <span class="mx-8">/</span>
  <span class="text-secondary">
    Page title
  </span>
</div>
`;
