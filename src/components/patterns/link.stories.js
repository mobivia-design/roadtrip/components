import { html } from 'lit';

export default {
  title: 'Navigation/Link',
};

export const Playground = () => html`
<a class="link" href="#">Link</a>

<br><br>

<a class="link d-inline-flex align-items-center" href="#">
  <road-icon size="sm" class="mr-4" name="edit-pen" role="img"></road-icon>
  Link
</a>

<br><br>

<a class="link d-inline-flex align-items-center" href="#">
  Link
  <road-icon size="sm" class="mr-4" name="arrow" role="img"></road-icon>
</a>
`;

export const Default = () => html`
<a class="link link-default" href="#">Link</a>
`;

export const White = () => html`
<div class="bg-secondary p-24">
  <a class="link link-white" href="#">Link</a>
</div>`
;


export const Sizes = () => html`
<a class="link link-md" href="#">Link</a>

<br><br>

<a class="link link-sm" href="#">Link</a>
`;
