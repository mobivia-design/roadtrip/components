import { html } from 'lit';

export default {
  title: 'Patterns/Cross Selling',
};

export const CrossSellingProduct = () => {
  const crossConfig = {
    slidesPerView: 1.5,
    spaceBetween: 16,
    breakpoints: {
      // when window width is >= 320px
      576: {
        slidesPerView: 2.5,
      },
      // when window width is >= 480px
      768: {
        slidesPerView: 2.5,
      },
      1024: {
        slidesPerView: 3.5,
      },
      1200: {
        slidesPerView: 6,
      },
    }
  };
  
  return html`
    <road-carousel pager="false" .options=${crossConfig}>

      <road-carousel-item>
        <road-card href="https://www.norauto.fr/p/2-haut-parleurs-pioneer-ts-g1320f-2151435.html?CatalogID=&CatalogCategoryName=">
          <road-img class="mb-16 mx-auto" style="width:200px;height:200px;" src="https://s1.medias-norauto.fr/images_produits/2151435/200x200/2-haut-parleurs-pioneer-ts-g1320f--2151435.jpg" alt="2 haut-parleurs PIONEER TS-G1320F"></road-img>
          <p class="text-content text-left mb-8">2 haut-parleurs PIONEER TS-G1320F</p>
          <road-rating class="mb-16" rate="3.5" reviews="11"></road-rating>
          <p class="h7 text-left mb-0">27,95€</p>
        </road-card>
      </road-carousel-item>

      <road-carousel-item>
        <road-card href="https://www.norauto.fr/p/amplificateur-focal-auditor-ap-4340-2179610.html?CatalogID=&CatalogCategoryName=">
          <road-img class="mb-16 mx-auto" style="width:200px;height:200px;" src="https://s2.medias-norauto.fr/images_produits/2179610/200x200/amplificateur-focal-auditor-ap-4340--2179610.jpg" alt="Amplificateur Focal Auditor AP 4340"></road-img>
          <p class="text-content text-left mb-8">Amplificateur Focal Auditor AP 4340</p>
          <road-rating class="mb-16" rate="3" reviews="4"></road-rating>
          <p class="h7 text-left mb-0">99,95€</p>
        </road-card>
      </road-carousel-item>

      <road-carousel-item>
        <road-card href="https://www.norauto.fr/p/autoradio-pioneer-mvh-330dab-2278093.html">
          <road-img class="mb-16 mx-auto" style="width:200px;height:200px;" src="https://s1.medias-norauto.fr/images_produits/4988028478239/200x200/autoradio-pioneer-mvh-330dab--2278093.jpg" alt="Autoradio PIONEER MVH-330DAB"></road-img>
          <p class="text-content text-left mb-8">Autoradio PIONEER MVH-330DAB</p>
          <road-rating class="mb-16" rate="4" reviews="20"></road-rating>
          <p class="h7 text-left mb-0">99,95€</p>
        </road-card>
      </road-carousel-item>

      <road-carousel-item>
        <road-card href="https://www.norauto.fr/p/jack-connecteur-d-antenne-phonocar-ref.-085261-2239275.html?CatalogID=&CatalogCategoryName=">
          <road-img class="mb-16 mx-auto" style="width:200px;height:200px;" src="https://s1.medias-norauto.fr/images_produits/8020065852613/200x200/jack-connecteur-d-antenne-phonocar-ref-085261--2239275.jpg" alt="Jack connecteur d'antenne PHONOCAR REF. 085261"></road-img>
          <p class="text-content text-left mb-8">Jack connecteur d'antenne PHONOCAR REF. 085261</p>
          <road-rating class="mb-16" rate="4" reviews="3"></road-rating>
          <p class="h7 text-left mb-0">4,99€</p>
        </road-card>
      </road-carousel-item>

    </road-carousel>
  `;
}

export const withAddToCart = () => {
  const crossConfig = {
    slidesPerView: 1.5,
    spaceBetween: 16,
    breakpoints: {
      // when window width is >= 320px
      576: {
        slidesPerView: 2.5,
      },
      // when window width is >= 480px
      768: {
        slidesPerView: 3.5,
      },
      1024: {
        slidesPerView: 3.5,
      },
      1200: {
        slidesPerView: 6,
      },
    }
  };
  
  return html`
    <road-carousel pager="false" .options=${crossConfig}>

      <road-carousel-item>
        <road-card class="bg-white">
          <a href="https://www.norauto.fr/p/2-haut-parleurs-pioneer-ts-g1320f-2151435.html?CatalogID=&CatalogCategoryName=">
            <road-img class="mb-16 mx-auto" style="width:200px;height:200px;" src="https://s1.medias-norauto.fr/images_produits/2151435/200x200/2-haut-parleurs-pioneer-ts-g1320f--2151435.jpg" alt="2 haut-parleurs PIONEER TS-G1320F"></road-img>
          </a>
          <a class="text-content text-left mb-8" href="https://www.norauto.fr/p/2-haut-parleurs-pioneer-ts-g1320f-2151435.html?CatalogID=&CatalogCategoryName=">
            <p class="text-content text-left mb-8">2 haut-parleurs PIONEER TS-G1320F</p>
          </a>
          <road-rating class="mb-16" rate="3.5" reviews="11"></road-rating>
          <p class="h7 text-left mb-8">27,95€</p>
          <p class="text-content text-left">
            <road-badge bubble color="success"></road-badge>
            <strong style="color: var(--road-success-text)">In stock: collect in 1 hour</strong> in Villeneuve d'Ascq
          </p>
          <road-button color="secondary" expand>
            <road-icon name="shopping-cart-add"></road-icon>
            Add
          </road-button>
        </road-card>
      </road-carousel-item>

      <road-carousel-item>
        <road-card class="bg-white">
          <road-img class="mb-16 mx-auto" style="width:200px;height:200px;" src="https://s2.medias-norauto.fr/images_produits/2179610/200x200/amplificateur-focal-auditor-ap-4340--2179610.jpg" alt="Amplificateur Focal Auditor AP 4340"></road-img>
          <p class="text-content text-left mb-8">Amplificateur Focal Auditor AP 4340</p>
          <road-rating class="mb-16" rate="3" reviews="4"></road-rating>
          <p class="h7 text-left mb-8">99,95€</p>
          <p class="text-content text-left">
            <road-badge bubble color="success"></road-badge>
            <strong style="color: var(--road-success-text)">From 13 September</strong> in Villeneuve d'Ascq
          </p>
          <road-button color="secondary" expand>
            <road-icon name="shopping-cart-add"></road-icon>
            Add
          </road-button>
        </road-card>
      </road-carousel-item>

      <road-carousel-item>
        <road-card class="bg-white">
          <road-img class="mb-16 mx-auto" style="width:200px;height:200px;" src="https://s1.medias-norauto.fr/images_produits/4988028478239/200x200/autoradio-pioneer-mvh-330dab--2278093.jpg" alt="Autoradio PIONEER MVH-330DAB"></road-img>
          <p class="text-content text-left mb-8">Autoradio PIONEER MVH-330DAB</p>
          <road-rating class="mb-16" rate="4" reviews="20"></road-rating>
          <p class="h7 text-left mb-8">99,95€</p>
          <p class="text-content text-left">
            <road-badge bubble color="success"></road-badge>
            <strong style="color: var(--road-success-text)">In stock: collect in 1 hour</strong> in Villeneuve d'Ascq
          </p>
          <road-button color="secondary" expand>
            <road-icon name="shopping-cart-add"></road-icon>
            Add
          </road-button>
        </road-card>
      </road-carousel-item>

      <road-carousel-item>
        <road-card class="bg-white">
          <road-img class="mb-16 mx-auto" style="width:200px;height:200px;" src="https://s1.medias-norauto.fr/images_produits/8020065852613/200x200/jack-connecteur-d-antenne-phonocar-ref-085261--2239275.jpg" alt="Jack connecteur d'antenne PHONOCAR REF. 085261"></road-img>
          <p class="text-content text-left mb-8">Jack connecteur d'antenne PHONOCAR REF. 085261</p>
          <road-rating class="mb-16" rate="4" reviews="3"></road-rating>
          <p class="h7 text-left mb-8">4,99€</p>
          <p class="text-content text-left">
            <road-badge bubble color="success"></road-badge>
            <strong style="color: var(--road-success-text)">In stock: collect in 1 hour</strong> in Villeneuve d'Ascq
          </p>
          <road-button color="secondary" expand>
            <road-icon name="shopping-cart-add"></road-icon>
            Add
          </road-button>
        </road-card>
      </road-carousel-item>

    </road-carousel>
  `;
}
