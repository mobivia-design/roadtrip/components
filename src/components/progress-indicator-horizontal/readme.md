# road-spinner



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute           | Description                              | Type                                                                 | Default     |
| ----------------- | ------------------- | ---------------------------------------- | -------------------------------------------------------------------- | ----------- |
| `color`           | `color`             | The color progress-indicator-horizontal. | `"default" \| "header" \| undefined`                                 | `'default'` |
| `numberStep`      | `number-step`       | The nombre of steps.                     | `2 \| 3 \| undefined`                                                | `3`         |
| `stateFirstStep`  | `state-first-step`  | The color of the first step.             | `"comleted" \| "current" \| "default" \| "in-progress" \| undefined` | `undefined` |
| `stateSecondStep` | `state-second-step` | The color of the second step.            | `"comleted" \| "current" \| "default" \| "in-progress" \| undefined` | `undefined` |
| `stateThirdStep`  | `state-third-step`  | The color of the third step.             | `"comleted" \| "current" \| "default" \| "in-progress" \| undefined` | `undefined` |
| `urlStep1`        | `url-step-1`        | The url of the first step.               | `string \| undefined`                                                | `undefined` |
| `urlStep2`        | `url-step-2`        | The url of the first step.               | `string \| undefined`                                                | `undefined` |
| `urlStep3`        | `url-step-3`        | The url of the first step.               | `string \| undefined`                                                | `undefined` |


## Slots

| Slot                                     | Description                                                                                                                                       |
| ---------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| `"progress-indicator-horizontal-icon"`   | if the state of the step 1 is completed add `<road-icon name="check-small" class="d-block" style="color: currentColor;"></road-icon>` by default. |
| `"progress-indicator-horizontal-icon2"`  | if the state of the step 2 is completed add `<road-icon name="check-small" class="d-block" style="color: currentColor;"></road-icon>` by default. |
| `"progress-indicator-horizontal-icon3"`  | if the state of the step 3 is completed add `<road-icon name="check-small" class="d-block" style="color: currentColor;"></road-icon>` by default. |
| `"progress-indicator-horizontal-title"`  |                                                                                                                                                   |
| `"progress-indicator-horizontal-title2"` |                                                                                                                                                   |
| `"progress-indicator-horizontal-title3"` |                                                                                                                                                   |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
