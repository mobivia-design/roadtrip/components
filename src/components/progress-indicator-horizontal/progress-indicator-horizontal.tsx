import { Component, Host, h, Prop } from '@stencil/core';

/**
 * @slot progress-indicator-horizontal-icon - if the state of the step 1 is completed add
 * `<road-icon name="check-small" class="d-block" style="color: currentColor;"></road-icon>` by default.
 * @slot progress-indicator-horizontal-title 
 * @slot progress-indicator-horizontal-icon2 - if the state of the step 2 is completed add
 * `<road-icon name="check-small" class="d-block" style="color: currentColor;"></road-icon>` by default.
 * @slot progress-indicator-horizontal-title2 
 * @slot progress-indicator-horizontal-icon3 - if the state of the step 3 is completed add
 * `<road-icon name="check-small" class="d-block" style="color: currentColor;"></road-icon>` by default.
 * @slot progress-indicator-horizontal-title3
 */

@Component({
  tag: 'road-progress-indicator-horizontal',
  styleUrl: 'progress-indicator-horizontal.css',
  shadow: true,
})
export class Stepper {

  /**
    * The color progress-indicator-horizontal.
    */
    @Prop({ reflect: true }) color?: 'default' | 'header' = 'default';

    /**
    * The nombre of steps.
    */
    @Prop() numberStep?: 3 | 2 = 3;

    /**
   * The color of the first step.
   */
   @Prop() stateFirstStep?: 'default' | 'current' | 'comleted' | 'in-progress';

    /**
   * The color of the second step.
   */
    @Prop() stateSecondStep?: 'default' | 'current' | 'comleted' | 'in-progress';

    /**
   * The color of the third step.
   */
     @Prop() stateThirdStep?: 'default' | 'current' | 'comleted' | 'in-progress';

    /**
   * The url of the first step.
   */
    @Prop() urlStep1?: string;

    /**
   * The url of the first step.
   */
    @Prop() urlStep2?: string;

    /**
   * The url of the first step.
   */
    @Prop() urlStep3?: string;
    

     render() {

       const colorClass = this.color !== undefined ? `progress-indicator-horizontal progress-indicator-horizontal-${this.color}` : 'progress-indicator-horizontal';
       const stateClass = this.stateFirstStep !== undefined ? `progress-indicator-horizontal-item ${this.stateFirstStep}` : 'progress-indicator-horizontal-item';
       const stateSecondStepClass = this.stateSecondStep !== undefined ? `progress-indicator-horizontal-item ${this.stateSecondStep}` : 'progress-indicator-horizontal-item';
       const stateThirdStepClass = this.stateThirdStep !== undefined ? `progress-indicator-horizontal-item ${this.stateThirdStep}` : 'progress-indicator-horizontal-item';
       const urlStep1 = this.urlStep1 !== undefined ? `${this.urlStep1}` : '#';
       const urlStep2 = this.urlStep2 !== undefined ? `${this.urlStep2}` : '#';
       const urlStep3 = this.urlStep3 !== undefined ? `${this.urlStep3}` : '#';

       return (
         <Host>
           <nav>
                <ul class={`${colorClass}`}>
                <li class={`${stateClass}`}>
                  <a class="progress-indicator-horizontal-link" href={`${urlStep1}`}>
                    <span class="progress-indicator-horizontal-icon">
                      <slot name="progress-indicator-horizontal-icon"/>
                    </span>
                    <span class="progress-indicator-horizontal-title">
                      <slot name="progress-indicator-horizontal-title"/>
                    </span>
                  </a>
                </li>
                <li class={`${stateSecondStepClass}`}>
                 <a class="progress-indicator-horizontal-link" href={`${urlStep2}`}>
                   <span class="progress-indicator-horizontal-icon">
                     <slot name="progress-indicator-horizontal-icon2"/>
                   </span>
                   <span class="progress-indicator-horizontal-title">
                     <slot name="progress-indicator-horizontal-title2"/>
                   </span>
                 </a>
               </li>
               {this.numberStep == 3 && <li class={`${stateThirdStepClass}`}>
                 <a class="progress-indicator-horizontal-link" href={`${urlStep3}`}>
                   <span class="progress-indicator-horizontal-icon">
                     <slot name="progress-indicator-horizontal-icon3"/>
                   </span>
                   <span class="progress-indicator-horizontal-title">
                     <slot name="progress-indicator-horizontal-title3"/>
                   </span>
                 </a>
               </li>}
               
             </ul>
           </nav>
         </Host>
       );
     }

}
