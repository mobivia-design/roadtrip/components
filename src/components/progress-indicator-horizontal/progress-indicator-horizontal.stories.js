import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Indicators/Progress indicator horizontal',
  component: 'road-progress-indicator-horizontal',
  argTypes: {
    color: {
      description: "The color progress-indicator-horizontal.",
      options: ['default', 'header'],
      control: {
        type: 'select',
      },
    },
    'number-step': {
      description: "The nombre of steps.",
      options: [3, 2],
      control: {
        type: 'select',
      },
    },
    'state-first-step': {
      description: "The color of the first step.",
      options: ['default', 'current', 'completed', 'in-progress'],
      control: {
        type: 'select',
      },
    },
    'state-second-step': {
      description: "The color of the second step.",
      options: ['default', 'current', 'completed', 'in-progress'],
      control: {
        type: 'select',
      },
    },
    'state-third-step': {
      description: "The color of the third step.",
      options: ['default', 'current', 'completed', 'in-progress'],
      control: {
        type: 'select',
      },
    },
    'progress-indicator-horizontal-icon': {
      description: "if the state of the step 1 is completed add\n`<road-icon name=\"check-small\" class=\"d-block\" style=\"color: currentColor;\"></road-icon>` by default.",
      control: 'text',
    },
    'progress-indicator-horizontal-title': {
      description: "if the state of the step 2 is completed add\n`<road-icon name=\"check-small\" class=\"d-block\" style=\"color: currentColor;\"></road-icon>` by default.",
      control: 'text',
    },
    'progress-indicator-horizontal-icon2': {
      description: "if the state of the step 3 is completed add\n`<road-icon name=\"check-small\" class=\"d-block\" style=\"color: currentColor;\"></road-icon>` by default.",
      control: 'text',
    },
    'progress-indicator-horizontal-title2': {
      control: 'text',
    },
    'progress-indicator-horizontal-icon3': {
      control: 'text',
    },
    'progress-indicator-horizontal-title3': {
      control: 'text',
    },
  },
  args: {
    color: 'default',
    'number-step': 3,
    'state-first-step': 'default',
    'state-second-step': 'default',
    'state-third-step': 'default',
    'progress-indicator-horizontal-icon': `1`,
    'progress-indicator-horizontal-title': `First step`,
    'progress-indicator-horizontal-icon2': `2`,
    'progress-indicator-horizontal-title2': `Second step`,
    'progress-indicator-horizontal-icon3': `3`,
    'progress-indicator-horizontal-title3': `Third step`,
  },
};

export const Template = (args) => html`
  <road-progress-indicator-horizontal color="${ifDefined(args.color)}" number-step="${ifDefined(args['number-step'])}" state-first-step="${ifDefined(args['state-first-step'])}" state-second-step="${ifDefined(args['state-second-step'])}" state-third-step="${ifDefined(args['state-third-step'])}">
      <div slot="progress-indicator-horizontal-icon">
        ${unsafeHTML(args['progress-indicator-horizontal-icon'])}
      </div>
      <div slot="progress-indicator-horizontal-title">
        ${unsafeHTML(args['progress-indicator-horizontal-title'])}
      </div>
      <div slot="progress-indicator-horizontal-icon2">
      ${unsafeHTML(args['progress-indicator-horizontal-icon2'])}
    </div>
      <div slot="progress-indicator-horizontal-title2">
      ${unsafeHTML(args['progress-indicator-horizontal-title2'])}
      </div>
      <div slot="progress-indicator-horizontal-icon3">
      ${unsafeHTML(args['progress-indicator-horizontal-icon3'])}
    </div>
    <div slot="progress-indicator-horizontal-title3">
      ${unsafeHTML(args['progress-indicator-horizontal-title3'])}
      </div>
  </road-progress-indicator-horizontal>
`;

export const withoutlabel = (args) => html`
  <road-progress-indicator-horizontal color="${ifDefined(args.color)}" number-step="${ifDefined(args['number-step'])}" state-first-step="${ifDefined(args['state-first-step'])}" state-second-step="${ifDefined(args['state-second-step'])}" state-third-step="${ifDefined(args['state-third-step'])}">
    <div slot="progress-indicator-horizontal-icon">
      ${unsafeHTML(args['progress-indicator-horizontal-icon'])}
    </div>
      <div slot="progress-indicator-horizontal-icon2">
      ${unsafeHTML(args['progress-indicator-horizontal-icon2'])}
    </div>
      <div slot="progress-indicator-horizontal-icon3">
      ${unsafeHTML(args['progress-indicator-horizontal-icon3'])}
    </div>
  </road-progress-indicator-horizontal>
`;

export const primary = (args) => html`
  <road-progress-indicator-horizontal color="default" number-step="${ifDefined(args['number-step'])}" state-first-step="current" state-second-step="default" state-third-step="default">
  <div slot="progress-indicator-horizontal-icon">
  ${unsafeHTML(args['progress-indicator-horizontal-icon'])}
  </div>
  <div slot="progress-indicator-horizontal-title">
    ${unsafeHTML(args['progress-indicator-horizontal-title'])}
    </div>
    <div slot="progress-indicator-horizontal-icon2">
    ${unsafeHTML(args['progress-indicator-horizontal-icon2'])}
  </div>
    <div slot="progress-indicator-horizontal-title2">
    ${unsafeHTML(args['progress-indicator-horizontal-title2'])}
    </div>
    <div slot="progress-indicator-horizontal-icon3">
    ${unsafeHTML(args['progress-indicator-horizontal-icon3'])}
  </div>
  <div slot="progress-indicator-horizontal-title3">
    ${unsafeHTML(args['progress-indicator-horizontal-title3'])}
    </div>
  </road-progress-indicator-horizontal>
`;

export const header = (args) => html`
  <road-progress-indicator-horizontal color="header" number-step="${ifDefined(args['number-step'])}" state-first-step="current" state-second-step="default" state-third-step="default">
  <div slot="progress-indicator-horizontal-icon">
  ${unsafeHTML(args['progress-indicator-horizontal-icon'])}
  </div>
  <div slot="progress-indicator-horizontal-title">
    ${unsafeHTML(args['progress-indicator-horizontal-title'])}
    </div>
    <div slot="progress-indicator-horizontal-icon2">
    ${unsafeHTML(args['progress-indicator-horizontal-icon2'])}
  </div>
    <div slot="progress-indicator-horizontal-title2">
    ${unsafeHTML(args['progress-indicator-horizontal-title2'])}
    </div>
    <div slot="progress-indicator-horizontal-icon3">
    ${unsafeHTML(args['progress-indicator-horizontal-icon3'])}
  </div>
  <div slot="progress-indicator-horizontal-title3">
    ${unsafeHTML(args['progress-indicator-horizontal-title3'])}
    </div>
  </road-progress-indicator-horizontal>
`;

export const stepcompleted = (args) => html`
  <road-progress-indicator-horizontal color="header" number-step="${ifDefined(args['number-step'])}" state-first-step="completed" state-second-step="current" state-third-step="default">
  <div slot="progress-indicator-horizontal-icon">
    <road-icon name="check-small" class="d-block" color="default"></road-icon>
  </div>
  <div slot="progress-indicator-horizontal-title">
    ${unsafeHTML(args['progress-indicator-horizontal-title'])}
    </div>
    <div slot="progress-indicator-horizontal-icon2">
    ${unsafeHTML(args['progress-indicator-horizontal-icon2'])}
  </div>
    <div slot="progress-indicator-horizontal-title2">
    ${unsafeHTML(args['progress-indicator-horizontal-title2'])}
    </div>
    <div slot="progress-indicator-horizontal-icon3">
    ${unsafeHTML(args['progress-indicator-horizontal-icon3'])}
  </div>
  <div slot="progress-indicator-horizontal-title3">
    ${unsafeHTML(args['progress-indicator-horizontal-title3'])}
    </div>
  </road-progress-indicator-horizontal>
`;
