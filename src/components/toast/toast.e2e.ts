import { newE2EPage } from '@stencil/core/testing';

describe('road-toast', () => {
  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-toast label="Information sticky toast"></road-toast>
      `,
    });
  });

  it('should render closed by default', async () => {
    const close = await page.spyOnEvent('close');

    const element = await page.find('road-toast');
    expect(element).toHaveClass('hydrated');

    const isOpen = await element.getProperty('isOpen');
    expect(isOpen).toBeFalsy();

    element.setProperty('timeout', 800);
    await element.callMethod('open');

    await page.waitForChanges();

    const isOpened = await element.getProperty('isOpen');
    expect(isOpened).toBeTruthy();

    await page.waitForTimeout(2000);
    expect(close).toHaveReceivedEvent();
  });

  it('should render Success alert', async () => {
    const element = await page.find('road-toast');
    expect(element).toHaveClass('hydrated');

    element.setProperty('color', 'success');
    element.setProperty('label', 'Success sticky toast');
    await element.callMethod('open');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render Warning alert', async () => {
    const element = await page.find('road-toast');
    expect(element).toHaveClass('hydrated');

    element.setProperty('color', 'warning');
    element.setProperty('label', 'Warning sticky toast');
    await element.callMethod('open');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render Danger alert', async () => {
    const element = await page.find('road-toast');
    expect(element).toHaveClass('hydrated');

    element.setProperty('color', 'danger');
    element.setProperty('label', 'Danger sticky toast');
    await element.callMethod('open');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});