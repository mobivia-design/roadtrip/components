import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Indicators/Toast',
  component: 'road-toast',
  subcomponents: {
    'road-progress': 'road-progress',
  },
  parameters: {
    layout: 'fullscreen',
    actions: {
      handles: ['close'],
    },
  },
  argTypes: {
    'is-open': {
      description: "Set `open` propertie to `true` to open the toast",
      control: 'boolean',
    },
    color: {
      description: "Set the color of the toast. e.g. info, success, warning, danger",
      options: ['info', 'success', 'warning', 'danger'],
      control: {
        type: 'radio',
      },
    },
    label: {
      description: "Text display in the toast",
      control: 'text',
    },
    timeout: {
      description: "How many milliseconds to wait before hiding the toast. if `\"0\"`, it will show\nuntil `close()` is called.",
      control: 'number',
      min: 0,
    },
    close: {
      description: "Indicate when closing the toast",
      control: {
        type: null,
      },
    },
    progress: {
      description: "the progress bar in the toast.\ncolor=\"info\" for Info\ncolor=\"success\" for success\ncolor=\"warning\" for warning\ncolor=\"danger\" for danger",
      control: 'text',
    },
  },
  args: {
    'is-open': true,
    label: 'Information sticky toast',
    timeout: 5000,
    progress: undefined,
  },
};

const Template = (args) => html`
<road-toast 
  is-open="${ifDefined(args['is-open'])}"  
  color="${ifDefined(args.color)}" 
  label="${ifDefined(args.label)}" 
  timeout="${ifDefined(args.timeout)}"
>
  ${unsafeHTML(args.progress)}
</road-toast>
`;

export const Playground = Template.bind({});
Playground.args = {
  progress: `<road-progress slot="progress" light="true" color="info" animation="true"></road-progress>`,
};

export const Success = Template.bind({});
Success.args = {
  color: 'success',
  label: 'Success sticky toast',
  timeout: 5000,
};

export const Warning = Template.bind({});
Warning.args = {
  color: 'warning',
  label: 'Warning sticky toast',
  timeout: 5000,
};

export const Danger = Template.bind({});
Danger.args = {
  color: 'danger',
  label: 'Danger sticky toast',
  timeout: 5000,
};