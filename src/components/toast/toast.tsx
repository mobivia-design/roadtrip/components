import { Component, Event, EventEmitter, Host, Method, Prop, Watch, h } from '@stencil/core';
import { FeedbackColors } from '../../interface';
import { navigationClose, alertDangerOutline, alertInfoOutline, alertSuccessOutline, alertWarningOutline } from '../../../icons';


/**
 *
 * @slot progress - the progress bar in the toast.
 * color="info" for Info
 * color="success" for success
 * color="warning" for warning
 * color="danger" for danger
 */

@Component({
  tag: 'road-toast',
  styleUrl: 'toast.css',
  shadow: true,
})
export class Toast {

  /**
   * Set `open` propertie to `true` to open the toast
   */
  @Prop({ mutable: true }) isOpen: boolean = false;

  /**
   * Set the color of the toast. e.g. info, success, warning, danger
   */
  @Prop() color: FeedbackColors = 'info';

  /**
   * Text display in the toast
   */
  @Prop() label?: string;

  /**
   * How many milliseconds to wait before hiding the toast. if `"0"`, it will show
   * until `close()` is called.
   */
  @Prop() timeout: number = 5000;

  /**
    * Indicate when closing the toast
    */
  @Event({ eventName: 'close' }) onClose!: EventEmitter<void>;

  /**
   * Watch the isOpen property to start the timeout before closing
   */
  @Watch('isOpen')
  isOpenChanged() {
    if (this.isOpen && this.timeout > 0) {
      setTimeout(() => this.close(), this.timeout);
    }
  }

  /**
   * Open the toast
   */
  @Method()
  async open() {
    this.isOpen = true;
    if(this.timeout > 0) {
      setTimeout(() => {
        this.close();
      }, this.timeout);
    }
  }

  /**
   * Close the toast
   */
  @Method()
  async close() {
    this.isOpen = false;
    this.onClose.emit();
  }

  /**
   * Close the dialog when clicking on the cross or layer
   */
  private onClick = (ev: UIEvent) => {
    ev.stopPropagation();
    ev.preventDefault();

    this.close();
  };

  componentDidLoad() {
    if (this.isOpen && this.timeout > 0) {
      setTimeout(() => this.close(), this.timeout);
    }
  }

  render() {
    const toastIsOpenClass = this.isOpen ? 'toast-open' : '';
    let icon;
    switch(this.color) {
    case 'info':
      icon = alertInfoOutline;
      break;
    case 'success':
      icon = alertSuccessOutline;
      break;
    case 'warning':
      icon = alertWarningOutline;
      break;
    case 'danger':
      icon = alertDangerOutline;
      break;
    default:
      icon = alertInfoOutline;
      break;
    }

    if (this.color =='danger') {
      this.timeout = 0
    }

    return (
      <Host class={`${toastIsOpenClass}`} role="alert">
        <div class={`toast toast-${this.color}`}>
          <road-icon class="toast-icon" icon={icon} aria-hidden="true"></road-icon>
          <p class="toast-label">{this.label}</p>
          <button type="button" class="toast-close" aria-label="Close" onClick={this.onClick}>
            <road-icon icon={navigationClose} size="md"></road-icon>
          </button>
          <slot name="progress"/>
        </div>
      </Host>
    );
  }
}