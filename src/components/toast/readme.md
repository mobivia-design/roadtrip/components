# road-toast



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                                                                                              | Type                                           | Default     |
| --------- | --------- | -------------------------------------------------------------------------------------------------------- | ---------------------------------------------- | ----------- |
| `color`   | `color`   | Set the color of the toast. e.g. info, success, warning, danger                                          | `"danger" \| "info" \| "success" \| "warning"` | `'info'`    |
| `isOpen`  | `is-open` | Set `open` propertie to `true` to open the toast                                                         | `boolean`                                      | `false`     |
| `label`   | `label`   | Text display in the toast                                                                                | `string \| undefined`                          | `undefined` |
| `timeout` | `timeout` | How many milliseconds to wait before hiding the toast. if `"0"`, it will show until `close()` is called. | `number`                                       | `5000`      |


## Events

| Event   | Description                     | Type                |
| ------- | ------------------------------- | ------------------- |
| `close` | Indicate when closing the toast | `CustomEvent<void>` |


## Methods

### `close() => Promise<void>`

Close the toast

#### Returns

Type: `Promise<void>`



### `open() => Promise<void>`

Open the toast

#### Returns

Type: `Promise<void>`




## Slots

| Slot         | Description                                                                                                                            |
| ------------ | -------------------------------------------------------------------------------------------------------------------------------------- |
| `"progress"` | the progress bar in the toast. color="info" for Info color="success" for success color="warning" for warning color="danger" for danger |


## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-toast --> road-icon
  style road-toast fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
