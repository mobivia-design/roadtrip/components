import { Component, h } from '@stencil/core';

/**
 * @slot  - Content of the label.
 */
@Component({
  tag: 'road-label',
  styleUrl: 'label.css',
  shadow: true,
})
export class Label {

  render() {
    return (
      <slot/>
    );
  }

}
