# road-label



<!-- Auto Generated Below -->


## Slots

| Slot | Description           |
| ---- | --------------------- |
|      | Content of the label. |


## Dependencies

### Used by

 - [road-content-card](../content-card)
 - [road-duration](../duration)
 - [road-item](../item)
 - [road-profil-dropdown](../profil-dropdown)
 - [road-rating](../rating)

### Graph
```mermaid
graph TD;
  road-content-card --> road-label
  road-duration --> road-label
  road-item --> road-label
  road-profil-dropdown --> road-label
  road-rating --> road-label
  style road-label fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
