import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Forms/Counter',
  component: 'road-counter',
  parameters: {
    actions: {
      handles: ['roadchange', 'roadinput', 'roadblur', 'roadfocus', 'roadincrease', 'roaddecrease'],
    },
  },
  argTypes: {
    'input-id': {
      control: 'text',
      description: 'Unique identifier for the input field.',
    },
    size: {
      options: ['sm', 'md', 'lg'],
      control: { type: 'select' },
      description: 'Defines the size of the counter.',
      table: {
        defaultValue: { summary: 'md' },
      },
    },
    min: {
      control: 'number',
      description: 'Minimum value allowed for the counter.',
      table: {
        defaultValue: { summary: 1 },
      },
    },
    max: {
      control: 'number',
      description: 'Maximum value allowed for the counter.',
      table: {
        defaultValue: { summary: 20 },
      },
    },
    step: {
      control: 'number',
      description: 'Step interval for increasing or decreasing the counter value.',
      table: {
        defaultValue: { summary: 1 },
      },
    },
    value: {
      control: 'number',
      description: 'Current value of the counter.',
      table: {
        defaultValue: { summary: 1 },
      },
    },
    dustbin: {
      control: 'boolean',
      description: 'Displays a delete (dustbin) icon when set to `true`.',
    },
    readonly: {
      control: 'boolean',
      description: 'Prevents editing the value when set to `true`.',
    },
    '--counter-margin-bottom': {
      table: { defaultValue: { summary: '1rem' } },
      control: 'text',
      description: 'Defines the margin-bottom of the counter.',
    },
  },
  args: {
    min: 1,
    max: 20,
    step: 1,
    value: 1,
    dustbin: false,
    readonly: false,
  },
};

const Template = (args) => html`
  <road-counter 
    size="${ifDefined(args.size)}" 
    min="${ifDefined(args.min)}" 
    max="${ifDefined(args.max)}" 
    step="${ifDefined(args.step)}" 
    value="${ifDefined(args.value)}"
    ?dustbin="${args.dustbin}"
    ?readonly="${args.readonly}"
    input-id="${ifDefined(args['input-id'])}"
    style="--counter-margin-bottom: ${ifDefined(args['--counter-margin-bottom'])};"
  ></road-counter>
`;

export const Playground = Template.bind({});

export const Medium = Template.bind({});
Medium.args = {
  size: 'md',
};

export const Small = Template.bind({});
Small.args = {
  size: 'sm',
};

export const ReadOnly = Template.bind({});
ReadOnly.args = {
  readonly: true,
};

export const WithDustbin = Template.bind({});
WithDustbin.args = {
  dustbin: true,
};
