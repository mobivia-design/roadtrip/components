import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('input-counter', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-counter min="1" max="10"></road-counter>
      `,
    });
  });

  describe("render", () => {
    it('should render', async () => {
      const element = await page.find('road-counter');
      expect(element).toHaveClass('hydrated');

      const results = await page.compareScreenshot();
      expect(results).toMatchScreenshot();
    });
  });

  describe("Dustbin", () => {
    it("Should render the dustbin.", async () => {
      page = await newE2EPage({
        html: `
          <road-counter value="1" min="1" max="10" dustbin="true"/>
        `,
      });
      const nameValue = await page.evaluate(`document.querySelector('road-counter').querySelector('road-button[slot="prepend"]').querySelector('road-icon').getAttribute('aria-label')`);

      expect(nameValue).toEqual('delete forever')
    });
    it("Should not render the dustbin.", async () => {
      page = await newE2EPage({
        html: `
          <road-counter value="5" min="1" max="10" dustbin="true"/>
        `,
      });
      const nameValue = await page.evaluate(`document.querySelector('road-counter').querySelector('road-button[slot="prepend"]').querySelector('road-icon').getAttribute('aria-label')`);

      expect(nameValue).toEqual('navigation add less')
    });

    it("Should not render the dustbin when the value is superior to the minimum.", async () => {
      page = await newE2EPage({
        html: `
          <road-counter value="2" min="1" max="10" dustbin="true"/>
        `,
      });
      let ariaLabelValue = await page.evaluate(`document.querySelector('road-counter').querySelector('road-button[slot="prepend"]').querySelector('road-icon').getAttribute('aria-label')`);
      expect(ariaLabelValue).toEqual('navigation add less');
    });
  });

  describe("Input", () => {
    beforeEach(async () => {
      page = await newE2EPage({
        html: `
          <road-counter value="2" min="1" max="10" dustbin="true"/>
        `,
      });
    });


    it("Should set max value when the entered value is superior to the max.", async () => {
      await page.focus(`road-counter road-input input`)
      await page.keyboard.type("9", { delay: 100 })
      const inputValue = await page.evaluate(`document.querySelector('road-counter').querySelector('input').value`);

      expect(inputValue).toEqual('10');
    });

    it("Should set min value when the entered value is inferior to the min.", async () => {
      await page.focus(`road-counter road-input input`)
      await page.keyboard.press('Backspace');
      await page.keyboard.type("0", { delay: 100 })
      const inputValue = await page.evaluate(`document.querySelector('road-counter').querySelector('input').value`);

      expect(inputValue).toEqual('1');
    });

    it("Should set max value when the entered value is superior to the max.", async () => {
      await page.focus(`road-counter road-input input`)
      await page.keyboard.type("0", { delay: 100 })
      const inputValue = await page.evaluate(`document.querySelector('road-counter').querySelector('input').value`);

      expect(inputValue).toEqual('10');
    });
  });

  describe("Buttons", () => {
    describe("increase button", () => {
      it("Should deactivate the increase button when the actual value is equal to the max.", async () => {
        page = await newE2EPage({
          html: `
            <road-counter value="9" min="1" max="10"/>
          `,
        });

        const increaseButton = await page.evaluateHandle(`document.querySelector('road-counter').querySelector('road-button[slot="append"]')`);

        const increaseButtonElt = await page.find(`road-counter road-button[slot="append"]`);

        expect(increaseButtonElt.className).not.toContain("disabled");

        await increaseButton.click();

        await page.waitForChanges();

        expect(increaseButtonElt.className).toContain("disabled");
      });
      it("Should deactivate the increase button when the value set in the input is superior to the max.", async () => {
        page = await newE2EPage({
          html: `
            <road-counter value="7" min="1" max="10"/>
          `,
        });

        const increaseButtonElt = await page.find(`road-counter road-button[slot="append"]`);

        expect(increaseButtonElt.className).not.toContain("disabled");

        await page.focus(`road-counter road-input input`)
        await page.keyboard.type("1", { delay: 100 })

        await page.waitForChanges();

        expect(increaseButtonElt.className).toContain("disabled");
      });
      it("Should deactivate the increase button when the value set in the input is equal to the max.", async () => {
        page = await newE2EPage({
          html: `
            <road-counter value="7" min="1" max="9"/>
          `,
        });

        const increaseButtonElt = await page.find(`road-counter road-button[slot="append"]`);

        expect(increaseButtonElt.className).not.toContain("disabled");

        await page.focus(`road-counter road-input input`)
        await page.keyboard.press('Backspace');

        await page.keyboard.type("9", { delay: 100 })

        await page.waitForChanges();

        expect(increaseButtonElt.className).toContain("disabled");
      });
    });
    describe("decrease button", () => {
      it("Should deactivate the decrease button when the actual value is equal to the min.", async () => {
        page = await newE2EPage({
          html: `
          <road-counter value="2" min="1" max="10"/>
        `,
        });

        const decreaseButton = await page.evaluateHandle(`document.querySelector('road-counter').querySelector('road-button[slot="prepend"]')`);

        const decreaseButtonElt = await page.find('road-counter road-button[slot="prepend"]');

        expect(decreaseButtonElt.className).not.toContain("disabled");

        await decreaseButton.click();

        await page.waitForChanges();

        expect(decreaseButtonElt.className).toContain("disabled");
      });
      it("Should deactivate the decrease button when the value set in the input is inferior to the min.", async () => {
        page = await newE2EPage({
          html: `
          <road-counter value="7" min="1" max="10"/>
        `,
        });

        const decreaseButtonElt = await page.find(`road-counter road-button[slot="prepend"]`);

        expect(decreaseButtonElt.className).not.toContain("disabled");

        await page.focus(`road-counter road-input input`)
        await page.keyboard.press('Backspace');

        await page.keyboard.type("0", { delay: 100 })

        await page.waitForChanges();

        expect(decreaseButtonElt.className).toContain("disabled");
      });
      it("Should deactivate the decrease button when the value set in the input is equal to the min.", async () => {
        page = await newE2EPage({
          html: `
          <road-counter value="7" min="1" max="9"/>
        `,
        });

        const decreaseButtonElt = await page.find(`road-counter road-button[slot="prepend"]`);

        expect(decreaseButtonElt.className).not.toContain("disabled");

        await page.focus(`road-counter road-input input`)
        await page.keyboard.press('Backspace');

        await page.keyboard.type("1", { delay: 100 })

        await page.waitForChanges();

        expect(decreaseButtonElt.className).toContain("disabled");
      });
    });
  });

  describe("Events", () => {
    beforeEach(async () => {
      page = await newE2EPage({
        html: `
          <road-counter value="2" min="1" max="10" dustbin="true"/>
        `,
      });
    });
    it("Should emit roadIncrease on click on the append button.", async () => {
      const roadincrease = await page.spyOnEvent('roadIncrease');
      const increaseButton = await page.evaluateHandle(`document.querySelector('road-counter').querySelector('road-button[slot="append"]')`);
      await increaseButton.click();
      expect(roadincrease).toHaveReceivedEventDetail({ value: 3 });
    });
    it("Should emit roadDecrease on click on the prepend button.", async () => {
      const roaddecrease = await page.spyOnEvent('roadDecrease');
      const decreaseButton = await page.evaluateHandle(`document.querySelector('road-counter').querySelector('road-button[slot="prepend"]')`);
      await decreaseButton.click();
      expect(roaddecrease).toHaveReceivedEventDetail({ value: 1 });
    });
    it("Should emit roadChange on change on the input.", async () => {
      const roadChange = await page.spyOnEvent('roadChange');
      await page.focus(`road-counter road-input input`)
      await page.keyboard.type("9", { delay: 100 })
      expect(roadChange).toHaveReceivedEventDetail({ value: 10 });
    });
    it("Should emit roadChange on change on the input when the input is empty with null.", async () => {
      const roadChange = await page.spyOnEvent('roadChange');
      await page.focus(`road-counter road-input input`)
      await page.keyboard.press('Backspace');
      expect(roadChange).toHaveReceivedEventDetail({ value: null });
    });

    it("Should emit roadDustbinClick when the dustbin is clicked.", async () => {
      page = await newE2EPage({
        html: `
          <road-counter value="1" min="1" max="10" dustbin="true"/>
        `,
      });
      const roadDustbinClick = await page.spyOnEvent('roadDustbinClick');
      const decreaseButton = await page.evaluateHandle(`document.querySelector('road-counter').querySelector('road-button[slot="prepend"]')`);
      await decreaseButton.click();

      expect(roadDustbinClick).toHaveReceivedEvent();
    });
  });
});
