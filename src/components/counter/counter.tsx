import { Component, h, Prop, Element, Event, EventEmitter, State, Watch } from '@stencil/core';

@Component({
  tag: 'road-counter',
  styleUrl: 'counter.css',
  scoped: true,
})
export class Counter {

  @Element() el!: HTMLRoadCounterElement;

  iconCounterLess!: HTMLRoadIconElement;
  inputElement!: HTMLRoadInputElement;

  @State() leftIconClasses: string = "";
  @State() rightIconClasses: string = "";
  @State() isDustbinVisible: boolean = false;

  /**
   * The id of counter
   */
  @Prop() inputId: string = `road-counter-${counterIds++}`;

  /**
   * The minimum value, which must not be greater than its maximum (max attribute) value.
   */
  @Prop() min: number = 0;

  /**
   * The maximum value, which must not be less than its minimum (min attribute) value.
   */
  @Prop() max?: number;

  /**
   * Works with the min and max attributes to limit the increments at which a value can be set.
   * Possible values are: `"any"` or a positive floating point number.
   */
  @Prop() step?: string;

  /**
   * The value of the counter.
   */
  @Prop() value: number = 1;

  /**
   * The size of the counter.
   */
  @Prop() size?: 'sm' | 'md' | 'lg' = "lg";

  /**
  * Set to `true` to add the dustbin icon if the value is equal to the min.
  */
  @Prop() dustbin: boolean = false;

    /**
  * Set to `true` to not modify the input field
  */
  @Prop() readonly: boolean = false;

  /**
   * Emitted when the plus button is clicked
   */
  @Event() roadIncrease!: EventEmitter<{
    value: number | undefined | null;
  }>;

  /**
   * Emitted when the minus button is clicked
   */
  @Event() roadDecrease!: EventEmitter<{
    value: number | undefined | null;
  }>;

  /**
   * Emitted when the minus button is clicked
   */
  @Event() roadChange!: EventEmitter<{
    value: number | undefined | null;
  }>;

  /**
   * Emitted when the dustbin is clicked
  */
  @Event() roadDustbinClick!: EventEmitter<void>;

  @Watch('value')
  private onValueChange(valueInput: number) {
    this.setIsDustbinVisible(valueInput);
    this.setIconsClasses(valueInput);
  }

  private setIconsClasses(valueInput?: number) {
    valueInput = valueInput ?? parseInt(this.inputElement.querySelector('input')!.value as string);

    this.leftIconClasses = valueInput <= this.min && !this.dustbin ? "disabled" : "";
    this.rightIconClasses = this.max && valueInput >= this.max ? "disabled" : "";
  }

  private setIsDustbinVisible = (valueInput: number) => {
    if (valueInput > this.min) {
      this.isDustbinVisible = false;
    } else if (this.dustbin) {
      this.isDustbinVisible = true;
    }
    else {
      this.isDustbinVisible = false;
    }
  }

  private increase = () => {
    (this.inputElement as unknown as HTMLInputElement).querySelector('input')!.stepUp();
    let valueInput = parseInt(this.inputElement.querySelector('input')!.value as string);
    if(!valueInput) {
      valueInput = this.min
    };
    this.onValueChange(valueInput);
    this.roadIncrease.emit({ value: valueInput });
  };

  private decrease = () => {
    if(this.isDustbinVisible){
      this.roadDustbinClick.emit();
      return;
    }

    (this.inputElement as unknown as HTMLInputElement).querySelector('input')!.stepDown();

    let valueInput = parseInt(this.inputElement.querySelector('input')!.value as string);
    if(!valueInput) {
      valueInput = this.min
    };
    this.onValueChange(valueInput);

    this.roadDecrease.emit({ value: valueInput });
  };

  private checkValue = (ev: Event) => {
    const input = ev.target as HTMLInputElement;
    const value = input.value && parseInt(input.value)

    if (value == null || value === "") return;

    if (value < this.min) {
      input.value = this.min.toString()
    }
    else if (this.max && value > this.max) {
      input.value = this.max.toString()
    }

    this.onValueChange(parseInt(input.value));
  }

  private onRoadChange = (ev: Event) => {
    ev.stopPropagation();
    const input = ev.target as HTMLInputElement;
    const value = input.value && parseInt(input.value)
    if (value !== null && value !== "") {
      if (value < this.min) {
        this.roadChange.emit({ value: this.min });
      }
      else if (this.max && value > this.max) {
        this.roadChange.emit({ value: this.max });
      }
      else {
        this.roadChange.emit({ value: parseInt(input.value) });
      }
    }
    else {
      this.roadChange.emit({ value: null });
    }
  };

  componentWillLoad() {
    this.onValueChange(this.value);
  }

  componentDidLoad() {
    const buttons = this.el.querySelectorAll('road-button') as NodeListOf<HTMLRoadButtonElement>;

    // Ajouter les événements pour chaque road-button
    buttons.forEach((btn) => {
      btn.addEventListener('touchstart', () => {
        btn.style.backgroundColor = 'var(--road-button-tertiary-variant)'; // Appliquer le style hover
      });
  
      btn.addEventListener('touchend', () => {
        btn.style.backgroundColor = 'var(--road-surface)'; // Supprimer le style hover après touchend
      });
    });

    // Accessibilité : Cacher le label
    const label = this.el.querySelector('.form-label') as HTMLElement;
    const input = this.el.querySelector('.form-control.sc-road-input') as HTMLElement;
    if (label) {
      label.style.clip = 'rect(0 0 0 0)';
      label.style.border = '0';
      label.style.height = '1px';
      label.style.left = '0';
      label.style.margin = '-1px';
      label.style.overflow = 'hidden';
      label.style.padding = '0';
      label.style.position = 'absolute';
      label.style.top = '0';
      label.style.width = '1px';

      input.style.padding = '0 1rem 0';
      
    }
  }

  render() {

    const dataCi = this.isDustbinVisible ? "road-dustbin" : "";

    return (
      <road-input-group
        class={this.size && `counter-${this.size}`}
      >
        <road-button
          slot="prepend"
          size={this.size}
          onClick={this.decrease}
          class={this.leftIconClasses}
          data-cy="road-decrease"
        >
          <road-icon
            name={this.isDustbinVisible ? "delete-forever" : "navigation-add-less"}
            ref={(el) => this.iconCounterLess = el as HTMLRoadIconElement}
            size={this.size}
            data-cy={dataCi}
            role="button"
          />
        </road-button>
        <road-input
          ref={(el) => this.inputElement = el as HTMLRoadInputElement}
          type="number"
          min={this.min.toString()}
          max={(this.max && this.max.toString()) as string}
          step={this.step}
          value={this.value}
          onRoadChange={this.onRoadChange}
          onKeyUp={this.checkValue}
          data-cy="road-input-counter"
          readonly={this.readonly}
          label="Quantité"
        />
        <road-button
          slot="append"
          size={this.size}
          onClick={this.increase}
          class={this.rightIconClasses}
          data-cy="road-increase"
        >
          <road-icon
            name="navigation-add-more"
            size={this.size}
            role="button"
          />
        </road-button>
      </road-input-group>
    );
  }
}

let counterIds = 0;
