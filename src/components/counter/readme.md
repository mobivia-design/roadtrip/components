# counter



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                                                                                                                  | Type                                | Default                              |
| ---------- | ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------- | ------------------------------------ |
| `dustbin`  | `dustbin`  | Set to `true` to add the dustbin icon if the value is equal to the min.                                                                                      | `boolean`                           | `false`                              |
| `inputId`  | `input-id` | The id of counter                                                                                                                                            | `string`                            | `` `road-counter-${counterIds++}` `` |
| `max`      | `max`      | The maximum value, which must not be less than its minimum (min attribute) value.                                                                            | `number \| undefined`               | `undefined`                          |
| `min`      | `min`      | The minimum value, which must not be greater than its maximum (max attribute) value.                                                                         | `number`                            | `0`                                  |
| `readonly` | `readonly` | Set to `true` to not modify the input field                                                                                                                  | `boolean`                           | `false`                              |
| `size`     | `size`     | The size of the counter.                                                                                                                                     | `"lg" \| "md" \| "sm" \| undefined` | `"lg"`                               |
| `step`     | `step`     | Works with the min and max attributes to limit the increments at which a value can be set. Possible values are: `"any"` or a positive floating point number. | `string \| undefined`               | `undefined`                          |
| `value`    | `value`    | The value of the counter.                                                                                                                                    | `number`                            | `1`                                  |


## Events

| Event              | Description                              | Type                                                   |
| ------------------ | ---------------------------------------- | ------------------------------------------------------ |
| `roadChange`       | Emitted when the minus button is clicked | `CustomEvent<{ value: number \| null \| undefined; }>` |
| `roadDecrease`     | Emitted when the minus button is clicked | `CustomEvent<{ value: number \| null \| undefined; }>` |
| `roadDustbinClick` | Emitted when the dustbin is clicked      | `CustomEvent<void>`                                    |
| `roadIncrease`     | Emitted when the plus button is clicked  | `CustomEvent<{ value: number \| null \| undefined; }>` |


## CSS Custom Properties

| Name                      | Description                |
| ------------------------- | -------------------------- |
| `--counter-margin-bottom` | margin bottom of the field |


## Dependencies

### Depends on

- [road-input-group](../input-group)
- [road-button](../button)
- [road-icon](../icon)
- [road-input](../input)

### Graph
```mermaid
graph TD;
  road-counter --> road-input-group
  road-counter --> road-button
  road-counter --> road-icon
  road-counter --> road-input
  road-input --> road-icon
  style road-counter fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
