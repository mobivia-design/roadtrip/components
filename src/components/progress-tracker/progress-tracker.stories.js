import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Indicators/Progress tracker',
  component: 'road-progress-tracker',
  subcomponents: {
    'road-progress-tracker-item': 'road-progress-tracker-item',
  },
  argTypes: {
    ' ': {
      description: "content of the progress stepper tracker item, it should be road-progress-tracker-item elements.\n\nif the state of the step is completed add the class `completed` on the road-progress-tracker-item\nif the state of the step is in progress add the class `in-progress` on the road-progress-tracker-item\nif the state of the step is current add the class `current` on the road-progress-tracker-item",
      control: 'text',
    },
  },
  args: {
    ' ': `<road-progress-tracker-item class="in-progress">
    <div class="progress-tracker-link">
      <span class="progress-tracker-circle"></span>
      <span class="progress-tracker-line"></span>
    </div>
    <div class="progress-tracker-item-content">
        <road-label class="progress-tracker-title">Label</road-label>
        <road-label class="progress-tracker-description">Description</road-label>
    </div>
  </road-progress-tracker-item>
  <road-progress-tracker-item class="completed">
    <div class="progress-tracker-link">
      <span class="progress-tracker-substep-circle"></span>
      <span class="progress-tracker-line"></span>
    </div>
    <div class="progress-tracker-item-content">
      <road-label class="progress-tracker-substep-title">Title of the sub step</road-label>
      <road-label class="progress-tracker-substep-description">Description</road-label>
    </div>
  </road-progress-tracker-item>
  <road-progress-tracker-item class="current">
    <div class="progress-tracker-link">
      <span class="progress-tracker-substep-circle"></span>
      <span class="progress-tracker-line"></span>
    </div>
    <div class="progress-tracker-item-content">
      <road-label class="progress-tracker-substep-title">Title of the sub step</road-label>
      <road-label class="progress-tracker-substep-description">Description</road-label>
    </div>
  </road-progress-tracker-item>
  <road-progress-tracker-item>
    <div class="progress-tracker-link">
      <span class="progress-tracker-circle">
      </span>
      <span class="progress-tracker-line"></span>
    </div>
    <div class="progress-tracker-item-content">
      <road-label class="progress-tracker-title">Label</road-label>
      <road-label class="progress-tracker-description">Description</road-label>
    </div>
  </road-progress-tracker-item>
  <road-progress-tracker-item>
  <div class="progress-tracker-link">
    <span class="progress-tracker-circle">
    </span>
    <span class="progress-tracker-line"></span>
  </div>
  <div class="progress-tracker-item-content">
    <road-label class="progress-tracker-title">Label</road-label>
    <road-label class="progress-tracker-description">Description</road-label>
    </div>
</road-progress-tracker-item>
<road-progress-tracker-item>
<div class="progress-tracker-link">
  <span class="progress-tracker-circle">
  </span>
  <span class="progress-tracker-line"></span>
</div>
<div class="progress-tracker-item-content">
  <road-label class="progress-tracker-title">Label</road-label>
  <road-label class="progress-tracker-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item>
<div class="progress-tracker-link">
  <span class="progress-tracker-circle">
  </span>
  <span class="progress-tracker-line"></span>
</div>
<div class="progress-tracker-item-content">
  <road-label class="progress-tracker-title">Label</road-label>
  <road-label class="progress-tracker-description">Description</road-label>
  </div>
</road-progress-tracker-item>
`,
  },
};

export const Template = (args) => html`
  <road-progress-tracker>
  ${unsafeHTML(args[' '])}
      
  </road-progress-tracker>
`;


export const OneCompletedStep = Template.bind({});
OneCompletedStep.args = {
  ' ': `<road-progress-tracker-item class="completed">
  <div class="progress-tracker-link">
    <span class="progress-tracker-circle"></span>
    <span class="progress-tracker-line"></span>
  </div>
  <div class="progress-tracker-item-content">
      <road-label class="progress-tracker-title">Label</road-label>
      <road-label class="progress-tracker-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item class="completed">
  <div class="progress-tracker-link">
    <span class="progress-tracker-substep-circle"></span>
    <span class="progress-tracker-line"></span>
  </div>
  <div class="progress-tracker-item-content">
    <road-label class="progress-tracker-substep-title">Title of the sub step</road-label>
    <road-label class="progress-tracker-substep-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item class="completed">
  <div class="progress-tracker-link">
    <span class="progress-tracker-substep-circle"></span>
    <span class="progress-tracker-line"></span>
  </div>
  <div class="progress-tracker-item-content">
    <road-label class="progress-tracker-substep-title">Title of the sub step</road-label>
    <road-label class="progress-tracker-substep-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item class="completed">
  <div class="progress-tracker-link">
    <span class="progress-tracker-circle">
    </span>
    <span class="progress-tracker-line"></span>
  </div>
  <div class="progress-tracker-item-content">
    <road-label class="progress-tracker-title">Label</road-label>
    <road-label class="progress-tracker-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item class="current">
<div class="progress-tracker-link">
  <span class="progress-tracker-circle">
  </span>
  <span class="progress-tracker-line"></span>
</div>
<div class="progress-tracker-item-content">
  <road-label class="progress-tracker-title">Label</road-label>
  <road-label class="progress-tracker-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item>
<div class="progress-tracker-link">
<span class="progress-tracker-circle">
</span>
<span class="progress-tracker-line"></span>
</div>
<div class="progress-tracker-item-content">
<road-label class="progress-tracker-title">Label</road-label>
<road-label class="progress-tracker-description">Description</road-label>
</div>
</road-progress-tracker-item>
<road-progress-tracker-item>
<div class="progress-tracker-link">
<span class="progress-tracker-circle">
</span>
<span class="progress-tracker-line"></span>
</div>
<div class="progress-tracker-item-content">
<road-label class="progress-tracker-title">Label</road-label>
<road-label class="progress-tracker-description">Description</road-label>
</div>
</road-progress-tracker-item>
`,
};


export const ShowMoreStep = Template.bind({});
ShowMoreStep.args = {
  ' ': `<road-collapse show-more="show more" show-less="show less">
  <road-progress-tracker-item class="completed">
  <div class="progress-tracker-link">
    <span class="progress-tracker-circle"></span>
    <span class="progress-tracker-line"></span>
  </div>
  <div class="progress-tracker-item-content">
      <road-label class="progress-tracker-title">Label</road-label>
      <road-label class="progress-tracker-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item class="completed">
  <div class="progress-tracker-link">
    <span class="progress-tracker-substep-circle"></span>
    <span class="progress-tracker-line"></span>
  </div>
  <div class="progress-tracker-item-content">
    <road-label class="progress-tracker-substep-title">Title of the sub step</road-label>
    <road-label class="progress-tracker-substep-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item class="completed">
  <div class="progress-tracker-link">
    <span class="progress-tracker-substep-circle"></span>
    <span class="progress-tracker-line"></span>
  </div>
  <div class="progress-tracker-item-content">
    <road-label class="progress-tracker-substep-title">Title of the sub step</road-label>
    <road-label class="progress-tracker-substep-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item class="in-progress">
  <div class="progress-tracker-link">
    <span class="progress-tracker-circle">
    </span>
    <span class="progress-tracker-line"></span>
  </div>
  <div class="progress-tracker-item-content">
    <road-label class="progress-tracker-title">Label</road-label>
    <road-label class="progress-tracker-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<road-progress-tracker-item class="current before-collapse">
<div class="progress-tracker-link">
  <span class="progress-tracker-circle">
  </span>
  <span class="progress-tracker-line"></span>
</div>
<div class="progress-tracker-item-content">
  <road-label class="progress-tracker-title">Label</road-label>
  <road-label class="progress-tracker-description">Description</road-label>
  </div>
</road-progress-tracker-item>
<div slot="collapsed-content">
    <road-progress-tracker-item>
    <div class="progress-tracker-link">
    <span class="progress-tracker-circle">
    </span>
    <span class="progress-tracker-line"></span>
    </div>
    <div class="progress-tracker-item-content">
    <road-label class="progress-tracker-title">Label</road-label>
    <road-label class="progress-tracker-description">Description</road-label>
    </div>
    </road-progress-tracker-item>
    <road-progress-tracker-item>
    <div class="progress-tracker-link">
    <span class="progress-tracker-circle">
    </span>
    <span class="progress-tracker-line"></span>
    </div>
    <div class="progress-tracker-item-content">
    <road-label class="progress-tracker-title">Label</road-label>
    <road-label class="progress-tracker-description">Description</road-label>
    </div>
    </road-progress-tracker-item>
  </div>
</road-collapse>
`,
};