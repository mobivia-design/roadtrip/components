# road-spinner



<!-- Auto Generated Below -->


## Slots

| Slot | Description                                                                                                                                                                                                                                                                                                                                                                                            |
| ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
|      | content of the progress stepper tracker item, it should be road-progress-tracker-item elements.  if the state of the step is completed add the class `completed` on the road-progress-tracker-item if the state of the step is in progress add the class `in-progress` on the road-progress-tracker-item if the state of the step is current add the class `current` on the road-progress-tracker-item |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
