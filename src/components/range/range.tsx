import {
  Component,
  h,
  Element,
  Prop,
  Event,
  Host,
  EventEmitter,
  Watch,
  Listen,
} from "@stencil/core";
import { navigationAddLessSolid, navigationAddMoreSolid } from "../../../icons";

/**
 * @slot datalist - list of labels. also put show-labels="true" and max="10" for this exemple
 * `<datalist id="tickmarks" slot="datalist" class="tickmarks">`
      `<option value="0" label="0%"></option>`
      `<option value="10" label="10%"></option>`
      `<option value="20" label="20%"></option>`
      `<option value="30" label="30%"></option>`
      `<option value="40" label="40%"></option>`
      `<option value="50" label="50%"></option>`
      `<option value="60" label="60%"></option>`
     `<option value="70" label="70%"></option>`
     `<option value="80" label="80%"></option>`
      `<option value="90" label="90%"></option>`
      `<option value="100" label="100%"></option>`
    `</datalist>`
 */

@Component({
  tag: "road-range",
  styleUrl: "range.css",
  scoped: true,
})
export class Range {
  @Element() el!: HTMLRoadRangeElement;

  /**
   * The id of range
   */
  @Prop() rangeId: string = `road-range-${rangeIds++}`;

  /**
   * The value of the range.
   */
  @Prop({ mutable: true }) value?: string | number | null = "";

  /**
   * The minimum value, which must not be greater than its maximum (max attribute) value.
   */
  @Prop() min!: string;

  /**
   * The maximum value, which must not be less than its minimum (min attribute) value.
   */
  @Prop() max!: string;

  /**
   * Works with the min and max attributes to limit the increments at which a value can be set.
   * Possible values are: `"any"` or a positive floating point number.
   */
  @Prop() step?: string;

  /**
   * Display the current value of the range
   */
  @Prop() showValue: boolean = false;

  /**
   * Display Tick of the range
   */
  @Prop() showTick: boolean = false;

  /**
   * Display labels of the range
   */
  @Prop() showLabels: boolean = false;

  /**
   * If true, the range will be disabled
   */
  @Prop() disabled: boolean = false;

  /**
   * Update the native input element when the value changes
   */
  @Watch("value")
  protected valueChanged() {
    this.roadchange.emit({
      value: this.value == null ? this.value : this.value.toString(),
    });
    this.roadChange.emit({
      value: this.value == null ? this.value : this.value.toString(),
    });
  }

  /**
   * Emitted when the value has changed.
   */
  @Event() roadchange!: EventEmitter<{
    value: string | undefined | null;
  }>;
  /** @internal */
  @Event() roadChange!: EventEmitter<{
    value: string | undefined | null;
  }>;

  private getValue(): string {
    return typeof this.value === "number"
      ? this.value.toString()
      : (this.value || "").toString();
  }

  private onInput = (ev: Event) => {
    const input = ev.target as HTMLInputElement | null;
    if (input) {
      this.value = input.value || "";
    }
    if (this.value !== null && this.value !== undefined) {
      this.el.style.setProperty("--value", `${this.value}`);
    }
  };

  @Listen("focus", { capture: true })
  handleFocus() {
    this.el.classList.add("focus-visible");
  }

  @Listen("blur", { capture: true })
  handleBlur() {
    this.el.classList.remove("focus-visible");
  }

  componentDidLoad() {
    // Cacher le label pour accessibilité

    const label = this.el.querySelector(".form-label") as HTMLElement;
    const input = this.el.querySelector(
      ".form-control.sc-road-input"
    ) as HTMLElement;
    if (label) {
      label.style.clip = "rect(0 0 0 0)";
      label.style.border = "0";
      label.style.height = "1px";
      label.style.left = "0";
      label.style.margin = "-1px";
      label.style.overflow = "hidden";
      label.style.padding = "0";
      label.style.position = "absolute";
      label.style.top = "0";
      label.style.width = "1px";

      input.style.padding = "0 1rem 0";
    }
  }

  render() {
    const value = this.getValue();
    const datalist = this.showLabels !== undefined ? `tickmarks` : "";

    return (
      <Host tabindex="0" class={{ disabled: this.disabled }}>
        <div class="form-group d-flex align-items-end">
          {this.showTick && (
            <road-icon
              icon={navigationAddLessSolid}
              class="mr-8"
              size="sm"
            ></road-icon>
          )}
          <div
            class="form-range"
            style={{
              "--min": this.min,
              "--max": this.max,
              "--value": value,
              "--background-color": this.disabled
                ? "var(--road-surface-disabled)"
                : "initial",
            }}
          >
            {this.showValue && <output></output>}
            {this.showLabels && <slot name="datalist" />}
            <input
              type="range"
              class="form-range-input"
              id={this.rangeId}
              min={this.min}
              max={this.max}
              value={value}
              onInput={this.onInput}
              list={datalist}
              aria-label="Valeur"
              tabindex="0"
              disabled={this.disabled}
            />
            <label class="form-range-label">Valeur</label>
            <div class="form-range-progress"></div>
          </div>
          {this.showTick && (
            <road-icon
              icon={navigationAddMoreSolid}
              class="ml-8"
              size="sm"
            ></road-icon>
          )}
        </div>
      </Host>
    );
  }
}

let rangeIds = 0;
