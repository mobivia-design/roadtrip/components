# road-range



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute     | Description                                                                                                                                                  | Type                                    | Default                          |
| ------------------ | ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------- | -------------------------------- |
| `disabled`         | `disabled`    | If true, the range will be disabled                                                                                                                          | `boolean`                               | `false`                          |
| `max` _(required)_ | `max`         | The maximum value, which must not be less than its minimum (min attribute) value.                                                                            | `string`                                | `undefined`                      |
| `min` _(required)_ | `min`         | The minimum value, which must not be greater than its maximum (max attribute) value.                                                                         | `string`                                | `undefined`                      |
| `rangeId`          | `range-id`    | The id of range                                                                                                                                              | `string`                                | `` `road-range-${rangeIds++}` `` |
| `showLabels`       | `show-labels` | Display labels of the range                                                                                                                                  | `boolean`                               | `false`                          |
| `showTick`         | `show-tick`   | Display Tick of the range                                                                                                                                    | `boolean`                               | `false`                          |
| `showValue`        | `show-value`  | Display the current value of the range                                                                                                                       | `boolean`                               | `false`                          |
| `step`             | `step`        | Works with the min and max attributes to limit the increments at which a value can be set. Possible values are: `"any"` or a positive floating point number. | `string \| undefined`                   | `undefined`                      |
| `value`            | `value`       | The value of the range.                                                                                                                                      | `null \| number \| string \| undefined` | `""`                             |


## Events

| Event        | Description                         | Type                                                   |
| ------------ | ----------------------------------- | ------------------------------------------------------ |
| `roadchange` | Emitted when the value has changed. | `CustomEvent<{ value: string \| null \| undefined; }>` |


## Slots

| Slot         | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `"datalist"` | list of labels. also put show-labels="true" and max="10" for this exemple `<datalist id="tickmarks" slot="datalist" class="tickmarks">` `<option value="0" label="0%"></option>` `<option value="10" label="10%"></option>` `<option value="20" label="20%"></option>` `<option value="30" label="30%"></option>` `<option value="40" label="40%"></option>` `<option value="50" label="50%"></option>` `<option value="60" label="60%"></option>` `<option value="70" label="70%"></option>` `<option value="80" label="80%"></option>` `<option value="90" label="90%"></option>` `<option value="100" label="100%"></option>` `</datalist>` |


## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-range --> road-icon
  style road-range fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
