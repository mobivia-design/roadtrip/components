import { newE2EPage } from '@stencil/core/testing';

describe('road-range', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<road-range></road-range>');

    const element = await page.find('road-range');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
