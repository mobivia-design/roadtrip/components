import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Forms/Range',
  component: 'road-range',
  argTypes: {
    min: {
      description: "The minimum value, which must not be greater than its maximum (max attribute) value.",
      control: 'number',
    },
    max: {
      description: "The maximum value, which must not be less than its minimum (min attribute) value.",
      control: 'number',
    },
    step: {
      description: "Works with the min and max attributes to limit the increments at which a value can be set.\nPossible values are: `\"any\"` or a positive floating point number.",
      control: 'number',
    },
    value: {
      description: "The value of the range.",
      control: 'number',
    },
    'show-value': {
      description: "Display the current value of the range",
      control: 'boolean',
    },
    'show-tick': {
      description: "Display Tick of the range",
      control: 'boolean',
    },
    'show-labels': {
      description: "Display labels of the range",
      control: 'boolean',
    },
    datalist: {
      description: "list of labels. also put show-labels=\"true\" and max=\"10\" for this exemple\n`<datalist id=\"tickmarks\" slot=\"datalist\" class=\"tickmarks\">`\n`<option value=\"0\" label=\"0%\"></option>`\n`<option value=\"10\" label=\"10%\"></option>`\n`<option value=\"20\" label=\"20%\"></option>`\n`<option value=\"30\" label=\"30%\"></option>`\n`<option value=\"40\" label=\"40%\"></option>`\n`<option value=\"50\" label=\"50%\"></option>`\n`<option value=\"60\" label=\"60%\"></option>`\n`<option value=\"70\" label=\"70%\"></option>`\n`<option value=\"80\" label=\"80%\"></option>`\n`<option value=\"90\" label=\"90%\"></option>`\n`<option value=\"100\" label=\"100%\"></option>`\n`</datalist>`",
      control: 'text',
    },
    'range-id': {
      description: "The id of range",
      control: 'text',
    },
    roadchange: {
      description: "Emitted when the value has changed.",
      action: 'roadchange',
      control: {
        type: null,
      },
    },
    disabled: {
      description: "If true, the range will be disabled",
      control: 'boolean',
    },
  },
  args: {
    min: 0,
    max: 10,
    step: null,
    value: 8,
    'show-value': null,
    'show-tick': null,
    'show-labels': null,
    datalist: '',
    disabled: null,
  },
};

export const Playground = (args) => html`
  <road-range
    min="${ifDefined(args.min)}" 
    max="${ifDefined(args.max)}"
    step="${ifDefined(args.step)}" 
    value="${ifDefined(args.value)}"
    show-value="${ifDefined(args['show-value'])}"
    show-tick="${ifDefined(args['show-tick'])}"
    show-labels="${ifDefined(args['show-labels'])}"
    disabled="${ifDefined(args.disabled)}"
    @roadchange=${event => args.roadchange(event.detail.value)}
    class="d-flex align-items-end"
  >
    ${unsafeHTML(args.datalist)}
  </road-range>
`;
