import { html } from 'lit';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Navigation/Tabs',
  component: 'road-tabs',
  subcomponents: {
    'road-tab-bar': 'road-tab-bar',
    'road-tab-button': 'road-tab-button',
    'road-tab': 'road-tab',
  },
  parameters: {
    actions: {
      handles: ['roadtabbuttonclick'],
    },
    backgrounds: {
      default: 'grey',
    },
  },
  argTypes: {
    top: {
      description: "Content is placed at the top of the screen.",
      control: 'text',
    },
    ' ': {
      description: "Content is placed between the named slots if provided without a slot.",
      control: 'text',
    },
    roadtabsdidchange: {
      control: {
        type: null,
      },
    },
    roadtabswillchange: {
      control: {
        type: null,
      },
    },
    getSelected : {
      description: 'Get the currently selected tab.',
      table: {
        category: 'methods',
        type: {
          summary: 'getSelected() => Promise<string | undefined>',
        },
      },
      defaultValue: {
        summary: null,
      },
    },
    getTab : {
      description: 'Get a specific tab by the value of its `tab` property or an element reference.',
      table: {
        category: 'methods',
        type: {
          summary: 'getTab(tab: string | HTMLRoadTabElement) => Promise<HTMLRoadTabElement | undefined>',
        },
      },
      defaultValue: {
        summary: null,
      },
    },
    select : {
      description: 'Select a tab by the value of its `tab` property or an element reference.',
      table: {
        category: 'methods',
        type: {
          summary: 'select(tab: string | HTMLRoadTabElement) => Promise<boolean>',
        },
      },
      defaultValue: {
        summary: null,
      },
    },
  },
};

const Template = (args) => html`
<road-tabs>
  ${unsafeHTML(args.top)}
  ${unsafeHTML(args[' '])}
</road-tabs>
`;

export const Playground = Template.bind({});
Playground.args = {
  top: `<road-tab-bar slot="top" selected-tab="tab-description">
    <road-tab-button tab="tab-description">
      <road-label>Description</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-services">
      <road-label>Service</road-label>
    </road-tab-button>
  </road-tab-bar>`,
  ' ': `<road-tab tab="tab-description">
    Description
  </road-tab>

  <road-tab tab="tab-services">
    Services
  </road-tab>`,
};

export const Scrollable = Template.bind({});
Scrollable.args = {
  top: `<road-tab-bar slot="top" selected-tab="tab-description">
    <road-tab-button tab="tab-description">
      <road-label>Description</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-services">
      <road-label>Service</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-accessories">
      <road-label>Accessories</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-reviews">
      <road-label>Reviews</road-label>
    </road-tab-button>
  </road-tab-bar>`,
  ' ': `<road-tab tab="tab-description">
    Description
  </road-tab>

  <road-tab tab="tab-services">
    Services
  </road-tab>

  <road-tab tab="tab-accessories">
    Accessories
  </road-tab>

  <road-tab tab="tab-reviews">
    Reviews
  </road-tab>`,
};

export const Center = Template.bind({});
Center.args = {
  top: `<road-tab-bar slot="top" center="true">
    <road-tab-button tab="tab-list">
      <road-label>List</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-map">
      <road-label>Map</road-label>
    </road-tab-button>
  </road-tab-bar>`,
  ' ': `<road-tab tab="tab-list">
    List
  </road-tab>

  <road-tab tab="tab-map">
    Map
  </road-tab>`,
};

export const Expand = Template.bind({});
Expand.args = {
  top: `<road-tab-bar slot="top" expand="true">
    <road-tab-button tab="tab-list">
      <road-label>List</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-map">
      <road-label>Map</road-label>
    </road-tab-button>
  </road-tab-bar>`,
  ' ': `<road-tab tab="tab-list">
    List
  </road-tab>

  <road-tab tab="tab-map">
    Map
  </road-tab>`,
};

export const Secondary = Template.bind({});
Secondary.args = {
  top: `<road-tab-bar slot="top" secondary="true">
    <road-tab-button tab="tab-list">
      <road-label>List</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-map">
      <road-label>Map</road-label>
    </road-tab-button>
  </road-tab-bar>`,
  ' ': `<road-tab tab="tab-list">
    List
  </road-tab>

  <road-tab tab="tab-map">
    Map
  </road-tab>`,
};

export const LeadingIcon = Template.bind({});
LeadingIcon.args = {
  top: `<road-tab-bar slot="top" expand="true">
    <road-tab-button tab="tab-list">
      <road-icon name="garage" aria-hidden="true"></road-icon>
      <road-label>List</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-map">
      <road-icon name="location-pin-all" aria-hidden="true"></road-icon>
      <road-label>Map</road-label>
    </road-tab-button>
  </road-tab-bar>`,
  ' ': `<road-tab tab="tab-list">
    List
  </road-tab>

  <road-tab tab="tab-map">
    Map
  </road-tab>`,
};

export const TopIcon = Template.bind({});
TopIcon.args = {
  top: `<road-tab-bar slot="top" expand="true">
    <road-tab-button tab="tab-shops" layout="icon-top">
      <road-icon name="shop" aria-hidden="true"></road-icon>
      <road-label>Shops</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-warehouses" layout="icon-top">
      <road-icon name="warehouse" aria-hidden="true"></road-icon>
      <road-label>Warehouses</road-label>
    </road-tab-button>

    <road-tab-button tab="tab-factories" layout="icon-top">
      <road-icon name="factory" aria-hidden="true"></road-icon>
      <road-label>Factories</road-label>
    </road-tab-button>
  </road-tab-bar>`,
  ' ': `<road-tab tab="tab-shops">
    Shops
  </road-tab>

  <road-tab tab="tab-warehouses">
    Warehouses
  </road-tab>

  <road-tab tab="tab-factories">
    Factories
  </road-tab>`,
};
