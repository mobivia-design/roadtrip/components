import { newE2EPage } from '@stencil/core/testing';

describe('road-tabs', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <road-tabs>
        <road-tab-bar slot="top" selected-tab="tab-description">
          <road-tab-button tab="tab-description">
            <road-label>Description</road-label>
          </road-tab-button>

          <road-tab-button tab="tab-services">
            <road-label>Service</road-label>
          </road-tab-button>
        </road-tab-bar>

        <road-tab tab="tab-description">
          Description
        </road-tab>

        <road-tab tab="tab-services">
          Services
        </road-tab>
      </road-tabs>
    `);

    const element = await page.find('road-tabs');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
