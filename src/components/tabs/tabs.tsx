import { Component, Element, Event, EventEmitter, Host, Method, State, h } from '@stencil/core';

/**
 * @slot - Content is placed between the named slots if provided without a slot.
 * @slot top - Content is placed at the top of the screen.
 */
@Component({
  tag: 'road-tabs',
  styleUrl: 'tabs.css',
  shadow: true,
})
export class Tabs {

  private transitioning = false;
  private leavingTab?: HTMLRoadTabElement;

  @Element() el!: HTMLRoadTabsElement;

  @State() selectedTab?: HTMLRoadTabElement;

  /**
   * Emitted when the navigation is about to transition to a new component.
   */
  @Event({ bubbles: false }) roadtabswillchange!: EventEmitter<{tab: string}>;
  /** @internal */
  @Event({ bubbles: false }) roadTabsWillChange!: EventEmitter<{tab: string}>;

  /**
   * Emitted when the navigation has finished transitioning to a new component.
   */
  @Event({ bubbles: false }) roadtabsdidchange!: EventEmitter<{tab: string}>;
  /** @internal */
  @Event({ bubbles: false }) roadTabsDidChange!: EventEmitter<{tab: string}>;

  async componentWillLoad() {
    const tabs = this.tabs;
    const tabBar = this.el.querySelector('road-tab-bar');
    const selectedTabId = tabBar ? tabBar.getAttribute('selected-tab') : null;
    
    if (tabs.length > 0) {
      if (selectedTabId) {
        const selectedTab = tabs.find(tab => tab.tab === selectedTabId);
        if (selectedTab) {
          await this.select(selectedTab);
        } else {
          await this.select(tabs[0]);
        }
      } else {
        await this.select(tabs[0]);
      }
    }
  }

  componentWillRender() {
    const tabBar = this.el.querySelector('road-tab-bar');
    if (tabBar) {
      const tab = this.selectedTab ? this.selectedTab.tab : undefined;
      tabBar.selectedTab = tab;
    }
  }

  /**
   * Select a tab by the value of its `tab` property or an element reference.
   *
   * @param tab The tab instance to select. If passed a string, it should be the value of the tab's `tab` property.
   */
  @Method()
  async select(tab: string | HTMLRoadTabElement): Promise<boolean> {
    const selectedTab = getTab(this.tabs, tab);
    if (!this.shouldSwitch(selectedTab)) {
      return false;
    }
    await this.setActive(selectedTab);
    this.tabSwitch();

    return true;
  }

  /**
   * Get a specific tab by the value of its `tab` property or an element reference.
   *
   * @param tab The tab instance to select. If passed a string, it should be the value of the tab's `tab` property.
   */
  @Method()
  async getTab(tab: string | HTMLRoadTabElement): Promise<HTMLRoadTabElement | undefined> {
    return getTab(this.tabs, tab);
  }

  /**
   * Get the currently selected tab.
   */
  @Method()
  getSelected(): Promise<string | undefined> {
    return Promise.resolve(this.selectedTab ? this.selectedTab.tab : undefined);
  }

  private setActive(selectedTab: HTMLRoadTabElement): Promise<void> {
    if (this.transitioning) {
      return Promise.reject('transitioning already happening');
    }

    this.transitioning = true;
    this.leavingTab = this.selectedTab;
    this.selectedTab = selectedTab;
    this.roadTabsWillChange.emit({ tab: selectedTab.tab });
    selectedTab.active = true;
    return Promise.resolve();
  }

  private tabSwitch() {
    const selectedTab = this.selectedTab;
    const leavingTab = this.leavingTab;

    this.leavingTab = undefined;
    this.transitioning = false;
    if (!selectedTab) {
      return;
    }

    if (leavingTab !== selectedTab) {
      if (leavingTab) {
        leavingTab.active = false;
      }
      this.roadTabsDidChange.emit({ tab: selectedTab.tab });
    }
  }

  private shouldSwitch(selectedTab: HTMLRoadTabElement | undefined): selectedTab is HTMLRoadTabElement {
    const leavingTab = this.selectedTab;
    return selectedTab !== undefined && selectedTab !== leavingTab && !this.transitioning;
  }

  private get tabs() {
    return Array.from(this.el.querySelectorAll('road-tab'));
  }

  private onTabClicked = (ev: CustomEvent) => {
    const { tab } = ev.detail;
    this.select(tab);
  };

  render() {
    return (
      <Host
        onRoadTabButtonClick={this.onTabClicked}
      >
        <slot name="top"/>
        <div class="tabs-inner">
          <slot/>
        </div>
      </Host>
    );
  }
}

const getTab = (tabs: HTMLRoadTabElement[], tab: string | HTMLRoadTabElement): HTMLRoadTabElement | undefined => {
  const tabEl = (typeof tab === 'string')
    ? tabs.find(t => t.tab === tab)
    : tab;

  if (!tabEl) {
    console.error(`tab with id: "${tabEl}" does not exist`);
  }
  return tabEl;
};