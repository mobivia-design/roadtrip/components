import { newE2EPage } from '@stencil/core/testing';

describe('road-banner', () => {

  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-banner label="Lorem ipsum dolor sit amet, consectetur adipiscing elit." link="See more"></road-banner>
      `,
    });
  });

  it('should render', async () => {
    const element = await page.find('road-banner');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should closed', async () => {
    const close = await page.spyOnEvent('close');

    const element = await page.find('road-banner');
    expect(element).toHaveClass('hydrated');

    const isOpen = await element.getProperty('isOpen');
    expect(isOpen).toBeTruthy();

    await element.callMethod('close');

    await page.waitForChanges();

    const isOpened = await element.getProperty('isOpen');
    expect(isOpened).toBeFalsy();

    await page.waitForTimeout(2000);
    expect(close).toHaveReceivedEvent();
  });
});
