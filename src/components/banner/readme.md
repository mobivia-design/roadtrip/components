# road-banner



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                      | Type                  | Default     |
| -------- | --------- | ------------------------------------------------ | --------------------- | ----------- |
| `isOpen` | `is-open` | Set `open` property to `true` to open the banner | `boolean`             | `true`      |
| `label`  | `label`   | Text display in the banner                       | `string`              | `''`        |
| `link`   | `link`    | Text Link display in the banner                  | `string \| undefined` | `undefined` |
| `url`    | `url`     | Text Link display in the banner                  | `string \| undefined` | `undefined` |


## Events

| Event   | Description                      | Type                |
| ------- | -------------------------------- | ------------------- |
| `close` | Indicate when closing the banner | `CustomEvent<void>` |


## Methods

### `close() => Promise<void>`

Close the banner

#### Returns

Type: `Promise<void>`




## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-banner --> road-icon
  style road-banner fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
