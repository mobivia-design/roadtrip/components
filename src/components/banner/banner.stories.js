import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Indicators/Banner',
  component: 'road-banner',
  argTypes: {
    label: {
      control: 'text',
      description: 'The main text content of the banner.',
      table: {
        defaultValue: { summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' },
      },
    },
    link: {
      control: 'text',
      description: 'The label of the link inside the banner.',
      table: {
        defaultValue: { summary: 'See more' },
      },
    },
    url: {
      control: 'text',
      description: 'The URL for the link inside the banner.',
      table: {
        defaultValue: { summary: '#' },
      },
    },
    'is-open': {
      control: 'boolean',
      description: 'Controls whether the banner is visible or not.',
      table: {
        defaultValue: { summary: true },
      },
    },
    close: {
      description: 'Event triggered when the banner is closed.',
      table: {
        category: 'Events',
        type: { summary: 'CustomEvent' },
      },
      control: { type: null }, // Désactive l'affichage du contrôle dans Storybook
    },
  },
  args: {
    label: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    link: 'See more',
    url: '#',
    'is-open': true,
  },
};

const Template = (args) => html`
  <road-banner
    label="${ifDefined(args.label)}"
    link="${ifDefined(args.link)}"
    url="${ifDefined(args.url)}"
    ?is-open="${args['is-open']}"
  ></road-banner>
`;

export const Playground = Template.bind({});
Playground.args = {
  label: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  link: 'See more',
  url: '#',
  'is-open': true,
};
