import { Component, Host, h, Prop, Method, Event, EventEmitter} from '@stencil/core';
import { navigationClose } from '../../../icons';

@Component({
  tag: 'road-banner',
  styleUrl: 'banner.css',
  shadow: true,
})
export class Banner {

  /**
   * Set `open` property to `true` to open the banner
   */
  @Prop({ mutable: true }) isOpen: boolean = true;

  /**
   * Text display in the banner
   */
  @Prop() label: string = '';

  /**
   * Text Link display in the banner
   */
  @Prop() link?: string;

  /**
   * Text Link display in the banner
   */
  @Prop() url?: string;

  /**
   * Close the dialog when clicking on the cross or layer
   */
  private onClick = (ev: UIEvent) => {
    ev.stopPropagation();
    ev.preventDefault();

    this.close();
  };

  /**
   * Close the banner
   */
  @Method()
  async close() {
    this.isOpen = false;
    this.onClose.emit();
  }

  /**
   * Indicate when closing the banner
   */
  @Event({ eventName: 'close' }) onClose!: EventEmitter<void>;

  render() {
    const bannerIsOpenClass = this.isOpen ? 'banner-open' : '';

    return (
      <Host class={bannerIsOpenClass}>
        <div>
          <span class="banner-open-label">{this.label}</span>
          {this.link && <a href={this.url} class="banner-open-link">{this.link}</a>}
          <button type="button" class="banner-close" onClick={this.onClick} aria-label="closed">
            <road-icon icon={navigationClose} size="md"></road-icon>
          </button>
        </div>
      </Host>
    );
  }

}
