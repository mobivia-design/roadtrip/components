# road-navbar



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                | Type                  | Default     |
| ------------- | -------------- | -------------------------- | --------------------- | ----------- |
| `selectedTab` | `selected-tab` | The selected tab component | `string \| undefined` | `undefined` |


## Slots

| Slot | Description                                                                          |
| ---- | ------------------------------------------------------------------------------------ |
|      | Content of the navbar, it should be road-navbar-item elements. Max 5 items on Mobile |


## CSS Custom Properties

| Name        | Description                |
| ----------- | -------------------------- |
| `--z-index` | The z-index of the Navbar. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
