import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Navigation/NavBar',
  component: 'road-navbar',
  subcomponents: {
    'road-navbar-item': 'road-navbar-item',
    'road-tooltip': 'road-tooltip',
  },
  parameters: {
    actions: {
      handles: ['roadnavbaritemclick'],
    },
    backgrounds: {
      default: 'grey',
    },
    layout: 'fullscreen',
  },
  argTypes: {
    ' ': {
      description: "Content of the navbar, it should be road-navbar-item elements. Max 5 items on Mobile",
      control: 'text',
    },
    'selected-tab': {
      description: "The selected tab component",
      control: 'text',
    },
    '--z-index': {
      description: "The z-index of the Navbar.",
      table: {
        defaultValue: { summary: '10' },
      },
      control: {
        type: null,
      },
    },
  },
  args: {
    ' ': `
    <road-profil-dropdown is-open="false" class="m-24 d-none d-xl-block" role="menuitem">
      <road-list slot="list">
        <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;" class="border-0">
          <road-icon slot="start" name="edit-outline" size="md" aria-hidden="true"></road-icon>
          <road-label style="font-size: 0.75rem">
            Edit profile
          </road-label>
        </road-item>
        <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;" class="border-0">
          <road-icon slot="start" name="log-out-outline" size="md" aria-hidden="true"></road-icon>
          <road-label style="font-size: 0.75rem">
            Log out
          </road-label>
        </road-item>
      </road-list>
      <road-img slot="avatar" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
      <road-img slot="avatarItem" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
      <road-label slot="name">First and Last name</road-label>
      <road-label slot="email">email</road-label>
    </road-profil-dropdown>

    <road-navbar-item tab="tab-home" aria-label="Home" role="menuitem">
    <road-tooltip content="Home" position="right" trigger="hover" contentalign="center">
      <road-icon name="navigation-home-outline" aria-hidden="true" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Home</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-search" aria-label="Search" role="menuitem">
    <road-tooltip content="Search" position="right" trigger="hover" contentalign="center">
      <road-icon name="search" aria-hidden="true" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Search</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-print" role="menuitem">
    <road-tooltip content="Print" position="right" trigger="hover" contentalign="center">
      <road-icon name="print-outline" aria-hidden="true" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Print</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-notification" class="d-none d-xl-flex" role="menuitem">
    <road-tooltip content="Notifications" position="right" trigger="hover" contentalign="center">
      <road-icon name="alert-notification-outline" aria-hidden="true" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Notifications</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-scan" class="d-none d-xl-flex" aria-label="Scan" role="menuitem">
    <road-tooltip content="Scan" position="right" trigger="hover" contentalign="center">
      <road-icon name="scan" aria-hidden="true" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Scan</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-catalogue" class="d-none d-xl-flex" role="menuitem">
    <road-tooltip content="Catalog" position="right" trigger="hover" contentalign="center">
      <road-icon name="file-catalog" aria-hidden="true" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Catalogue</road-label>
    </road-tootltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-diag" class="d-none d-xl-flex" role="menuitem">
    <road-tooltip content="Diagnostic" position="right" trigger="hover" contentalign="center">
      <road-icon name="Diagnostic" aria-hidden="true" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Diagnostic</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-menu" class="d-block d-xl-none tab-menu" role="menuitem">
    <road-icon name="navigation-menu" aria-hidden="true"></road-icon>
    <road-label>Menu</road-label>
  </road-navbar-item>
  
  <road-drawer is-open="false" position="right" drawer-width="480" drawer-title="Menu" class="d-xl-none drawer-menu" tabindex="0" role="menuitem">
  <road-item class="mb-16 text-left bg-light profil" tabindex="0" button="true">
    <road-avatar slot="start" class="mr-16">  
      <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
    </road-avatar>
      <road-label>
      <p class="h8 mb-4">First and last name</p>
      <p class="text-medium text-truncate m-0">Email</p>
    </road-label>

    <road-icon slot="end" name="edit-outline" size="sm" class="mr-8" aria-hidden="true"></road-icon>
  </road-item>

  <road-item class="bg-white border-0" button="true">
    <road-icon name="scan" aria-hidden="true"></road-icon>
    Scan
  </road-item>

  <road-item class="bg-white border-0" button="true">
    <road-icon name="file-catalog" aria-hidden="true"></road-icon>
    Catalogue
  </road-item>

  <road-item class="bg-white border-0" button="true">
    <road-icon name="Diagnostic" aria-hidden="true"></road-icon>
    Diagnostic
  </road-item>

  <road-item class="bg-white border-left-0 border-right-0 border-bottom-0 border-top" button="true">
    <road-icon name="log-out" aria-hidden="true"></road-icon>
    Log out
  </road-item>

  </road-drawer>


  <road-drawer is-open="false" position="right" drawer-width="480" drawer-title="Menu" class="d-xl-none drawer-menu2" has-back-icon="true" has-close-icon="true" back-text="back">
    <road-avatar class="mx-auto mb-16 mt-24" size="lg">  
      <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
    </road-avatar>
      <road-label class="mb-24">
      <p class="h8 mb-4">First and last name</p>
      <p class="text-medium text-truncate m-0">Email</p>
    </road-label>

    <div class="p-16" style="position: absolute; bottom: 0; width: 100%">
      <road-button color="secondary" expand="" class="mb-0">Save changes</road-button>
    </div>
  </road-drawer>
  
  <script>
  // Fonction pour gérer l'activation de la touche "Enter" ou "Space"
  function handleKeydown(event, element) {
    if (event.key === 'Enter' || event.key === ' ') {
      event.preventDefault();
      element.setAttribute('is-open', 'true');
    }
  }
  
  // Gestionnaire de clic et clavier pour le menu principal
  const tabMenu = document.querySelector('.tab-menu');
  tabMenu.setAttribute('tabindex', '0'); // Rendre focusable
  tabMenu.addEventListener('click', () => {
    document.querySelector('.drawer-menu').setAttribute('is-open', 'true');
  });
  tabMenu.addEventListener('keydown', (event) => {
    handleKeydown(event, document.querySelector('.drawer-menu'));
  });
  
  // Gestionnaire de clic et clavier pour le menu du profil
  const profil = document.querySelector('.profil');
  profil.setAttribute('tabindex', '0'); // Rendre focusable
  profil.addEventListener('click', () => {
    document.querySelector('.drawer-menu2').setAttribute('is-open', 'true');
  });
  profil.addEventListener('keydown', (event) => {
    handleKeydown(event, document.querySelector('.drawer-menu2'));
  });
  

</script>`,
  },
};

const Template = (args) => html`
<road-navbar selected-tab="${ifDefined(args['selected-tab'])}">
  ${unsafeHTML(args[' '])}
</road-navbar>
`;

export const Playground = Template.bind({});

export const Selected = Template.bind({});
Selected.args = {
  ' ': `
  <road-profil-dropdown is-open="false" class="m-24 d-none d-xl-block">
    <road-list slot="list">
      <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
        <road-icon slot="start" name="edit-outline" size="md" aria-hidden="true"></road-icon>
        <road-label style="font-size: 0.75rem">
          Edit profile
        </road-label>
      </road-item>
      <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
        <road-icon slot="start" name="log-out-outline" size="md" aria-hidden="true"></road-icon>
        <road-label style="font-size: 0.75rem">
          Log out
        </road-label>
      </road-item>
    </road-list>
    <road-img slot="avatar" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
    <road-img slot="avatarItem" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
  </road-profil-dropdown>
  
  <road-navbar-item tab="tab-home" selected="true" aria-label="Home" role="menuitem">
    <road-tooltip content="Home" position="right" trigger="hover" contentalign="center">
      <road-icon name="navigation-home-outline" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Home</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-search" aria-label="Search" role="menuitem">
    <road-tooltip content="Search" position="right" trigger="hover" contentalign="center">
      <road-icon name="search" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Search</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-print" role="menuitem">
    <road-tooltip content="Print" position="right" trigger="hover" contentalign="center">
      <road-icon name="print-outline" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Print</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-catalogue" class="d-none d-xl-flex" role="menuitem">
    <road-tooltip content="Catalogue" position="right" trigger="hover" contentalign="center">
      <road-icon name="file-catalog" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Catalogue</road-label>
    </road-tooltip>
  </road-navbar-item>
  
  <road-navbar-item tab="tab-scan" class="d-none d-xl-flex" aria-label="Scan" role="menuitem">
    <road-tooltip content="Scan" position="right" trigger="hover" contentalign="center">
      <road-icon name="scan" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold">Scan</road-label>
    </road-tooltip>
  </road-navbar-item>`,
};

export const withNotifications = Template.bind({});
withNotifications.args = {
  ' ': `
  <road-profil-dropdown is-open="false" class="m-24 d-none d-xl-block">
    <road-list slot="list">
      <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
        <road-icon slot="start" name="edit-outline" size="md" aria-hidden="true"></road-icon>
        <road-label style="font-size: 0.75rem">
          Edit profile
        </road-label>
      </road-item>
      <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
        <road-icon slot="start" name="log-out-outline" size="md" aria-hidden="true"></road-icon>
        <road-label style="font-size: 0.75rem">
          Log out
        </road-label>
      </road-item>
    </road-list>
    <road-img slot="avatar" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
    <road-img slot="avatarItem" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
  </road-profil-dropdown>

  <road-navbar-item tab="tab-home" selected="true" aria-label="Home" role="menuitem">
    <road-tooltip content="Home" position="right" trigger="hover" contentalign="center">
      <road-icon name="navigation-home-outline" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Home</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-search" aria-label="Search" role="menuitem">
    <road-tooltip content="Search" position="right" trigger="hover" contentalign="center">
      <road-icon name="search" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Search</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-catalog" aria-label="Catalog" role="menuitem">
    <road-tooltip content="Scan" position="right" trigger="hover" contentalign="center">
      <road-icon name="print-outline" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Scan</road-label>
    </road-tooltip>
    <road-badge>3</road-badge>
  </road-navbar-item>

  <road-navbar-item tab="tab-catalogue" class="d-none d-xl-flex" aria-label="Catalog" role="menuitem">
    <road-tooltip content="Catalog" position="right" trigger="hover" contentalign="center">
      <road-icon name="file-catalog" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Catalog</road-label>
    </road-tooltip>
  </road-navbar-item>
  
  <road-navbar-item tab="tab-scan" class="d-none d-xl-flex" aria-label="Scan" role="menuitem">
    <road-tooltip content="Scan" position="right" trigger="hover" contentalign="center">
      <road-icon name="scan" aria-hidden="true"></road-icon>
      <road-label class="font-weight-bold" style="font-size:0.75rem;">Scan</road-label>
    </road-tooltip>
  </road-navbar-item>`,
};
