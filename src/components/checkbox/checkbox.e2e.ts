import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-checkbox', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-checkbox label="Label" value="on"></road-checkbox>
      `,
    });
  });

  it('should render unchecked by default', async () => {
    const roadchange = await page.spyOnEvent('roadchange');
    const roadblur = await page.spyOnEvent('roadblur');
    const roadfocus = await page.spyOnEvent('roadfocus');

    const element = await page.find('road-checkbox');

    const isCheck = await element.getProperty('checked');
    expect(isCheck).toBeFalsy();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();

    await page.focus('road-checkbox input');
    await page.click('road-checkbox input');
    await page.$eval('road-checkbox input', e => e.blur());

    await page.waitForChanges();

    const isChecked = await element.getProperty('checked');
    expect(isChecked).toBeTruthy();

    expect(roadfocus).toHaveReceivedEvent();
    expect(roadblur).toHaveReceivedEvent();

    expect(roadchange).toHaveReceivedEventDetail({
      checked: true,
      value: 'on',
    });
  });

  it('should render checked', async () => {
    const element = await page.find('road-checkbox');
    expect(element).toHaveClass('hydrated');

    element.setProperty('checked', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render disabled', async () => {
    const element = await page.find('road-checkbox');
    expect(element).toHaveClass('hydrated');

    element.setProperty('disabled', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with error', async () => {
    const element = await page.find('road-checkbox');
    expect(element).toHaveClass('hydrated');

    element.setProperty('error', 'This field is required');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
