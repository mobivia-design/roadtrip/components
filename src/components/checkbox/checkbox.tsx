import { Component, Event, EventEmitter, Host, Prop, Watch, h } from '@stencil/core';
import { checkWide, navigationAddLess } from '../../../icons';

import './../../utils/polyfill';

/**
 * @slot  - Adding additional elements next to the label (e.g. number of items in filters).
 */

@Component({
  tag: 'road-checkbox',
  styleUrl: 'checkbox.css',
  scoped: true,
})
export class Checkbox {

  /**
   * The id of checkbox
   */
  @Prop() checkboxId: string = `road-checkbox-${checkboxIds++}`;

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.checkboxId;

  /**
   * If `true`, the user must fill in a value before submitting a form.
   */
  @Prop() required: boolean = false;

  /**
   * If `true`, the checkbox is checked.
   */
  @Prop({ mutable: true }) checked = false;

  /**
   * If `true`, the checkbox will visually appear as indeterminate.
   */
   @Prop() indeterminate: boolean = false;

  /**
   * If `true`, the user cannot interact with the checkbox.
   */
  @Prop() disabled: boolean = false;

  /**
   * Value the form will get
   */
  @Prop() value: string = 'on';

  /**
   * Label for the field
   */
  @Prop() label: string = `${this.checkboxId}-label`;

    /**
   * Secondary Label for the field
   */
    @Prop() secondaryLabel?: string;

  /**
   * If `true`, the label and the checkbox are inverse and spaced
   */
  @Prop() inverse: boolean = false;

  /**
   * Error message for the field
   */
  @Prop() error?: string;

  /**
   * Helper message for the field
   */
   @Prop() helper?: string;

  /**
   * Emitted when the checked property has changed.
   */
  @Event() roadchange!: EventEmitter<{
    checked: boolean,
    value: string | undefined | null
  }>;
  /** @internal */
  @Event() roadChange!: EventEmitter<{
    checked: boolean,
    value: string | undefined | null
  }>;

  /**
   * Emitted when the checkbox has focus.
   */
  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  /**
   * Emitted when the checkbox loses focus.
   */
  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;

  @Watch('checked')
  checkedChanged(isChecked: boolean) {
    this.roadchange.emit({
      checked: isChecked,
      value: this.value,
    });
    this.roadChange.emit({
      checked: isChecked,
      value: this.value,
    });
  }

  private onClick = () => {
    this.checked = !this.checked;
    this.indeterminate = false;
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  private onBlur = () => {
    this.roadblur.emit();
    this.roadBlur.emit();
  };

  render() {
    const labelId = this.checkboxId + '-label';
    const inverseClass = this.inverse && 'form-checkbox-inverse';
    const isInvalidClass = this.error !== undefined && !this.checked && this.error !== '' ? 'is-invalid' : '';

    return (
      <Host>
        <input
          class={`form-check-input ${isInvalidClass}`}
          type="checkbox"
          id={this.checkboxId}
          name={this.name}
          required={this.required}
          disabled={this.disabled}
          indeterminate={this.indeterminate}
          checked={this.checked}
          value={this.value}
          aria-checked={`${this.checked}`}
          aria-disabled={this.disabled ? 'true' : null}
          aria-labelledby={labelId}
          onClick={this.onClick}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
        <label class={`form-check-label ${inverseClass}`} id={labelId} htmlFor={this.checkboxId}>
          <div>
            {this.label} <span class="form-check-label-span">{this.secondaryLabel}</span><slot/>
          </div>
          {this.checked && !this.indeterminate && <road-icon class="form-check-icon" icon={checkWide}></road-icon>}
          {this.indeterminate && <road-icon class="form-check-icon" icon={navigationAddLess}></road-icon>}
        </label>
        {this.error && this.error !== '' && <p class="invalid-feedback">{this.error}</p>}
        {this.helper && this.helper !== '' && <p class="helper">{this.helper}</p>}
      </Host>
    );
  }
}

let checkboxIds = 0;
