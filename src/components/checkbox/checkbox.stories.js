import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Forms/Checkbox',
  component: 'road-checkbox',
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus', 'roadchange'],
    },
  },
  argTypes: {
    checked: {
      control: 'boolean',
      description: 'Specifies whether the checkbox is checked.',
      table: {
        defaultValue: { summary: 'false' },
      },
    },
    indeterminate: {
      control: 'boolean',
      description: 'Specifies whether the checkbox is in an indeterminate state.',
    },
    disabled: {
      control: 'boolean',
      description: 'Disables the checkbox if set to `true`.',
    },
    required: {
      control: 'boolean',
      description: 'Marks the checkbox as required in a form.',
    },
    inverse: {
      control: 'boolean',
      description: 'Applies an inverse color style to the checkbox.',
    },
    error: {
      control: 'text',
      description: 'Displays an error message below the checkbox.',
    },
    helper: {
      control: 'text',
      description: 'Displays a helper text below the checkbox.',
    },
    label: {
      control: 'text',
      description: 'Sets the main label of the checkbox.',
      table: {
        defaultValue: { summary: 'Label' },
      },
    },
    'secondary-label': {
      control: 'text',
      description: 'Sets a secondary label, usually displayed next to the main label.',
    },
    name: {
      control: 'text',
      description: 'Sets the name attribute for form submission.',
    },
    'checkbox-id': {
      control: 'text',
      description: 'Sets a unique ID for the checkbox element.',
    },
    value: {
      control: 'text',
      description: 'Defines the value of the checkbox when checked.',
      table: {
        defaultValue: { summary: 'on' },
      },
    },
    roadblur: {
      action: 'roadblur',
      description: 'Triggered when the checkbox loses focus.',
      table: {
        category: 'Events',
      },
    },
    roadfocus: {
      action: 'roadfocus',
      description: 'Triggered when the checkbox gains focus.',
      table: {
        category: 'Events',
      },
    },
    roadchange: {
      action: 'roadchange',
      description: 'Triggered when the checkbox state changes.',
      table: {
        category: 'Events',
      },
    },
  },
  args: {
    checked: false,
    disabled: false,
    indeterminate: false,
    inverse: false,
    required: false,
    label: 'Label',
    'secondary-label': '',
    value: 'on',
  },
};

const Template = (args) => html`
  <road-checkbox 
    ?checked="${args.checked}"
    ?indeterminate="${args.indeterminate}"
    ?disabled="${args.disabled}"
    ?inverse="${args.inverse}"
    ?required="${args.required}"
    value="${ifDefined(args.value)}"
    label="${ifDefined(args.label)}"
    secondary-label="${ifDefined(args['secondary-label'])}"
    name="${ifDefined(args.name)}"
    checkbox-id="${ifDefined(args['checkbox-id'])}"
    error="${ifDefined(args.error)}"
    helper="${ifDefined(args.helper)}"
    @roadchange=${event => args.roadchange(event.detail)}
  ></road-checkbox>
`;

export const Playground = Template.bind({});

export const Checked = Template.bind({});
Checked.args = {
  checked: true,
};

export const Indeterminate = Template.bind({});
Indeterminate.args = {
  indeterminate: true,
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
};

export const Error = Template.bind({});
Error.args = {
  required: true,
  error: 'Check the checkbox to continue',
};

export const Inverse = Template.bind({});
Inverse.args = {
  inverse: true,
};
