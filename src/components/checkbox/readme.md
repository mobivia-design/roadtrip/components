# road-checkbox



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute         | Description                                                        | Type                  | Default                                |
| ---------------- | ----------------- | ------------------------------------------------------------------ | --------------------- | -------------------------------------- |
| `checkboxId`     | `checkbox-id`     | The id of checkbox                                                 | `string`              | `` `road-checkbox-${checkboxIds++}` `` |
| `checked`        | `checked`         | If `true`, the checkbox is checked.                                | `boolean`             | `false`                                |
| `disabled`       | `disabled`        | If `true`, the user cannot interact with the checkbox.             | `boolean`             | `false`                                |
| `error`          | `error`           | Error message for the field                                        | `string \| undefined` | `undefined`                            |
| `helper`         | `helper`          | Helper message for the field                                       | `string \| undefined` | `undefined`                            |
| `indeterminate`  | `indeterminate`   | If `true`, the checkbox will visually appear as indeterminate.     | `boolean`             | `false`                                |
| `inverse`        | `inverse`         | If `true`, the label and the checkbox are inverse and spaced       | `boolean`             | `false`                                |
| `label`          | `label`           | Label for the field                                                | `string`              | `` `${this.checkboxId}-label` ``       |
| `name`           | `name`            | The name of the control, which is submitted with the form data.    | `string`              | `this.checkboxId`                      |
| `required`       | `required`        | If `true`, the user must fill in a value before submitting a form. | `boolean`             | `false`                                |
| `secondaryLabel` | `secondary-label` | Secondary Label for the field                                      | `string \| undefined` | `undefined`                            |
| `value`          | `value`           | Value the form will get                                            | `string`              | `'on'`                                 |


## Events

| Event        | Description                                    | Type                                                                     |
| ------------ | ---------------------------------------------- | ------------------------------------------------------------------------ |
| `roadblur`   | Emitted when the checkbox loses focus.         | `CustomEvent<void>`                                                      |
| `roadchange` | Emitted when the checked property has changed. | `CustomEvent<{ checked: boolean; value: string \| null \| undefined; }>` |
| `roadfocus`  | Emitted when the checkbox has focus.           | `CustomEvent<void>`                                                      |


## Slots

| Slot | Description                                                                     |
| ---- | ------------------------------------------------------------------------------- |
|      | Adding additional elements next to the label (e.g. number of items in filters). |


## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-checkbox --> road-icon
  style road-checkbox fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
