import { Component, h } from '@stencil/core';

/**
 * @slot  - Content of the column.
 */

@Component({
  tag: 'road-col',
  styleUrl: 'col.css',
  shadow: true,
})
export class Col {

  render() {
    return (
      <slot/>
    );
  }

}
