# road-col

Columns are cellular components of the [grid](../grid) system and go inside of a [row](../row).
They will expand to fill their row. All content within a grid should go inside of a column.

See [Grid Layout](../grid) for more information.

## Column Alignment

By default, columns will stretch to fill the entire height of the row. Columns are [flex items](https://developer.mozilla.org/en-US/docs/Glossary/Flex_Item), so there are several [CSS classes](/docs/layout/css-utilities#flex-item-properties) that can be applied to a column to customize this behavior.

<!-- Auto Generated Below -->


## Slots

| Slot | Description            |
| ---- | ---------------------- |
|      | Content of the column. |


## Dependencies

### Used by

 - [road-duration](../duration)

### Graph
```mermaid
graph TD;
  road-duration --> road-col
  style road-col fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
