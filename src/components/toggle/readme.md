# road-toggle



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute        | Description                                                     | Type      | Default                            |
| -------------- | ---------------- | --------------------------------------------------------------- | --------- | ---------------------------------- |
| `checked`      | `checked`        | If `true`, the toggle is checked.                               | `boolean` | `false`                            |
| `disabled`     | `disabled`       | If `true`, the user cannot interact with the toggle.            | `boolean` | `false`                            |
| `hasLeftLabel` | `has-left-label` | If `true`, the label is at left of the toggle                   | `boolean` | `false`                            |
| `isSpaced`     | `is-spaced`      | Add space between label and toggle element                      | `boolean` | `false`                            |
| `label`        | `label`          | Label for the field                                             | `string`  | `` `${this.toggleId}-label` ``     |
| `name`         | `name`           | The name of the control, which is submitted with the form data. | `string`  | `this.toggleId`                    |
| `off`          | `off`            | Text display for "`off`" state in the toggle lever              | `string`  | `"no"`                             |
| `on`           | `on`             | Text display for "`on`" state in the toggle lever               | `string`  | `"yes"`                            |
| `toggleId`     | `toggle-id`      | The id of toggle                                                | `string`  | `` `road-toggle-${toggleIds++}` `` |
| `value`        | `value`          | Value the form will get                                         | `string`  | `'on'`                             |


## Events

| Event        | Description                                    | Type                                                                     |
| ------------ | ---------------------------------------------- | ------------------------------------------------------------------------ |
| `roadblur`   | Emitted when the toggle loses focus.           | `CustomEvent<void>`                                                      |
| `roadchange` | Emitted when the checked property has changed. | `CustomEvent<{ checked: boolean; value: string \| null \| undefined; }>` |
| `roadfocus`  | Emitted when the toggle has focus.             | `CustomEvent<void>`                                                      |


## CSS Custom Properties

| Name                   | Description        |
| ---------------------- | ------------------ |
| `--toggle-lever-width` | width of the lever |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
