import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Forms/Toggle',
  component: 'road-toggle',
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus'],
    },
  },
  argTypes: {
    checked: {
      description: "If `true`, the toggle is checked.",
      control: 'boolean',
    },
    disabled: {
      description: "If `true`, the user cannot interact with the toggle.",
      control: 'boolean',
    },
    label: {
      description: "Label for the field",
      control: 'text',
    },
    name: {
      description: "The name of the control, which is submitted with the form data.",
      control: 'text',
    },
    'toggle-id': {
      description: "The id of toggle",
      control: 'text',
    },
    value: {
      description: "Value the form will get",
      control: 'text',
    },
    off: {
      description: "Text display for \"`off`\" state in the toggle lever",
      control: 'text',
    },
    on: {
      description: "Text display for \"`on`\" state in the toggle lever",
      control: 'text',
    },
    'has-left-label': {
      description: "If `true`, the label is at left of the toggle",
      control: 'boolean',
    },
    'is-spaced': {
      description: "Add space between label and toggle element",
      control: 'boolean',
    },
    roadblur: {
      description: "Emitted when the toggle loses focus.",
      control: {
        type: null,
      },
    },
    roadfocus: {
      description: "Emitted when the toggle has focus.",
      control: {
        type: null,
      },
    },
    roadchange: {
      description: "Emitted when the checked property has changed.",
      action: 'roadchange',
      control: {
        type: null,
      },
    },
    '--toggle-lever-width': {
      description: "width of the lever",
      table: {
        defaultValue: { summary: '4.5rem' },
      },
      control: {
        type: null,
      },
    },
  },
  args: {
    checked: null,
    'has-left-label': null,
    'is-spaced': null,
    label: 'Label',
    value: 'on',
    off: 'no',
    on: 'yes',
    disabled: null,
  },
};

const Template = (args) => html`
  <road-toggle 
    checked="${ifDefined(args.checked)}"
    disabled="${ifDefined(args.disabled)}"
    label="${ifDefined(args.label)}"
    name="${ifDefined(args.name)}"
    value="${ifDefined(args.value)}"
    off="${ifDefined(args.off)}"
    on="${ifDefined(args.on)}"
    has-left-label="${ifDefined(args['has-left-label'])}"
    is-spaced="${ifDefined(args['is-spaced'])}"
    toggle-id="${ifDefined(args['toggle-id'])}"
    @roadchange=${event => args.roadchange(event.detail)}
  ></road-toggle>
`;

export const Playground = Template.bind({});

export const Checked = Template.bind({});
Checked.args = {
  checked: true,
};

export const Inverse = Template.bind({});
Inverse.args = {
  'has-left-label': true,
  'is-spaced': true,
};
