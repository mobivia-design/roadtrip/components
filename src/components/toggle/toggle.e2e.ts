import { newE2EPage } from '@stencil/core/testing';

describe('road-toggle', () => {
  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-toggle label="Label" input-id="inputId" value="on"></road-toggle>
      `,
    });
  });

  it('should render unchecked by default', async () => {
    const roadchange = await page.spyOnEvent('roadchange');
    const roadblur = await page.spyOnEvent('roadblur');
    const roadfocus = await page.spyOnEvent('roadfocus');

    const element = await page.find('road-toggle');

    const isCheck = await element.getProperty('checked');
    expect(isCheck).toBeFalsy();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();

    await page.focus('road-toggle input');
    await page.click('road-toggle input');
    await page.$eval('road-toggle input', e => e.blur());

    await page.waitForChanges();

    const isChecked = await element.getProperty('checked');
    expect(isChecked).toBeTruthy();

    expect(roadfocus).toHaveReceivedEvent();
    expect(roadblur).toHaveReceivedEvent();

    expect(roadchange).toHaveReceivedEventDetail({
      checked: true,
      value: 'on',
    });
  });

  it('should render checked by default', async () => {
    const element = await page.find('road-toggle');

    await element.setProperty('checked', true);

    await page.waitForChanges();

    const isChecked = await element.getProperty('checked');
    expect(isChecked).toBeTruthy();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render information toggle', async () => {
    const element = await page.find('road-toggle');

    await element.setProperty('checked', true);
    await element.setProperty('color', 'info');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render Warning toggle', async () => {
    const element = await page.find('road-toggle');

    await element.setProperty('checked', true);
    await element.setProperty('color', 'warning');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render danger toggle', async () => {
    const element = await page.find('road-toggle');

    await element.setProperty('checked', true);
    await element.setProperty('color', 'danger');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});