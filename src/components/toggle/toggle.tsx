import { Component, Event, EventEmitter, Host, Prop, Watch, h } from '@stencil/core';
import '../../utils/polyfill';

@Component({
  tag: 'road-toggle',
  styleUrl: 'toggle.css',
  scoped: true,
})
export class toggle {

  /**
   * The id of toggle
   */
  @Prop() toggleId: string = `road-toggle-${toggleIds++}`;

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.toggleId;

  /**
   * If `true`, the toggle is checked.
   */
  @Prop({ mutable: true }) checked = false;

  /**
   * If `true`, the user cannot interact with the toggle.
   */
  @Prop() disabled = false;

  /**
   * Label for the field
   */
  @Prop() label: string = `${this.toggleId}-label`;

  /**
   * If `true`, the label is at left of the toggle
   */
  @Prop() hasLeftLabel: boolean = false;

  /**
   * Add space between label and toggle element
   */
  @Prop() isSpaced: boolean = false;

  /**
   * Value the form will get
   */
  @Prop() value: string = 'on';

  /**
   * Text display for "`on`" state in the toggle lever
   */
  @Prop() on: string = "yes";

  /**
   * Text display for "`off`" state in the toggle lever
   */
  @Prop() off: string = "no";

  /**
   * Emitted when the checked property has changed.
   */
  @Event() roadchange!: EventEmitter<{
    checked: boolean;
    value: string | undefined | null
  }>;
  /** @internal */
  @Event() roadChange!: EventEmitter<{
    checked: boolean;
    value: string | undefined | null
  }>;

  /**
   * Emitted when the toggle has focus.
   */
  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  /**
   * Emitted when the toggle loses focus.
   */
  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;

  @Watch('checked')
  checkedChanged(isChecked: boolean) {
    this.roadchange.emit({
      checked: isChecked,
      value: this.value,
    });
    this.roadChange.emit({
      checked: isChecked,
      value: this.value,
    });
  }

  private onClick = () => {
    this.checked = !this.checked;
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  private onBlur = () => {
    this.roadBlur.emit();
    this.roadblur.emit();
  };

  render() {
    const labelId = this.toggleId + '-label';
    const textLabel = <label class="form-toggle-label" id={labelId} htmlFor={this.toggleId}>{this.label}</label>;
    const isSpacedClass = this.isSpaced && 'form-toggle-spaced';
    const righttoggleClass = this.hasLeftLabel ? 'form-toggle-right' : '';
    const disabledClass = this.disabled ? 'disabled' : '';

    return (
      <Host>
        <input
          class="form-toggle-input"
          type="checkbox"
          id={this.toggleId}
          name={this.name}
          checked={this.checked}
          disabled={this.disabled}
          value={this.value}
          aria-checked={`${this.checked}`}
          aria-disabled={this.disabled ? 'true' : null}
          aria-labelledby={labelId}
          onClick={this.onClick}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
        <label class={`form-toggle-label ${isSpacedClass} ${disabledClass}`} htmlFor={this.toggleId}>
          {this.hasLeftLabel && textLabel}
          <div class={`form-toggle-lever ${righttoggleClass}`} data-off={this.off} data-on={this.on}></div>
          {this.hasLeftLabel ? '' : textLabel}
        </label>
      </Host>
    );
  }
}

let toggleIds = 0;
