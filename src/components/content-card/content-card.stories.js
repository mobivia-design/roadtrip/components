import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Listing/ContentCard',
  component: 'road-content-card',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
  },
  argTypes: {
    label: {
      control: 'text',
      description: 'Title text displayed in the card.',
    },
    description: {
      control: 'text',
      description: 'Main description inside the card.',
    },
    ' ': {
      control: 'text',
      description: 'Slot for additional content, such as buttons or links.',
    },
    'inset-image': {
      control: 'boolean',
      description: 'If true, the image is inset within the card.',
    },
  },
  args: {
    label: `Lorem ipsum`,
    description: `Elevate your car's performance with Norauto's all-in-one maintenance kit.`,
    ' ': `
      <road-button color="primary" class="mt-8 mb-0" expand="true" outline="true">Label</road-button>
      <road-button color="primary" class="mt-8 mb-0 mr-0" expand="true">Add to cart</road-button>
    `,
    'inset-image': false,
  },
};

const Template = (args) => html`
<road-content-card ?inset-image="${args['inset-image']}">
  <div slot="label">
    ${unsafeHTML(args.label)}
  </div>
  <div slot="description">
    ${unsafeHTML(args.description)}
  </div>
  ${unsafeHTML(args[' '])}
</road-content-card>
`;

export const Playground = Template.bind({});

export const WithInsetImage = Template.bind({});
WithInsetImage.args = {
  'inset-image': true,
};

export const CustomContent = Template.bind({});
CustomContent.args = {
  label: 'Premium Car Wax',
  description: 'Protect your car’s paint with our long-lasting premium wax formula.',
  ' ': `<road-button color="secondary" class="mt-8 mb-0" expand="true">Learn more</road-button>`,
};
