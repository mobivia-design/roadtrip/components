import { Component, h, Host, Prop } from '@stencil/core';

/**
* @slot label - Content of the title.
* @slot description - Content of the description.
* @slot - Content buttons sections - Minimum 1 button 2 max

*/

@Component({
  tag: 'road-content-card',
  styleUrl: 'content-card.css',
  shadow: true,
})
export class ContentCard {

     /**
   * Set to `true` to add padding around img.
   */
     @Prop() insetImage : boolean = false;


  render() {

    const insetImageClass = this.insetImage ? '' : 'content-card-image';

    return (
      <Host>
        <road-img src="https://s1.medias-norauto.fr/visuals/desktop/fr/banners/blog_loimontagne.png" alt="loi montagne" class={insetImageClass}></road-img>
        <div class="content-card-description">
          <road-label class="content-card-description-title">
            <slot name="label"/>
          </road-label>
          <p class="content-card-description-text">
            <slot name="description"/>
          </p>
          <div class="content-card-description-buttons">
            <slot />
          </div>
        </div>
      </Host>
    );
  }

}
