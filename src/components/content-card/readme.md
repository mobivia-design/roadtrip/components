# road-card



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                              | Type      | Default |
| ------------ | ------------- | ---------------------------------------- | --------- | ------- |
| `insetImage` | `inset-image` | Set to `true` to add padding around img. | `boolean` | `false` |


## Slots

| Slot            | Description                                       |
| --------------- | ------------------------------------------------- |
|                 | Content buttons sections - Minimum 1 button 2 max |
| `"description"` | Content of the description.                       |
| `"label"`       | Content of the title.                             |


## Dependencies

### Depends on

- [road-img](../img)
- [road-label](../label)

### Graph
```mermaid
graph TD;
  road-content-card --> road-img
  road-content-card --> road-label
  style road-content-card fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
