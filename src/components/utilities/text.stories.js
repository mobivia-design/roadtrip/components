import { html } from 'lit';

export default {
  title: 'Utilities/Text',
};

export const Alignement = () => html`
  <p class="text-left">Text left</p>
  <p class="text-center">Text center</p>
  <p class="text-right">Text right</p>
`;

export const Transformation = () => html`
  <p class="text-lowercase">Lowercased text.</p>
  <p class="text-uppercase">Uppercased text.</p>
  <p class="text-capitalize">capitalized text.</p>
`;

export const Weight = () => html`
  <p class="font-weight-bold">Bold text.</p>
  <p class="font-weight-normal">Normal weight text.</p>
  <p class="font-weight-semibold">SemiBold weight text.</p>
  <p class="font-weight-black">Black weight text.</p>
  <p class="font-italic">Italic text.</p>
`;

export const Truncate = () => html`
  <div class="text-truncate" style="width: 120px">
    This text is too long so it will end with 3 dots
  </div>
`;
