import { html } from 'lit';

export default {
  title: 'Utilities/Border',
};

export const Additive = () => html`
  <div class="border" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);"></div>

  <div class="border-left" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);"></div>

  <div class="border-top" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);"></div>

  <div class="border-right" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);"></div>

  <div class="border-bottom" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);"></div>
`;

export const Subtractive = () => html`
  <div class="border-0" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);border: 1px solid var(--road-grey-300);"></div>

  <div class="border-left-0" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);border: 1px solid var(--road-grey-300);"></div>

  <div class="border-top-0" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);border: 1px solid var(--road-grey-300);"></div>

  <div class="border-right-0" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);border: 1px solid var(--road-grey-300);"></div>

  <div class="border-bottom-0" 
    style="display: inline-block;width: 5rem;height: 5rem;margin: .25rem;background-color: var(--road-grey-100);border: 1px solid var(--road-grey-300);"></div>
`;
