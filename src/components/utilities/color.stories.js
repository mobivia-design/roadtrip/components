import { html } from 'lit';

export default {
  title: 'Utilities/Colors',
};

export const TextColors = () => html`
<div style="font-size: 1.125rem">

  <p class="text-primary">.text-primary</p>

  <p class="text-secondary">.text-secondary</p>

  <p class="text-success">.text-success</p>

  <p class="text-warning">.text-warning</p>

  <p class="text-danger">.text-danger</p>

  <p class="text-gray">.text-gray</p>

  <p class="text-gray-second">.text-gray-second</p>

  <p class="text-disabled">.text-disabled</p>

  <p class="text-white bg-dark">.text-white</p>

</div>
`;

export const BackgroundColors = () => html`
<div style="font-size: 1.125rem">

  <p class="bg-primary text-white pl-16 pt-16 pb-16">.bg-primary</p>

  <p class="bg-secondary text-white pl-16 pt-16 pb-16">.bg-secondary</p>

  <p class="bg-success text-white pl-16 pt-16 pb-16">.bg-success</p>

  <p class="bg-warning text-dark pl-16 pt-16 pb-16">.bg-warning</p>

  <p class="bg-danger text-white pl-16 pt-16 pb-16">.bg-danger</p>

  <p class="bg-light pl-16 pt-16 pb-16">.bg-light</p>

  <p class="bg-white pl-16 pt-16 pb-16">.bg-white</p>

</div>
`;
