import { html } from 'lit';

export default {
  title: 'Utilities/Typography',
};

export const Heading = () => html`
  <p class="h1">Heading 1</p>
  <p class="h2">Heading 2</p>
  <p class="h3">Heading 3</p>
  <p class="h4">Heading 4</p>
  <p class="h5">Heading 5</p>
  <p class="h6">Heading 6</p>
  <p class="h7">Heading 7</p>
  <p class="h8">Heading 8</p>
  <p class="h9">Heading 9</p>

  <p class="h1 title-primary">Heading 1</p>
  <p class="h2 title-primary">Heading 2</p>
  <p class="h3 title-primary">Heading 3</p>
  <p class="h4 title-primary">Heading 4</p>
  <p class="h5 title-primary">Heading 5</p>
  <p class="h6 title-primary">Heading 6</p>
  <p class="h7 title-primary">Heading 7</p>
  <p class="h8 title-primary">Heading 8</p>
  <p class="h9 title-primary">Heading 9</p>
`;

export const HeadingUnderline = () => html`
  <p class="h1 title-underline">Heading 1</p>
  <p class="h1 title-underline text-center">Heading 1</p>
  <p class="h1 title-underline text-right">Heading 1</p>

  <p class="h1 title-underline title-primary">Heading 1</p>
  <p class="h1 title-underline text-center title-primary">Heading 1</p>
  <p class="h1 title-underline text-right title-primary">Heading 1</p>
`;

export const Content = () => html`
  <p class="text-large">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tristique interdum elementum. Curabitur volutpat orci nulla, sed scelerisque est tincidunt eget. Fusce dolor tortor, porta sit amet mauris id, molestie viverra urna.</p>
  <p class="text-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tristique interdum elementum. Curabitur volutpat orci nulla, sed scelerisque est tincidunt eget. Fusce dolor tortor, porta sit amet mauris id, molestie viverra urna.</p>
  <p class="text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tristique interdum elementum. Curabitur volutpat orci nulla, sed scelerisque est tincidunt eget. Fusce dolor tortor, porta sit amet mauris id, molestie viverra urna.</p>
  <p class="text-small">Small text content lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tristique interdum elementum. Curabitur volutpat orci nulla, sed scelerisque est tincidunt eget. Fusce dolor tortor, porta sit amet mauris id, molestie viverra urna.</p>
  <p class="text-legal">Legal text content lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tristique interdum elementum. Curabitur volutpat orci nulla, sed scelerisque est tincidunt eget. Fusce dolor tortor, porta sit amet mauris id, molestie viverra urna.</p>
`;
