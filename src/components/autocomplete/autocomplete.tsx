import { Component, Element, Host, Listen, Prop, State, h, Event, EventEmitter } from '@stencil/core';

/**
 * @slot  - Input element for the widget, it should be a road-input element.
 */

@Component({
  tag: 'road-autocomplete',
  styleUrl: 'autocomplete.css',
  scoped: true,
})
export class Autocomplete {

  @Element() el!: HTMLRoadAutocompleteElement;

  @State() visible: boolean = false;

  /**
   * List of autocomplete options
   */
  @Prop() options: Array<{
    value: string | number;
    label: string;
  }> = [];

  @Listen('roadFocus')
  @Listen('roadfocus')
  handleFocus() {
    this.visible = true;
  }

  @Listen('click', { target: 'document' })
  onClickOutside(event: MouseEvent) {
    if (this.visible && !this.el.contains(event.target as Node)) {
      this.visible = false;
    }
  }

  /**
   * Emitted the value and label of the option selected.
   */
  @Event() roadselected!: EventEmitter<{
    value: string | undefined | null,
    label: string
  }>;
  /** @internal */
  @Event() roadSelected!: EventEmitter<{
    value: string | undefined | null,
    label: string
  }>;

  private onClick = (value: string | number, label: string) => {
    (this.el.querySelector('road-input') as HTMLRoadInputElement).value = value;
    this.visible = false;
    this.roadselected.emit({
      value: value.toString(),
      label: label,
    });
    this.roadSelected.emit({
      value: value.toString(),
      label: label,
    });
  };

  render() {
    return (
      <Host>
        <slot/>
        {this.visible && this.options.length > 0 &&
          <ul class="autocomplete-list">
            {this.options && this.options.map(option => (
              <li class="autocomplete-item" role="menuitem" onClick={() => this.onClick(option.value, option.label)} tabindex="0">
                {option.label}
              </li>
            ))}
          </ul>}
      </Host>
    );
  }
}
