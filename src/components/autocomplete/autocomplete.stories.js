import { html } from 'lit';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Forms/Autocomplete',
  component: 'road-autocomplete',
  parameters: {
    actions: {
      handles: ['roadchange', 'roadblur', 'roadfocus', 'roadselected'],
    },
  },
  argTypes: {
    options: {
      control: 'object',
      description: 'Array of options available for autocomplete.',
      table: {
        defaultValue: { summary: '[{ value: "value1", label: "value1" }]' },
      },
    },
    ' ': {
      control: 'text',
      description: 'Slot for inserting an input inside the autocomplete.',
    },
    roadselected: {
      action: 'roadselected',
      description: 'Fires when an option is selected.',
      table: {
        category: 'Events',
        type: { summary: 'CustomEvent' },
      },
      control: { type: null },
    },
  },
  args: {
    options: [
      { value: 'value1', label: 'value1' },
      { value: 'value2', label: 'value2' },
      { value: 'value3', label: 'value3' }
    ],
  },
};

const Template = (args) => html`
  <road-autocomplete .options=${args.options} @roadselected=${event => args.roadselected(event.detail)}>
    ${unsafeHTML(args[' '])}
  </road-autocomplete>
`;

export const Playground = Template.bind({});
Playground.args = {
  ' ': `<road-input 
    class="mb-0" 
    sizes="xl" 
    input-id="country" 
    label="Country" 
    debounce="500"></road-input>`,
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  ' ': `
    <road-input-group>  
      <road-input 
        class="mb-0" 
        sizes="xl" 
        input-id="country" 
        label="Country" 
        debounce="500">
      </road-input>
      <label slot="prepend" for="search">
        <road-icon name="search" role="img"></road-icon>
      </label>
    </road-input-group>
  `,
};

export const WithIconButton = Template.bind({});
WithIconButton.args = {
  ' ': `
    <road-input-group>  
      <road-input 
        class="mb-0" 
        sizes="xl" 
        input-id="country" 
        label="Country" 
        debounce="500">
      </road-input>
      <label slot="prepend" for="search">
        <road-icon name="search" role="img"></road-icon>
      </label>
      <road-button slot="append" class="autocomplete-button" button-type="submit">
        OK     
      </road-button>
    </road-input-group>
  `,
};

export const Example = (args) => {
  const getCountry = (event) => {
    fetch(`https://photon.komoot.io/api/?q=${event.detail.value}&osm_tag=place:country&limit=10&lang=en`)
      .then(res => res.json())
      .then(result => {
        document.querySelector('road-autocomplete').options = result.features.map(item => ({
          value: item.properties.country,
          label: item.properties.country,
        }));
      });
  };

  return html`
    <road-autocomplete @roadselected=${event => args.roadselected(event.detail)}>
      <road-input 
        class="mb-0" 
        sizes="xl" 
        input-id="country" 
        label="Country" 
        debounce="500" 
        @roadchange=${event => getCountry(event)}
      ></road-input>
    </road-autocomplete>
  `;
};
Example.parameters = {
  docs: {
    source: {
      type: 'code',
    },
  },
};
