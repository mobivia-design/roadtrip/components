# road-autocomplete



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                  | Type                                            | Default |
| --------- | --------- | ---------------------------- | ----------------------------------------------- | ------- |
| `options` | --        | List of autocomplete options | `{ value: string \| number; label: string; }[]` | `[]`    |


## Events

| Event          | Description                                         | Type                                                                  |
| -------------- | --------------------------------------------------- | --------------------------------------------------------------------- |
| `roadselected` | Emitted the value and label of the option selected. | `CustomEvent<{ value: string \| null \| undefined; label: string; }>` |


## Slots

| Slot | Description                                                      |
| ---- | ---------------------------------------------------------------- |
|      | Input element for the widget, it should be a road-input element. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
