import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-autocomplete', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-autocomplete>
          <road-input-group>
            <road-input label="Brand" class="mb-0"></road-input>
          </road-input-group>
        </road-autocomplete>
      `,
    });

    await page.$eval('road-autocomplete', (elm: any) => {
      elm.options = [
        { value: 'audi', label: 'audi' },
        { value: 'bmw', label: 'bmw' },
        { value: 'citroen', label: 'citroën' },
        { value: 'mercedes-benz', label: 'mercedes-benz' },
        { value: 'peugeot', label: 'peugeot' },
        { value: 'renault', label: 'renault' }
      ];
    });

    await page.waitForChanges();
  });

  it('should render', async () => {

    const element = await page.find('road-autocomplete');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with value', async () => {

    const element = await page.find('road-autocomplete');
    expect(element).toHaveClass('hydrated');

    const input = await page.find('road-input');
    input.setProperty('value', 'audi');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render disabled', async () => {

    const element = await page.find('road-autocomplete');
    expect(element).toHaveClass('hydrated');

    const input = await page.find('road-input');
    input.setProperty('disabled', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with error', async () => {

    const element = await page.find('road-autocomplete');
    expect(element).toHaveClass('hydrated');

    const input = await page.find('road-input');
    input.setProperty('error', 'Select a Brand');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
