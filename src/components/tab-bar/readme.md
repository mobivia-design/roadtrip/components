# road-tab-bar



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                 | Type                  | Default     |
| ------------- | -------------- | --------------------------------------------------------------------------- | --------------------- | ----------- |
| `center`      | `center`       | Set to `true` to center buttons in the bar.                                 | `boolean`             | `false`     |
| `expand`      | `expand`       | Set to `true` to expand buttons width to take the full size of the bar.     | `boolean`             | `false`     |
| `secondary`   | `secondary`    | The color to use the color Secondary from your application's color palette. | `boolean`             | `false`     |
| `selectedTab` | `selected-tab` | The selected tab component                                                  | `string \| undefined` | `undefined` |


## Slots

| Slot | Description                                                   |
| ---- | ------------------------------------------------------------- |
|      | Content of the tabBar, it should be road-tab-button elements. |


## CSS Custom Properties

| Name               | Description                     |
| ------------------ | ------------------------------- |
| `--tab-background` | background color of the tab bar |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
