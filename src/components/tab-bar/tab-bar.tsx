import { Component, Element, Event, EventEmitter, Host, Prop, Watch, Listen, h } from '@stencil/core';
import './../../utils/polyfill';

/**
 * @slot  - Content of the tabBar, it should be road-tab-button elements.
 */

@Component({
  tag: 'road-tab-bar',
  styleUrl: 'tab-bar.css',
  shadow: true,
})
export class TabBar {

  @Element() el!: HTMLRoadTabBarElement;

  /**
   * The color to use the color Secondary from your application's color palette.
   */
  @Prop() secondary: boolean = false;

  /**
   * Set to `true` to expand buttons width to take the full size of the bar.
   */
  @Prop() expand: boolean = false;

  /**
   * Set to `true` to center buttons in the bar.
   */
  @Prop() center: boolean = false;

  /**
   * The selected tab component
   */
  @Prop({ mutable: true, reflect: true }) selectedTab?: string;
  @Watch('selectedTab')
  selectedTabChanged() {
    if (this.selectedTab !== undefined) {
      this.roadtabbarchanged.emit({
        tab: this.selectedTab,
      });
      this.roadTabBarChanged.emit({
        tab: this.selectedTab,
      });
    }
  }

  /** @internal */
  @Event() roadtabbarchanged!: EventEmitter;
  /** @internal */
  @Event() roadTabBarChanged!: EventEmitter;

  @Listen('roadTabButtonClick')
  onTabButtonClick(ev: CustomEvent) {
    this.selectedTab = ev.detail.tab;
  }

  componentWillLoad() {
    this.selectedTabChanged();
  }

  render() {
    const { expand, center, secondary } = this;
    return (
      <Host
        role="tablist"
        class={{
          'tab-expand': expand,
          'tab-center': center,
          'tab-secondary': secondary,
        }}
      >
        <slot/>
      </Host>
    );
  }
}
