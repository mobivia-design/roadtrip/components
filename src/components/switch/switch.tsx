import { Component, Event, EventEmitter, Host, Prop, Watch, h } from '@stencil/core';
import './../../utils/polyfill';

import { FeedbackColors } from '../../interface';

@Component({
  tag: 'road-switch',
  styleUrl: 'switch.css',
  scoped: true,
})
export class Switch {

  /**
   * The id of switch
   */
  @Prop() switchId: string = `road-switch-${switchIds++}`;

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.switchId;

  /**
   * If `true`, the switch is checked.
   */
  @Prop({ mutable: true }) checked = false;

  /**
   * If `true`, the user cannot interact with the switch.
   */
  @Prop() disabled = false;

  /**
   * Label for the field
   */
  @Prop() label: string = `${this.switchId}-label`;

  /**
   * Set the color of alert. e.g. info, success, warning, danger
   */
  @Prop() color?: 'secondary' | FeedbackColors = 'secondary';

  /**
   * If `true`, the label is at left of the switch
   */
  @Prop() hasLeftLabel: boolean = false;

  /**
   * Add space between label and switch element
   */
  @Prop() isSpaced: boolean = false;

  /**
   * Value the form will get
   */
  @Prop() value: string = 'on';

  /**
   * Text display for "`on`" state in the switch lever
   */
  @Prop() on: string = "yes";

  /**
   * Text display for "`off`" state in the switch lever
   */
  @Prop() off: string = "no";

  /**
   * Emitted when the checked property has changed.
   */
  @Event() roadchange!: EventEmitter<{
    checked: boolean;
    value: string | undefined | null
  }>;
  /** @internal */
  @Event() roadChange!: EventEmitter<{
    checked: boolean;
    value: string | undefined | null
  }>;

  /**
   * Emitted when the switch has focus.
   */
  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  /**
   * Emitted when the switch loses focus.
   */
  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;

  @Watch('checked')
  checkedChanged(isChecked: boolean) {
    this.roadchange.emit({
      checked: isChecked,
      value: this.value,
    });
    this.roadChange.emit({
      checked: isChecked,
      value: this.value,
    });
  }

  private onClick = () => {
    this.checked = !this.checked;
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  private onBlur = () => {
    this.roadBlur.emit();
    this.roadblur.emit();
  };

  render() {
    const labelId = this.switchId + '-label';
    const textLabel = <label class="form-switch-label" id={labelId} htmlFor={this.switchId}>{this.label}</label>;
    const colorClass = this.color !== undefined ? 'form-switch-' + this.color : '';
    const isSpacedClass = this.isSpaced && 'form-switch-spaced';
    const rightSwitchClass = this.hasLeftLabel ? 'form-switch-right' : '';

    return (
      <Host>
        <input
          class="form-switch-input"
          type="checkbox"
          id={this.switchId}
          name={this.name}
          checked={this.checked}
          disabled={this.disabled}
          value={this.value}
          aria-checked={`${this.checked}`}
          aria-disabled={this.disabled ? 'true' : null}
          aria-labelledby={labelId}
          onClick={this.onClick}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
        <label class={`form-switch-label ${isSpacedClass} ${colorClass}`} htmlFor={this.switchId}>
          {this.hasLeftLabel && textLabel}
          <div class={`form-switch-lever ${rightSwitchClass}`} data-off={this.off} data-on={this.on}></div>
          {this.hasLeftLabel ? '' : textLabel}
        </label>
      </Host>
    );
  }
}

let switchIds = 0;
