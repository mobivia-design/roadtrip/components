# road-switch



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute        | Description                                                     | Type                                                                       | Default                            |
| -------------- | ---------------- | --------------------------------------------------------------- | -------------------------------------------------------------------------- | ---------------------------------- |
| `checked`      | `checked`        | If `true`, the switch is checked.                               | `boolean`                                                                  | `false`                            |
| `color`        | `color`          | Set the color of alert. e.g. info, success, warning, danger     | `"danger" \| "info" \| "secondary" \| "success" \| "warning" \| undefined` | `'secondary'`                      |
| `disabled`     | `disabled`       | If `true`, the user cannot interact with the switch.            | `boolean`                                                                  | `false`                            |
| `hasLeftLabel` | `has-left-label` | If `true`, the label is at left of the switch                   | `boolean`                                                                  | `false`                            |
| `isSpaced`     | `is-spaced`      | Add space between label and switch element                      | `boolean`                                                                  | `false`                            |
| `label`        | `label`          | Label for the field                                             | `string`                                                                   | `` `${this.switchId}-label` ``     |
| `name`         | `name`           | The name of the control, which is submitted with the form data. | `string`                                                                   | `this.switchId`                    |
| `off`          | `off`            | Text display for "`off`" state in the switch lever              | `string`                                                                   | `"no"`                             |
| `on`           | `on`             | Text display for "`on`" state in the switch lever               | `string`                                                                   | `"yes"`                            |
| `switchId`     | `switch-id`      | The id of switch                                                | `string`                                                                   | `` `road-switch-${switchIds++}` `` |
| `value`        | `value`          | Value the form will get                                         | `string`                                                                   | `'on'`                             |


## Events

| Event        | Description                                    | Type                                                                     |
| ------------ | ---------------------------------------------- | ------------------------------------------------------------------------ |
| `roadblur`   | Emitted when the switch loses focus.           | `CustomEvent<void>`                                                      |
| `roadchange` | Emitted when the checked property has changed. | `CustomEvent<{ checked: boolean; value: string \| null \| undefined; }>` |
| `roadfocus`  | Emitted when the switch has focus.             | `CustomEvent<void>`                                                      |


## CSS Custom Properties

| Name                   | Description        |
| ---------------------- | ------------------ |
| `--switch-lever-width` | width of the lever |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
