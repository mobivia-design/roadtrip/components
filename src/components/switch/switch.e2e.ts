import { newE2EPage } from '@stencil/core/testing';

describe('road-switch', () => {
  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-switch label="Label" input-id="inputId" value="on"></road-switch>
      `,
    });
  });

  it('should render unchecked by default', async () => {
    const roadchange = await page.spyOnEvent('roadchange');
    const roadblur = await page.spyOnEvent('roadblur');
    const roadfocus = await page.spyOnEvent('roadfocus');

    const element = await page.find('road-switch');

    const isCheck = await element.getProperty('checked');
    expect(isCheck).toBeFalsy();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();

    await page.focus('road-switch input');
    await page.click('road-switch input');
    await page.$eval('road-switch input', e => e.blur());

    await page.waitForChanges();

    const isChecked = await element.getProperty('checked');
    expect(isChecked).toBeTruthy();

    expect(roadfocus).toHaveReceivedEvent();
    expect(roadblur).toHaveReceivedEvent();

    expect(roadchange).toHaveReceivedEventDetail({
      checked: true,
      value: 'on',
    });
  });

  it('should render checked by default', async () => {
    const element = await page.find('road-switch');

    await element.setProperty('checked', true);

    await page.waitForChanges();

    const isChecked = await element.getProperty('checked');
    expect(isChecked).toBeTruthy();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render information switch', async () => {
    const element = await page.find('road-switch');

    await element.setProperty('checked', true);
    await element.setProperty('color', 'info');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render Warning switch', async () => {
    const element = await page.find('road-switch');

    await element.setProperty('checked', true);
    await element.setProperty('color', 'warning');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render danger switch', async () => {
    const element = await page.find('road-switch');

    await element.setProperty('checked', true);
    await element.setProperty('color', 'danger');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});