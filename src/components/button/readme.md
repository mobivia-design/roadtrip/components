# road-button



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                                                                                                                                                                                                                                                                               | Type                                                            | Default     |
| ------------ | ------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------- | ----------- |
| `buttonType` | `button-type` | The type of the button.                                                                                                                                                                                                                                                                   | `"button" \| "reset" \| "submit"`                               | `'button'`  |
| `color`      | `color`       | The color to use from your application's color palette.                                                                                                                                                                                                                                   | `"default" \| "ghost" \| "primary" \| "secondary" \| undefined` | `'default'` |
| `disabled`   | `disabled`    | If `true`, the user cannot interact with the button.                                                                                                                                                                                                                                      | `boolean`                                                       | `false`     |
| `download`   | `download`    | This attribute instructs browsers to download a URL instead of navigating to it, so the user will be prompted to save it as a local file. If the attribute has a value, it is used as the pre-filled file name in the Save prompt (the user can still change the file name if they want). | `string \| undefined`                                           | `undefined` |
| `expand`     | `expand`      | Set to `true` for a full-width button.                                                                                                                                                                                                                                                    | `boolean`                                                       | `false`     |
| `href`       | `href`        | Contains a URL or a URL fragment that the hyperlink points to. If this property is set, an anchor tag will be rendered.                                                                                                                                                                   | `string \| undefined`                                           | `undefined` |
| `iconOnly`   | `icon-only`   | If `true`, display only an icon in the button.                                                                                                                                                                                                                                            | `boolean`                                                       | `false`     |
| `outline`    | `outline`     | Set to `false` for a ghost button, set to `true` for a default outline button                                                                                                                                                                                                             | `boolean`                                                       | `false`     |
| `rel`        | `rel`         | Specifies the relationship of the target object to the link object. The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).                                                                                                    | `string \| undefined`                                           | `undefined` |
| `size`       | `size`        | The button size.                                                                                                                                                                                                                                                                          | `"lg" \| "md" \| "sm" \| "xl" \| undefined`                     | `'lg'`      |
| `target`     | `target`      | Specifies where to display the linked URL. Only applies when an `href` is provided. Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.                                                                                                                                       | `string \| undefined`                                           | `undefined` |


## Events

| Event       | Description                          | Type                |
| ----------- | ------------------------------------ | ------------------- |
| `roadblur`  | Emitted when the button loses focus. | `CustomEvent<void>` |
| `roadfocus` | Emitted when the button has focus.   | `CustomEvent<void>` |


## Slots

| Slot      | Description                                                                              |
| --------- | ---------------------------------------------------------------------------------------- |
|           | Content of the button. ex if onlyIcon `<road-icon name="shopping-cart-add"></road-icon>` |
| `"end"`   | Right content of the button text, usually icon.                                          |
| `"start"` | Left content of the button text, usually for icon.                                       |


## Shadow Parts

| Part       | Description                                                             |
| ---------- | ----------------------------------------------------------------------- |
| `"native"` | The native HTML button or anchor element that wraps all child elements. |


## CSS Custom Properties

| Name              | Description                  |
| ----------------- | ---------------------------- |
| `--border-radius` | Border radius of the button  |
| `--font-size`     | Font size of the text button |
| `--margin-bottom` | Bottom margin of the button  |
| `--padding-end`   | Right padding of the button  |
| `--padding-start` | Left padding of the button   |


## Dependencies

### Used by

 - [road-collapse](../collapse)
 - [road-counter](../counter)

### Graph
```mermaid
graph TD;
  road-collapse --> road-button
  road-counter --> road-button
  style road-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
