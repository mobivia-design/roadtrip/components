import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-button', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-button color="primary">Click me!</road-button>
      `,
    });
  });

  it('should render', async () => {
    const element = await page.find('road-button');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render outlined', async () => {
    const element = await page.find('road-button');
    expect(element).toHaveClass('hydrated');

    element.setProperty('outline', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render disabled', async () => {
    const element = await page.find('road-button');
    expect(element).toHaveClass('hydrated');

    element.setProperty('disabled', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with icon', async () => {
    const element = await page.find('road-button');
    expect(element).toHaveClass('hydrated');

    await page.$eval('road-button', element => element.insertAdjacentHTML('afterbegin', '<road-icon slot="start" name="shopping-cart-add"></road-icon>'));

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
