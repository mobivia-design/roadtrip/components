import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Forms/Button',
  component: 'road-button',
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus'],
    },
  },
  argTypes: {
    color: {
      options: ['default', 'primary', 'secondary', 'ghost', 'destructive'],
      control: { type: 'select' },
      description: 'Defines the button color.',
      table: {
        defaultValue: { summary: 'default' },
      },
    },
    'button-type': {
      options: ['button', 'reset', 'submit'],
      control: { type: 'select' },
      description: 'Specifies the type of the button.',
    },
    disabled: {
      control: 'boolean',
      description: 'Disables the button when set to `true`.',
    },
    'icon-only': {
      control: 'boolean',
      description: 'If `true`, the button will only contain an icon without text.',
    },
    expand: {
      control: 'boolean',
      description: 'Expands the button to the full width of its container.',
    },
    outline: {
      control: 'boolean',
      description: 'Applies an outline style to the button.',
    },
    size: {
      options: ['sm', 'md', 'lg', 'xl'],
      control: { type: 'select' },
      description: 'Defines the size of the button.',
    },
    start: {
      control: 'text',
      description: 'Slot for an icon or content at the **start** of the button.',
    },
    ' ': {
      control: 'text',
      description: 'Label or main content inside the button.',
    },
    end: {
      control: 'text',
      description: 'Slot for an icon or content at the **end** of the button.',
    },
    roadblur: {
      action: 'roadblur',
      description: 'Triggered when the button loses focus.',
      table: {
        category: 'Events',
        type: { summary: 'CustomEvent' },
      },
    },
    roadfocus: {
      action: 'roadfocus',
      description: 'Triggered when the button gains focus.',
      table: {
        category: 'Events',
        type: { summary: 'CustomEvent' },
      },
    },
    // 🔹 Ajout des CSS Variables comme contrôles
    '--border-radius': {
      control: 'text',
      description: 'Defines the border radius of the button.',
      table: {
        defaultValue: { summary: '0.25rem' },
      },
    },
    '--font-size': {
      control: 'text',
      description: 'Sets the font size of the button text.',
      table: {
        defaultValue: { summary: '1rem' },
      },
    },
    '--margin-bottom': {
      control: 'text',
      description: 'Defines the margin bottom of the button.',
      table: {
        defaultValue: { summary: '1rem' },
      },
    },
    '--padding-start': {
      control: 'text',
      description: 'Sets the padding on the **left** side of the button.',
      table: {
        defaultValue: { summary: '1rem' },
      },
    },
    '--padding-end': {
      control: 'text',
      description: 'Sets the padding on the **right** side of the button.',
      table: {
        defaultValue: { summary: '1rem' },
      },
    },
  },
  args: {
    'button-type': 'button',
    'icon-only': false,
    color: 'default',
    disabled: false,
    expand: false,
    outline: false,
    size: 'md',
    start: '',
    ' ': 'Label',
    end: '',
  },
};

const Template = (args) => html`
<road-button 
  button-type="${ifDefined(args['button-type'])}"
  ?icon-only="${args['icon-only']}"
  color="${ifDefined(args.color)}"
  ?disabled="${args.disabled}"
  ?expand="${args.expand}"
  ?outline="${args.outline}"
  size="${ifDefined(args.size)}"
  style="
    --border-radius: ${ifDefined(args['--border-radius'])};
    --font-size: ${ifDefined(args['--font-size'])};
    --margin-bottom: ${ifDefined(args['--margin-bottom'])};
    --padding-start: ${ifDefined(args['--padding-start'])};
    --padding-end: ${ifDefined(args['--padding-end'])};
  "
>
  ${unsafeHTML(args.start)}
  ${unsafeHTML(args[' '])}
  ${unsafeHTML(args.end)}
</road-button>
`;

export const Playground = Template.bind({});
Playground.args = {
  color: 'primary',
  start: `<road-icon slot="start" name="shopping-cart-add" aria-hidden="true"></road-icon>`,
  ' ': `Add to cart`,
};

export const Default = Template.bind({});
Default.args = {
  color: 'primary',
};

export const Outline = Template.bind({});
Outline.args = {
  color: 'primary',
  outline: true,
};

export const OutlineDefault = Template.bind({});
OutlineDefault.args = {
  color: 'default',
  outline: true,
};

export const Ghost = Template.bind({});
Ghost.args = {
  color: 'ghost',
};

export const Disabled = Template.bind({});
Disabled.args = {
  color: 'secondary',
  disabled: true,
};

export const Destructive = Template.bind({});
Destructive.args = {
  color: 'destructive',
  outline: false,
};

export const DestructiveOutline = Template.bind({});
DestructiveOutline.args = {
  color: 'destructive',
  outline: true,
};

export const Icon = Template.bind({});
Icon.args = {
  color: 'primary',
  outline: true,
  'icon-only': true,
  ' ': `<road-icon name="shopping-cart-add" role="button"></road-icon>`,
};

export const BackButton = Template.bind({});
BackButton.args = {
  start: `<road-icon slot="start" name="navigation-chevron" rotate="180" role="button"></road-icon>`,
  ' ': `Back`,
};
