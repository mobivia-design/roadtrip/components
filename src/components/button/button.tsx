import { Component, Element, Event, EventEmitter, Host, Prop, h } from '@stencil/core';

import './../../utils/polyfill';

/**
 * @slot - Content of the button.
 * ex if onlyIcon `<road-icon name="shopping-cart-add"></road-icon>`
 * @slot start - Left content of the button text, usually for icon.
 * @slot end - Right content of the button text, usually icon.
 *
 * @part native - The native HTML button or anchor element that wraps all child elements.
 */

@Component({
  tag: 'road-button',
  styleUrl: 'button.css',
  shadow: true,
})
export class Button {

  @Element() el!: HTMLRoadButtonElement;

  /**
   * The color to use from your application's color palette.
   */
  @Prop() color?: 'primary' | 'secondary' | 'ghost' |'default' = 'default';

  /**
    * The button size.
    */
  @Prop({ reflect: true }) size?: 'sm' | 'md' | 'lg' | 'xl' = 'lg';

  /**
   * The type of the button.
   */
  @Prop() buttonType: 'submit' | 'reset' | 'button' = 'button';

  /**
   * If `true`, display only an icon in the button.
   */
  @Prop({ reflect: true }) iconOnly: boolean = false;

  /**
   * If `true`, the user cannot interact with the button.
   */
  @Prop({ reflect: true }) disabled: boolean = false;

  /**
   * Set to `true` for a full-width button.
   */
  @Prop({ reflect: true }) expand: boolean = false;

  /**
   * This attribute instructs browsers to download a URL instead of navigating to
   * it, so the user will be prompted to save it as a local file. If the attribute
   * has a value, it is used as the pre-filled file name in the Save prompt
   * (the user can still change the file name if they want).
   */
   @Prop() download: string | undefined;

  /**
   * Contains a URL or a URL fragment that the hyperlink points to.
   * If this property is set, an anchor tag will be rendered.
   */
  @Prop() href?: string;

  /**
   * Specifies the relationship of the target object to the link object.
   * The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).
   */
  @Prop() rel?: string;

  /**
   * Specifies where to display the linked URL.
   * Only applies when an `href` is provided.
   * Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.
   */
  @Prop() target?: string;

  /**
   * Set to `false` for a ghost button, set to `true` for a default outline button
   */
  @Prop() outline: boolean = false;

  /**
   * Emitted when the button has focus.
   */
  @Event() roadfocus!: EventEmitter<void>;
  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  /**
   * Emitted when the button loses focus.
   */
  @Event() roadblur!: EventEmitter<void>;
  /** @internal */
  @Event() roadBlur!: EventEmitter<void>;

  private onClick = (ev: Event) => {
    if (this.el.shadowRoot && (this.el as any).attachShadow) {
      // this button wants to specifically submit a form
      // climb up the dom to see if we're in a <form>
      // and if so, then use JS to submit it
      const form = this.el.closest('form');
      if (form) {
        ev.preventDefault();

        const fakeButton = document.createElement('button');
        fakeButton.type = this.buttonType;
        fakeButton.style.display = 'none';
        form.appendChild(fakeButton);
        fakeButton.click();
        fakeButton.remove();
      }
    }
  };

  private onFocus = () => {
    this.roadfocus.emit();
    this.roadFocus.emit();
  };

  private onBlur = () => {
    this.roadblur.emit();
    this.roadBlur.emit();
  };

  render() {
    const { buttonType, disabled, href, rel, target, color } = this;
    const TagType = this.href === undefined ? 'button' : 'a' as any;
    const attrs = (TagType === 'button')
      ? { type: buttonType }
      : {
        download: this.download,
        href,
        rel,
        target,
      };
    const colorClass = this.outline ? `btn-outline-${color}` : `btn-${color}`;
    const sizeClass = this.size !== undefined ? `btn-${this.size}` : '';
    const expandClass = this.expand ? 'btn-block' : '';
    const iconOnlyClass = this.iconOnly ? 'btn-icon' : '';

    return (
      <Host
          class={[colorClass, sizeClass, expandClass, iconOnlyClass].filter(Boolean).join(' ')}
          onClick={this.onClick}
          aria-disabled={disabled ? 'true' : null}
        >
        <TagType
          {...attrs}
          class="button-native"
          part="native"
          disabled={disabled}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        >
          <slot name="start"/>
          <slot/>
          <slot name="end"/>
        </TagType>
      </Host>
    );
  }
}