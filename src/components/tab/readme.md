# road-tab



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute | Description                                                                                                                                | Type     | Default     |
| ------------------ | --------- | ------------------------------------------------------------------------------------------------------------------------------------------ | -------- | ----------- |
| `tab` _(required)_ | `tab`     | A tab id must be provided for each `road-tab`. It's used internally to reference the selected tab or by the router to switch between them. | `string` | `undefined` |


## Methods

### `setActive() => Promise<void>`

Set the active component for the tab

#### Returns

Type: `Promise<void>`




## Slots

| Slot | Description                                |
| ---- | ------------------------------------------ |
|      | content of the tab sync with selected tab. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
