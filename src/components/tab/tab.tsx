import { Component, Element, Host, Listen, Method, Prop, h } from '@stencil/core';

/**
 * @slot  - content of the tab sync with selected tab.
 */

@Component({
  tag: 'road-tab',
  styleUrl: 'tab.css',
  shadow: true,
})
export class Tab {

  @Element() el!: HTMLRoadTabElement;

  /** @internal */
  @Prop({ mutable: true }) active = false;

  /**
   * A tab id must be provided for each `road-tab`. It's used internally to reference
   * the selected tab or by the router to switch between them.
   */
  @Prop() tab!: string;

  async componentWillLoad() {
    if (this.active) {
      await this.setActive();
    }
  }

  @Listen('roadtabbarchanged', { target: 'window' })
  onTabBarChanged(ev: CustomEvent) {
    this.active = this.tab === ev.detail.tab;
  }

  /** Set the active component for the tab */
  @Method()
  async setActive() {
    this.active = true;
  }

  render() {
    const { tab, active } = this;
    return (
      <Host
        role="tabpanel"
        aria-hidden={!active ? 'true' : null}
        aria-labelledby={`tab-button-${tab}`}
        class={{
          'tab-hidden': !active,
        }}
      >
        <slot/>
      </Host>
    );
  }
}
