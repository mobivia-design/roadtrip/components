import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-progress', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage();
  });

  it('should render information progress', async () => {
    await page.setContent(`
      <road-progress value="80"></road-progress>
    `);

    const element = await page.find('road-progress');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render success progress', async () => {
    await page.setContent(`
      <road-progress value="80" color="success"></road-progress>
    `);

    const element = await page.find('road-progress');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render warning progress', async () => {
    await page.setContent(`
      <road-progress value="80" color="warning"></road-progress>
    `);

    const element = await page.find('road-progress');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render danger progress', async () => {
    await page.setContent(`
      <road-progress value="80" color="danger"></road-progress>
    `);

    const element = await page.find('road-progress');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});