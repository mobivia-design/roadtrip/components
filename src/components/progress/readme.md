# road-progress



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description                                                                                           | Type                                                                                   | Default     |
| ------------- | ------------- | ----------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- | ----------- |
| `animation`   | `animation`   | Animation progress bar                                                                                | `boolean`                                                                              | `false`     |
| `color`       | `color`       | The color to use from your application's color palette.                                               | `"danger" \| "info" \| "primary" \| "rating" \| "secondary" \| "success" \| "warning"` | `'primary'` |
| `fullwidth`   | `fullwidth`   | Add padding if the progress is full width                                                             | `boolean`                                                                              | `false`     |
| `label`       | `label`       | Label display in progress bar                                                                         | `string`                                                                               | `''`        |
| `light`       | `light`       | Light progress background                                                                             | `boolean`                                                                              | `false`     |
| `numbersteps` | `numbersteps` | The number of steps should be 4 or 5.                                                                 | `"4" \| "5" \| "default" \| undefined`                                                 | `'4'`       |
| `showstep`    | `showstep`    | Show step                                                                                             | `boolean`                                                                              | `false`     |
| `value`       | `value`       | The value determines how much of the active bar should display. The value should be between [0, 100]. | `number`                                                                               | `0`         |


## CSS Custom Properties

| Name              | Description                   |
| ----------------- | ----------------------------- |
| `--border-radius` | Border radius of the progress |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
