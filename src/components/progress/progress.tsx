import { Component, Host, Prop, h } from '@stencil/core';

@Component({
  tag: 'road-progress',
  styleUrl: 'progress.css',
  shadow: true,
})
export class ProgressBar {

  /**
   * The value determines how much of the active bar should display.
   * The value should be between [0, 100].
   */
  @Prop() value: number = 0;

  /**
   * The number of steps should be 4 or 5.
   */
   @Prop() numbersteps?: '4' | '5' | 'default' = '4';

  /**
   * Label display in progress bar
   */
     @Prop() label: string = '';

  /**
   * Show step
   */
  @Prop() showstep: boolean = false;

  /**
   * Animation progress bar
   */
    @Prop() animation: boolean = false;

  /**
   * Light progress background
   */
  @Prop() light: boolean = false;

  /**
   * Add padding if the progress is full width
   */
   @Prop() fullwidth: boolean = false;

  /**
   * The color to use from your application's color palette.
   */
  @Prop() color: 'primary' | 'secondary' | 'info' | 'success' | 'warning' | 'danger' | 'rating' = 'primary';

  render() {

    const valueRound = Math.round(this.value/5/5);
    const fullwidth = this.fullwidth ? 'progress-element-info-full-width' : 'progress-element-info';
    const light = this.light ? 'progress progress-light' : 'progress';
    const animation = this.animation ? 'animation' : '';

    return (
      <Host class="progress-element">
        <div class={`${light} progress-${this.color} ${animation}`}>
          <div class="progress-bar" role="progressbar" style={{ width: `${this.value}%` }} aria-valuenow={this.value} aria-valuemin="0" aria-valuemax="100" aria-label="progress bar"></div>
        </div>
        <div class={`${fullwidth}`}>
          <span class="progress-element-label">{this.label}</span>
          {this.showstep && <span class="progress-element-step">{valueRound}/{this.numbersteps}</span>}
        </div>
      </Host>
    );
  }

}
