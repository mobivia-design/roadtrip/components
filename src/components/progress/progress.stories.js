import { html } from 'lit';

export default {
  title: 'Indicators/Progress',
  component: 'road-progress',
};

export const Playground = ({ value, color, label, showstep, animation, numbersteps, fullwidth, light }) => html`
  <road-progress value="${value}" color="${color}" light="${light}" label="${label}" showstep="${showstep}" animation="${animation}" numbersteps="${numbersteps}" fullwidth="${fullwidth}"></road-progress>
`;
Playground.args = {
  value: 25,
  color: 'primary',
  label: '',
  showstep:'false',
  animation:'false',
  numbersteps: '4',
  fullwidth:'false',
  light:'false',
};
Playground.argTypes = {
  value: {
    description: "The value determines how much of the active bar should display.\nThe value should be between [0, 100].",
    control: {
      type: 'number',
      min: '0',
      max: '100',
    },
  },
  numbersteps: {
    description: "The number of steps should be 4 or 5.",
    options: ['4', '5'],
    control: {
      type: 'select',
      min: '4',
      max: '5',
    },
  },
  label: {
    description: "Label display in progress bar",
    control: 'text',
  },
  showstep: {
    description: "Show step",
    control: 'boolean',
  },
  animation: {
    description: "Animation progress bar",
    control: 'boolean',
  },
  light: {
    description: "Light progress background",
    control: 'boolean',
  },
  fullwidth: {
    description: "Add padding if the progress is full width",
    control: 'boolean',
  },
  color: {
    description: "The color to use from your application's color palette.",
    options: ['primary', 'secondary', 'info', 'success', 'warning', 'danger', 'rating'],
    control: {
      type: 'radio',
    },
    table: {
      defaultValue: { summary: 'primary' },
    },
  },
  '--border-radius': {
    description: "Border radius of the progress",
    table: {
      defaultValue: { summary: '0.25rem' },
    },
    control: {
      type: null,
    },
  },
};

export const Value = () => html`
<road-progress value="0"></road-progress>

<br>

<road-progress value="25"></road-progress>

<br>

<road-progress value="50"></road-progress>

<br>

<road-progress value="75"></road-progress>

<br>

<road-progress value="100"></road-progress>
`;

export const color = () => html`
<road-progress value="80" color="primary"></road-progress>

<br>

<road-progress value="80" color="secondary"></road-progress>

<br>

<road-progress value="80" color="info"></road-progress>

<br>

<road-progress value="80" color="success"></road-progress>

<br>

<road-progress value="80" color="warning"></road-progress>

<br>

<road-progress value="80" color="danger"></road-progress>

<br>

<road-progress value="80" color="rating"></road-progress>

`;

export const Label = () => html`
<road-progress value="0" label="label" showstep="true"></road-progress>

<br>

<road-progress value="25" label="label" showstep="true"></road-progress>

<br>

<road-progress value="50" label="label" showstep="true"></road-progress>

<br>

<road-progress value="75" label="label" showstep="true"></road-progress>

<br>

<road-progress value="100" label="label" showstep="true"></road-progress>
`;
