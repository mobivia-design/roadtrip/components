import { Component, Host, h, Prop, Watch, Event, EventEmitter, Listen } from '@stencil/core';
import './../../utils/polyfill';

/**
 * @slot navbar - it should be road-navbar-item elements. Max 5 items on Mobile or add them to the drawer
 */

@Component({
  tag: 'road-global-navigation',
  styleUrl: 'global-navigation.css',
  shadow: true,
})
export class GlobalNavigation {

  /**
   * The selected tab component
   */
  @Prop() selectedTab?: string;
  @Watch('selectedTab')
  selectedTabChanged() {
    if (this.selectedTab !== undefined) {
      this.roadnavbarchanged.emit({
        tab: this.selectedTab,
      });
      this.roadNavbarChanged.emit({
        tab: this.selectedTab,
      });
    }
  }

  /** @internal */
  @Event() roadnavbarchanged!: EventEmitter;
  /** @internal */
  @Event() roadNavbarChanged!: EventEmitter;

  @Listen('roadNavbarItemClick')
  @Listen('roadnavbaritemclick')
  onNavbarChanged(ev: CustomEvent) {
    this.selectedTab = ev.detail.tab;
  }

  componentWillLoad() {
    this.selectedTabChanged();
  }

  render() {
    return (
      <Host
        role="application"
      >
        <slot/>
      </Host>
    );
  }

}
