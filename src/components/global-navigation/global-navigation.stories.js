import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Navigation/GlobalNavigation',
  component: 'road-global-navigation',
  subcomponents: {
    'road-navbar': 'road-navbar',
    'road-navbar-item': 'road-navbar-item',
    'road-toolbar': 'road-toolbar',
  },
  parameters: {
    actions: {
      handles: ['roadnavbaritemclick'],
    },
    backgrounds: {
      default: 'grey',
    },
    layout: 'fullscreen',
  },
  argTypes: {
    toolbar: {
      control: 'text',
    },
    navbar: {
      control: 'text',
    },
    'selected-tab': {
      control: 'text',
    },
    '--z-index': {
      table: {
        defaultValue: { summary: '10' },
      },
      control: {
        type: null,
      },
    },
  },
  args: {
    toolbar: `
    <road-toolbar role="tabpanel">
        <road-button slot="start" class="border-0 align-self-auto" color="ghost">
        <road-icon name="pass-maintain-logo-solid-color" role="img"></road-icon> <road-label class="font-weight-bold h6 mb-0 ml-8">App Name</road-label>
      </road-button>
      
      
      <road-button class="border-0 align-items-center" slot="secondary" color="ghost">
        <road-icon name="alert-question-outline" role="img"></road-icon><road-label class="d-none d-xl-block mx-8">Help</road-label>
      </road-button>
      <road-button class="d-none d-xl-flex align-items-center" slot="end" color="ghost">
        <road-icon name="speak-advice-outline" role="img"></road-icon><road-label class="d-none d-xl-block mx-8">Feedback</road-label>
      </road-button>
      
    </road-toolbar>
    `,
    navbar: `
    <road-navbar role="tabpanel">
    <road-profil-dropdown is-open="false" class="m-24 d-none d-xl-block">
      <road-list slot="list">
        <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;" class="edit-profil border-0">
          <road-icon slot="start" name="edit-outline" size="md"></road-icon>
          <road-label style="font-size: 0.75rem">
            Edit profile
          </road-label>
        </road-item>
        <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;" class="border-0">
          <road-icon slot="start" name="log-out-outline" size="md"></road-icon>
          <road-label style="font-size: 0.75rem">
            Log out
          </road-label>
        </road-item>
      </road-list>
      <road-img slot="avatar" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
      <road-img slot="avatarItem" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
      <road-label slot="name">First and Last name</road-label>
      <road-label slot="email">email</road-label>
    </road-profil-dropdown>

    <road-drawer is-open="false" position="left" drawer-width="480" drawer-title="Edit profil" class="drawer-profil" has-close-icon="true">
        <road-avatar class="mx-auto mb-16 mt-24" size="lg">  
          <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
        </road-avatar>
          <road-label class="mb-24">
          <p class="h8 mb-4">First and last name</p>
          <p class="text-medium text-truncate m-0">Email</p>
        </road-label>
    
        <div class="p-16" style="position: absolute; bottom: 0; width: 100%">
          <road-button color="secondary" expand="" class="mb-0">Save changes</road-button>
        </div>
      </road-drawer>
        
        <script>
        document.querySelector('.edit-profil').addEventListener('click', () => {
          document.querySelector('.drawer-profil').setAttribute('is-open', 'true');
        });
        </script>

    <road-navbar-item tab="tab-home" role="tabpanel">
    <road-tooltip content="Home" position="right" trigger="hover" contentalign="center">
      <road-icon name="navigation-home-outline" role="img"></road-icon>
      <road-label class="font-weight-bold">Home</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-search" role="tabpanel">
    <road-tooltip content="Search" position="right" trigger="hover" contentalign="center">
      <road-icon name="search" role="img"></road-icon>
      <road-label class="font-weight-bold">Search</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-catalog" role="tabpanel">
    <road-tooltip content="Print" position="right" trigger="hover" contentalign="center">
      <road-icon name="print-outline" role="img"></road-icon>
      <road-label class="font-weight-bold">Print</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-notification" class="d-none d-xl-flex" role="tabpanel">
    <road-tooltip content="Notifications" position="right" trigger="hover" contentalign="center">
      <road-icon name="alert-notification-outline" role="img"></road-icon>
      <road-label class="font-weight-bold">Notifications</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-scan" class="d-none d-xl-flex" role="tabpanel">
    <road-tooltip content="Scan" position="right" trigger="hover" contentalign="center">
      <road-icon name="scan" role="img"></road-icon>
      <road-label class="font-weight-bold">Scan</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-catalogue" class="d-none d-xl-flex" role="tabpanel">
    <road-tooltip content="Catalogue" position="right" trigger="hover" contentalign="center">
      <road-icon name="file-catalog" role="img"></road-icon>
      <road-label class="font-weight-bold">Catalogue</road-label>
    </road-tooltip>
  </road-navbar-item>

  <road-navbar-item tab="tab-diag" class="d-none d-xl-flex" role="tabpanel">
    <road-tooltip content="Diagnostic" position="right" trigger="hover" contentalign="center">
      <road-icon name="Diagnostic" role="img"></road-icon>
      <road-label class="font-weight-bold">Diagnostic</road-label>
    </road-toolip>
  </road-navbar-item>

  <road-navbar-item tab="tab-menu" class="d-block d-xl-none tab-menu">
    <road-icon name="navigation-menu" role="img"></road-icon>
    <road-label>Menu</road-label>
  </road-navbar-item>
  
  <road-drawer is-open="false" position="right" drawer-width="480" drawer-title="Menu" class="d-xl-none drawer-menu">
  <div class="p-16">
  <road-item class="mb-16 text-left bg-light profil" tabindex="0">
    <road-avatar slot="start" class="mr-16">  
      <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
    </road-avatar>
      <road-label>
      <p class="h8 mb-4">First and last name</p>
      <p class="text-medium text-truncate m-0">Email</p>
    </road-label>

    <road-icon slot="end" name="edit-outline" size="sm" class="mr-8"></road-icon>
  </road-item>

  <road-item class="bg-white border-0" button="true">
    <road-icon name="scan"></road-icon>
    Scan
  </road-item>

  <road-item class="bg-white border-0" button="true">
    <road-icon name="file-catalog"></road-icon>
    Catalogue
  </road-item>

  <road-item class="bg-white border-0" button="true">
    <road-icon name="Diagnostic"></road-icon>
    Diagnostic
  </road-item>

  <road-item class="bg-white border-left-0 border-right-0 border-bottom-0 border-top" button="true">
    <road-icon name="log-out"></road-icon>
    Log out
  </road-item>
</div>
  </road-drawer>


  <road-drawer is-open="false" position="right" drawer-width="480" drawer-title="Menu" class="d-xl-none drawer-menu2" has-back-icon="true" has-close-icon="true" back-text="back">
    <road-avatar class="mx-auto mb-16 mt-24" size="lg">  
      <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
    </road-avatar>
      <road-label class="mb-24">
      <p class="h8 mb-4">First and last name</p>
      <p class="text-medium text-truncate m-0">Email</p>
    </road-label>

    <div class="p-16" style="position: absolute; bottom: 0; width: 100%">
      <road-button color="secondary" expand="" class="mb-0">Save changes</road-button>
    </div>
  </road-drawer>
</road-navbar>

  
  <script>
document.querySelector('.tab-menu').addEventListener('click', () => {
  document.querySelector('.drawer-menu').setAttribute('is-open', 'true');
});

document.querySelector('.profil').addEventListener('click', () => {
  document.querySelector('.drawer-menu2').setAttribute('is-open', 'true');
});

</script>`,
  },
};

const Template = (args) => html`
<road-global-navigation selected-tab="${ifDefined(args['selected-tab'])}">
    ${unsafeHTML(args.toolbar)}
    ${unsafeHTML(args.navbar)}
</road-global-navigation>


`;

export const Playground = Template.bind({});

export const Page = Template.bind({});
Page.args = {
  toolbar: `
    <road-toolbar>
      <road-button slot="start" class="border-left-0">
        <road-icon name="navigation-chevron" rotate="180"></road-icon>
      </road-button>
      
      <road-toolbar-title>Search</road-toolbar-title>
      <road-button class="border-0 align-items-center" slot="secondary">
        <road-icon name="alert-question-outline"></road-icon><road-label class="d-none d-xl-block mx-8">Help</road-label>
      </road-button>
      <road-button class="d-none d-xl-flex align-items-center" slot="end">
        <road-icon name="speak-advice-outline"></road-icon><road-label class="d-none d-xl-block mx-8">Feedback</road-label>
      </road-button>
      
    </road-toolbar>
    `,
    navbar: `
    <road-navbar>
    <road-profil-dropdown is-open="false" class="m-24 d-none d-xl-block">
      <road-list slot="list">
        <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
          <road-icon slot="start" name="edit-outline" size="md"></road-icon>
          <road-label style="font-size: 0.75rem">
            Edit profile
          </road-label>
        </road-item>
        <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
          <road-icon slot="start" name="log-out-outline" size="md"></road-icon>
          <road-label style="font-size: 0.75rem">
            Log out
          </road-label>
        </road-item>
      </road-list>
      <road-img slot="avatar" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
      <road-img slot="avatarItem" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
      <road-label slot="name">First and Last name</road-label>
      <road-label slot="email">email</road-label>
    </road-profil-dropdown>

    <road-navbar-item tab="tab-home" role="menu">
    <road-icon name="navigation-home-outline"></road-icon>
    <road-label style="font-size:0.75rem;">Home</road-label>
  </road-navbar-item>

  <road-navbar-item tab="tab-search" role="menu">
    <road-icon name="search"></road-icon>
    <road-label style="font-size:0.75rem;">Search</road-label>
  </road-navbar-item>

  <road-navbar-item tab="tab-catalog" role="menu">
    <road-icon name="print-outline"></road-icon>
    <road-label style="font-size:0.75rem;">Print</road-label>
  </road-navbar-item>

  <road-navbar-item tab="tab-notification" class="d-none d-xl-flex" role="menu">
    <road-icon name="alert-notification-outline"></road-icon>
    <road-label style="font-size:0.75rem;">Notifications</road-label>
  </road-navbar-item>

  <road-navbar-item tab="tab-scan" class="d-none d-xl-flex" role="menu">
    <road-icon name="scan"></road-icon>
    <road-label>Scan</road-label>
  </road-navbar-item>

  <road-navbar-item tab="tab-catalogue" class="d-none d-xl-flex" role="menu">
    <road-icon name="file-catalog"></road-icon>
    <road-label style="font-size:0.75rem;">Catalogue</road-label>
  </road-navbar-item>

  <road-navbar-item tab="tab-diag" class="d-none d-xl-flex" role="menu">
    <road-icon name="Diagnostic"></road-icon>
    <road-label style="font-size:0.75rem;">Diagnostic</road-label>
  </road-navbar-item>

  <road-navbar-item tab="tab-menu" class="d-block d-xl-none tab-menu" role="menu">
    <road-icon name="navigation-menu"></road-icon>
    <road-label style="font-size:0.75rem;">Menu</road-label>
  </road-navbar-item>
  
  <road-drawer is-open="false" position="right" drawer-width="480" drawer-title="Menu" class="d-xl-none drawer-menu">
  <road-item class="mb-16 text-left bg-light profil" tabindex="0">
    <road-avatar slot="start" class="mr-16">  
      <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
    </road-avatar>
      <road-label>
      <p class="h8 mb-4">First and last name</p>
      <p class="text-medium text-truncate m-0">Email</p>
    </road-label>

    <road-icon slot="end" name="edit-outline" size="sm" class="mr-8"></road-icon>
  </road-item>

  <road-item class="bg-white">
    <road-icon name="scan"></road-icon>
    Scan
  </road-item>

  <road-item class="bg-white">
    <road-icon name="file-catalog"></road-icon>
    Catalogue
  </road-item>

  <road-item class="bg-white">
    <road-icon name="Diagnostic"></road-icon>
    Diagnostic
  </road-item>

  <road-item class="bg-white border-top ">
    <road-icon name="log-out"></road-icon>
    Log out
  </road-item>

  </road-drawer>


  <road-drawer is-open="false" position="right" drawer-width="480" drawer-title="Menu" class="d-xl-none drawer-menu2" has-back-icon="true" has-close-icon="true" back-text="back">
    <road-avatar class="mx-auto mb-16 mt-24" size="lg">  
      <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
    </road-avatar>
      <road-label class="mb-24">
      <p class="h8 mb-4">First and last name</p>
      <p class="text-medium text-truncate m-0">Email</p>
    </road-label>

    <div class="p-16" style="position: absolute; bottom: 0; width: 100%">
      <road-button color="secondary" expand="" class="mb-0">Save changes</road-button>
    </div>
  </road-drawer>
</road-navbar>

  
  <script>
document.querySelector('.tab-menu').addEventListener('click', () => {
  document.querySelector('.drawer-menu').setAttribute('is-open', 'true');
});

document.querySelector('.profil').addEventListener('click', () => {
  document.querySelector('.drawer-menu2').setAttribute('is-open', 'true');
});

</script>`,
};


