# road-navbar



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                | Type                  | Default     |
| ------------- | -------------- | -------------------------- | --------------------- | ----------- |
| `selectedTab` | `selected-tab` | The selected tab component | `string \| undefined` | `undefined` |


## Slots

| Slot       | Description                                                                             |
| ---------- | --------------------------------------------------------------------------------------- |
| `"navbar"` | it should be road-navbar-item elements. Max 5 items on Mobile or add them to the drawer |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
