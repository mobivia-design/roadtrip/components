import { newE2EPage } from '@stencil/core/testing';

describe('road-global-navigation', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(`
    <road-toolbar>
        <road-button slot="start" class="border-0 align-self-auto">
        <road-icon name="pass-maintain-logo-solid-color"></road-icon> <road-label class="font-weight-bold h6 mb-0 ml-8">App Name</road-label>
      </road-button>
      
      
      <road-button class="border-0 align-items-center" slot="secondary">
        <road-icon name="alert-question-outline"></road-icon><road-label class="d-none d-xl-block mx-8">Help</road-label>
      </road-button>
      <road-button class="d-none d-xl-flex align-items-center" slot="end">
        <road-icon name="speak-advice-outline"></road-icon><road-label class="d-none d-xl-block mx-8">Feedback</road-label>
      </road-button>
      
    </road-toolbar>
    <road-navbar>
      <road-profil-dropdown is-open="false" class="m-24 d-none d-xl-block">
        <road-list slot="list">
          <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
            <road-icon slot="start" name="edit-outline" size="md"></road-icon>
            <road-label style="font-size: 0.75rem">
              Edit profile
            </road-label>
          </road-item>
          <road-item button="" style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;">
            <road-icon slot="start" name="log-out-outline" size="md"></road-icon>
            <road-label style="font-size: 0.75rem">
              Log out
            </road-label>
          </road-item>
        </road-list>
        <road-img slot="avatar" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
        <road-img slot="avatarItem" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=800&amp;q=80" alt="avatar" title="Person name"></road-img>
      </road-profil-dropdown>

      <road-navbar-item tab="tab-home">
      <road-icon name="navigation-home-outline"></road-icon>
      <road-label>Home</road-label>
    </road-navbar-item>

    <road-navbar-item tab="tab-search">
      <road-icon name="search"></road-icon>
      <road-label>Search</road-label>
    </road-navbar-item>

    <road-navbar-item tab="tab-catalog">
      <road-icon name="print-outline"></road-icon>
      <road-label>Print</road-label>
    </road-navbar-item>

    <road-navbar-item tab="tab-notification" class="d-none d-xl-flex">
      <road-icon name="alert-notification-outline"></road-icon>
      <road-label>Notifications</road-label>
    </road-navbar-item>

    <road-navbar-item tab="tab-scan" class="d-none d-xl-flex">
      <road-icon name="scan"></road-icon>
      <road-label>Scan</road-label>
    </road-navbar-item>

    <road-navbar-item tab="tab-catalogue" class="d-none d-xl-flex">
      <road-icon name="file-catalog"></road-icon>
      <road-label>Catalogue</road-label>
    </road-navbar-item>

    <road-navbar-item tab="tab-diag" class="d-none d-xl-flex">
      <road-icon name="Diagnostic"></road-icon>
      <road-label>Diagnostic</road-label>
    </road-navbar-item>

    <road-navbar-item tab="tab-menu" class="d-block d-xl-none tab-menu">
      <road-icon name="navigation-menu"></road-icon>
      <road-label>Menu</road-label>
    </road-navbar-item>
    
    <road-drawer is-open="false" position="right" drawer-width="480" drawer-title="Menu" class="d-xl-none drawer-menu">
    <road-item class="mb-16 text-left bg-light profil">
      <road-avatar slot="start" class="mr-16">  
        <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
      </road-avatar>
        <road-label>
        <p class="h8 mb-4">First and last name</p>
        <p class="text-medium text-truncate m-0">Email</p>
      </road-label>

      <road-icon slot="end" name="edit-outline" size="sm" class="mr-8"></road-icon>
    </road-item>

    <road-item class="bg-white">
      <road-icon name="scan"></road-icon>
      Scan
    </road-item>

    <road-item class="bg-white">
      <road-icon name="file-catalog"></road-icon>
      Catalogue
    </road-item>

    <road-item class="bg-white">
      <road-icon name="Diagnostic"></road-icon>
      Diagnostic
    </road-item>

    <road-item class="bg-white border-top ">
      <road-icon name="log-out"></road-icon>
      Log out
    </road-item>

    </road-drawer>


    <road-drawer is-open="false" position="right" drawer-width="480" drawer-title="Menu" class="d-xl-none drawer-menu2" has-back-icon="true" has-close-icon="true" back-text="back">
      <road-avatar class="mx-auto mb-16 mt-24" size="lg">  
        <road-img src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar"></road-img>
      </road-avatar>
        <road-label class="mb-24">
        <p class="h8 mb-4">First and last name</p>
        <p class="text-medium text-truncate m-0">Email</p>
      </road-label>

      <div class="p-16" style="position: absolute; bottom: 0; width: 100%">
        <road-button color="secondary" expand="" class="mb-0">Save changes</road-button>
      </div>
    </road-drawer>
  </road-navbar>
    `);

    const element = await page.find('road-navbar');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
