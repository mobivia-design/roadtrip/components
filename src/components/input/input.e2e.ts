import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-input', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-input label="Label"></road-input>
      `,
    });
  });

  it('should render', async () => {
    const element = await page.find('road-input');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should add a value', async () => {
    const roadinput = await page.spyOnEvent('roadinput');
    const roadchange = await page.spyOnEvent('roadchange');
    const roadblur = await page.spyOnEvent('roadblur');
    const roadfocus = await page.spyOnEvent('roadfocus');

    await page.focus('road-input input');
    await page.type('road-input input', 'Value');
    await page.$eval('road-input input', e => e.blur());

    expect(roadfocus).toHaveReceivedEvent();
    expect(roadblur).toHaveReceivedEvent();
    expect(roadinput).toHaveReceivedEvent();

    expect(roadchange).toHaveReceivedEventDetail({
      value: 'Value',
    });

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render disabled', async () => {
    const element = await page.find('road-input');
    expect(element).toHaveClass('hydrated');

    element.setProperty('disabled', true);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render with error', async () => {
    const element = await page.find('road-input');
    expect(element).toHaveClass('hydrated');

    element.setProperty('error', 'This field is required');

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
