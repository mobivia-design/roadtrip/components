import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Forms/Input',
  component: 'road-input',
  parameters: {
    actions: {
      handles: ['roadblur', 'roadfocus'],
    },
  },
  argTypes: {
    disabled: {
      description: "If `true`, the user cannot interact with the input.",
      control: 'boolean',
    },
    readonly: {
      description: "If `true`, the user cannot modify the value.",
      control: 'boolean',
    },
    required: {
      description: "If `true`, the user must fill in a value before submitting a form.",
      control: 'boolean',
    },
    autofocus: {
      description: "This Boolean attribute lets you specify that a form control should have input focus when the page loads.",
      control: 'boolean',
    },
    spellcheck: {
      description: "If `true`, the element will have its spelling and grammar checked.",
      control: 'boolean',
    },
    blockdecimal: {
      description: "If `true`, block decimal.",
      control: 'boolean',
    },
    error: {
      description: "Error message for the field",
      control: 'text',
    },
    helper: {
      description: "Helper message for the field",
      control: 'text',
    },
    label: {
      description: "Label for the field",
      control: 'text',
    },
    name: {
      description: "The name of the control, which is submitted with the form data.",
      control: 'text',
    },
    placeholder: {
      description: "Instructional text that shows before the input has a value.",
      control: 'text',
    },
    size: {
      description: "The initial size of the control. This value is in pixels unless the value of the type attribute is `\"text\"` or `\"password\"`, in which case it is an integer number of characters. This attribute applies only when the `type` attribute is set to `\"text\"`, `\"search\"`, `\"tel\"`, `\"url\"`, `\"email\"`, or `\"password\"`, otherwise it is ignored.",
      control: 'number',
    },
    min: {
      description: "The minimum value, which must not be greater than its maximum (max attribute) value.",
      control: 'text',
    },
    minlength: {
      description: "If the value of the type attribute is `text`, `email`, `search`, `password`, `tel`, or `url`, this attribute specifies the minimum number of characters that the user can enter.",
      control: 'number',
    },
    max: {
      description: "The maximum value, which must not be less than its minimum (min attribute) value.",
      control: 'text',
    },
    maxlength: {
      description: "If the value of the type attribute is `text`, `email`, `search`, `password`, `tel`, or `url`, this attribute specifies the maximum number of characters that the user can enter.",
      control: 'number',
    },
    step: {
      description: "Works with the min and max attributes to limit the increments at which a value can be set.\nPossible values are: `\"any\"` or a positive floating point number.",
      control: 'number',
    },
    pattern: {
      description: "A regular expression that the value is checked against. The pattern must match the entire value, not just some subset. Use the title attribute to describe the pattern to help the user. This attribute applies when the value of the type attribute is `\"text\"`, `\"search\"`, `\"tel\"`, `\"url\"`, `\"email\"`, or `\"password\"`, otherwise it is ignored.",
      control: 'text',
    },
    value: {
      description: "The value of the input.(for dynamic use use v-model for vue instead)",
      control: 'text',
    },
    sizes: {
      description: "The sizes of the input.",
      options: ['lg', 'xl'],
      control: {
        type: 'select',
      },
    },
    type: {
      description: "The type of control to display. The default type is text.",
      options: ['date', 'email', 'number', 'password', 'search', 'tel', 'text', 'url', 'time'],
      control: {
        type: 'select',
      },
    },
    autocapitalize: {
      description: "Indicates whether and how the text value should be automatically capitalized as it is entered/edited by the user.",
      options: ['on', 'off'],
      control: {
        type: 'select',
      },
    },
    autocomplete: {
      description: "Indicates whether the value of the control can be automatically completed by the browser.",
      options: ['on', 'off', 'name', 'honorific-prefix', 'given-name', 'additional-name', 'family-name', 'honorific-suffix', 'nickname', 'email', 'username', 'new-password', 'current-password', 'one-time-code', 'organization-title', 'organization', 'street-address', 'address-line1', 'address-line2', 'address-line3', 'address-level4', 'address-level3', 'address-level2', 'address-level1', 'country', 'country-name', 'postal-code', 'cc-name', 'cc-given-name', 'cc-additional-name', 'cc-family-name', 'cc-family-name', 'cc-number', 'cc-exp', 'cc-exp-month', 'cc-exp-year', 'cc-csc', 'cc-type', 'transaction-currency', 'transaction-amount', 'language', 'bday', 'bday-day', 'bday-month', 'bday-year', 'sex', 'tel', 'tel-country-code', 'tel-national', 'tel-area-code', 'tel-local', 'tel-extension', 'impp', 'url', 'photo'],
      control: {
        type: 'select',
      },
    },
    autocorrect: {
      description: "Whether auto correction should be enabled when the user is entering/editing the text value.",
      options: ['on', 'off'],
      control: {
        type: 'select',
      },
    },
    enterkeyhint: {
      description: "A hint to the browser for which enter key to display.\nPossible values: `\"enter\"`, `\"done\"`, `\"go\"`, `\"next\"`,\n`\"previous\"`, `\"search\"`, and `\"send\"`.",
      options: ['enter', 'done', 'go', 'next', 'previous', 'search', 'send'],
      control: {
        type: 'select',
      },
    },
    inputmode: {
      description: "A hint to the browser for which keyboard to display.\nThis attribute applies when the value of the type attribute is `\"text\"`, `\"password\"`, `\"email\"`, or `\"url\"`. Possible values are: `\"verbatim\"`, `\"latin\"`, `\"latin-name\"`, `\"latin-prose\"`, `\"full-width-latin\"`, `\"kana\"`, `\"katakana\"`, `\"numeric\"`, `\"tel\"`, `\"email\"`, `\"url\"`.",
      options: ['none', 'text', 'tel', 'url', 'email', 'numeric', 'decimal', 'search'],
      control: {
        type: 'select',
      },
    },
    checklistPassword: {
      description: "Content the checklist Password exemple.\n`<div class=\"checklist-password mt-16\" slot=\"checklistPassword\">`\n`<span class=\"checklist-password-label\"><strong>Low Safety</strong></span>`\n`<road-progress color=\"danger\" value=\"25\" class=\"mt-8 mb-16\"></road-progress>`\n`<p class=\"text-medium mb-8\">For optimal safety your password must have at least :</p>`\n`<ul class=\"m-0 pl-16\">`\n`<li id=\"letter\" class=\"invalid mb-8\">1 lowercase and 1 uppercase</li>`\n`<li id=\"number\" class=\"invalid mb-8\">1 digit</li>`\n`<li id=\"length\" class=\"invalid\">8 characters minimum</li>`\n`</ul>`\n`</div>`",
      control: {
        type: 'text',
      },
    },
    debounce: {
      description: "Set the amount of time, in milliseconds, to wait to trigger the `roadChange` event after each keystroke.",
      control: 'number',
    },
    'input-id': {
      description: "The id of input",
      control: 'text',
    },
    roadblur: {
      control: {
        type: null,
      },
    },
    roadfocus: {
      control: {
        type: null,
      },
    },
    roadchange: {
      action: 'roadchange',
      control: {
        type: null,
      },
    },
    roadinput: {
      action: 'roadinput',
      control: {
        type: null,
      },
    },
    '--height': {
      description: "height of the field",
      table: {
        defaultValue: { summary: '3rem' },
      },
      control: {
        type: null,
      },
    },
    '--border-radius': {
      description: "Border radius of the field",
      table: {
        defaultValue: { summary: '0.25rem' },
      },
      control: {
        type: null,
      },
    },
    '--input-text-align': {
      description: "align the content of the field",
      table: {
        defaultValue: { summary: 'left' },
      },
      control: {
        type: null,
      },
    },
    '--margin-bottom': {
      description: "Bottom margin of the field",
      table: {
        defaultValue: { summary: '1rem' },
      },
      control: {
        type: null,
      },
    },
  },
  args: {
    label: 'Label',
    sizes: 'xl',
    disabled: null,
    readonly: null,
    required: null,
    autofocus: null,
    spellcheck: null,
    blockdecimal: null,
    'checklistPassword': ``,
  },
};

const Template = (args) => html`
  <road-input
    input-id="${ifDefined(args['input-id'])}"
    label="${ifDefined(args.label)}"
    disabled="${ifDefined(args.disabled)}"
    required="${ifDefined(args.required)}"
    autofocus="${ifDefined(args.autofocus)}"
    error="${ifDefined(args.error)}"
    helper="${ifDefined(args.helper)}"
    size="${ifDefined(args.size)}"
    sizes="${ifDefined(args.sizes)}"
    min="${ifDefined(args.min)}"
    max="${ifDefined(args.max)}"
    maxlength="${ifDefined(args.maxlength)}"
    minlength="${ifDefined(args.minlength)}"
    step="${ifDefined(args.step)}"
    value="${ifDefined(args.value)}"
    pattern="${ifDefined(args.pattern)}"
    spellcheck="${ifDefined(args.spellcheck)}"
    blockdecimal="${ifDefined(args.blockdecimal)}"
    placeholder="${ifDefined(args.placeholder)}"
    type="${ifDefined(args.type)}"
    autocapitalize="${ifDefined(args.autocapitalize)}"
    autocomplete="${ifDefined(args.autocomplete)}"
    autocorrect="${ifDefined(args.autocorrect)}"
    enterkeyhint="${ifDefined(args.enterkeyhint)}"
    inputmode="${ifDefined(args.inputmode)}"
    readonly="${ifDefined(args.readonly)}"
    debounce="${ifDefined(args.debounce)}"
    @roadchange=${event => args.roadchange(event.detail.value)}
    @roadinput=${event => args.roadinput(event.target.value)}
  >
  </road-input>
    ${unsafeHTML(args['checklistPassword'])}
`;

export const Playground = Template.bind({});

export const WithValue = Template.bind({});
WithValue.args = {
  value: 'Value',
};

export const Date = Template.bind({});
Date.args = {
  label: 'Date',
  type: 'date',
  value: '2021-04-21',
  min: '2021-01-01',
  max: '2021-12-31',
};

export const Hour = Template.bind({});
Hour.args = {
  label: 'Hour',
  type: 'time',
  value: '13:30',
  min: '09:00',
  max: '18:00',
};

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
};

export const Error = Template.bind({});
Error.args = {
  error: 'This field is required',
};

export const Helper = Template.bind({});
Helper.args = {
  helper: 'This field is required',
};

export const Password = (args) => html`
<road-input input-id="password" sizes="xl" label="password" type="password">

<div slot="checklistPassword">
    ${unsafeHTML(args['checklistPassword'])}
  </div>
</road-input>

`;
