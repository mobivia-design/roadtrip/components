import { Component, Event, EventEmitter, Host, Prop, Watch, h } from '@stencil/core';
import { debounce as lodashDebounce } from 'lodash';
import { AutocompleteTypes, TextFieldTypes } from '../../interface';

/**
 * @slot checklistPassword - Content the checklist Password exemple.
 * `<div class="checklist-password mt-16" slot="checklistPassword">`
 *   `<span class="checklist-password-label"><strong>Low Safety</strong></span>`
 *   `<road-progress color="danger" value="25" class="mt-8 mb-16"></road-progress>`
 *   `<p class="text-medium mb-8">For optimal safety your password must have at least :</p>`
 *   `<ul class="m-0 pl-16">`
 *     `<li id="letter" class="invalid mb-8">1 lowercase and 1 uppercase</li>`
 *     `<li id="number" class="invalid mb-8">1 digit</li>`
 *     `<li id="length" class="invalid">8 characters minimum</li>`
 *   `</ul>`
 * `</div>`
 */

@Component({
  tag: 'road-input',
  styleUrl: 'input.css',
  scoped: true,
})
export class Input {
  /**
   * The id of input
   */
  @Prop() inputId: string = `road-input-${inputIds++}`;

  /**
   * Indicates whether and how the text value should be automatically capitalized as it is entered/edited by the user.
   */
  @Prop() autocapitalize = 'off';

  /**
   * Indicates whether the value of the control can be automatically completed by the browser.
   */
  @Prop() autocomplete: AutocompleteTypes = 'off';

  /**
   * If `true`, block decimal.
   */
  @Prop() blockdecimal = false;


  /**
   * Whether auto correction should be enabled when the user is entering/editing the text value.
   */
  @Prop() autocorrect: 'on' | 'off' = 'off';

  /**
   * This Boolean attribute lets you specify that a form control should have input focus when the page loads.
   */
  @Prop() autofocus = false;

  /**
   * If `true`, the user cannot interact with the input.
   */
  @Prop() disabled = false;

  /**
   * A hint to the browser for which enter key to display.
   * Possible values: `"enter"`, `"done"`, `"go"`, `"next"`,
   * `"previous"`, `"search"`, and `"send"`.
   */
  @Prop() enterkeyhint?: 'enter' | 'done' | 'go' | 'next' | 'previous' | 'search' | 'send';

  /**
   * A hint to the browser for which keyboard to display.
   * This attribute applies when the value of the type attribute is `"text"`, `"password"`, `"email"`, or `"url"`. Possible values are: `"verbatim"`, `"latin"`, `"latin-name"`, `"latin-prose"`, `"full-width-latin"`, `"kana"`, `"katakana"`, `"numeric"`, `"tel"`, `"email"`, `"url"`.
   */
  @Prop() inputmode?: 'none' | 'text' | 'tel' | 'url' | 'email' | 'numeric' | 'decimal' | 'search';

  /**
   * The maximum value, which must not be less than its minimum (min attribute) value.
   */
  @Prop() max?: string;

  /**
   * If the value of the type attribute is `text`, `email`, `search`, `password`, `tel`, or `url`, this attribute specifies the maximum number of characters that the user can enter.
   */
  @Prop() maxlength?: number;

  /**
   * The minimum value, which must not be greater than its maximum (max attribute) value.
   */
  @Prop() min?: string;

  /**
   * If the value of the type attribute is `text`, `email`, `search`, `password`, `tel`, or `url`, this attribute specifies the minimum number of characters that the user can enter.
   */
  @Prop() minlength?: number;

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.inputId;

  /**
   * A regular expression that the value is checked against. The pattern must match the entire value, not just some subset. Use the title attribute to describe the pattern to help the user. This attribute applies when the value of the type attribute is `"text"`, `"search"`, `"tel"`, `"url"`, `"email"`, or `"password"`, otherwise it is ignored.
   */
  @Prop() pattern?: string;

  /**
   * Instructional text that shows before the input has a value.
   */
  @Prop() placeholder?: string;

  /**
   * If `true`, the user cannot modify the value.
   */
  @Prop() readonly = false;

  /**
   * If `true`, the user must fill in a value before submitting a form.
   */
  @Prop() required = false;

  /**
   * If `true`, the element will have its spelling and grammar checked.
   */
  @Prop() spellcheck = false;

  /**
   * Works with the min and max attributes to limit the increments at which a value can be set.
   * Possible values are: `"any"` or a positive floating point number.
   */
  @Prop() step?: string;

  /**
   * The initial size of the control. This value is in pixels unless the value of the type attribute is `"text"` or `"password"`, in which case it is an integer number of characters. This attribute applies only when the `type` attribute is set to `"text"`, `"search"`, `"tel"`, `"url"`, `"email"`, or `"password"`, otherwise it is ignored.
   */
  @Prop() size?: number;

  /**
   * The sizes of the input.
   */
  @Prop() sizes: 'lg' | 'xl' = 'lg';

  /**
   * The type of control to display. The default type is text.
   */
  @Prop() type: TextFieldTypes = 'text';

  /**
   * The value of the input.(for dynamic use use v-model for vue instead)
   */
  @Prop({ mutable: true }) value?: string | number | null = '';

  /**
   * Label for the field
   */
  @Prop() label?: string = '';

  /**
   * Error message for the field
   */
  @Prop() error?: string;

  /**
   * Helper message for the field
   */
  @Prop() helper?: string;

  /**
   * Set the amount of time, in milliseconds, to wait to trigger the `roadChange` event after each keystroke.
   */
  @Prop() debounce = 0;

  private handleKeyDown(event: KeyboardEvent) {
    if (this.type === 'number' && this.blockdecimal) {
      const forbiddenKeys = ['.', ',', 'e', '-', '¨', '^', '`', '+'];
      if (forbiddenKeys.includes(event.key)) {
        event.preventDefault();
      }
    }
  }


  private debouncedRoadChange = this.createDebouncedRoadChange();

  private createDebouncedRoadChange() {
    return lodashDebounce((value: any) => {
      this.roadChange.emit({ value: value == null ? value : value.toString() });
    }, this.debounce);
  }

  @Watch('debounce')
  debounceChanged() {
    // Créez une nouvelle fonction debouncedRoadChange avec la nouvelle valeur de debounce
    this.debouncedRoadChange = this.createDebouncedRoadChange();
  }

  /**
   * Update the native input element when the value changes
   */
  @Watch('value')
  protected valueChanged() {
    this.debouncedRoadChange(this.value);
  }

  /**
   * Emitted when a keyboard input occurred.
   */

  /** @internal */
  @Event() roadInput!: EventEmitter<KeyboardEvent>;

  /**
   * Emitted when the value has changed.
   */

  /** @internal */
  @Event() roadChange!: EventEmitter<{
    value: string | undefined | null;
  }>;

  /**
   * Emitted when the input loses focus.
   */

  /** @internal */
  @Event() roadBlur!: EventEmitter<string | null>;

  /**
   * Emitted when the input has focus.
   */

  /** @internal */
  @Event() roadFocus!: EventEmitter<void>;

  private getValue(): string {
    return typeof this.value === 'number'
      ? this.value.toString()
      : (this.value || '').toString();
  }

  private onInput = (ev: Event) => {
    const input = ev.target as HTMLInputElement;
    let newValue = input.value;

    if (this.type === 'number' && this.blockdecimal) {
      newValue = newValue.replace(/[.,]/g, ''); // Supprime les décimales si blockDecimal est activé
    }

    this.value = newValue;
    this.roadInput.emit(ev as KeyboardEvent);
    this.roadChange.emit({ value: newValue });
  };

  private onBlur = () => {
    let value = this.getValue();

    // Vérifier si les contraintes min et max sont définies
    if (this.min !== undefined && value !== '' && parseFloat(value) < parseFloat(this.min)) {
      value = this.min; // Ajuster la valeur à la valeur minimale
    }

    if (this.max !== undefined && value !== '' && parseFloat(value) > parseFloat(this.max)) {
      value = this.max; // Ajuster la valeur à la valeur maximale
    }

    this.value = value; // Mettre à jour la valeur avec la valeur ajustée

    // Émettre l'événement roadblur avec la valeur ajustée
    this.roadBlur.emit(value);
  };

  private onFocus = () => {
    this.roadFocus.emit();
  };

  private enforceMinMax(el: HTMLInputElement): void {
    if (el.value !== "") {
      const value: number = parseInt(el.value);
      const minValue: number = parseInt(el.min);
      const maxValue: number = parseInt(el.max);

      if (value < minValue) {
        el.value = el.min;
      }

      if (value > maxValue) {
        el.value = el.max;
      }
    }
  }

  componentWillLoad() {
    this.debounceChanged(); // Initialize debounce on component load
  }

  render() {
    const value = this.getValue();
    const labelId = this.inputId + '-label';
    const hasValueClass = this.value !== '' && this.value !== null ? 'has-value' : '';
    const lessLabelClass = this.label !== '' ? '' : 'less-label';
    const isInvalidClass = this.error !== undefined && this.error !== '' ? 'is-invalid' : '';

    return (
      <Host aria-disabled={this.disabled ? 'true' : null} class={this.sizes && `input-${this.sizes}`} value={value} blockdecimal={this.blockdecimal}>
        <input
          class={`form-control ${hasValueClass} ${isInvalidClass} ${lessLabelClass}`}
          id={this.inputId}
          aria-disabled={this.disabled ? 'true' : null}
          aria-labelledby={labelId}
          disabled={this.disabled}
          autoCapitalize={this.autocapitalize}
          autoComplete={this.autocomplete}
          autoCorrect={this.autocorrect}
          enterKeyHint={this.enterkeyhint}
          autoFocus={this.autofocus}
          inputMode={this.inputmode}
          min={this.min}
          max={this.max}
          minLength={this.minlength}
          maxLength={this.maxlength}
          name={this.name}
          pattern={this.pattern}
          placeholder={this.placeholder}
          readOnly={this.readonly}
          required={this.required}
          spellcheck={this.spellcheck}
          step={this.step}
          size={this.size}
          type={this.type}
          value={value}
          onInput={this.onInput}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          onKeyDown={this.type === 'number' ? (event: KeyboardEvent) => this.handleKeyDown(event) : undefined}
          onKeyUp={(event: Event) => this.enforceMinMax(event.target as HTMLInputElement)}
          data-cy='road-input'
        />
        <label class="form-label" id={labelId} htmlFor={this.inputId}>{this.label}</label>
        {this.error && this.error !== '' && <p class="invalid-feedback"><road-icon slot="start" name="alert-error-solid" aria-hidden="true" size="sm"></road-icon>{this.error}</p>}
        {this.helper && this.helper !== '' && <p class="helper">{this.helper}</p>}
        {this.type && this.type == 'password' && <slot name="checklistPassword" />}
      </Host>
    );
  }
}

let inputIds = 0;
