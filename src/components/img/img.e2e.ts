import { newE2EPage } from '@stencil/core/testing';

describe('road-img', () => {
  it('should render', async () => {
    const page = await newE2EPage();

    await page.setContent('<road-img src="https://medias-norauto.fr/fr_FR/desktop/homepage/encart_atelier/RDV-atelier-260x250-Version-bold.png" alt="glacières à partir de 27€95"></road-img>');

    const element = await page.find('road-img');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
