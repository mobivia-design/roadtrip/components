import { html } from 'lit';

export default {
  title: 'Media/Img',
  component: 'road-img',
  parameters: {
    actions: {
      handles: ['roadimgwillload', 'roadimgdidload', 'roaderror'],
    },
  },
};

export const Default = () => html`
  <road-grid>
  <road-row>
    <road-col class="col-6 col-md-3 mb-16">

      <road-img src="https://medias-norauto.fr/fr_FR/desktop/homepage/encart_atelier/RDV-atelier-260x250-Version-bold.png" alt="glacières à partir de 27€95"></road-img>

    </road-col>
    <road-col class="col-6 col-md-3 mb-16">

      <road-img src="https://medias-norauto.fr/fr_FR/desktop/homepage/encart_atelier/ma-revision-eco-260x250-2.png" alt="Vélo Anyway E100"></road-img>

    </road-col>
  </road-row>
</road-grid>
`;