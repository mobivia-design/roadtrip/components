import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';

export default {
  title: 'Listing/Rating',
  component: 'road-rating',
  argTypes: {
    size: {
      description: "The button size.",
      options: ['extra-small', 'small', 'medium'],
      control: {
        type: 'select',
      },
    },
    rate: {
      description: "Rate review between 0 and 5",
      control: {
        type: 'number',
        min: '0',
        max: '5',
      },
    },
    showreviews: {
      description: "Show reviews",
      control: 'boolean',
    },
    reviews: {
      description: "number of reviews",
      control: {
        type: 'number',
        min: '0',
      },
    },
    'reviews-text': {
      description: "Word display next to the number of reviews.",
      control: 'text',
    },
    url: {
      description: "Url.",
      control: 'text',
    },
    readonly: {
      description: "Read Only",
      control: 'boolean',
    },
  },
  args: {
    size: null,
  },
};

const Template = (args) => html`
<road-rating 
  rate="${ifDefined(args.rate)}" 
  reviews="${ifDefined(args.reviews)}" 
  reviews-text="${ifDefined(args['reviews-text'])}"
  size="${ifDefined(args.size)}"
  showreviews="${ifDefined(args.showreviews)}"
  readonly="${ifDefined(args.readonly)}"
  url="${ifDefined(args.url)}"
></road-rating>
`;

export const Playground = Template.bind({});
Playground.args = {
  rate: 3,
  reviews: 5,
  'reviews-text': 'reviews',
  showreviews:'true',
  url:'#'
};

export const Rating = Template.bind({});
Rating.args = {
  rate: 3.5,
  reviews: 4,
};

export const MaxRating = Template.bind({});
MaxRating.args = {
  rate: 5,
  reviews: 50,
};

export const LightRating = Template.bind({});
LightRating.args = {
  rate: 5,
  reviews: 50,
  showreviews:'false',
};

export const ReadOnly = Template.bind({});
ReadOnly.args = {
  rate: 5,
  reviews: 50,
  showreviews:'true',
  readonly:'true',
};
