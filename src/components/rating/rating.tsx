import { Component, Host, h, Prop } from '@stencil/core';
import { star, starHalfColor } from '../../../icons';

@Component({
  tag: 'road-rating',
  styleUrl: 'rating.css',
  shadow: true,
})
export class Rating {

  /**
    * The button size.
    */
   @Prop({ reflect: true }) size?: 'extra-small' | 'small' | 'medium' = 'medium';

  /**
   * Rate review between 0 and 5
   */
  @Prop() rate: number = 0;

  /**
 * Show reviews
 */
  @Prop() showreviews: boolean = true;

  /**
 * Read Only
 */
  @Prop() readonly: boolean = false;

  /**
   * number of reviews
   */
  @Prop() reviews: number = 0;

  /**
   * Word display next to the number of reviews.
   */
  @Prop() reviewsText: string = 'reviews';

    /**
   * Url.
   */
    @Prop() url: string = '#';

  render() {
    const rate = Math.floor(this.rate);
    const rateDecimale = this.rate - Math.floor(this.rate) > 0 ? 1 : 0;
    const sizeRatingStarsClass = this.size !== undefined ? `rating-stars rating-stars--${this.size}` : 'rating-stars';
    const sizeRatingNumberClass = this.size !== undefined ? `rating-number rating-number--${this.size}` : 'rating-number';
    const readOnly = this.readonly == true ? `readonly` : '';


    return (
      <Host>
        <div class={`${sizeRatingStarsClass}`}>
          {[...Array(rate)].map(() =>
            <road-icon icon={star} size="sm" color="warning"></road-icon>
          )}
          {[...Array(rateDecimale)].map(() =>
            <road-icon icon={starHalfColor} size="sm" class="rating-star"></road-icon>
          )}
          {[...Array(5 - rate - rateDecimale)].map(() =>
            <road-icon icon={star} size="sm" class="rating-star"></road-icon>
          )}
        </div>
        {this.readonly == false && this.showreviews && <a class={`${sizeRatingNumberClass}`} href={this.url}>({this.reviews}{this.reviewsText && ` ${this.reviewsText}`})</a>}
        {this.readonly == true && this.showreviews && <road-label class={`${sizeRatingNumberClass} ${readOnly}`}>({this.reviews}{this.reviewsText && ` ${this.reviewsText}`})</road-label>}

      </Host>
    );
  }
}
