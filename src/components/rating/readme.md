# road-rating



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                 | Type                                                | Default     |
| ------------- | -------------- | ------------------------------------------- | --------------------------------------------------- | ----------- |
| `rate`        | `rate`         | Rate review between 0 and 5                 | `number`                                            | `0`         |
| `readonly`    | `readonly`     | Read Only                                   | `boolean`                                           | `false`     |
| `reviews`     | `reviews`      | number of reviews                           | `number`                                            | `0`         |
| `reviewsText` | `reviews-text` | Word display next to the number of reviews. | `string`                                            | `'reviews'` |
| `showreviews` | `showreviews`  | Show reviews                                | `boolean`                                           | `true`      |
| `size`        | `size`         | The button size.                            | `"extra-small" \| "medium" \| "small" \| undefined` | `'medium'`  |
| `url`         | `url`          | Url.                                        | `string`                                            | `'#'`       |


## Dependencies

### Depends on

- [road-icon](../icon)
- [road-label](../label)

### Graph
```mermaid
graph TD;
  road-rating --> road-icon
  road-rating --> road-label
  style road-rating fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
