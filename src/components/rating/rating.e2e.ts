import { newE2EPage } from '@stencil/core/testing';

describe('road-rating', () => {
  let page: any;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-rating></road-rating>
      `,
    });
  });

  it('it should render with 0 reviews', async () => {
    const element = await page.find('road-rating');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('it should render with 3.5 rate rating and 5 reviews', async () => {
    const element = await page.find('road-rating');
    expect(element).toHaveClass('hydrated');

    element.setProperty('rate', 3.5);
    element.setProperty('rating', 5);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('it should render with 5 rate rating and 10 reviews', async () => {
    const element = await page.find('road-rating');
    expect(element).toHaveClass('hydrated');

    element.setProperty('rate', 5);
    element.setProperty('rating', 10);

    await page.waitForChanges();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
