# road-tab-bar



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                | Type                  | Default     |
| ------------- | -------------- | -------------------------- | --------------------- | ----------- |
| `selectedTab` | `selected-tab` | The selected tab component | `string \| undefined` | `undefined` |


## Slots

| Slot | Description                                                                     |
| ---- | ------------------------------------------------------------------------------- |
|      | Content of the segmentedButtonBar, it should be road-segmented-button elements. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
