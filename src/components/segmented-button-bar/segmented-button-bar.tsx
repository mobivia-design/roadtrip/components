import { Component, Element, Event, EventEmitter, Host, Prop, Watch, h } from '@stencil/core';
import '../../utils/polyfill';

/**
 * @slot  - Content of the segmentedButtonBar, it should be road-segmented-button elements.
 */

@Component({
  tag: 'road-segmented-button-bar',
  styleUrl: 'segmented-button-bar.css',
  shadow: true,
})
export class ButtonBar {

  @Element() el!: HTMLRoadSegmentedButtonBarElement;


  /**
   * The selected tab component
   */
  @Prop() selectedTab?: string;
  @Watch('selectedTab')
  selectedTabChanged() {
    if (this.selectedTab !== undefined) {
      this.roadsegmentedbuttonbarchanged.emit({
        tab: this.selectedTab,
      });
      this.roadSegmentedButtonBarChanged.emit({
        tab: this.selectedTab,
      });
    }
  }

  /** @internal */
  @Event() roadsegmentedbuttonbarchanged!: EventEmitter;
  /** @internal */
  @Event() roadSegmentedButtonBarChanged!: EventEmitter;

  componentWillLoad() {
    this.selectedTabChanged();
  }

  render() {
    return (
      <Host
        role="tablist"
      >
        <slot/>
      </Host>
    );
  }
}