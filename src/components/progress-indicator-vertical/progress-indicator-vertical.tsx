import { Component, Host, h } from '@stencil/core';

/**
 * @slot  - content of the stepper item, it should be road-vertical-stepper-item elements.
 * 
 * if the state of the step is completed add the class `completed` on the road-vertical-stepper-item
 * 
 * if the state of the step is in-progress add the class `in-progress` on the road-vertical-stepper-item
 * 
 * if the state of the step is current add the class `current` on the road-vertical-stepper-item
 * 
 * `<road-icon name="check-small" class="d-block" style="color: currentColor;"></road-icon>`
 */

@Component({
  tag: 'road-progress-indicator-vertical',
  styleUrl: 'progress-indicator-vertical.css',
  shadow: true,
})
export class ProgressIndicatorVertical {

     render() {

       return (
         <Host>
           <nav>
             <ul>
              <li>
                <slot/>
               </li>
             </ul>
           </nav>
         </Host>
       );
     }

}
