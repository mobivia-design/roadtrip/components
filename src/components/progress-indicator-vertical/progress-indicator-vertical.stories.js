import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Indicators/Progress Indicator Vertical',
  component: 'road-progress-indicator-vertical',
  subcomponents: {
    'road-progress-indicator-vertical-item': 'road-progress-indicator-vertical-item',
  },
  argTypes: {
    ' ': {
      description: "content of the stepper item, it should be road-vertical-stepper-item elements.\n\nif the state of the step is completed add the class `completed` on the road-vertical-stepper-item\n\nif the state of the step is in-progress add the class `in-progress` on the road-vertical-stepper-item\n\nif the state of the step is current add the class `current` on the road-vertical-stepper-item\n\n`<road-icon name=\"check-small\" class=\"d-block\" style=\"color: currentColor;\"></road-icon>`",
      control: 'text',
    },
  },
  args: {
    ' ': `<road-progress-indicator-vertical-item class="completed" tabindex="0">
    <a class="progress-indicator-vertical-link">
      <span class="progress-indicator-vertical-icon">
      <road-icon name="check-small" class="d-block" style="color: currentColor;" aria-hidden="true"></road-icon>
      </span>
      <span class="progress-indicator-vertical-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
      <span class="progress-indicator-vertical-title">
        <road-label>Label</road-label>
      </span>
      <div class="progress-indicator-vertical-description">Description</div>
    </div>
  </road-progress-indicator-vertical-item>
  <road-progress-indicator-vertical-item class="current" tabindex="0">
    <a class="progress-indicator-vertical-link">
      <span class="progress-indicator-vertical-icon">
      2
      </span>
      <span class="progress-indicator-vertical-line"></span>
    </a>
    <div class="progress-indicator-vertical-item-content">
      <span class="progress-indicator-vertical-title">
      <road-label>Label</road-label>
      </span>
      <div class="progress-indicator-vertical-description">Description</div>
    </div>
  </road-progress-indicator-vertical-item>
  <road-progress-indicator-vertical-item tabindex="0">
  <a class="progress-indicator-vertical-link">
    <span class="progress-indicator-vertical-icon">
      3
    </span>
    <span class="progress-indicator-vertical-line"></span>
  </a>
  <div class="progress-indicator-vertical-item-content">
    <span class="progress-indicator-vertical-title">
      <road-label>Label</road-label>
    </span>
    <div class="progress-indicator-vertical-description">Description</div>
    </div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item tabindex="0">
<a class="progress-indicator-vertical-link">
  <span class="progress-indicator-vertical-icon">
    4
  </span>
  <span class="progress-indicator-vertical-line"></span>
</a>
<div class="progress-indicator-vertical-item-content">
  <span class="progress-indicator-vertical-title">
    <road-label>Label</road-label>
  </span>
  <div class="progress-indicator-vertical-description">Description</div>
  </div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item tabindex="0">
<a class="progress-indicator-vertical-link">
  <span class="progress-indicator-vertical-icon">
    5
  </span>
  <span class="progress-indicator-vertical-line"></span>
</a>
<div class="progress-indicator-vertical-item-content">
  <span class="progress-indicator-vertical-title">
    <road-label>Label</road-label>
  </span>
  <div class="progress-indicator-vertical-description">Description</div>
  </div>
</road-progress-indicator-vertical-item>
`,
  },
};

export const Template = (args) => html`
  <road-progress-indicator-vertical color="${ifDefined(args.color)}" outline="${ifDefined(args.outline)}" light="${ifDefined(args.light)}" state-first-step="${ifDefined(args['state-first-step'])}" state-second-step="${ifDefined(args['state-second-step'])}" state-third-step="${ifDefined(args['state-third-step'])}">
  ${unsafeHTML(args[' '])}
      
  </road-progress-indicator-vertical>
`;

export const OneStepCompleted = Template.bind({});
OneStepCompleted.args = {
  ' ': `<road-progress-indicator-vertical-item class="completed" tabindex="0">
  <a class="progress-indicator-vertical-link">
    <span class="progress-indicator-vertical-icon">
    <road-icon name="check-small" class="d-block" style="color: currentColor;" aria-hidden="true"></road-icon>
    </span>
    <span class="progress-indicator-vertical-line"></span>
  </a>
  <div class="progress-indicator-vertical-item-content">
    <span class="progress-indicator-vertical-title">
      <road-label>Label</road-label>
    </span>
    <div class="progress-indicator-vertical-description">Description</div>
  </div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item class="current" tabindex="0">
  <a class="progress-indicator-vertical-link">
    <span class="progress-indicator-vertical-icon">
    2
    </span>
    <span class="progress-indicator-vertical-line"></span>
  </a>
  <div class="progress-indicator-vertical-item-content">
    <span class="progress-indicator-vertical-title">
    <road-label>Label</road-label>
    </span>
    <div class="progress-indicator-vertical-description">Description</div>
  </div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item tabindex="0">
<a class="progress-indicator-vertical-link">
  <span class="progress-indicator-vertical-icon">
    3
  </span>
  <span class="progress-indicator-vertical-line"></span>
</a>
<div class="progress-indicator-vertical-item-content">
  <span class="progress-indicator-vertical-title">
    <road-label>Label</road-label>
  </span>
  <div class="progress-indicator-vertical-description">Description</div>
  </div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item tabindex="0">
<a class="progress-indicator-vertical-link">
<span class="progress-indicator-vertical-icon">
  4
</span>
<span class="progress-indicator-vertical-line"></span>
</a>
<div class="progress-indicator-vertical-item-content">
<span class="progress-indicator-vertical-title">
  <road-label>Label</road-label>
</span>
<div class="progress-indicator-vertical-description">Description</div>
</div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item tabindex="0">
<a class="progress-indicator-vertical-link">
<span class="progress-indicator-vertical-icon">
  5
</span>
<span class="progress-indicator-vertical-line"></span>
</a>
<div class="progress-indicator-vertical-item-content">
<span class="progress-indicator-vertical-title">
  <road-label>Label</road-label>
</span>
<div class="progress-indicator-vertical-description">Description</div>
</div>
</road-progress-indicator-vertical-item>`,
};

export const SubSteps = Template.bind({});
SubSteps.args = {
  ' ': `<road-progress-indicator-vertical-item class="completed" tabindex="0">
  <a class="progress-indicator-vertical-link">
    <span class="progress-indicator-vertical-icon">
    <road-icon name="check-small" class="d-block" style="color: currentColor;" aria-hidden="true"></road-icon>
    </span>
    <span class="progress-indicator-vertical-line"></span>
  </a>
  <div class="progress-indicator-vertical-item-content">
    <span class="progress-indicator-vertical-title">
      <road-label>Label</road-label>
    </span>
    <div class="progress-indicator-vertical-description">Description</div>
  </div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item class="completed" tabindex="0">
  <a class="progress-indicator-vertical-link">
    <span class="progress-indicator-vertical-icon">
    <road-icon name="check-small" class="d-block" style="color: currentColor;" aria-hidden="true"></road-icon>
    </span>
    <span class="progress-indicator-vertical-line"></span>
  </a>
  <div class="progress-indicator-vertical-item-content">
    <span class="progress-indicator-vertical-title">
    <road-label>Label</road-label>
    </span>
    <div class="progress-indicator-vertical-description">Description</div>
  </div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item class="completed" tabindex="0">
<a class="progress-indicator-vertical-substep-link">
  <span class="progress-indicator-vertical-substep-beforeline"></span>
  <span class="progress-indicator-vertical-substep-icon"></span>
  <span class="progress-indicator-vertical-substep-line"></span>
</a>
<div class="progress-indicator-vertical-item-content">
  <span class="progress-indicator-vertical-title">
    <road-label>Label</road-label>
  </span>
  <div class="progress-indicator-vertical-description">Description</div>
</div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item class="current" tabindex="0">
<a class="progress-indicator-vertical-substep-link">
  <span class="progress-indicator-vertical-substep-beforeline"></span>
  <span class="progress-indicator-vertical-substep-icon"></span>
  <span class="progress-indicator-vertical-substep-line"></span>
</a>
<div class="progress-indicator-vertical-item-content">
  <span class="progress-indicator-vertical-title">
    <road-label>Label</road-label>
  </span>
  <div class="progress-indicator-vertical-description">Description</div>
</div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item tabindex="0">
<a class="progress-indicator-vertical-link">
<span class="progress-indicator-vertical-icon">
  3
</span>
<span class="progress-indicator-vertical-line"></span>
</a>
<div class="progress-indicator-vertical-item-content">
<span class="progress-indicator-vertical-title">
  <road-label>Label</road-label>
</span>
<div class="progress-indicator-vertical-description">Description</div>
</div>
</road-progress-indicator-vertical-item>
<road-progress-indicator-vertical-item tabindex="0">
<a class="progress-indicator-vertical-link">
<span class="progress-indicator-vertical-icon">
  4
</span>
<span class="progress-indicator-vertical-line"></span>
</a>
<div class="progress-indicator-vertical-item-content">
<span class="progress-indicator-vertical-title">
  <road-label>Label</road-label>
</span>
<div class="progress-indicator-vertical-description">Description</div>
</div>
</road-progress-indicator-vertical-item>`,
};