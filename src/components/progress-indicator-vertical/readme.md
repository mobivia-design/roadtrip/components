# road-spinner



<!-- Auto Generated Below -->


## Slots

| Slot | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ---- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|      | content of the stepper item, it should be road-vertical-stepper-item elements.  if the state of the step is completed add the class `completed` on the road-vertical-stepper-item  if the state of the step is in-progress add the class `in-progress` on the road-vertical-stepper-item  if the state of the step is current add the class `current` on the road-vertical-stepper-item  `<road-icon name="check-small" class="d-block" style="color: currentColor;"></road-icon>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
