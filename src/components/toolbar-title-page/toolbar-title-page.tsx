import { Component, h } from '@stencil/core';

/**
 * @slot - Name of the app.
 */

@Component({
  tag: 'road-toolbar-title-page',
  styleUrl: 'toolbar-title-page.css',
  shadow: true,
})
export class ToolbarAppName {

  render() {
    return (
      <div class="toolbar-title-page">
        <slot/>
      </div>
    );
  }

}
