import { Component, Host, Prop, h } from '@stencil/core';
import './../../utils/polyfill';

/**
 * @slot - Content of the tag.
 */

@Component({
  tag: 'road-tag',
  styleUrl: 'tag.css',
  shadow: true,
})
export class Tag {
  /**
   * The color to use from your application's color palette.
   */
  @Prop() color?: 'grey' | 'yellow' | 'red' | 'violet' | 'blue' | 'green'= 'grey';

    /**
   * Set to `true` for a contrast tag, for example on a gryy surface
   */
    @Prop() contrast: boolean = false;

  render() {

    const contrastClass = this.contrast ? `tag-${this.color} tag-${this.color}-contrast` : `tag-${this.color}`;

    return (
      <Host
        class={`${contrastClass}`}
      >
        <slot/>
      </Host>
    );
  }
}