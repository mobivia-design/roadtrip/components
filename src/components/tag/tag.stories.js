import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Indicators/Tag',
  component: 'road-tag',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
  },
  argTypes: {
    ' ': {
      description: "Content of the tag.",
      control: 'text',
    },
    color: {
      description: "The color to use from your application's color palette.",
      options: ['grey', 'yellow', 'red', 'violet', 'blue', 'green'],
      control: {
        type: 'select',
      },
    },
    contrast: {
      description: "Set to `true` for a contrast tag, for example on a gryy surface",
      control: 'boolean',
    },
  },
  args: {
    ' ': `Label`,
  },
};

const Template = (args) => html`
<road-tag
  color="${ifDefined(args.color)}" contrast="${ifDefined(args.contrast)}"
>
  ${unsafeHTML(args[' '])}
</road-tag>
`;

export const Color = Template.bind({});
Color.args = {
  color: 'grey',
  ' ': `Reset`,
};

export const Contrast = Template.bind({});
Contrast.args = {
  color: 'grey',
  contrast: true,
};