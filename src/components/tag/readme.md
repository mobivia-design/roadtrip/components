# ion-chip



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description                                                     | Type                                                                        | Default  |
| ---------- | ---------- | --------------------------------------------------------------- | --------------------------------------------------------------------------- | -------- |
| `color`    | `color`    | The color to use from your application's color palette.         | `"blue" \| "green" \| "grey" \| "red" \| "violet" \| "yellow" \| undefined` | `'grey'` |
| `contrast` | `contrast` | Set to `true` for a contrast tag, for example on a gryy surface | `boolean`                                                                   | `false`  |


## Slots

| Slot | Description         |
| ---- | ------------------- |
|      | Content of the tag. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
