# phone-number-input



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                                                                                                | Type                  | Default     |
| -------------- | --------------- | ------------------------------------------------------------------------------------------------------------------------------------------ | --------------------- | ----------- |
| `codeLabel`    | `code-label`    | Country selection placeHolder                                                                                                              | `string`              | `''`        |
| `countryCode`  | `country-code`  | Selected country code                                                                                                                      | `string \| undefined` | `undefined` |
| `countryData`  | --              | List of countries displayed in the country selection. Countries will be automatically sorted by alphabetical order (see format in example) | `CountryType[]`       | `[]`        |
| `disabled`     | `disabled`      | Disable both fields                                                                                                                        | `boolean`             | `false`     |
| `errorMessage` | `error-message` | Error message (displayed only if ther is a message)                                                                                        | `string`              | `''`        |
| `language`     | `language`      | Used to display countries with the right language                                                                                          | `string`              | `'fr'`      |
| `phoneLabel`   | `phone-label`   | Phone input placeHolder                                                                                                                    | `string`              | `''`        |
| `phoneValue`   | `phone-value`   | Value displayed in the phone input                                                                                                         | `string \| undefined` | `undefined` |
| `required`     | `required`      | Add a star in the phone input                                                                                                              | `boolean`             | `false`     |


## Events

| Event                  | Description         | Type                                                                                                                                                         |
| ---------------------- | ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `roadPhoneNumberInput` | Computed phone data | `CustomEvent<{ isError: boolean; phone: { numberType: string; countryCode: string; nationalNumber: string; code: string; internationalNumber: string; }; }>` |


## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-phone-number-input --> road-icon
  style road-phone-number-input fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
