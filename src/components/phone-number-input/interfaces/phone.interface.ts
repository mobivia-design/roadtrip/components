export interface PhoneReturnType {
  numberType: string;
  countryCode: string;
  nationalNumber: string;
  code: string;
  internationalNumber: string;
}

export interface ReturnObject {
  isError: boolean;
  phone: PhoneReturnType;
}
