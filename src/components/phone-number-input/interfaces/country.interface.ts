export type CountryType = {
  translations: { [key: string]: string },
  countryCode: string,
  phoneCode: string
};

export interface CountryOption {
  value: string;
  label: string;
  selected: boolean;
  disabled: boolean;
}
