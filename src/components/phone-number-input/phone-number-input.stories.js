import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';


export default {
  title: 'Forms/Phone Number Input',
  component: 'road-phone-number-input',
  argTypes: {
    countryData: { control: 'object' },
    language: {
      description: "Used to display countries with the right language",
      control: {
        type: 'select',
        options: ['en', 'fr', 'es', 'pt', 'it', 'nl', 'de']
      },
      table: {
        defaultValue: { summary: 'fr' },
      },
    },
    required: {
      description: "Add a star in the phone input",
      control: 'boolean',
    },
    disabled: {
      description: "Disable both fields",
      control: 'boolean',
    },
    'country-code': {
      description: "Selected country code",
      control: 'text',
    },
    'code-label': {
      description: "Country selection placeHolder",
      control: 'text',
      table: {
        defaultValue: { summary: '' },
      },
    },
    'phone-label': {
      description: "Phone input placeHolder",
      control: 'text',
    },
    'phone-value': {
      description: "Value displayed in the phone input",
      control: 'text',
    },
    'error-message': {
      description: "Error message (displayed only if ther is a message)",
      control: 'text',
    },
  },
  args: {
    required: null,
    countryData: [
      {
        "translations": {
          "en": "France",
          "fr": "France",
          "es": "Francia",
          "de": "Frankreich",
          "it": "Francia",
          "pt": "França",
          "nl": "Frankrijk"
        },
        "countryCode": "FR",
        "phoneCode": "33"
      },
      {
        "translations": {
          "fr": "Saint-Martin",
          "en": "Saint Martin (French part)",
          "es": "San Martín",
          "pt": "São Martinho",
          "it": "Saint-Martin",
          "nl": "Saint-Martin",
          "de": "Saint-Martin (französischer Teil)"
        },
        "countryCode": "MF",
        "phoneCode": "590"
      },
      {
        "translations": {
          "fr": "Mayotte",
          "en": "Mayotte",
          "es": "Mayotte",
          "pt": "Maiote",
          "it": "Mayotte",
          "nl": "Mayotte",
          "de": "Mayotte"
        },
        "countryCode": "YT",
        "phoneCode": "262"
      },
      {
        "translations": {
          "fr": "Saint-Barthélemy",
          "en": "Saint Barthélemy",
          "es": "San Bartolomé",
          "pt": "São Bartolomeu",
          "it": "Saint-Barthélemy",
          "nl": "Saint-Barthélemy",
          "de": "Saint-Barthélemy"
        },
        "countryCode": "BL",
        "phoneCode": "590"
      },
      {
        "translations": {
          "fr": "La Réunion",
          "en": "Réunion",
          "es": "Reunión",
          "pt": "Reunião",
          "it": "La Riunione",
          "nl": "Réunion",
          "de": "Réunion"
        },
        "countryCode": "RE",
        "phoneCode": "262"
      },
      {
        "translations": {
          "fr": "Guadeloupe",
          "en": "Guadeloupe",
          "es": "Guadalupe",
          "pt": "Guadalupe",
          "it": "Guadalupa",
          "nl": "Guadeloupe",
          "de": "Guadeloupe"
        },
        "countryCode": "GP",
        "phoneCode": "590"
      },
      {
        "translations": {
          "en": "Austria",
          "fr": "Autriche",
          "es": "Austria",
          "de": "Österreich",
          "it": "Austria",
          "pt": "Áustria",
          "nl": "Oostenrijk"
        },
        "countryCode": "AT",
        "phoneCode": "43"
      },
      {
        "translations": {
          "en": "Belgium",
          "fr": "Belgique",
          "es": "Bélgica",
          "de": "Belgien",
          "it": "Belgio",
          "pt": "Bélgica",
          "nl": "België"
        },
        "countryCode": "BE",
        "phoneCode": "32"
      },
      {
        "translations": {
          "en": "Bulgaria",
          "fr": "Bulgarie",
          "es": "Bulgaria",
          "de": "Bulgarien",
          "it": "Bulgaria",
          "pt": "Bulgária",
          "nl": "Bulgarije"
        },
        "countryCode": "BG",
        "phoneCode": "359"
      },
      {
        "translations": {
          "en": "Croatia",
          "fr": "Croatie",
          "es": "Croacia",
          "de": "Kroatien",
          "it": "Croazia",
          "pt": "Croácia",
          "nl": "Kroatië"
        },
        "countryCode": "HR",
        "phoneCode": "385"
      },
      {
        "translations": {
          "en": "Finland",
          "fr": "Finlande",
          "es": "Finlandia",
          "de": "Finnland",
          "it": "Finlandia",
          "pt": "Finlândia",
          "nl": "Finland"
        },
        "countryCode": "FI",
        "phoneCode": "358"
      },
      {
        "translations": {
          "en": "Germany",
          "fr": "Allemagne",
          "es": "Alemania",
          "de": "Deutschland",
          "it": "Germania",
          "pt": "Alemanha",
          "nl": "Duitsland"
        },
        "countryCode": "DE",
        "phoneCode": "49"
      },
      {
        "translations": {
          "en": "Greece",
          "fr": "Grèce",
          "es": "Grecia",
          "de": "Griechenland",
          "it": "Grecia",
          "pt": "Grécia",
          "nl": "Griekenland"
        },
        "countryCode": "GR",
        "phoneCode": "30"
      },
      {
        "translations": {
          "en": "Holy See",
          "fr": "Vatican",
          "es": "Vaticano",
          "de": "Vatikanstadt",
          "it": "Città del Vaticano",
          "pt": "Vaticano",
          "nl": "Vaticaanstad"
        },
        "countryCode": "VA",
        "phoneCode": "379"
      },
      {
        "translations": {
          "en": "Hungary",
          "fr": "Hongrie",
          "es": "Hungría",
          "de": "Ungarn",
          "it": "Ungheria",
          "pt": "Hungria",
          "nl": "Hongarije"
        },
        "countryCode": "HU",
        "phoneCode": "36"
      },
      {
        "translations": {
          "en": "Iceland",
          "fr": "Islande",
          "es": "Islandia",
          "de": "Island",
          "it": "Islanda",
          "pt": "Islândia",
          "nl": "IJsland"
        },
        "countryCode": "IS",
        "phoneCode": "354"
      },
      {
        "translations": {
          "en": "Ireland",
          "fr": "Irlande",
          "es": "Irlanda",
          "de": "Irland",
          "it": "Irlanda",
          "pt": "Irlanda",
          "nl": "Ierland"
        },
        "countryCode": "IE",
        "phoneCode": "353"
      },
      {
        "translations": {
          "en": "Italy",
          "fr": "Italie",
          "es": "Italia",
          "de": "Italien",
          "it": "Italia",
          "pt": "Itália",
          "nl": "Italië"
        },
        "countryCode": "IT",
        "phoneCode": "39"
      },
      {
        "translations": {
          "en": "Latvia",
          "fr": "Lettonie",
          "es": "Letonia",
          "de": "Lettland",
          "it": "Lettonia",
          "pt": "Letônia",
          "nl": "Letland"
        },
        "countryCode": "LV",
        "phoneCode": "371"
      },
      {
        "translations": {
          "en": "Lithuania",
          "fr": "Lituanie",
          "es": "Lituania",
          "de": "Litauen",
          "it": "Lituania",
          "pt": "Lituânia",
          "nl": "Litouwen"
        },
        "countryCode": "LT",
        "phoneCode": "370"
      },
      {
        "translations": {
          "en": "Luxembourg",
          "fr": "Luxembourg",
          "es": "Luxemburgo",
          "de": "Luxemburg",
          "it": "Lussemburgo",
          "pt": "Luxemburgo",
          "nl": "Luxemburg"
        },
        "countryCode": "LU",
        "phoneCode": "352"
      },
      {
        "translations": {
          "en": "Moldova, Republic of",
          "fr": "Moldavie",
          "es": "Moldavia",
          "de": "Moldau",
          "it": "Moldavia",
          "pt": "Moldávia",
          "nl": "Moldavië"
        },
        "countryCode": "MD",
        "phoneCode": "373"
      },
      {
        "translations": {
          "en": "Monaco",
          "fr": "Monaco",
          "es": "Mónaco",
          "de": "Monaco",
          "it": "Monaco",
          "pt": "Mónaco",
          "nl": "Monaco"
        },
        "countryCode": "MC",
        "phoneCode": "377"
      },
      {
        "translations": {
          "en": "Montenegro",
          "fr": "Monténégro",
          "es": "Montenegro",
          "de": "Montenegro",
          "it": "Montenegro",
          "pt": "Montenegro",
          "nl": "Montenegro"
        },
        "countryCode": "ME",
        "phoneCode": "382"
      },
      {
        "translations": {
          "en": "Netherlands",
          "fr": "Pays-Bas",
          "es": "Países Bajos",
          "de": "Niederlande",
          "it": "Paesi Bassi",
          "pt": "Países Baixos",
          "nl": "Nederland"
        },
        "countryCode": "NL",
        "phoneCode": "31"
      },
      {
        "translations": {
          "en": "Norway",
          "fr": "Norvège",
          "es": "Noruega",
          "de": "Norwegen",
          "it": "Norvegia",
          "pt": "Noruega",
          "nl": "Noorwegen"
        },
        "countryCode": "NO",
        "phoneCode": "47"
      },
      {
        "translations": {
          "en": "Poland",
          "fr": "Pologne",
          "es": "Polonia",
          "de": "Polen",
          "it": "Polonia",
          "pt": "Polónia",
          "nl": "Polen"
        },
        "countryCode": "PL",
        "phoneCode": "48"
      },
      {
        "translations": {
          "en": "Portugal",
          "fr": "Portugal",
          "es": "Portugal",
          "de": "Portugal",
          "it": "Portogallo",
          "pt": "Portugal",
          "nl": "Portugal"
        },
        "countryCode": "PT",
        "phoneCode": "351"
      },
      {
        "translations": {
          "en": "Romania",
          "fr": "Roumanie",
          "es": "Rumania",
          "de": "Rumänien",
          "it": "Romania",
          "pt": "Roménia",
          "nl": "Roemenië"
        },
        "countryCode": "RO",
        "phoneCode": "40"
      },
      {
        "translations": {
          "en": "Russian Federation",
          "fr": "Russie",
          "es": "Rusia",
          "de": "Russland",
          "it": "Russia",
          "pt": "Rússia",
          "nl": "Rusland"
        },
        "countryCode": "RU",
        "phoneCode": "7"
      },
      {
        "translations": {
          "en": "San Marino",
          "fr": "Saint-Marin",
          "es": "San Marino",
          "de": "San Marino",
          "it": "San Marino",
          "pt": "San Marino",
          "nl": "San Marino"
        },
        "countryCode": "SM",
        "phoneCode": "378"
      },
      {
        "translations": {
          "en": "Serbia",
          "fr": "Serbie",
          "es": "Serbia",
          "de": "Serbien",
          "it": "Serbia",
          "pt": "Sérvia",
          "nl": "Servië"
        },
        "countryCode": "RS",
        "phoneCode": "381"
      },
      {
        "translations": {
          "en": "Slovakia",
          "fr": "Slovaquie",
          "es": "Eslovaquia",
          "de": "Slowakei",
          "it": "Slovacchia",
          "pt": "Eslováquia",
          "nl": "Slowakije"
        },
        "countryCode": "SK",
        "phoneCode": "421"
      },
      {
        "translations": {
          "en": "Slovenia",
          "fr": "Slovénie",
          "es": "Eslovenia",
          "de": "Slowenien",
          "it": "Slovenia",
          "pt": "Eslovênia",
          "nl": "Slovenië"
        },
        "countryCode": "SI",
        "phoneCode": "386"
      },
      {
        "translations": {
          "en": "Spain",
          "fr": "Espagne",
          "es": "España",
          "de": "Spanien",
          "it": "Spagna",
          "pt": "Espanha",
          "nl": "Spanje"
        },
        "countryCode": "ES",
        "phoneCode": "34"
      },
      {
        "translations": {
          "en": "Sweden",
          "fr": "Suède",
          "es": "Suecia",
          "de": "Schweden",
          "it": "Svezia",
          "pt": "Suécia",
          "nl": "Zweden"
        },
        "countryCode": "SE",
        "phoneCode": "46"
      },
      {
        "translations": {
          "en": "Switzerland",
          "fr": "Suisse",
          "es": "Suiza",
          "de": "Schweiz",
          "it": "Svizzera",
          "pt": "Suíça",
          "nl": "Zwitserland"
        },
        "countryCode": "CH",
        "phoneCode": "41"
      },
      {
        "translations": {
          "en": "Ukraine",
          "fr": "Ukraine",
          "es": "Ucrania",
          "de": "Ukraine",
          "it": "Ucraina",
          "pt": "Ucrânia",
          "nl": "Oekraïne"
        },
        "countryCode": "UA",
        "phoneCode": "380"
      },
      {
        "translations": {
          "en": "United Kingdom of Great Britain and Northern Ireland",
          "fr": "Royaume-Uni",
          "es": "Reino Unido",
          "de": "Vereinigtes Königreich",
          "it": "Regno Unito",
          "pt": "Reino Unido",
          "nl": "Verenigd Koninkrijk"
        },
        "countryCode": "GB",
        "phoneCode": "44"
      }
    ],
    'code-label': 'Indicatif',
    'country-code': 'FR',
    language: 'fr',
    'phone-label': 'Phone Number',
    'phone-value': '0645362719',
    'error-message': '',
    disabled: false,
  },
};

const Template = (args) => html`
  <road-phone-number-input
    required="${ifDefined(args.required)}"
    disabled="${ifDefined(args.disabled)}"
    .countryData=${args.countryData}
    language=${args.language}
    code-label=${args['code-label']}
    phone-label=${args['phone-label']}
    country-code=${args['country-code']}
    phone-value=${args['phone-value']}
    error-message=${args['error-message']}
  >
  </road-phone-number-input>
`;

export const Playground = Template.bind({});
