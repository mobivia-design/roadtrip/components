import { Component, Prop, Element, Host, State, h, Event, EventEmitter } from '@stencil/core';
import { PhoneNumberFormat, PhoneNumberType, PhoneNumberUtil } from 'google-libphonenumber';
import { CountryOption, CountryType, PhoneReturnType, ReturnObject } from './interfaces';
import { forbiddenChars } from './constants';

let selectIds = 0;

@Component({
  tag: 'road-phone-number-input',
  styleUrl: 'phone-number-input.css',
  scoped: true,
})
export class RoadPhoneNumberInput {

  /** Computed phone data */
  @Event() roadPhoneNumberInput!: EventEmitter<{
    isError: boolean;
    phone: {
      numberType: string;
      countryCode: string;
      nationalNumber: string;
      code: string;
      internationalNumber: string;
    };
  }>;

  @Element() el!: HTMLRoadPhoneNumberInputElement;

  /** Disable both fields */
  @Prop() disabled: boolean = false;

  /** List of countries displayed in the country selection. Countries will be automatically sorted by alphabetical order (see format in example) */
  @Prop() countryData: CountryType[] = [];

  /** Used to display countries with the right language */
  @Prop() language: string = 'fr';

  /** Country selection placeHolder */
  @Prop() codeLabel: string = '';

  /** Phone input placeHolder */
  @Prop() phoneLabel: string = '';

  /** Value displayed in the phone input */
  @Prop() phoneValue?: string;

  /** Selected country code */
  @Prop() countryCode?: string;

  /** Error message (displayed only if ther is a message) */
  @Prop() errorMessage: string = '';

  /** Add a star in the phone input */
  @Prop() required = false;

  @State() selectedCountry: string = this.countryData[0].countryCode;
  @State() selectedCountryCode: string = this.countryData[0].phoneCode;
  @State() phoneNumber: string | null = '';
  @State() countryOptions: CountryOption[] = [];
  @State() returnObject: ReturnObject = {
    isError: false,
    phone: this.resetPhoneUtilData(),
  };

  // On component load
  componentWillLoad() {
    let fromSelect = false;

    if (this.phoneValue) {
      this.phoneNumber = this.phoneValue;
    }

    if (this.countryCode) {
      this.selectedCountry = this.countryCode;
      if (this.getPhoneCodeFromCountryCode(this.countryCode)) {
        this.updateSelectedCountryCode(this.getPhoneCodeFromCountryCode(this.countryCode)!);
        fromSelect = true;
      }
    }

    this.updateCountryOptions();
    this.createAndDispatchPhoneData(fromSelect);
  }

  // On user input
  handleInput(event: Event) {
    const input = event.target as HTMLInputElement;
    this.phoneNumber = input.value;
    this.createAndDispatchPhoneData();
    }


  // On user key down
  handleKeyDown(event: KeyboardEvent) {
    // checks if the forbidden characters's list contains the pressed key
    if (forbiddenChars.includes(event.key)) {
      event.preventDefault();
    }
  }

  // On user select
  handleSelect(event: Event) {
    const select = event.target as HTMLSelectElement | null;
    if (select) {
      this.updateSelectedCountry(select.value);
      this.updateSelectedCountryCode(this.getPhoneCodeFromCountryCode(this.selectedCountry)!);
    }
    this.createAndDispatchPhoneData(true);
    this.updateCountryOptions();
  }

  createAndDispatchPhoneData(fromSelect?: boolean) {
    // Init phone util
    const phoneUtil = PhoneNumberUtil.getInstance();
    const PNF = PhoneNumberFormat;

    // Reset the return object data
    this.returnObject.phone = this.resetPhoneUtilData();

    const tmpCountry = this.selectedCountry.length ? this.selectedCountry : this.countryData[0].countryCode;

    try {
      const parsedPhoneNumber = phoneUtil.parse(this.phoneNumber ?? '', tmpCountry);
      this.returnObject.isError = !phoneUtil.isValidNumber(parsedPhoneNumber) && !this.isEmptyPhoneNumber();
      this.returnObject.phone.nationalNumber = phoneUtil.format(parsedPhoneNumber, PNF.NATIONAL);
      this.returnObject.phone.internationalNumber = phoneUtil.format(parsedPhoneNumber, PNF.INTERNATIONAL);

      // Set the country code values depending on the event origin
      if (phoneUtil.getRegionCodeForNumber(parsedPhoneNumber)) {
        if (fromSelect) {
          this.returnObject.isError = !(
            phoneUtil.isValidNumber(parsedPhoneNumber) &&
            phoneUtil.isValidNumberForRegion(parsedPhoneNumber, this.selectedCountry) &&
            !this.isEmptyPhoneNumber()
          );
        } else {
          this.updateSelectedCountryCode(parsedPhoneNumber.getCountryCode()!.toString());
          this.updateSelectedCountry(phoneUtil.getRegionCodeForNumber(parsedPhoneNumber)!);

          this.returnObject.phone.countryCode = this.selectedCountry;
          this.returnObject.phone.code = this.selectedCountryCode;
        }
        this.updateCountryOptions();
      }

      const phoneType = Object.entries(PhoneNumberType).find(entry => entry[1] === phoneUtil.getNumberType(parsedPhoneNumber))!;
      this.returnObject.phone.numberType = phoneType[0];
    } catch {} finally {
      this.roadPhoneNumberInput.emit(this.returnObject);
    }
  }

  // utils

  updateSelectedCountryCode(countryCode: string) {
    this.selectedCountryCode = countryCode;
  }

  updateSelectedCountry(value: string) {
    this.selectedCountry = value;
  }

  resetPhoneUtilData(): PhoneReturnType {
    return {
      numberType: 'UNKNOWN',
      countryCode: this.selectedCountry,
      code: this.selectedCountryCode,
      nationalNumber: this.phoneNumber ?? '',
      internationalNumber: this.phoneNumber ?? '',
    };
  }

  isEmptyPhoneNumber(): boolean {
    return !this.phoneNumber?.length;
  }

  getPhoneCodeFromCountryCode(countryCode: string): string | undefined {
    return this.countryData.find(country => country.countryCode === countryCode)?.phoneCode;
  }

  updateCountryOptions() {
    const validLanguage = this.countryData[0].translations[this.language] ? this.language : 'fr';
    this.countryOptions = [
      ...this.countryData
        .sort((a, b) => a.translations[validLanguage].localeCompare(b.translations[validLanguage]))
        .map(country => ({
          value: country.countryCode,
          label: `${country.translations[validLanguage]} (+${country.phoneCode})`,
          selected: country.countryCode === this.selectedCountry,
          disabled: false,
        })
      ),
      {
        value: '',
        label: '--',
        selected: this.countryData.every((country) => country.countryCode !== this.selectedCountry),
        disabled: true,
      },
    ];
  }

  private hasValue(): boolean {
    return this.phoneNumber !== '' && this.phoneNumber !== null;
  }

  render() {
    const id = selectIds++;
    const selectId = `road-phone-number-select-${id}`;
    const inputId = `road-phone-number-input-${id}`;
    const labelId = selectId + '-label';
    const valueId = selectId + '-value';
    const isInvalidClass = this.errorMessage.length ? 'is-invalid' : '';
    const isErrorClass = this.errorMessage.length ? 'is-error' : '';
    const hasValueClass = this.hasValue() ? 'has-value' : '';
    const sizes = 'xl';

    return (
      <Host>
        <div class='phone-number-inputs-container'>
          <div class={`road-phone-input-select ${isErrorClass} ${isInvalidClass}`} slot="prepend">
            <select
              id={selectId}
              class={`form-select-area has-value`}
              aria-disabled={this.disabled ? 'true' : null}
              disabled={this.disabled}
              onChange={(event) => this.handleSelect(event)}
              aria-label={this.codeLabel}
              tabIndex={this.disabled ? -1 : 0}
            >
              {this.countryOptions && this.countryOptions.map(option => (
                <option value={option.value} disabled={option.disabled} selected={option.selected}>{option.label}</option>
              ))}
            </select>
            <label class="form-select-area-label" id={labelId} htmlFor={selectId}>{this.codeLabel}</label>
            <label class="form-select-area-value" id={valueId} htmlFor={selectId}>+{this.selectedCountryCode}</label>
          </div>
          <input
            type='tel'
            id={inputId}
            class={`input-${sizes} phone-number-input-field ${hasValueClass} ${isErrorClass}`}
            value={this.phoneValue}
            required={this.required}
            tabIndex={0}
            onInput={(event: InputEvent) => this.handleInput(event)}
            onKeyDown={(event: KeyboardEvent) => this.handleKeyDown(event)}
          />
          <label
            class="phone-number-input-label"
            id={labelId}
            htmlFor={inputId}
          >
            {this.phoneLabel}
          </label>
        </div>
        {
          this.errorMessage
          && this.errorMessage !== ''
          && <p class="invalid-feedback">
            <road-icon
              slot="start"
              color='danger'
              name="alert-error-solid"
              aria-hidden="true"
              size="sm"
            />
            {this.errorMessage}
          </p>
        }
      </Host>
    );
  }
}
