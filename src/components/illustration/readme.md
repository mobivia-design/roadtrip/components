# road-icon



<!-- Auto Generated Below -->


## Usage

### React

```javascript
import React from 'react';
import { apps, noResult } from "@roadtrip/components/illustrations";

const MyComponent: React.FC = () => {
  return (
    <>
      <road-illustration icon={apps}></road-illustration>
      <road-illustration icon={noResult}></road-illustration>
    </>
  );
};

export default MyComponent;
```


### Vue

```html
<template>
  <road-illustration icon="apps"></road-illustration>
  <road-illustration icon="no-result"></road-illustration>
</template>

<script>
  import { apps, noResult } from "@roadtrip/components/illustrations";
  import { addIcons } from "@roadtrip/illustrations";
  addIcons({
    apps,
    "no-result": noResult
  });
</script>
```



## Properties

| Property       | Attribute      | Description                                                                                                                                                                                   | Type                                        | Default     |
| -------------- | -------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------- | ----------- |
| `ariaHidden`   | `aria-hidden`  | Set the illustration to hidden, respectively `true`, to remove it from the accessibility tree.                                                                                                | `string \| undefined`                       | `undefined` |
| `ariaLabel`    | `aria-label`   | Specifies the label to use for accessibility. Defaults to the illustration name.                                                                                                              | `string \| undefined`                       | `undefined` |
| `illustration` | `illustration` | A combination of both `name` and `src`. If a `src` url is detected it will set the `src` property. Otherwise it assumes it's a built-in named SVG and set the `name` property.                | `any`                                       | `undefined` |
| `lazy`         | `lazy`         | If enabled, road-illustration will be loaded lazily when it's visible in the viewport. Default, `false`.                                                                                      | `boolean`                                   | `false`     |
| `name`         | `name`         | Specifies which illustration to use from the built-in set of illustrations.                                                                                                                   | `string \| undefined`                       | `undefined` |
| `rotate`       | `rotate`       | The rotation of the illustration. Available options are: `"90"`, `"180"`, `"270"`.                                                                                                            | `"180" \| "270" \| "90" \| undefined`       | `undefined` |
| `sanitize`     | `sanitize`     | When set to `false`, SVG content that is HTTP fetched will not be checked if the response SVG content has any `<script>` elements, or any attributes that start with `on`, such as `onclick`. | `boolean`                                   | `true`      |
| `size`         | `size`         | The size of the illustration. Available options are: `"sm"`, `"md"`, `"lg"`, `"2x"`.                                                                                                          | `"2x" \| "lg" \| "md" \| "sm" \| undefined` | `"2x"`      |
| `src`          | `src`          | Specifies the exact `src` of an SVG file to use.                                                                                                                                              | `string \| undefined`                       | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
