import { getAssetPath } from '@stencil/core';
import { Illustration } from './illustration';


let CACHED_MAP: Map<string, string>;

export const getIllustrationMap = (): Map<string, string> => {
  if (typeof window === 'undefined') {
    return new Map();
  } else {
    if (!CACHED_MAP) {
      const win = window as any;
      win.Ionicons = win.Ionicons || {};
      CACHED_MAP = win.Ionicons.map = win.Ionicons.map || new Map();
    }
    return CACHED_MAP;
  }
};

export const addIllustrations = (illustrations: { [name: string]: string; }) => {
  const map = getIllustrationMap();
  Object.keys(illustrations).forEach(name => map.set(name, illustrations[name]));
};


export const getUrl = (i: Illustration) => {
  let url = getSrc(i.src);
  if (url) {
    return url;
  }

  url = getName(i.name, i.illustration);
  if (url) {
    return getNamedUrl(url);
  }

  if (i.illustration) {
    url = getSrc(i.illustration);
    if (url) {
      return url;
    }
  }

  return null;
};


const getNamedUrl = (illustrationName: string) => {
  const url = getIllustrationMap().get(illustrationName);
  if (url) {
    return url;
  }
  return getAssetPath(`svg/${illustrationName}.svg`);
};


export const getName = (
  illustrationName: string | undefined,
  illustration: string | undefined
) => {

  if (!illustrationName && illustration && !isSrc(illustration)) {
    illustrationName = illustration;
  }
  if (isStr(illustrationName)) {
    illustrationName = toLower(illustrationName);
  }

  if (!isStr(illustrationName) || illustrationName.trim() === '') {
    return null;
  }

  // only allow alpha characters and dash
  const invalidChars = illustrationName.replace(/[a-z]|-|\d/gi, '');
  if (invalidChars !== '') {
    return null;
  }

  return illustrationName;
};

export const getSrc = (src: string | undefined) => {
  if (isStr(src)) {
    src = src.trim();
    if (isSrc(src)) {
      return src;
    }
  }
  return null;
};

export const isSrc = (str: string) => str.length > 0 && /(\/|\.)/.test(str);

export const isStr = (val: any): val is string => typeof val === 'string';

export const toLower = (val: string) => val.toLowerCase();