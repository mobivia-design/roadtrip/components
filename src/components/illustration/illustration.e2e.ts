import { newE2EPage } from '@stencil/core/testing';

describe('road-illustration', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<road-illustration name="apps"></road-illustration>');

    const element = await page.find('road-illustration');
    expect(element).toHaveClass('hydrated');
  });
});
