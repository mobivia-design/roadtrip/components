import { validateContent } from './validate';

export const roadillustrationContent = new Map<string, string>();
const requests = new Map<string, Promise<any>>();

export const getIllustrationSvgContent = (url: string, sanitize: boolean) => {
  // see if we already have a request for this url
  let req = requests.get(url);

  if (!req) {
    if (typeof fetch !== 'undefined' && typeof document !== 'undefined') {
      // we don't already have a request
      // @ts-ignore
      req = fetch(url).then((rsp) => {
        if (rsp.ok) {
          return rsp.text().then((svgContent) => {
            if (svgContent && sanitize !== false) {
              svgContent = validateContent(svgContent);
            }
            roadillustrationContent.set(url, svgContent || '');
          });
        }
        roadillustrationContent.set(url, '');
      });

      // cache for the same requests
      requests.set(url, req);
    } else {
      // set to empty for ssr scenarios and resolve promise
      roadillustrationContent.set(url, '');
      return Promise.resolve();
    }
  }

  return req;
};
