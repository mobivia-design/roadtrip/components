```javascript
import React from 'react';
import { apps, noResult } from "@roadtrip/components/illustrations";

const MyComponent: React.FC = () => {
  return (
    <>
      <road-illustration icon={apps}></road-illustration>
      <road-illustration icon={noResult}></road-illustration>
    </>
  );
};

export default MyComponent;
```