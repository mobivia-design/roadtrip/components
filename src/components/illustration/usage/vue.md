```html
<template>
  <road-illustration icon="apps"></road-illustration>
  <road-illustration icon="no-result"></road-illustration>
</template>

<script>
  import { apps, noResult } from "@roadtrip/components/illustrations";
  import { addIcons } from "@roadtrip/illustrations";
  addIcons({
    apps,
    "no-result": noResult
  });
</script>
```
