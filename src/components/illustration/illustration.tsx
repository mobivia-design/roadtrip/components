import { Build, Component, Element, Host, Prop, State, Watch, h } from '@stencil/core';
import { getIllustrationSvgContent, roadillustrationContent } from './request';
import { getName, getUrl } from './utils';

@Component({
  tag: 'road-illustration',
  assetsDirs: ['svg'],
  styleUrl: 'illustration.css',
  shadow: true,
})
export class Illustration {
  private io?: IntersectionObserver;

  @Element() el!: HTMLRoadIllustrationElement;

  @State() private illustrationSvgContent?: string;

  @State() private isVisible = false;



  /**
   * Specifies the label to use for accessibility. Defaults to the illustration name.
   */
  @Prop({ mutable: true, reflect: true }) ariaLabel?: string;

  /**
   * Set the illustration to hidden, respectively `true`, to remove it from the accessibility tree.
   */
  @Prop({ reflect: true }) ariaHidden?: string;

  /**
   * Specifies which illustration to use from the built-in set of illustrations.
   */
  @Prop() name?: string;

  /**
   * Specifies the exact `src` of an SVG file to use.
   */
  @Prop() src?: string;

  /**
   * A combination of both `name` and `src`. If a `src` url is detected
   * it will set the `src` property. Otherwise it assumes it's a built-in named
   * SVG and set the `name` property.
   */
  @Prop() illustration?: any;

  /**
   * The size of the illustration.
   * Available options are: `"sm"`, `"md"`, `"lg"`, `"2x"`.
   */
  @Prop() size?: 'sm' | 'md' | 'lg' | '2x' = "2x";

  /**
   * The rotation of the illustration.
   * Available options are: `"90"`, `"180"`, `"270"`.
   */
  @Prop() rotate?: '90' | '180' | '270';

  /**
   * If enabled, road-illustration will be loaded lazily when it's visible in the viewport.
   * Default, `false`.
   */
  @Prop() lazy: boolean = false;

  /**
   * When set to `false`, SVG content that is HTTP fetched will not be checked
   * if the response SVG content has any `<script>` elements, or any attributes
   * that start with `on`, such as `onclick`.
   */
  @Prop() sanitize = true;

  componentWillLoad() {

    this.waitUntilVisible(this.el, '50px', () => {
      this.isVisible = true;
      this.loadIllustration();
    });
  }

  disconnectedCallback() {
    if (this.io) {
      this.io.disconnect();
      this.io = undefined;
    }
  }

  private waitUntilVisible(el: HTMLRoadIllustrationElement, rootMargin: string, cb: () => void) {
    if (this.lazy && (window as any).IntersectionObserver) {
      const io = this.io = new (window as any).IntersectionObserver((data: IntersectionObserverEntry[]) => {
        if (data[0].isIntersecting) {
          io.disconnect();
          this.io = undefined;
          cb();
        }
      }, { rootMargin });

      io.observe(el);

    } else {
      // browser doesn't support IntersectionObserver
      // so just fallback to always show it
      cb();
    }
  }

  @Watch('name')
  @Watch('src')
  @Watch('illustration')
  loadIllustration() {
    if (Build.isBrowser && this.isVisible) {
      const url = getUrl(this);
      if (url) {
        if (roadillustrationContent.has(url)) {
          // sync if it's already loaded
          this.illustrationSvgContent = roadillustrationContent.get(url);
        } else {
          // async if it hasn't been loaded
          getIllustrationSvgContent(url, this.sanitize).then(() => (this.illustrationSvgContent = roadillustrationContent.get(url)));
        }
      }
    }

    if (!this.ariaLabel && this.ariaHidden !== 'true') {
      const label = getName(this.name, this.illustration);
      // user did not provide a label
      // come up with the label based on the icon name
      if (label) {
        this.ariaLabel = label.replace(/-/g, ' ');
      }
    }
  }

  render() {
    const sizeClass = this.size !== undefined ? `illustration-${this.size}` : '';
    const rotateClass = this.rotate !== undefined ? `illustration-rotate-${this.rotate}` : '';

    return (
      <Host class={`${sizeClass} ${rotateClass}`} aria-hidden="true" role="img">
        {(
          (this.illustrationSvgContent !== '')
            ? <div class="icon-inner" innerHTML={this.illustrationSvgContent}></div>
            : <div class="icon-inner"></div>
        )}
      </Host>
    );
  }
}