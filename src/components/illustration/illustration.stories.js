import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { illustration } from '../../../.storybook/illustration';

export default {
  title: 'Media/Illustration',
  component: 'road-illustration',
};

export const Playground = ({ name, size, rotate, lazy }) => html`
<road-illustration  
  name="${ifDefined(name)}" 
  size="${ifDefined(size)}" 
  rotate="${ifDefined(rotate)}" 
  lazy="${ifDefined(lazy)}"
></road-illustration>
`;
Playground.args = {
  name: 'apps',
  size: null,
  lazy: null,
};
Playground.argTypes = {
  name: {
    options: illustration,
    control: {
      type: 'select',
    },
  },
  size: {
    options: ['sm', 'md', 'lg', '2x'],
    control: {
      type: 'select',
    },
  },
  rotate: {
    options: ['0', '90', '180', '270'],
    control: {
      type: 'select',
    },
  },
  lazy: {
    control: 'boolean',
  },
  'aria-label': {
    control: 'text',
  },
  'aria-hidden': {
    control: 'text',
  },
  src: {
    control: 'text',
  },
  sanitize: {
    control: 'boolean',
  },
};