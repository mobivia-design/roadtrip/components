import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Forms/Chip',
  component: 'road-chip',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
  },
  argTypes: {
    outline: {
      control: 'boolean',
      description: 'Displays the chip with an outline style.',
    },
    'has-close-icon': {
      control: 'boolean',
      description: 'Adds a close icon to the chip.',
    },
    size: {
      options: ['md', 'lg'],
      control: { type: 'select' },
      description: 'Defines the size of the chip.',
      table: {
        defaultValue: { summary: 'md' },
      },
    },
    color: {
      options: ['default', 'secondary', 'inverse'],
      control: { type: 'select' },
      description: 'Changes the chip color.',
      table: {
        defaultValue: { summary: 'default' },
      },
    },
    ' ': {
      control: 'text',
      description: 'Content inside the chip (label or icon).',
    },
  },
  args: {
    outline: false,
    'has-close-icon': false,
    size: 'md',
    color: 'default',
    ' ': 'Label',
  },
};

const Template = (args) => html`
  ${['Label A', 'Label B', 'Label C', 'Label D'].map(label => html`
    <road-chip
      ?outline="${args.outline}"
      ?has-close-icon="${args['has-close-icon']}"
      size="${ifDefined(args.size)}"
      color="${ifDefined(args.color)}"
    >
      ${unsafeHTML(args[' '] || label)}
    </road-chip>
  `)}
`;

export const Playground = Template.bind({});
Playground.args = {
  outline: true,
  'has-close-icon': true,
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  outline: true,
  'has-close-icon': true,
  ' ': `<road-icon name="weather-rain" role="button"></road-icon> B`,
};

export const Color = Template.bind({});
Color.args = {
  color: 'secondary',
  ' ': `Reset`,
};

export const Size = Template.bind({});
Size.args = {
  outline: true,
  size: 'lg',
  ' ': `Revision`,
};
