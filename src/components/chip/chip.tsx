import { Component, Host, Prop, h } from '@stencil/core';
import { navigationClose } from '../../../icons';
import './../../utils/polyfill';

/**
 * @slot - Content of the chip.
 */

@Component({
  tag: 'road-chip',
  styleUrl: 'chip.css',
  shadow: true,
})
export class Chip {
  /**
   * The color to use from your application's color palette.
   */
  @Prop() color?: 'default' | 'secondary' | 'inverse' = 'default';

  /**
   * Display an outline style chip.
   */
  @Prop() outline: boolean = false;

  /**
   * The chip size.
   */
  @Prop() size: 'md' | 'lg' = 'md';

  /**
   * Display a close icon
   */
  @Prop() hasCloseIcon: boolean = false;

  render() {
    const outlineClass = this.outline ? 'chip-outline' : '';

    return (
      <Host
        class={`${outlineClass} chip-${this.color} chip-${this.size}`} tabindex="0"
      >
        <div class="chip-description">
          <slot/>
        </div>
        {this.hasCloseIcon && <road-icon class="chip-close" icon={navigationClose}></road-icon>}
      </Host>
    );
  }
}