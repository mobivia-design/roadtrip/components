# ion-chip



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute        | Description                                             | Type                                                 | Default     |
| -------------- | ---------------- | ------------------------------------------------------- | ---------------------------------------------------- | ----------- |
| `color`        | `color`          | The color to use from your application's color palette. | `"default" \| "inverse" \| "secondary" \| undefined` | `'default'` |
| `hasCloseIcon` | `has-close-icon` | Display a close icon                                    | `boolean`                                            | `false`     |
| `outline`      | `outline`        | Display an outline style chip.                          | `boolean`                                            | `false`     |
| `size`         | `size`           | The chip size.                                          | `"lg" \| "md"`                                       | `'md'`      |


## Slots

| Slot | Description          |
| ---- | -------------------- |
|      | Content of the chip. |


## Dependencies

### Depends on

- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-chip --> road-icon
  style road-chip fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
