# road-item



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                                                                                                                                                                                                                                                                               | Type                                       | Default             |
| ------------ | ------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------ | ------------------- |
| `active`     | `active`      | If `true`, display an active state item                                                                                                                                                                                                                                                   | `boolean \| undefined`                     | `undefined`         |
| `button`     | `button`      | If `true`, a button tag will be rendered and the item will be tappable.                                                                                                                                                                                                                   | `boolean`                                  | `false`             |
| `detail`     | `detail`      | If `true`, a detail arrow will appear on the item. Defaults to `false` unless the `mode` is `ios` and an `href` or `button` property is present.                                                                                                                                          | `boolean \| undefined`                     | `undefined`         |
| `detailIcon` | `detail-icon` | The icon to use when `detail` is set to `true`.                                                                                                                                                                                                                                           | `string`                                   | `navigationChevron` |
| `disabled`   | `disabled`    | If `true`, the user cannot interact with the item.                                                                                                                                                                                                                                        | `boolean`                                  | `false`             |
| `download`   | `download`    | This attribute instructs browsers to download a URL instead of navigating to it, so the user will be prompted to save it as a local file. If the attribute has a value, it is used as the pre-filled file name in the Save prompt (the user can still change the file name if they want). | `string \| undefined`                      | `undefined`         |
| `href`       | `href`        | Contains a URL or a URL fragment that the hyperlink points to. If this property is set, an anchor tag will be rendered.                                                                                                                                                                   | `string \| undefined`                      | `undefined`         |
| `layout`     | `layout`      | How the bottom border should be displayed on the item.                                                                                                                                                                                                                                    | `"horizontal" \| "vertical" \| undefined`  | `'horizontal'`      |
| `lines`      | `lines`       | How the bottom border should be displayed on the item.                                                                                                                                                                                                                                    | `"full" \| "inset" \| "none" \| undefined` | `undefined`         |
| `rel`        | `rel`         | Specifies the relationship of the target object to the link object. The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).                                                                                                    | `string \| undefined`                      | `undefined`         |
| `target`     | `target`      | Specifies where to display the linked URL. Only applies when an `href` is provided. Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.                                                                                                                                       | `string \| undefined`                      | `undefined`         |
| `text`       | `text`        | Description for the item                                                                                                                                                                                                                                                                  | `string \| undefined`                      | `''`                |
| `titleItem`  | `title-item`  | Title for the item                                                                                                                                                                                                                                                                        | `string \| undefined`                      | `''`                |
| `type`       | `type`        | The type of the button. Only used when an `onclick` or `button` property is present.                                                                                                                                                                                                      | `"button" \| "reset" \| "submit"`          | `'button'`          |


## Slots

| Slot      | Description                               |
| --------- | ----------------------------------------- |
|           | Content of the item.                      |
| `"end"`   | Placed to the right of all other content. |
| `"start"` | Placed to the left of all other content.  |


## Shadow Parts

| Part       | Description                                                                  |
| ---------- | ---------------------------------------------------------------------------- |
| `"native"` | The native HTML button, anchor or div element that wraps all child elements. |


## CSS Custom Properties

| Name                 | Description                        |
| -------------------- | ---------------------------------- |
| `--background-color` | Background of the item             |
| `--border-color`     | Border of the item button          |
| `--border-radius`    | Radius of the item border          |
| `--detail-color`     | color of the detail icon           |
| `--inner-padding`    | inner padding of the item          |
| `--min-height`       | Minimum height of the item         |
| `--padding-left`     | Padding left of the native element |


## Dependencies

### Depends on

- [road-label](../label)
- [road-icon](../icon)

### Graph
```mermaid
graph TD;
  road-item --> road-label
  road-item --> road-icon
  style road-item fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
