import { newE2EPage } from '@stencil/core/testing';

describe('road-item', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <road-item>
        <road-icon lazy name="vehicle-car" color="secondary" class="mr-16"></road-icon>
        <road-label>
          <p class="text-uppercase text-truncate m-0">
            Nissan Qashqai<br/>
            Diesel 1.5 115 TDI Suv 2018
          </p>
        </road-label>
        <road-button slot="end" size="sm" color="link" class="font-weight-normal">Modify</road-button>
      </road-item>
    `);

    const element = await page.find('road-item');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
