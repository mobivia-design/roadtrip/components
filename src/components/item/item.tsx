import { Component, Host, h, Prop, State, Element } from '@stencil/core';
import { navigationChevron } from '../../../icons';

/**
 * @slot start - Placed to the left of all other content.
 * @slot - Content of the item.
 * @slot end - Placed to the right of all other content.
 *
 * @part native - The native HTML button, anchor or div element that wraps all child elements.
 */

@Component({
  tag: 'road-item',
  styleUrl: 'item.css',
  shadow: true,
})
export class Item {

  @Element() el!: HTMLRoadItemElement;

  @State() multipleInputs = false;

  /**
   * Title for the item
   */
    @Prop() titleItem?: string = '';

  /**
   * Description for the item
   */
    @Prop() text?: string = '';

  /**
   * If `true`, a button tag will be rendered and the item will be tappable.
   */
  @Prop() button = false;

  /**
   * If `true`, a detail arrow will appear on the item. Defaults to `false` unless the `mode`
   * is `ios` and an `href` or `button` property is present.
   */
  @Prop() detail?: boolean;

  /**
   * If `true`, display an active state item
   */
  @Prop() active?: boolean;

  /**
   * The icon to use when `detail` is set to `true`.
   */
  @Prop() detailIcon = navigationChevron;

  /**
   * If `true`, the user cannot interact with the item.
   */
  @Prop() disabled = false;

  /**
   * This attribute instructs browsers to download a URL instead of navigating to
   * it, so the user will be prompted to save it as a local file. If the attribute
   * has a value, it is used as the pre-filled file name in the Save prompt
   * (the user can still change the file name if they want).
   */
  @Prop() download: string | undefined;

  /**
   * Contains a URL or a URL fragment that the hyperlink points to.
   * If this property is set, an anchor tag will be rendered.
   */
  @Prop() href: string | undefined;

  /**
   * Specifies the relationship of the target object to the link object.
   * The value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).
   */
  @Prop() rel: string | undefined;

  /**
   * How the bottom border should be displayed on the item.
   */
  @Prop() lines?: 'full' | 'inset' | 'none';
  
  /**
   * How the bottom border should be displayed on the item.
   */
  @Prop() layout?: 'horizontal' | 'vertical' = 'horizontal';

  /**
   * Specifies where to display the linked URL.
   * Only applies when an `href` is provided.
   * Special keywords: `"_blank"`, `"_self"`, `"_parent"`, `"_top"`.
   */
  @Prop() target: string | undefined;

  /**
   * The type of the button. Only used when an `onclick` or `button` property is present.
   */
  @Prop() type: 'submit' | 'reset' | 'button' = 'button';

  // If the item has an href or button property it will render a native
  // anchor or button that is clickable
  private isClickable(): boolean {
    return (this.href !== undefined || this.button);
  }

  render() {
    const { detail, detailIcon, download, layout, lines, disabled, href, rel, target } = this;
    const clickable = this.isClickable();
    const TagType = clickable ? (href === undefined ? 'button' : 'a') : 'div' as any;
    const attrs = (TagType === 'button')
      ? { type: this.type }
      : {
        download,
        href,
        rel,
        target,
      };
    const showDetail = detail !== undefined && detail;

    return (
      <Host
        aria-disabled={disabled ? 'true' : null}
        class={{
          [`item-lines-${lines}`]: lines !== undefined,
        }}
      >
        <TagType
          {...attrs}
          class={{
            'item-native': true,
            'item-active': this.active,
            [`layout-${layout}`]: layout !== undefined,
          }}
          part="native"
          disabled={disabled}
        >
          <slot name="start"/>
          <div class="item-inner">
            <div class="input-wrapper">
              <slot/>
              <div class="input-wrapper-info">
                <road-label class="input-wrapper-info-title">{this.titleItem}</road-label>
                <p class="input-wrapper-info-text">{this.text}</p>
              </div>
            </div>
            <slot name="end"/>
            {showDetail && <road-icon icon={detailIcon} lazy={false} class="item-detail-icon"></road-icon>}
          </div>
        </TagType>
      </Host>
    );
  }

}
