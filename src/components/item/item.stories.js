import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';
import { list } from '../../../.storybook/list';

export default {
  title: 'Listing/Item',
  component: 'road-item',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
  },
};

export const Playground = (args) => html`
<road-item
  button="${ifDefined(args.button)}" 
  detail="${ifDefined(args.detail)}" 
  detail-icon="${ifDefined(args['detail-icon'])}" 
  active="${ifDefined(args.active)}"
  layout="${ifDefined(args.layout)}"
  lines="${ifDefined(args.lines)}"
  disabled="${ifDefined(args.disabled)}"
  title-item="${ifDefined(args['title-item'])}"
  text="${ifDefined(args.text)}"
>
  ${unsafeHTML(args.start)}
  ${unsafeHTML(args[' '])}
  ${unsafeHTML(args.end)}
</road-item>
`;
Playground.args = {
  button: null,
  detail: null,
  active: null,
  layout: undefined,
  lines: undefined,
  'detail-icon': undefined,
  disabled: null,
  start: '<road-icon name="shipping" role="img"></road-icon>',
  'title-item': 'OPEL Insignia',
  text: 'hatchback 2.0 CDTI 16V 130 cv',
  ' ': ``,
};
Playground.argTypes = {
  button: {
    description: "If `true`, a button tag will be rendered and the item will be tappable.",
    control: 'boolean',
  },
  detail: {
    description: "If `true`, a detail arrow will appear on the item. Defaults to `false` unless the `mode`\nis `ios` and an `href` or `button` property is present.",
    control: 'boolean',
  },
  active: {
    description: "If `true`, display an active state item",
    control: 'boolean',
  },
  layout: {
    description: "How the bottom border should be displayed on the item.",
    options: ['horizontal', 'vertical'],
    control: {
      type: 'radio',
    },
  },
  lines: {
    description: "How the bottom border should be displayed on the item.",
    options: ['full', 'inset', 'none'],
    control: {
      type: 'radio',
    },
  },
  'detail-icon': {
    description: "The icon to use when `detail` is set to `true`.",
    options: list,
    control: {
      type: 'select',
    },
  },
  disabled: {
    description: "If `true`, the user cannot interact with the item.",
    control: 'boolean',
  },
  download: {
    description: "This attribute instructs browsers to download a URL instead of navigating to\nit, so the user will be prompted to save it as a local file. If the attribute\nhas a value, it is used as the pre-filled file name in the Save prompt\n(the user can still change the file name if they want).",
    control: {
      type: null,
    },
  },
  'title-item': {
    description: "Title for the item",
    control: 'text',
  },
  text: {
    description: "Description for the item",
    control: 'text',
  },
  href: {
    description: "Contains a URL or a URL fragment that the hyperlink points to.\nIf this property is set, an anchor tag will be rendered.",
    control: {
      type: null,
    },
  },
  rel: {
    description: "Specifies the relationship of the target object to the link object.\nThe value is a space-separated list of [link types](https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types).",
    control: {
      type: null,
    },
  },
  target: {
    description: "Specifies where to display the linked URL.\nOnly applies when an `href` is provided.\nSpecial keywords: `\"_blank\"`, `\"_self\"`, `\"_parent\"`, `\"_top\"`.",
    control: {
      type: null,
    },
  },
  type: {
    description: "The type of the button. Only used when an `onclick` or `button` property is present.",
    control: {
      type: null,
    },
  },
  start: {
    description: "Placed to the left of all other content.",
    control: 'text',
  },
  ' ': {
    description: "Content of the item.",
    control: 'text',
  },
  end: {
    description: "Placed to the right of all other content.",
    control: 'text',
  },
  '--border-radius': {
    description: "Radius of the item border",
    table: {
      defaultValue: { summary: '0.25rem' },
    },
    control: {
      type: null,
    },
  },
  '--background-color': {
    description: "Background of the item",
    table: {
      defaultValue: { summary: 'transparent' },
    },
    control: {
      type: null,
    },
  },
  '--border-color': {
    description: "Border of the item button",
    table: {
      defaultValue: { summary: 'var(--road-outline-variant)' },
    },
    control: {
      type: null,
    },
  },
  '--detail-color': {
    description: "color of the detail icon",
    table: {
      defaultValue: { summary: 'var(--road-grey-900)' },
    },
    control: {
      type: null,
    },
  },
  '--inner-padding': {
    description: "inner padding of the item",
    table: {
      defaultValue: { summary: '1rem 0.5rem 1rem 0' },
    },
    control: {
      type: null,
    },
  },
  '--min-height': {
    description: "Minimum height of the item",
    table: {
      defaultValue: { summary: '3.5rem' },
    },
    control: {
      type: null,
    },
  },
  '--padding-left': {
    description: "Padding left of the native element",
    table: {
      defaultValue: { summary: '1rem' },
    },
    control: {
      type: null,
    },
  },
  native: {
    description: "The native HTML button, anchor or div element that wraps all child elements.",
    control: {
      type: null,
    },
  },
};

export const Compatibility = () => html`
<road-item class="mb-16">
  <road-label>
    <p class="h8 my-4">OPEL Insignia</p>
    <p class="text-small text-truncate m-0">hatchback 2.0 CDTI 16V 130 cv</p>
  </road-label>

  <road-button slot="end" size="sm" color="secondary" class="ml-16">Modify</road-button>
</road-item>

<road-item class="mb-16">
  <road-icon slot="start" name="alert-valid-outline" color="success" class="mr-16" role="img"></road-icon>

  <road-label>
    <p class="text-content text-success font-weight-bold mb-0">Compatible product</p>
    <p class="h8 my-4">OPEL Insignia</p>
    <p class="text-small text-truncate m-0">hatchback 2.0 CDTI 16V 130 cv</p>
  </road-label>

  <road-button slot="end" size="sm" color="secondary" class="ml-16">Modify</road-button>
</road-item>

<road-item class="mb-16">
  <road-icon slot="start" name="alert-error-outline" color="danger" class="mr-16" role="img"></road-icon>

  <road-label>
    <p class="text-content text-danger font-weight-bold mb-4">Product not compatible</p>
    <p class="h8 mb-4">OPEL Insignia</p>
    <p class="text-small text-truncate mb-4">hatchback 2.0 CDTI 16V 130 cv</p>
    <a href="#" class="link">See compatible products</a>
  </road-label>

  <road-button slot="end" size="sm" color="secondary" class="ml-16">Modify</road-button>
</road-item>
`;

export const ShoppingGuides = () => html`
<road-item class="mb-16">
  <road-icon slot="start" name="vehicle-car-outline" class="mr-16" role="img"></road-icon>

  <road-label>
    <p class="h8 mb-4">OPEL Insignia</p>
    <p class="text-small text-truncate m-0">hatchback 2.0 CDTI 16V 130 cv</p>
  </road-label>

  <road-icon slot="end" name="edit" color="secondary" size="sm" class="ml-16" role="img"></road-icon>
</road-item>

<road-item class="mb-16">
  <road-icon slot="start" name="vehicle-car-outline-front" class="mr-16" role="img"></road-icon>

  <road-label>
    <p class="text-small text-truncate mb-4">Bulb category</p>
    <p class="h8 m-0">Front lighting</p>
  </road-label>

  <road-icon slot="end" name="edit" color="secondary" size="sm" class="ml-16" role="button"></road-icon>
</road-item>

<road-item class="mb-16">
  <road-icon slot="start" name="light" class="mr-16" role="img"></road-icon>

  <road-label>
    <p class="text-small text-truncate mb-4">Bulb type</p>
    <p class="h8 m-0">Position light</p>
  </road-label>

  <road-icon slot="end" name="edit" color="secondary" size="sm" class="ml-16" role="button"></road-icon>
</road-item>
`;
