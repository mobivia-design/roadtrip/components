import { Component, Host, h, Prop } from '@stencil/core';

/**
 *
 * @slot - Content is placed between the named slots if provided without a slot.
 * @slot start - Content is placed to the left of the toolbar text and left to primery slot if provided.
 * @slot primary - Content is placed to the left of the toolbar text.
 * @slot secondary - Content is placed to the right of the toolbar text.
 * @slot end - Content is placed to the right of the toolbar text and right to secondary slot if provided.
 */

@Component({
  tag: 'road-toolbar',
  styleUrl: 'toolbar.css',
  shadow: true,
})
export class Toolbar {

  /**
   * Background color of the toolbar
   */
  @Prop() color?: 'primary' | 'secondary';

  render() {
    const colorClass = this.color !== undefined ? `toolbar-${this.color}` : '';

    return (
      <Host class={colorClass}>
        <div class="toolbar-container">
          <slot name="start"/>
          <slot name="primary"/>
          <div class="toolbar-content">
            <slot/>
          </div>
          <slot name="secondary"/>
          <slot name="end"/>
        </div>
      </Host>
    );
  }

}
