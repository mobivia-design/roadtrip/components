# road-toolbar



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                     | Type                                    | Default     |
| -------- | --------- | ------------------------------- | --------------------------------------- | ----------- |
| `color`  | `color`   | Background color of the toolbar | `"primary" \| "secondary" \| undefined` | `undefined` |


## Slots

| Slot          | Description                                                                                 |
| ------------- | ------------------------------------------------------------------------------------------- |
|               | Content is placed between the named slots if provided without a slot.                       |
| `"end"`       | Content is placed to the right of the toolbar text and right to secondary slot if provided. |
| `"primary"`   | Content is placed to the left of the toolbar text.                                          |
| `"secondary"` | Content is placed to the right of the toolbar text.                                         |
| `"start"`     | Content is placed to the left of the toolbar text and left to primery slot if provided.     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
