import { newE2EPage } from '@stencil/core/testing';

describe('road-toolbar', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <road-toolbar>
        <road-button slot="start">
          <road-icon name="navigation-back"></road-icon>
        </road-button>
        <road-toolbar-title>Title</road-toolbar-title>
        <road-button slot="end">
          <road-icon name="more-horizontal"></road-icon>
        </road-button>
      </road-toolbar>
    `);

    const element = await page.find('road-toolbar');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
