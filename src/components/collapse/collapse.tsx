import { Component, Host, h, Prop } from '@stencil/core';

/**
 * @slot  - Content visible when collapsed.
 * @slot collapsed-content - content not visible when collapsed.
 */

@Component({
  tag: 'road-collapse',
  styleUrl: 'collapse.css',
  shadow: true,
})
export class Collapse {

  /**
   * Set to `true` to display the collapsed part and to `false` to hide it.
   */
  @Prop({ mutable: true }) isOpen : boolean = false;

  /**
   * Text displayed in the button when the content is collapsed
   */
  @Prop() showMore : string = 'show more';

  /**
   * Text displayed in the button when the content is not collapsed
   */
  @Prop() showLess : string = 'show less';

  /**
   * if `true` the button will be centered
   */
  @Prop() centered: boolean = false;

  /**
   * Toggle the display when clicking header
   */
  private onClick = () => {
    this.isOpen = !this.isOpen;
  };

  render() {
    const isOpenClass = this.isOpen ? 'collapse-open' : '';
    const buttonText = this.isOpen ? this.showLess : this .showMore;
    const centerClass = this.centered ? 'collapse-btn-centered' : '';

    return (
      <Host>
        <slot/>
        <div class={`collapsed-content ${isOpenClass}`}>
          <slot name="collapsed-content"/>
        </div>
        <div class={`collapse-btn-wrapper ${centerClass}`}>
          <road-button onClick={this.onClick} class="collapse-btn btn-link">{buttonText}</road-button>
        </div>
      </Host>
    );
  }

}
