# road-collapse



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description                                                            | Type      | Default       |
| ---------- | ----------- | ---------------------------------------------------------------------- | --------- | ------------- |
| `centered` | `centered`  | if `true` the button will be centered                                  | `boolean` | `false`       |
| `isOpen`   | `is-open`   | Set to `true` to display the collapsed part and to `false` to hide it. | `boolean` | `false`       |
| `showLess` | `show-less` | Text displayed in the button when the content is not collapsed         | `string`  | `'show less'` |
| `showMore` | `show-more` | Text displayed in the button when the content is collapsed             | `string`  | `'show more'` |


## Slots

| Slot                  | Description                         |
| --------------------- | ----------------------------------- |
|                       | Content visible when collapsed.     |
| `"collapsed-content"` | content not visible when collapsed. |


## CSS Custom Properties

| Name                  | Description                            |
| --------------------- | -------------------------------------- |
| `--btn-font-size`     | Font size of the text button           |
| `--btn-margin-bottom` | Bottom margin of the button            |
| `--btn-padding-end`   | Right padding of the button            |
| `--btn-padding-start` | Left padding of the button             |
| `--max-height`        | maximum height of the collapse content |


## Dependencies

### Depends on

- [road-button](../button)

### Graph
```mermaid
graph TD;
  road-collapse --> road-button
  style road-collapse fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
