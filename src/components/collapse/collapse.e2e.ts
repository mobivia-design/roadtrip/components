import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-collapse', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage({
      html: `
        <road-collapse showMore="show more" showLess="show less">
          <ul class="m-0">
            <li>Car tire 205/55 R16 91 V</li>
            <li>Good traction on dry and wet soils.</li>
            <li>Reduced risk of aquaplaning.</li>
            <li>Reduced braking distances.</li>
          </ul>
          <div slot="collapsed-content">
            <ul class="m-0">
              <li>Specific Homologation : <b>non</b></li>
              <li>XL : <b>non</b></li>
              <li>Runflat : <b>non</b></li>
              <li>Seal : <b>non</b></li>
              <li>Various tire characteristics : <b>MFS</b></li>
              <li>Tire profile : <b>Energy saver</b></li>
            </ul>
          </div>
        </road-collapse>
      `,
    });
  });

  it('It should render', async () => {
    const element = await page.find('road-collapse');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('It should open when the button is clicked', async () => {
    const element = await page.find('road-collapse');
    expect(element).toHaveClass('hydrated');

    const collapseBtn = await page.evaluateHandle(`document.querySelector('road-collapse').shadowRoot.querySelector('road-button')`);
    await collapseBtn.click();

    await page.waitForChanges();

    const isOpen = await element.getProperty('isOpen');
    expect(isOpen).toBeTruthy();

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
