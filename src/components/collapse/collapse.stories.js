import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Expand/Collapse',
  component: 'road-collapse',
  argTypes: {
    'is-open': {
      control: 'boolean',
      description: 'Defines whether the collapse is open or closed.',
    },
    centered: {
      control: 'boolean',
      description: 'Centers the expand/collapse button.',
    },
    'show-less': {
      control: 'text',
      description: 'Label for the button when content is expanded.',
      table: {
        defaultValue: { summary: 'show less' },
      },
    },
    'show-more': {
      control: 'text',
      description: 'Label for the button when content is collapsed.',
      table: {
        defaultValue: { summary: 'show more' },
      },
    },
    ' ': {
      control: 'text',
      description: 'Expanded content of the collapse component.',
    },
    'collapsed-content': {
      control: 'text',
      description: 'Content displayed when the collapse is closed.',
    },
    // 🔹 Ajout des CSS Variables pour personnalisation
    '--btn-padding-start': {
      table: { defaultValue: { summary: '1rem' } },
      control: 'text',
      description: 'Padding at the start of the button.',
    },
    '--btn-padding-end': {
      table: { defaultValue: { summary: '1rem' } },
      control: 'text',
      description: 'Padding at the end of the button.',
    },
    '--btn-margin-bottom': {
      table: { defaultValue: { summary: '1rem' } },
      control: 'text',
      description: 'Margin below the button.',
    },
    '--btn-font-size': {
      table: { defaultValue: { summary: '1rem' } },
      control: 'text',
      description: 'Font size of the button text.',
    },
    '--max-height': {
      table: { defaultValue: { summary: '500px' } },
      control: 'text',
      description: 'Maximum height of the collapse content when expanded.',
    },
  },
  args: {
    'is-open': false,
    centered: false,
    'show-less': 'show less',
    'show-more': 'show more',
    ' ': `<ul class="m-0">
      <li>Car tire 205/55 R16 91 V</li>
      <li>Good traction on dry and wet soils.</li>
      <li>Reduced risk of aquaplaning.</li>
      <li>Reduced braking distances.</li>
    </ul>`,
    'collapsed-content': `<ul class="m-0">
      <li>Specific Homologation: <b>non</b></li>
      <li>XL: <b>non</b></li>
      <li>Runflat: <b>non</b></li>
      <li>Seal: <b>non</b></li>
      <li>Various tire characteristics: <b>MFS</b></li>
      <li>Tire profile: <b>Energy saver</b></li>
    </ul>`,
  },
};

const Template = (args) => html`
  <road-collapse 
    ?is-open="${args['is-open']}" 
    ?centered="${args.centered}" 
    show-more="${ifDefined(args['show-more'])}" 
    show-less="${ifDefined(args['show-less'])}"
    style="
      --btn-padding-start: ${ifDefined(args['--btn-padding-start'])};
      --btn-padding-end: ${ifDefined(args['--btn-padding-end'])};
      --btn-margin-bottom: ${ifDefined(args['--btn-margin-bottom'])};
      --btn-font-size: ${ifDefined(args['--btn-font-size'])};
      --max-height: ${ifDefined(args['--max-height'])};
    "
  >
    ${unsafeHTML(args[' '])}
    <div slot="collapsed-content">
      ${unsafeHTML(args['collapsed-content'])}
    </div>
  </road-collapse>
`;

export const Playground = Template.bind({});

export const Open = Template.bind({});
Open.args = {
  'is-open': true,
};

export const VehicleTypes = Template.bind({});
VehicleTypes.args = {
  centered: true,
  'show-less': 'show less',
  'show-more': 'show more',
  ' ': `<road-grid>
    <road-row>
      <road-col class="col-6 col-md-4 col-lg-3">
        <road-card button value="car">
          <road-icon name="vehicle-car" size="3x" aria-hidden="true"></road-icon>
          <road-label>Car</road-label>
        </road-card>
      </road-col>
      <road-col class="col-6 col-md-4 col-lg-3">
        <road-card button value="4x4">
          <road-icon name="vehicle-suv" size="3x" aria-hidden="true"></road-icon>
          <road-label>4x4</road-label>
        </road-card>
      </road-col>
      <road-col class="col-6 col-md-4 col-lg-3">
        <road-card button value="truck">
          <road-icon name="vehicle-pickup-van" size="3x" aria-hidden="true"></road-icon>
          <road-label>Truck</road-label>
        </road-card>
      </road-col>
      <road-col class="col-6 col-md-4 col-lg-3">
        <road-card button value="bike">
          <road-icon name="vehicle-moto" size="3x" aria-hidden="true"></road-icon>
          <road-label>Bike</road-label>
        </road-card>
      </road-col>
    </road-row>
  </road-grid>`,
  'collapsed-content': `<road-grid>
    <road-row>
      <road-col class="col-6 col-md-4 col-lg-3">
        <road-card button value="scooter">
          <road-icon name="vehicle-scooter" size="3x" aria-hidden="true"></road-icon>
          <road-label>Scooter</road-label>
        </road-card>
      </road-col>
      <road-col class="col-6 col-md-4 col-lg-3">
        <road-card button value="competition">
          <road-icon name="vehicle-rally" size="3x" aria-hidden="true"></road-icon>
          <road-label>Compétition</road-label>
        </road-card>
      </road-col>
      <road-col class="col-6 col-md-4 col-lg-3">
        <road-card button value="collection">
          <road-icon name="vehicle-collector" size="3x" aria-hidden="true"></road-icon>
          <road-label>Collection</road-label>
        </road-card>
      </road-col>
      <road-col class="col-6 col-md-4 col-lg-3">
        <road-card button value="quad">
          <road-icon name="vehicle-quad" size="3x" aria-hidden="true"></road-icon>
          <road-label>Quad</road-label>
        </road-card>
      </road-col>
    </road-row>
  </road-grid>`,
};
VehicleTypes.parameters = {
  backgrounds: {
    default: 'grey',
  },
};
