import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Navigation/ToolbarV2',
  component: 'road-toolbar-v2',
  subcomponents: {
    'road-toolbar-title': 'road-toolbar-title',
  },
  parameters: {
    backgrounds: {
      default: 'grey',
    },
    layout: 'fullscreen',
  },
  argTypes: {
    start: {
      description: "Content is placed to the left of the toolbar text and left to primery slot if provided.",
      control: 'text',
    },
    primary: {
      description: "Content is placed to the left of the toolbar text.",
      control: 'text',
    },
    ' ': {
      description: "Content is placed between the named slots if provided without a slot.",
      control: 'text',
    },
    secondary: {
      description: "Content is placed to the right of the toolbar text.",
      control: 'text',
    },
    end: {
      description: "Content is placed to the right of the toolbar text and right to secondary slot if provided.",
      control: 'text',
    },
  },
  args: {
    start: `<road-button slot="start" class="border-left-0" tabindex="0" color="ghost">
    <road-icon name="navigation-chevron" rotate="180"></road-icon>
    </road-button>`,
    primary: undefined,
    ' ': ``,
    secondary: undefined,
    end: undefined,
  },
};

const Template = (args) => html`
<road-toolbar-v2>
  ${unsafeHTML(args.start)}
  ${unsafeHTML(args.primary)}
  ${unsafeHTML(args[' '])}
  ${unsafeHTML(args.secondary)}
  ${unsafeHTML(args.end)}
</road-toolbar-v2>
`;

export const Playground = Template.bind({});

export const withButtons = Template.bind({});
withButtons.args = {
  start: `<road-button slot="start" class="border-left-0" tabindex="0" color="ghost">
    <road-icon name="navigation-chevron" rotate="180"></road-icon>
  </road-button>`,
  end: `<road-button slot="end" tabindex="0" color="ghost">
    <road-icon name="more-horizontal"></road-icon>
  </road-button>`,
};


export const headerHomeApp = Template.bind({});
headerHomeApp.args = {
  ' ': ``,
  start: `<road-button slot="start" class="border-left-0" tabindex="0" color="ghost">
    <road-icon name="navigation-chevron" rotate="180"></road-icon>
  </road-button>`
};


export const headerApp = Template.bind({});
headerApp.args = {
  ' ': `<road-toolbar-title>Title</road-toolbar-title>`,
  start: `<road-button slot="start" class="border-left-0" color="ghost">
    <road-icon name="navigation-chevron" rotate="180"></road-icon>
  </road-button>`,
  secondary: `<road-button class="border-0 align-items-center" slot="secondary" color="ghost">
    <road-icon name="alert-question-outline"></road-icon><road-label class="d-none d-xl-block mx-8">Help</road-label>
  </road-button>`,
  end: `<road-button class="d-none d-xl-flex align-items-center" slot="end" color="ghost">
    <road-icon name="speak-advice-outline"></road-icon><road-label class="d-none d-xl-block mx-8">Feedback</road-label>
  </road-button>
  </div>`,
};
