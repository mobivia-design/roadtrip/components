import { Component, Host, h } from '@stencil/core';

/**
 *
 * @slot - Content is placed between the named slots if provided without a slot.
 * @slot start - Content is placed to the left of the toolbar text and left to primery slot if provided.
 * @slot primary - Content is placed to the left of the toolbar text.
 * @slot secondary - Content is placed to the right of the toolbar text.
 * @slot end - Content is placed to the right of the toolbar text and right to secondary slot if provided.
 */

@Component({
  tag: 'road-toolbar-v2',
  styleUrl: 'toolbar-v2.css',
  shadow: true,
})
export class ToolbarV2 {


  render() {

    return (
      <Host class="toolbar">
        <div class="toolbar-container">
          <div class="toolbar-container-content-left">
            <slot name="start"/>
            <slot name="primary"/>
          </div>
          <div class="toolbar-content">
            <slot/>
          </div>
          <div class="toolbar-container-content-right">
            <slot name="secondary"/>
            <slot name="end"/>
          </div>
        </div>
      </Host>
    );
  }

}
