import { Component, Host, Prop, h } from '@stencil/core';

import { FeedbackColors } from '../../interface';

/**
 * @slot icon - Icon of the alert, it should be a road-icon element.
 * @slot  - Message of the alert.
 */

@Component({
  tag: 'road-alert',
  styleUrl: 'alert.css',
  shadow: true,
})
export class Alert {

  /**
   * Set the color of alert. e.g. info, success, warning, danger
   */
  @Prop() color: FeedbackColors = 'info';

  /**
   * Title display in the alert
   */
   @Prop() label?: string;

   /**
   * Button display in the alert
   * `<road-button size="sm">Label</road-button>`
   */
   @Prop() button?: string;

   /**
   * Text Link display in the alert
   */
   @Prop() link?: string;

   /**
    * url display in the link
    */
   @Prop() url?: string;

   render() {
     const colorClass = this.color !== undefined ? `alert-${this.color}` : '';

     return (
       <Host class={colorClass} role="alert">
         <div class="alert-icon">
           <slot name="icon"/>
         </div>
         <div>
           {this.label && <span class="alert-title d-block">{this.label}</span>}
           <p class="alert-description">
             <slot/>
             {this.link && <a href={this.url} class="alert-link link link-default">{this.link}</a>}
           </p>
         </div>
       </Host>
     );
   }
}