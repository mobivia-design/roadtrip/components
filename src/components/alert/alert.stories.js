import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';
import { list } from '../../../.storybook/list';

export default {
  title: 'Indicators/Alert',
  component: 'road-alert',
  argTypes: {
    color: {
      description: "Set the color of alert. e.g. info, success, warning, danger",
      options: ['info', 'success', 'warning', 'danger'],
      control: {
        type: 'radio',
      },
    },
    icon: {
      description: "Icon of the alert, it should be a road-icon element.",
      options: list,
      control: {
        type: 'select',
      },
    },
    link: {
      description: "Text Link display in the alert",
      control: 'text',
    },
    button: {
      description: "Button display in the alert\n`<road-button size=\"sm\">Label</road-button>`",
      control: 'text',
    },
    label: {
      description: "Title display in the alert",
      control: 'text',
    },
    url: {
      description: "url display in the link",
      control: 'text',
    },
    ' ': {
      description: "Message of the alert.",
      control: 'text',
    },
  },
  args: {
    color: 'info',
    icon: `alert-info`,
    button: ``,
    ' ': `Information`,
  },
};

const Template = (args) => html`
<road-alert color="${ifDefined(args.color)}" label="${ifDefined(args.label)}" link="${ifDefined(args.link)}" url="${ifDefined(args.url)}">
  <road-icon slot="icon" name="${ifDefined(args.icon)}" role="img"></road-icon>
  ${unsafeHTML(args[' '])}
  ${unsafeHTML(args.button)}
</road-alert>
`;

export const Playground = Template.bind({});
Playground.args = {
  color: 'info',
  icon: `alert-info`,
  ' ': `Information`,
};

export const Success = Template.bind({});
Success.args = {
  color: 'success',
  icon: `alert-success`,
  ' ': `Success`,
};

export const Warning = Template.bind({});
Warning.args = {
  color: 'warning',
  icon: `alert-warning`,
  ' ': `Warning`,
};

export const Danger = Template.bind({});
Danger.args = {
  color: 'danger',
  icon: `alert-danger`,
  ' ': `Danger`,
};
