# road-alert



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                              | Type                                           | Default     |
| -------- | --------- | ------------------------------------------------------------------------ | ---------------------------------------------- | ----------- |
| `button` | `button`  | Button display in the alert `<road-button size="sm">Label</road-button>` | `string \| undefined`                          | `undefined` |
| `color`  | `color`   | Set the color of alert. e.g. info, success, warning, danger              | `"danger" \| "info" \| "success" \| "warning"` | `'info'`    |
| `label`  | `label`   | Title display in the alert                                               | `string \| undefined`                          | `undefined` |
| `link`   | `link`    | Text Link display in the alert                                           | `string \| undefined`                          | `undefined` |
| `url`    | `url`     | url display in the link                                                  | `string \| undefined`                          | `undefined` |


## Slots

| Slot     | Description                                          |
| -------- | ---------------------------------------------------- |
|          | Message of the alert.                                |
| `"icon"` | Icon of the alert, it should be a road-icon element. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
