import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('road-alert', () => {
  let page: E2EPage;

  beforeEach(async () => {
    page = await newE2EPage();
  });

  it('should render information alert', async () => {
    await page.setContent(`
      <road-alert>
        <road-icon slot="icon" name="alert-info"></road-icon>
        Information
      </road-alert>
  `);

    const element = await page.find('road-alert');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render success alert', async () => {
    await page.setContent(`
      <road-alert color="success">
        <road-icon slot="icon" name="alert-success"></road-icon>
        Success
      </road-alert>
  `);

    const element = await page.find('road-alert');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render warning alert', async () => {
    await page.setContent(`
      <road-alert color="warning">
        <road-icon slot="icon" name="alert-warning"></road-icon>
        Warning
      </road-alert>
  `);

    const element = await page.find('road-alert');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });

  it('should render danger alert', async () => {
    await page.setContent(`
    <road-alert color="danger">
      <road-icon slot="icon" name="alert-danger"></road-icon>
      Danger
    </road-alert>
  `);

    const element = await page.find('road-alert');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});