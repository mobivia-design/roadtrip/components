# road-list



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                             | Type                                       | Default     |
| -------- | --------- | ------------------------------------------------------- | ------------------------------------------ | ----------- |
| `lines`  | `lines`   | How the bottom border should be displayed on all items. | `"full" \| "inset" \| "none" \| undefined` | `undefined` |


## Slots

| Slot | Description                                           |
| ---- | ----------------------------------------------------- |
|      | Content of the list, it should be road-item elements. |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
