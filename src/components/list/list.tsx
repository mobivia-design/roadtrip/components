import { Component, Host, h, Prop } from '@stencil/core';

/**
 * @slot  - Content of the list, it should be road-item elements.
 */

@Component({
  tag: 'road-list',
  styleUrl: 'list.css',
  shadow: true,
})
export class List {

  /**
   * How the bottom border should be displayed on all items.
   */
  @Prop() lines?: 'full' | 'inset' | 'none';

  render() {
    const { lines } = this;

    return (
      <Host
        class={{
          [`list-lines-${lines}`]: lines !== undefined,
        }}
      >
        <slot/>
      </Host>
    );
  }

}
