import { newE2EPage } from '@stencil/core/testing';

describe('road-list', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(`
      <road-list>
        <road-item button active>
          <road-icon slot="start" name="picture" size="md"></road-icon>
          <road-label>
            Drawer link
          </road-label>
        </road-item>
        <road-item button>
          <road-icon slot="start" name="picture" size="md"></road-icon>
          <road-label>
            Drawer link
          </road-label>
        </road-item>
        <road-item button>
          <road-icon slot="start" name="picture" size="md"></road-icon>
          <road-label>
            Drawer link
          </road-label>
        </road-item>
        <road-item button>
          <road-icon slot="start" name="picture" size="md"></road-icon>
          <road-label>
            Drawer link
          </road-label>
        </road-item>
      </road-list>
    `);

    const element = await page.find('road-list');
    expect(element).toHaveClass('hydrated');

    const results = await page.compareScreenshot();
    expect(results).toMatchScreenshot();
  });
});
