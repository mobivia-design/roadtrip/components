import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Listing/List',
  component: 'road-list',
  parameters: {
    backgrounds: {
      default: 'grey',
    },
  },
  argTypes: {
    lines: {
      description: "How the bottom border should be displayed on all items.",
      options: ['full', 'inset', 'none'],
      control: {
        type: 'radio',
      },
    },
    ' ': {
      description: "Content of the list, it should be road-item elements.",
      control: 'text',
    },
  },
};

const Template = (args) => html`
<road-list lines="${ifDefined(args.lines)}" class="bg-white">
  ${unsafeHTML(args[' '])}
</road-list>
`;

export const Playground = Template.bind({});
Playground.args = {
  lines: undefined,
  ' ': `<road-item button>
    <road-icon slot="start" name="picture" size="md" role="img"></road-icon>
    <road-label>
      Label
    </road-label>
  </road-item>
  <road-item button>
    <road-icon slot="start" name="picture" size="md" role="img"></road-icon>
    <road-label>
    Label
    </road-label>
  </road-item>
  <road-item button>
    <road-icon slot="start" name="picture" size="md" role="img"></road-icon>
    <road-label>
    Label
    </road-label>
  </road-item>
  <road-item button>
    <road-icon slot="start" name="picture" size="md" role="img"></road-icon>
    <road-label>
      Label
    </road-label>
  </road-item>`,
};

export const LinkList = Template.bind({});
LinkList.args = {
  ' ': `<road-item button active>
    <road-icon slot="start" name="picture" size="md" role="img"></road-icon>
    <road-label>
      Drawer link
    </road-label>
  </road-item>
  <road-item button>
    <road-icon slot="start" name="picture" size="md" role="img"></road-icon>
    <road-label>
      Drawer link
    </road-label>
  </road-item>
  <road-item button>
    <road-icon slot="start" name="picture" size="md" role="img"></road-icon>
    <road-label>
      Drawer link
    </road-label>
  </road-item>
  <road-item button>
    <road-icon slot="start" name="picture" size="md" role="img"></road-icon>
    <road-label>
      Drawer link
    </road-label>
  </road-item>`,
};
