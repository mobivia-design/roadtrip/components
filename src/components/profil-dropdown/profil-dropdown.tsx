import { Component, h, Prop, Element, Listen } from '@stencil/core';

/**
 * @slot  - Element how will open the dropdown when clicked
 * @slot list - List of item values (you can add border with lines=`full` but not on the last item)
 * @slot avatar - Content of the avatar (img or First letter) <road-img> or <road-label>.
 * @slot avatarItem - Content of the avatar item (img or First letter) <road-img> or <road-label>.
 * @slot name - Content First and Last Name.
 * @slot email - Content email.


 */

@Component({
  tag: 'road-profil-dropdown',
  styleUrl: 'profil-dropdown.css',
  shadow: true,
})
export class Dropdown {

  @Element() el!: HTMLRoadProfilDropdownElement;

  /**
   * Set to `true` to open the dropdown menu and to `false` to close it.
   */
  @Prop({ mutable: true }) isOpen : boolean = false;

  /**
   * Toggle the display when clicking element in slot
   */
  private onClick = () => {
    this.isOpen = !this.isOpen;
  };

  @Listen('click', { target: 'document' })
  handleDocumentClick(ev: MouseEvent) {
    // Check if the clicked element is the dropdown button
    if ((ev.target as HTMLElement).closest('road-profil-dropdown') === this.el) {
      return; // Do nothing if clicked element is the dropdown button
    }
    // Close dropdown if click is outside the dropdown
    this.isOpen = false;
  }

  render() {

    return (
      <details class="dropdown" open={this.isOpen}>
        <summary aria-expanded={`${this.isOpen}`} tabindex="0" role="button" onClick={this.onClick}>
          <div class={`d-flex`}>
            <div>
              <road-avatar>
                <slot name="avatar"/>
              </road-avatar>
            </div>
          </div>
        </summary>
        <div class={`dropdown-menu`}>
          <div class="profil-item">
            <road-avatar size="sm">
              <slot name="avatarItem"/>
            </road-avatar>
            <div class="profil-item-info">
              <span class="profil-item-info-name">
                <slot name="name"/>
              </span>
              <road-label>
                <slot name="email"/>
              </road-label>
            </div>
          </div>
          <slot name="list"/>
        </div>
      </details>
    );
  }

}
