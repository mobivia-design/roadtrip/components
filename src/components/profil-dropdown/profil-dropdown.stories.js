import { html } from 'lit';
import { ifDefined } from 'lit/directives/if-defined.js';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

export default {
  title: 'Expand/Profil Dropdown',
  component: 'road-profil-dropdown',
  argTypes: {
    'is-open': {
      description: "Set to `true` to open the dropdown menu and to `false` to close it.",
      control: 'boolean',
    },
    ' ': {
      description: "Element how will open the dropdown when clicked",
      control: {
        type: null,
      },
    },
    list: {
      description: "List of item values (you can add border with lines=`full` but not on the last item)",
      control: {
        type: 'text',
      },
    },
    avatar: {
      description: "Content of the avatar (img or First letter) <road-img> or <road-label>.",
      control: {
        type: 'text',
      },
    },
    avatarItem: {
      description: "Content of the avatar item (img or First letter) <road-img> or <road-label>.",
      control: {
        type: 'text',
      },
    },
    name: {
      description: "Content First and Last Name.",
      control: {
        type: 'text',
      },
    },
    email: {
      description: "Content email.",
      control: {
        type: 'text',
      },
    },
    '--margin-top': {
      description: "Top margin of the menu",
      table: {
        defaultValue: { summary: '0.5rem' },
      },
      control: {
        type: null,
      },
    },
  },
  args: {
    'is-open': true,
    avatar: `<road-img slot="avatar" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar" title="Person name"></road-img>`,
    avatarItem: `<road-img slot="avatarItem" src="https://images.unsplash.com/photo-1527980965255-d3b416303d12?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="avatar" title="Person name"></road-img>`,
    name: `<road-label slot="name">First and Last name</road-label>`,
    email: `<road-label slot="email">Email</road-label>`,
    list: `<road-list slot="list">
    <road-item button style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;" class="edit-profil border-0">
      <road-icon slot="start" name="edit-outline" size="md" aria-hidden="true"></road-icon>
      <road-label style="font-size: 1rem">
        Edit profile
      </road-label>
    </road-item>
    <road-item button style="--border-radius: 0; --min-height: 2.5rem; --inner-padding: 0;" class="border-0">
      <road-icon slot="start" name="log-out-outline" size="md" aria-hidden="true"></road-icon>
      <road-label style="font-size: 1rem">
        Log out
      </road-label>
    </road-item>
  </road-list>
  
  
  `,
  },
};

export const Playground = (args) => html`
<road-profil-dropdown is-open=${ifDefined(args['is-open'])}>
  ${unsafeHTML(args.list)}
  ${unsafeHTML(args.avatar)}
  ${unsafeHTML(args.avatarItem)} 
  ${unsafeHTML(args.name)} 
  ${unsafeHTML(args.email)} 
</road-profil-dropdown>
`;
