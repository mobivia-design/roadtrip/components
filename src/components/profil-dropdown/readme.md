# road-dropdown



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                         | Type      | Default |
| -------- | --------- | ------------------------------------------------------------------- | --------- | ------- |
| `isOpen` | `is-open` | Set to `true` to open the dropdown menu and to `false` to close it. | `boolean` | `false` |


## Slots

| Slot           | Description                                                                         |
| -------------- | ----------------------------------------------------------------------------------- |
|                | Element how will open the dropdown when clicked                                     |
| `"avatar"`     | Content of the avatar (img or First letter) <road-img> or <road-label>.             |
| `"avatarItem"` | Content of the avatar item (img or First letter) <road-img> or <road-label>.        |
| `"email"`      | Content email.                                                                      |
| `"list"`       | List of item values (you can add border with lines=`full` but not on the last item) |
| `"name"`       | Content First and Last Name.                                                        |


## CSS Custom Properties

| Name           | Description            |
| -------------- | ---------------------- |
| `--margin-top` | Top margin of the menu |


## Dependencies

### Depends on

- [road-avatar](../avatar)
- [road-label](../label)

### Graph
```mermaid
graph TD;
  road-profil-dropdown --> road-avatar
  road-profil-dropdown --> road-label
  style road-profil-dropdown fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
