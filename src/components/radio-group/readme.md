# road-radio-group

A radio group is a group of [radio buttons](../radio). It allows
a user to select at most one radio button from a set. Checking one radio
button that belongs to a radio group unchecks any previous checked
radio button within the same group.

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute               | Description                                                     | Type                   | Default                                     |
| --------------------- | ----------------------- | --------------------------------------------------------------- | ---------------------- | ------------------------------------------- |
| `allowEmptySelection` | `allow-empty-selection` | If `true`, the radios can be deselected.                        | `boolean`              | `false`                                     |
| `ariaLabel`           | `aria-label`            | Label for the field                                             | `string`               | `` `${this.radioGroupId}-label` ``          |
| `asterisk`            | `asterisk`              | add an asterisk to the label of the field                       | `boolean \| undefined` | `false`                                     |
| `error`               | `error`                 | Error message for the radio group                               | `string \| undefined`  | `undefined`                                 |
| `helper`              | `helper`                | Helper message for the radio group                              | `string \| undefined`  | `undefined`                                 |
| `label`               | `label`                 | Label for the field                                             | `string \| undefined`  | `undefined`                                 |
| `name`                | `name`                  | The name of the control, which is submitted with the form data. | `string`               | `this.radioGroupId`                         |
| `radioGroupId`        | `radio-group-id`        | The id of checkbox                                              | `string`               | `` `road-radio-group-${radioGroupIds++}` `` |
| `value`               | `value`                 | the value of the radio group.                                   | `any`                  | `undefined`                                 |


## Events

| Event        | Description                         | Type                                                   |
| ------------ | ----------------------------------- | ------------------------------------------------------ |
| `roadchange` | Emitted when the value has changed. | `CustomEvent<{ value: string \| null \| undefined; }>` |


## Slots

| Slot | Description                            |
| ---- | -------------------------------------- |
|      | Used to add multiple radio components. |


## CSS Custom Properties

| Name               | Description           |
| ------------------ | --------------------- |
| `--asterisk-color` | color of the asterisk |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
