import { Component, Element, Event, EventEmitter, Host, Prop, Watch, h } from '@stencil/core';

/**
 * @slot  - Used to add multiple radio components.
 */

@Component({
  tag: 'road-radio-group',
  styleUrl: 'radio-group.css',
  scoped: true,
})
export class RadioGroup {

  @Element() el!: HTMLRoadRadioGroupElement;

  /**
   * The id of checkbox
   */
  @Prop() radioGroupId: string = `road-radio-group-${radioGroupIds++}`;

  /**
   * If `true`, the radios can be deselected.
   */
  @Prop() allowEmptySelection = false;

  /**
   * The name of the control, which is submitted with the form data.
   */
  @Prop() name: string = this.radioGroupId;

  /**
   * the value of the radio group.
   */
  @Prop({ mutable: true }) value?: any | null;

  /**
   * Label for the field
   */
  @Prop() label?: string;

  /**
   * add an asterisk to the label of the field
   */
   @Prop() asterisk?: boolean = false;

  /**
   * Label for the field
   */
  @Prop({reflect: true}) ariaLabel: string = `${this.radioGroupId}-label`;

  /**
   * Error message for the radio group
   */
  @Prop({ mutable: true }) error?: string;

  /**
   * Helper message for the radio group
   */
  @Prop() helper?: string;

  @Watch('value')
  valueChanged(value: any | undefined) {
    this.roadchange.emit({ value });
    this.roadChange.emit({ value });
  }

  /**
   * Emitted when the value has changed.
   */
  @Event() roadchange!: EventEmitter<{
    value: string | undefined | null
  }>;
  /** @internal */
  @Event() roadChange!: EventEmitter<{
    value: string | undefined | null
  }>;

  private onClick = (ev: Event) => {
    const selectedRadio = ev.target && (ev.target as HTMLElement).closest('road-radio');
    if (selectedRadio) {
      const currentValue = this.value;
      const newValue = (selectedRadio as HTMLRoadRadioElement).value;
      if (newValue !== currentValue) {
        this.value = newValue;
      } else if (this.allowEmptySelection) {
        this.value = undefined;
      }
      this.error = undefined;
    }
  };

  @Watch('error')
  errorChanged() {
    if (this.error !== undefined && this.error !== '') {
      this.el.querySelectorAll('road-radio').forEach(item => item.error = true);
    } else {
      this.el.querySelectorAll('road-radio').forEach(item => item.error = false);
    }
  }

  componentWillLoad() {
    if (this.error !== undefined && this.error !== '') {
      this.el.querySelectorAll('road-radio').forEach(item => item.error = true);
    }
  }

  render() {
    const labelId = `${this.radioGroupId}-label`;
    const isInvalidClass = this.error !== undefined && this.error !== '' ? 'is-invalid' : '';

    return (
      <Host
        role="radiogroup"
        class={`form-group ${isInvalidClass}`}
        aria-label={this.ariaLabel}
        onClick={this.onClick}
      >
        {this.label && <p class="text-content" id={labelId}>{this.label} {this.asterisk && <span class="asterisk">*</span>}</p>}
        <slot/>
        {this.error && this.error !== '' && <p class="invalid-feedback">{this.error}</p>}
        {this.helper && this.helper !== '' && <p class="helper">{this.helper}</p>}
      </Host>
    );
  }
}

let radioGroupIds = 0;
