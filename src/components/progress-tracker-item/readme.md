# road-carousel-item

The Carousel Item component is a child component of [Carousel](../carousel). The template
should be written as `road-carousel-item`. Any slide content should be written
in this component and it should be used in conjunction with [Carousel](../carousel).

See the [Carousel API Docs](../carousel) for more usage information.

<!-- Auto Generated Below -->


## Slots

| Slot | Description                                                                                                                                                                                                                                                                                                                                                                                       |
| ---- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|      | Content of the progress tracker item it should be `<div class="progress-tracker-link">` `<span class="progress-tracker-circle"></span>` `<span class="progress-tracker-line"></span>` `</div>` `<div class="progress-tracker-item-content">` `<road-label class="progress-tracker-title">Label</road-label>` `<road-label class="progress-tracker-description">Description</road-label>` `</div>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
