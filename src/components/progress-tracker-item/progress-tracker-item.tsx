import { Component, Host, h } from '@stencil/core';

/**
 * @slot - Content of the progress tracker item
 * it should be `<div class="progress-tracker-link">`
 *`<span class="progress-tracker-circle"></span>`
 *`<span class="progress-tracker-line"></span>`
 *`</div>`
 *`<div class="progress-tracker-item-content">`
 *`<road-label class="progress-tracker-title">Label</road-label>`
 *`<road-label class="progress-tracker-description">Description</road-label>`
 *`</div>`
 */

@Component({
  tag: 'road-progress-tracker-item',
  styleUrl: 'progress-tracker-item.css',
})
export class ProgressTrackerItem {

  render() {
    return (
      <Host>
        <slot/>
      </Host>
    );
  }
}
