import { Component, Host, h } from '@stencil/core';

/**
 * @slot  - Used to be able to add multiple columns for a row.
 */

@Component({
  tag: 'road-row',
  styleUrl: 'row.css',
  shadow: true,
})
export class Row {

  render() {
    return (
      <Host>
        <slot/>
      </Host>
    );
  }

}
