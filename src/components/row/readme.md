# road-row

Rows are horizontal components of the [grid](../grid) system and contain varying numbers of
[columns](../col). They ensure the columns are positioned properly.

See [Grid Layout](../grid) for more information.


## Row Alignment

By default, columns will stretch to fill the entire height of the row and wrap when necessary. Rows are [flex containers](https://developer.mozilla.org/en-US/docs/Glossary/Flex_Container), so there are several [CSS classes](/docs/layout/css-utilities#flex-container-properties) that can be applied to a row to customize this behavior.

<!-- Auto Generated Below -->


## Slots

| Slot | Description                                        |
| ---- | -------------------------------------------------- |
|      | Used to be able to add multiple columns for a row. |


## Dependencies

### Used by

 - [road-duration](../duration)

### Graph
```mermaid
graph TD;
  road-duration --> road-row
  style road-row fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
