import type { HTMLProps } from "react";

declare module "react/jsx-runtime" {
  namespace JSX {
    interface IntrinsicElements {
      ["road-accordion"]: HTMLProps<AccordionElement>;
      ["road-alert"]: HTMLProps<AlertElement>;
      ["road-area-code"]: HTMLProps<AreaCodeElement>;
      ["road-asset"]: HTMLProps<AssetElement>;
      ["road-autocomplete"]: HTMLProps<AutocompleteElement>;
      ["road-avatar"]: HTMLProps<AvatarElement>;
      ["road-badge"]: HTMLProps<BadgeElement>;
      ["road-banner"]: HTMLProps<BannerElement>;
      ["road-button"]: HTMLProps<ButtonElement> & { size?: string };
      ["road-button-floating"]: HTMLProps<ButtonFloatingElement>;
      ["road-card"]: HTMLProps<CardElement>;
      ["road-carousel"]: HTMLProps<CarouselElement>;
      ["road-carousel-item"]: HTMLProps<CarouselItemElement>;
      ["road-checkbox"]: HTMLProps<CheckboxElement>;
      ["road-chip"]: HTMLProps<ChipElement>;
      ["road-col"]: HTMLProps<ColElement>;
      ["road-collapse"]: HTMLProps<CollapseElement>;
      ["road-content-card"]: HTMLProps<ContentCardElement>;
      ["road-counter"]: HTMLProps<CounterElement>;
      ["road-dialog"]: HTMLProps<DialogElement>;
      ["road-drawer"]: HTMLProps<DrawerElement>;
      ["road-dropdown"]: HTMLProps<DropdownElement>;
      ["road-duration"]: HTMLProps<DurationElement>;
      ["road-flap"]: HTMLProps<FlapElement>;
      ["road-global-navigation"]: HTMLProps<GlobalNavigationElement>;
      ["road-grid"]: HTMLProps<GridElement>;
      ["road-icon"]: HTMLProps<IconElement> & { size?: string };
      ["road-illustration"]: HTMLProps<IllustrationElement>;
      ["road-img"]: HTMLProps<ImgElement>;
      ["road-input"]: HTMLProps<InputElement>;
      ["road-input-group"]: HTMLProps<InputGroupElement>;
      ["road-item"]: HTMLProps<ItemElement>;
      ["road-label"]: HTMLProps<LabelElement>;
      ["road-list"]: HTMLProps<ListElement>;
      ["road-modal"]: HTMLProps<ModalElement>;
      ["road-navbar"]: HTMLProps<NavbarElement>;
      ["road-navbar-item"]: HTMLProps<NavbarItemElement> & { tab?: string }; // Ajout de `tab`
      ["road-phone-number-input"]: HTMLProps<PhoneNumberInputElement>;
      ["road-plate-number"]: HTMLProps<PlateNumberElement>;
      ["road-profil-dropdown"]: HTMLProps<ProfilDropdownElement>;
      ["road-progress"]: HTMLProps<ProgressElement>;
      ["road-progress-indicator-horizontal"]: HTMLProps<ProgressIndicatorHorizontalElement>;
      ["road-progress-indicator-vertical"]: HTMLProps<ProgressIndicatorVerticalElement>;
      ["road-progress-indicator-vertical-item"]: HTMLProps<ProgressIndicatorVerticalItemElement>;
      ["road-progress-tracker"]: HTMLProps<ProgressTrackerElement>;
      ["road-progress-tracker-item"]: HTMLProps<ProgressTrackerItemElement>;
      ["road-radio"]: HTMLProps<RadioElement>;
      ["road-radio-group"]: HTMLProps<RadioGroupElement>;
      ["road-range"]: HTMLProps<RangeElement>;
      ["road-rating"]: HTMLProps<RatingElement>;
      ["road-row"]: HTMLProps<RowElement>;
      ["road-segmented-button"]: HTMLProps<SegmentedButtonElement>;
      ["road-segmented-button-bar"]: HTMLProps<SegmentedButtonBarElement>;
      ["road-segmented-buttons"]: HTMLProps<SegmentedButtonsElement>;
      ["road-select"]: HTMLProps<SelectElement>;
      ["road-select-filter"]: HTMLProps<SelectFilterElement>;
      ["road-skeleton"]: HTMLProps<SkeletonElement>;
      ["road-spinner"]: HTMLProps<SpinnerElement>;
      ["road-switch"]: HTMLProps<SwitchElement>;
      ["road-tab"]: HTMLProps<TabElement> & { tab?: string }; // Ajout pour `road-tab`
      ["road-tab-bar"]: HTMLProps<TabBarElement>;
      ["road-tab-button"]: HTMLProps<TabButtonElement>;
      ["road-table"]: HTMLProps<TableElement>;
      ["road-tabs"]: HTMLProps<TabsElement>;
      ["road-tag"]: HTMLProps<TagElement>;
      ["road-text"]: HTMLProps<TextElement>;
      ["road-textarea"]: HTMLProps<TextareaElement>;
      ["road-toast"]: HTMLProps<ToastElement>;
      ["road-toggle"]: HTMLProps<ToggleElement>;
      ["road-toolbar"]: HTMLProps<ToolbarElement>;
      ["road-toolbar-title"]: HTMLProps<ToolbarTitleElement>;
      ["road-toolbar-title-page"]: HTMLProps<ToolbarTitlePageElement>;
      ["road-tooltip"]: HTMLProps<TooltipElement>;
    }
  }
}