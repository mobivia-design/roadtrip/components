import { BADGE } from '@geometricpanda/storybook-addon-badges';

export const parameters = {
  controls: {
    expanded: true,
    hideNoControlsWarning: true
  },
  themes: [
    { name: 'Mobivia', class: 'mobivia-theme', color: '#B40063' },
    { name: 'Norauto', class: 'norauto-theme', color: '#002B6F', default: true },
    { name: 'Auto5', class: 'auto5-theme', color: '#E00008' },
    { name: 'ATU', class: 'atu-theme', color: '#bb1e10' },
    { name: 'Midas', class: 'midas-theme', color: '#FFD300' }
  ],
  docs: {
    source: { 
      state: 'open',
    },
  },
  options: {
    storySort: {
      method: 'alphabetical', // Cette ligne indique que les stories seront triées par ordre alphabétique
      order: ['Intro', 'Components', 'Other'], // Optionnel, pour organiser les sections si nécessaire
    },
  },
  badges: [BADGE.DEFAULT, BADGE.STABLE],
  backgrounds: {
    default: 'white',
    values: [
      { 
        name: 'white', 
        value: '#ffffff'
      },
      { 
        name: 'grey', 
        value: '#f4f5fa' 
      },
    ],
    grid: {
      disable: true
    }
  },
  viewport: { 
    viewports: {
      mobile: {
        name: 'Mobile',
        styles: {
          width: '360px',
          height: '640px',
        },
      },
      tablet: {
        name: 'Tablet',
        styles: {
          width: '768px',
          height: '1024px',
        },
      },
      desktop: {
        name: 'Desktop',
        styles: {
          width: '1600px',
          height: '864px',
        },
      },
    },
    defaultViewport: 'mobile',
  },
};
