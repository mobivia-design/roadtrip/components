module.exports = {
  "stories": [
    "../src/components/**/*.mdx",
    "../src/components/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  staticDirs: ['../dist/roadtrip/'],
  "addons": [{
    name: '@storybook/addon-essentials',
    options: {
      measure: false,
      outline: false,
    }
  }, "@storybook/addon-a11y", "storybook-addon-themes", "@geometricpanda/storybook-addon-badges", "@storybook/addon-webpack5-compiler-babel", "@chromatic-com/storybook"],

  plugins: ["@babel/plugin-proposal-private-property-in-object", { "loose": true }],

  framework: {
    name: "@storybook/web-components-webpack5",
    options: {}
  },

  docs: {
    autodocs: true
  },
  webpackFinal: async (config) => {
    config.resolve.alias['@custom-elements'] = require('path').resolve(__dirname, '../custom-elements.json');
    return config;
  }
}
