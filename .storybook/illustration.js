export const illustration = [
    'apps',
    'dashboard-illustration',
    'delivery-not-found',
    'failure',
    'inactive',
    'log-out-illustration',
    'maintenance',
    'no-result',
    'not-found',
    'redirect',
    'wait'
  ];