import {addons} from '@storybook/addons';
import {create} from '@storybook/theming/create';

const theme = create({
  base: 'dark',

  colorPrimary: '#f4f5fa',
  colorSecondary: '#B40063',

  // UI
  appBg: '#32394D',
  appContentBg: '#32394D',

  // Typography
  fontBase: '"Muli", sans-serif',

  // Text colors
  textColor: '#f4f5fa',
  textInverseColor: '#ffffff',

  // Toolbar default and active colors
  barTextColor: '#edeef7',
  barSelectedColor: '#B40063',
  barBg: '#191d26',

  // BRAND
  brandTitle: 'Roadtrip Components',
});

addons.setConfig({
  theme,
  panelPosition: 'right',
  sidebar: {
    showRoots: true,
  },
  toolbar: {
    title: { hidden: false },
    zoom: { hidden: true, },
    eject: { hidden: true, },
    copy: { hidden: true, },
    fullscreen: { hidden: true, },
  },
});