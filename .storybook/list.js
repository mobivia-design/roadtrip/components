export const list = [
  'alert-danger-outline',
  'alert-danger',
  'alert-error-outline-color',
  'alert-error-outline',
  'alert-error',
  'alert-info-outline-color',
  'alert-info-outline',
  'alert-info',
  'alert-notification-alarm-outline',
  'alert-notification-alarm',
  'alert-notification-outline',
  'alert-notification-warning-outline',
  'alert-notification-warning',
  'alert-notification',
  'alert-question-outline-color',
  'alert-question-outline',
  'alert-question',
  'alert-success-outline',
  'alert-success',
  'alert-valid-outline-color',
  'alert-valid-outline',
  'alert-valid',
  'alert-warning-outline-color',
  'alert-warning-outline',
  'alert-warning',
  'alternator-color',
  'alternator',
  'app',
  'archive-outline-color',
  'archive-outline',
  'archive',
  'arrow-drop-down',
  'arrow-drop',
  'arrow-fullscreen-collapse',
  'arrow-fullscreen-expand',
  'arrow-swap',
  'arrow',
  'attachment',
  'axle-support-color',
  'axle-support',
  'bacterium-color',
  'bacterium',
  'battery-charger-color',
  'battery-charger',
  'battery-color',
  'battery-elctric-color',
  'battery-elctric-outline',
  'battery-electric-color',
  'battery-electric-outline',
  'battery-recycle-color',
  'battery-recycle',
  'battery-start-stop-color',
  'battery-start-stop',
  'battery',
  'best-customer',
  'bike-inflation-color',
  'bike-inflation-outline',
  'book-service-color',
  'book-service',
  'book-technical-color',
  'book-technical',
  'brake-color',
  'brake-outline-color',
  'brake-outline',
  'brake-pad-color',
  'brake-pad',
  'brake',
  'bulk-3d-color',
  'bulk-3d',
  'bundle-color',
  'bundle',
  'cable-color',
  'cable',
  'calculator-color',
  'calculator',
  'call-center-color',
  'call-center',
  'camera-off-outline-color',
  'camera-off-outline',
  'camera-off',
  'camera-outline-color',
  'camera-outline',
  'camera',
  'car-door-color',
  'car-door',
  'car-identity-paper-open',
  'car-identity-paper',
  'car-petrol-off-color',
  'car-petrol-off-outline',
  'car-petrol-off',
  'car-petrol-outline-color',
  'car-petrol-outline',
  'car-petrol',
  'car-power-color',
  'car-power-outline',
  'car-seat-baby',
  'car-seat',
  'carpet-color',
  'carpet',
  'certified-outline-color',
  'certified-outline',
  'certified-warning-outline-color',
  'certified-warning-outline',
  'certified-warning',
  'certified',
  'check-list-color',
  'check-list',
  'check-point-color',
  'check-point-location-color',
  'check-point-location',
  'check-point',
  'check-small',
  'check-wide',
  'cleanser-color',
  'cleanser',
  'cloud-download-color',
  'cloud-download',
  'cloud-outline',
  'cloud-upload-color',
  'cloud-upload',
  'clutch-color',
  'clutch',
  'collapse',
  'comodo',
  'control-switch-color',
  'control-switch',
  'cookie',
  'coolbox-color',
  'coolbox',
  'cooling-color',
  'cooling',
  'cover-color',
  'cover',
  'covid-stop-color',
  'covid-stop',
  'crown',
  'dashboard',
  'data-add-color',
  'data-add',
  'data-bar-color',
  'data-bar',
  'data-color',
  'data-tech-outline-color',
  'data-tech-outline',
  'data-tech',
  'data-up-color',
  'data-up',
  'data',
  'delete-forever-color',
  'delete-forever',
  'device-color',
  'device-laptop-color',
  'device-laptop',
  'device-rotate-color',
  'device-rotate',
  'device-smartphone-color',
  'device-smartphone-music-color',
  'device-smartphone-music',
  'device-smartphone-sms-color',
  'device-smartphone-sms-wrench-color',
  'device-smartphone-sms-wrench',
  'device-smartphone-sms',
  'device-smartphone',
  'device-tablet-color',
  'device-tablet',
  'device',
  'diagnostic-color',
  'diagnostic',
  'discount-prct-outline-color',
  'discount-prct-outline',
  'discount-prct-single',
  'discount-prct',
  'discount-ticket-color',
  'discount-ticket',
  'discount-worshop',
  'download-color',
  'download',
  'edit-outline-color',
  'edit-outline',
  'edit-pen-color',
  'edit-pen-outline',
  'edit-pen',
  'edit',
  'electric-color',
  'electric-outline',
  'electric-solid',
  'electric-plug-13pin-color',
  'electric-plug-13pin',
  'electric-plug-7pin-color',
  'electric-plug-7pin',
  'electric',
  'electricity-outline',
  'electricity',
  'electronic-diagnostic',
  'electronic-ethanol',
  'electronic-outline',
  'electronic',
  'engine-color',
  'engine-electric-color',
  'engine-electric',
  'engine-lubrication-color',
  'engine-lubrication',
  'engine-piston-color',
  'engine-piston',
  'engine',
  'exclamation-small',
  'exclamation-wide',
  'exhaust-pipe-color',
  'exhaust-pipe',
  'factory-color',
  'factory',
  'fidelity-card-color',
  'fidelity-card-wallet-color',
  'fidelity-card-wallet',
  'fidelity-card',
  'file-business-outline-color',
  'file-business-outline',
  'file-catalog-color',
  'file-catalog',
  'file-contract-color',
  'file-contract-outline',
  'file-copy-color',
  'file-copy',
  'file-edit-color',
  'file-edit',
  'file-excel-color',
  'file-excel-outline',
  'file-excel',
  'file-list-outline-color',
  'file-list-outline',
  'file-outline',
  'file-pdf-color',
  'file-pdf-outline-color',
  'file-pdf-outline',
  'file-pdf',
  'file-powerpoint-color',
  'file-powerpoint',
  'file-technical-color',
  'file-technical',
  'file-word-color',
  'file-word',
  'filter-color',
  'filter-particle-color',
  'filter-particle',
  'filter-sport-color',
  'filter-sport',
  'filter',
  'fingerprint',
  'flag-argentina',
  'flag-austria',
  'flag-belgium',
  'flag-france',
  'flag-germany',
  'flag-italy',
  'flag-poland',
  'flag-portugal',
  'flag-romania',
  'flag-russia',
  'flag-spain',
  'flag-sweden',
  'flower-color',
  'flower',
  'fuel-air-supply-color',
  'fuel-air-supply',
  'fuel-door-color',
  'fuel-door',
  'funding-best-price-color',
  'funding-best-price',
  'funding-outline-color',
  'funding-outline',
  'funding-small-color',
  'funding-small-outline',
  'funding-small',
  'funding',
  'fuse-color',
  'fuse',
  'garage-color',
  'garage-house-color',
  'garage-house',
  'garage',
  'gas-5-color',
  'gas-5-outline',
  'gas-5',
  'gasket-outline-color',
  'gasket-outline',
  'gasket',
  'gearbox-color',
  'gearbox',
  'gift-color',
  'gift-outline',
  'gift',
  'gps-color',
  'gps',
  'handicap',
  'hear-color',
  'hear',
  'helmet-bike-color',
  'helmet-bike',
  'helmet-cross-color',
  'helmet-cross',
  'helmet-full-color',
  'helmet-full',
  'helmet-half-jet-color',
  'helmet-half-jet',
  'helmet-jet-color',
  'helmet-jet',
  'helmet-modular-color',
  'helmet-modular',
  'helmet-skate-color',
  'helmet-skate',
  'home',
  'hook-hitch-color',
  'hook-hitch-outline',
  'hook-hitch',
  'house-color',
  'house',
  'ice-color',
  'ice-outline-color',
  'ice-outline',
  'ice',
  'identity-card-color',
  'identity-card',
  'identity-driver-licence-color',
  'identity-driver-licence-outline',
  'identity-passport-color',
  'identity-passport-outline',
  'keep-in-repair-outline',
  'keep-in-repair',
  'key-outline-color',
  'key-outline',
  'key',
  'leaf',
  'license-plate-be',
  'license-plate-es',
  'license-plate-eu',
  'license-plate-fr',
  'license-plate-it',
  'license-plate-pl',
  'license-plate-po',
  'license-plate-ru',
  'license-plate-star-be',
  'license-plate-star-es',
  'license-plate-star-eu',
  'license-plate-star-fr',
  'license-plate-star-it',
  'license-plate-star-pl',
  'license-plate-star-po',
  'light-beam-back-color',
  'light-beam-back',
  'light-beam-color',
  'light-beam-day-color',
  'light-beam-day',
  'light-beam-fog-color',
  'light-beam-fog',
  'light-beam-high-color',
  'light-beam-high',
  'light-beam-numberplate-color',
  'light-beam-numberplate',
  'light-beam-signal-color',
  'light-beam-signal',
  'light-beam-stop-color',
  'light-beam-stop',
  'light-beam-turn-color',
  'light-beam-turn',
  'light-beam',
  'light-box-color',
  'light-box',
  'light-bulb-color',
  'light-bulb',
  'light-color',
  'light-master-color',
  'light-master',
  'light-sidelight-color',
  'light-sidelight',
  'light',
  'link-broken-color',
  'link-broken',
  'link-color',
  'link',
  'load-cached-color',
  'load-cached',
  'load-history-outline',
  'load-history-color',
  'load-history',
  'load-refresh',
  'load-sync-problem-color',
  'load-sync-problem',
  'load-update-color',
  'load-update',
  'location-compass-outline',
  'location-compass',
  'location-direction',
  'location-navigation-outline',
  'location-navigation',
  'location-path',
  'location-pin-all-outline-color',
  'location-pin-all-outline',
  'location-pin-all',
  'location-pin-garage-auto5',
  'location-pin-garage-norauto',
  'location-pin-garage',
  'location-pin-number',
  'location-pin-outline-color',
  'location-pin-outline',
  'location-pin',
  'location-target-color',
  'location-target',
  'lock-secure-open',
  'lock-secure',
  'log-in-color',
  'log-in-solid',
  'log-in-outline',
  'log-out-door',
  'log-out',
  'log-out-color',
  'log-out-solid',
  'log-out-outline',
  'lowering-down-color',
  'lowering-up-color',
  'lowering',
  'mail-outline-send-color',
  'mail-outline-send',
  'mail-outline',
  'mail',
  'meeting-check-color',
  'meeting-check',
  'meeting-color',
  'meeting-maintenance-color',
  'meeting-maintenance',
  'meeting-off-color',
  'meeting-off',
  'meeting-online-color',
  'meeting-online',
  'meeting-week-color',
  'meeting-week',
  'meeting',
  'mic-off-outline-color',
  'mic-off-outline',
  'mic-off',
  'mic-outline-color',
  'mic-outline',
  'mic',
  'more-horizontal',
  'more-vertical',
  'motor-starter-color',
  'motor-starter-outline',
  'multi-service-color',
  'multi-service-outline',
  'multi-service-solid',
  'multi-service',
  'navigation-add-less',
  'navigation-add-more',
  'navigation-app',
  'navigation-back',
  'navigation-chevron',
  'navigation-close',
  'navigation-dashboard-outline-color',
  'navigation-dashboard-outline',
  'navigation-dashboard',
  'navigation-filter',
  'navigation-grid',
  'navigation-home-outline',
  'navigation-home',
  'navigation-launch-color',
  'navigation-launch-outline',
  'navigation-launch',
  'navigation-layers-outline-color',
  'navigation-layers-outline',
  'navigation-layers',
  'navigation-list',
  'navigation-menu',
  'navigation-more',
  'navigation-setting-outline-color',
  'navigation-setting-outline',
  'navigation-setting',
  'nose-odour-color',
  'nose-odour',
  'nut-outline-color',
  'nut-outline',
  'nut',
  'oil-can-outline-color',
  'oil-can-outline',
  'oil-can-small-color',
  'oil-can-small',
  'oil-can',
  'oil-change-color',
  'oil-change',
  'oil-color',
  'oil-cruet-color',
  'oil-cruet',
  'oil',
  'paiment-store',
  'paint-color',
  'paint-spary-gun',
  'paint',
  'parking-break-color',
  'parking-break',
  'pass-maintain-color',
  'pass-maintain-logo-color',
  'pass-maintain-logo-outline-color',
  'pass-maintain-logo-outline',
  'pass-maintain-logo',
  'pass-maintain',
  'password-reset',
  'payment-bill-color',
  'payment-bill',
  'payment-card-color',
  'payment-card',
  'payment-cash-color',
  'payment-cash',
  'payment-delivery-color',
  'payment-delivery',
  'payment-safety-color',
  'payment-safety',
  'payment-store-color',
  'payment-store',
  'people-add-outline-color',
  'people-add-outline',
  'people-add',
  'people-car-fleet-color',
  'people-car-fleet',
  'people-card-color',
  'people-card',
  'people-best-customer-color',
  'people-best-customer-outline',
  'people-community-outline-color',
  'people-community-outline',
  'people-community',
  'people-coworker-color',
  'people-coworker-norauto-color',
  'people-coworker-norauto',
  'people-coworker',
  'people-customer-outline',
  'people-customer',
  'people-customer-review-outline',
  'people-customer-review-color',
  'people-disability-wheelchair-color',
  'people-disability-wheelchair-outline',
  'people-disability-wheelchair-solid',
  'people-group-outline-color',
  'people-group-outline',
  'people-group',
  'people-home-outline-color',
  'people-home-outline',
  'people-home',
  'people-outline',
  'people-vip-outline-color',
  'people-vip-outline',
  'people-vip',
  'people',
  'petrol-outline-color',
  'petrol-outline',
  'petrol',
  'phone-ouline',
  'phone',
  'picture-color',
  'picture',
  'player-arrow-play',
  'player-break',
  'player-dvd-color',
  'player-dvd',
  'player-irregular',
  'player-pause',
  'player-radio-music-outline-color',
  'player-radio-music-outline',
  'player-radio-outline-color',
  'player-radio-outline',
  'player-radio',
  'player-regular',
  'player-touchscreen-color',
  'player-touchscreen-motorized-color',
  'player-touchscreen-motorized',
  'player-touchscreen',
  'player-unlimited',
  'player-video-color',
  'player-video',
  'power-color',
  'power',
  'print-outline-color',
  'print-outline',
  'print',
  'purchase-history-color',
  'purchase-history',
  'radiator-color',
  'radiator',
  'rent-back-color',
  'rent-back',
  'rent-color',
  'rent',
  'roof-rack-cross-bars-color',
  'roof-rack-cross-bars-outline',
  'roof-rack-cross-bars',
  'rubber-outline-color',
  'rubber-outline',
  'rubber',
  'save-disk-color',
  'save-disk',
  'scan-bar-code-color',
  'scan-bar-code',
  'scan-color',
  'scan-qr-code-color',
  'scan-qr-code',
  'scan-vehicle-card',
  'scan',
  'scissor-color',
  'scissor-outline',
  'scissor',
  'search',
  'seat-bike-front-color',
  'seat-bike-front-outline',
  'seat-bike-rear-color',
  'seat-bike-rear-junior-color',
  'seat-bike-rear-junior-outline',
  'seat-bike-rear-outline',
  'seat-bike-trailer-color',
  'seat-bike-trailer-outline',
  'secure-back-door-lock-color',
  'secure-back-door-lock',
  'secure-cable-color',
  'secure-cable-lock-color',
  'secure-cable-lock',
  'secure-cable',
  'secure-chain-color',
  'secure-chain',
  'secure-cuff-lock-color',
  'secure-cuff-lock',
  'secure-disk-lock-color',
  'secure-disk-lock',
  'secure-folding-lock-color',
  'secure-folding-lock',
  'secure-frame-lock-color',
  'secure-frame-lock',
  'secure-gearbox-color',
  'secure-gearbox',
  'secure-level-first-color',
  'secure-level-first',
  'secure-level-hours-color',
  'secure-level-hours',
  'secure-level-minute-color',
  'secure-level-minute',
  'secure-level-night-and-day-color',
  'secure-level-night-and-day',
  'secure-level-second-color',
  'secure-level-second',
  'secure-level-third-color',
  'secure-level-third',
  'secure-lock-color',
  'secure-lock-open-color',
  'secure-lock-open',
  'secure-lock',
  'secure-pedal-color',
  'secure-pedal',
  'secure-steering-color',
  'secure-steering-dashboard-color',
  'secure-steering-dashboard',
  'secure-steering-pedal-color',
  'secure-steering-pedal',
  'secure-steering',
  'secure-u-lock-color',
  'secure-u-lock',
  'secure-wheel-color',
  'secure-wheel',
  'security-belt-outline-color',
  'security-belt-outline',
  'security-belt',
  'security-color',
  'security',
  'sensor-color',
  'sensor',
  'setting',
  'shipping-checked-color',
  'shipping-checked',
  'shipping-click-and-collect-color',
  'shipping-click-and-collect',
  'shipping-color',
  'shipping-express-color',
  'shipping-express',
  'shipping-locator-color',
  'shipping-locator',
  'shipping-locker-color',
  'shipping-locker',
  'shipping-outline-color',
  'shipping-outline',
  'shipping-return-color',
  'shipping-return',
  'shipping',
  'shock-absorber-body-part-color',
  'shock-absorber-color',
  'shock-absorber',
  'shop-color',
  'shop',
  'shopping-cart-add-color',
  'shopping-cart-add',
  'shopping-cart-sad-color',
  'shopping-cart-sad',
  'shopping-cart',
  'sign',
  'sissor',
  'smart-repair-color',
  'smart-repair-outline',
  'snowflake-color',
  'snowflake-outline-color',
  'snowflake-outline',
  'snowflake-solid-color',
  'snowflake',
  'social-facebook-outline',
  'social-facebook-badge',
  'social-facebook-color',
  'social-facebook-solid',
  'social-facebook',
  'social-google-outline',
  'social-google-badge',
  'social-google-color',
  'social-google-solid',
  'social-google',
  'social-instagram-outline',
  'social-instagram-badge',
  'social-instagram-color',
  'social-instagram-solid',
  'social-instagram',
  'social-tiktok-badge',
  'social-linkedin-badge',
  'social-linkedin-color',
  'social-linkedin-solid',
  'social-tiktok-color',
  'social-tiktok-solid',
  'social-twitter-outline',
  'social-twitter',
  'social-youtube-outline',
  'social-youtube-badge',
  'social-youtube-color',
  'social-youtube-solid',
  'social-youtube',
  'sound-0-outline-color',
  'sound-0-outline',
  'sound-0',
  'sound-1-outline-color',
  'sound-1-outline',
  'sound-1',
  'sound-2-outline-color',
  'sound-2-outline',
  'sound-2',
  'sound-3-outline-color',
  'sound-3-outline',
  'sound-3',
  'sound-alarm',
  'spark-color',
  'spark-outline',
  'spark',
  'speak-advice-outline-color',
  'speak-advice-outline',
  'speak-advice',
  'speak-outline-color',
  'speak-outline',
  'speak',
  'speaker-color',
  'speaker',
  'star-half-color',
  'star-half',
  'star-smile',
  'star',
  'star-stop-color',
  'star-stop-outline',
  'station-electric-outline-color',
  'station-electric-outline',
  'station-biofuel-color',
  'station-biofuel-outline',
  'station-biofuel-solid',
  'steering-control-color',
  'steering-control-flat-color',
  'steering-control-flat',
  'steering-control-four-spoke-color',
  'steering-control-four-spoke',
  'steering-control',
  'sticker-certified-pickerl-color',
  'sticker-certified-pickerl-outline',
  'style-color',
  'style',
  'suspension',
  'thumb-outline',
  'thumb',
  'time-access-color',
  'time-access',
  'timer-1h-color',
  'timer-1h',
  'timer-1u-color',
  'timer-1u-outline',
  'timer-2h-color',
  'timer-2h',
  'timer-color',
  'timer-hourglass-outline-color',
  'timer-hourglass-outline',
  'timer-hourglass',
  'timer',
  'timing-belt-color',
  'timing-belt',
  'tire-all-terrain-color',
  'tire-all-terrain-outline',
  'tire-all-terrain',
  'tire-alone',
  'tire-color',
  'tire-diameter-color',
  'tire-diameter-outline',
  'tire-diameter',
  'tire-energy-color',
  'tire-energy-outline',
  'tire-energy',
  'tire-flat-color',
  'tire-flat-outline',
  'tire-flat',
  'tire-hotel-outline-color',
  'tire-hotel-outline',
  'tire-hotel',
  'tire-hub-cap-color',
  'tire-hub-cap-outline',
  'tire-hub-cap',
  'tire-ice-color',
  'tire-ice-outline',
  'tire-ice',
  'tire-inflation-color',
  'tire-inflation-outline',
  'tire-inflation-solid',
  'tire-outline',
  'tire-parallelism-color',
  'tire-parallelism',
  'tire-rain-color',
  'tire-rain-outline',
  'tire-rain',
  'tire-received-color',
  'tire-received-outline',
  'tire-received-solid',
  'tire-rim-color',
  'tire-rim-outline',
  'tire-rim',
  'tire-seize',
  'tire-sidewall-height-color',
  'tire-sidewall-height-outline',
  'tire-sidewall-height',
  'tire-snow-chain-color',
  'tire-snow-chain-outline',
  'tire-snow-chain',
  'tire-snow-color',
  'tire-snow-outline',
  'tire-snow-studded-color',
  'tire-snow-studded-outline',
  'tire-snow-studded',
  'tire-snow',
  'tire-sound-color',
  'tire-sound-outline',
  'tire-sound',
  'tire-steel',
  'tire-sun-color',
  'tire-sun-outline',
  'tire-sun-snow-color',
  'tire-sun-snow-outline',
  'tire-sun-snow',
  'tire-sun',
  'tire-suspension-color',
  'tire-suspension-outline',
  'tire-suspension',
  'tire-swap-color',
  'tire-swap-outline',
  'tire-swap-solid',
  'tire-transfer-color',
  'tire-transfer-outline',
  'tire-transfer-solid',
  'tire-wheel-diameter-color',
  'tire-wheel-diameter-outline',
  'tire-wheel-diameter',
  'tire-wheel-rim-color',
  'tire-wheel-rim-outline',
  'tire-wheel-rim',
  'tire-wheel-steel-color',
  'tire-wheel-steel-outline',
  'tire-wheel-steel',
  'tire-witdh-color',
  'tire-witdh-outline',
  'tire-witdh',
  'tire',
  'touch',
  'translate',
  'transmission-color',
  'transmission',
  'trash-bin-color',
  'trash-bin',
  'travel-backpack-color',
  'travel-backpack',
  'travel-bag-color',
  'travel-bag',
  'travel-briefcase-color',
  'travel-briefcase',
  'travel-rolling-suitcase-color',
  'travel-rolling-suitcase',
  'travel-suitcase-color',
  'travel-suitcase',
  'travel',
  'trick-outline-color',
  'trick-outline',
  'trick',
  'trust',
  'vehicle-3-doors-color',
  'vehicle-3-doors-hatchback-color',
  'vehicle-3-doors',
  'vehicle-4wd-color',
  'vehicle-4wd-outline',
  'vehicle-4wd-solid',
  'vehicle-5-doors-color',
  'vehicle-5-doors-hatchback-color',
  'vehicle-5-doors',
  'vehicle-acc-color',
  'vehicle-acc-outline',
  'vehicle-acc-solid',
  'vehicle-active-bike-color',
  'vehicle-active-bike-electric-color',
  'vehicle-active-bike-electric-outline',
  'vehicle-active-bike-outline',
  'vehicle-air-recirculation-color',
  'vehicle-air-recirculation-outline',
  'vehicle-air-recirculation-solid',
  'vehicle-battery-service-color',
  'vehicle-battery-service-outline',
  'vehicle-battery-service-solid',
  'vehicle-bike-color',
  'vehicle-bike-electric-color',
  'vehicle-bike-electric',
  'vehicle-bike-rack-outline-color',
  'vehicle-bike-rack-outline',
  'vehicle-bike-rack',
  'vehicle-bike-repair-color',
  'vehicle-bike-repair-outline',
  'vehicle-bike-repair-solid',
  'vehicle-bike',
  'vehicle-bus-color',
  'vehicle-bus-outline',
  'vehicle-bus-solid',
  'vehicle-camping-car-color',
  'vehicle-camping-car-outline',
  'vehicle-camping-car-solid',
  'vehicle-car-add-outline-color',
  'vehicle-car-add-outline',
  'vehicle-car-add-solid',
  'vehicle-car-add',
  'vehicle-car-all-authorize-outline-color',
  'vehicle-car-all-authorize-outline',
  'vehicle-car-all-outline-color',
  'vehicle-car-all-outline',
  'vehicle-car-all-solid',
  'vehicle-car-all-unauthorize-outline-color',
  'vehicle-car-all-unauthorize-outline',
  'vehicle-car-all',
  'vehicle-car-bodywork-outline-color',
  'vehicle-car-bodywork-outline',
  'vehicle-car-bodywork',
  'vehicle-car-checked-outline-color',
  'vehicle-car-checked-outline',
  'vehicle-car-checked-solid',
  'vehicle-car-checked',
  'vehicle-car-cockpit-color',
  'vehicle-car-cockpit-outline',
  'vehicle-car-cockpit-solid',
  'vehicle-car-color',
  'vehicle-car-electric-outline',
  'vehicle-car-electric-solid',
  'vehicle-car-moto-color',
  'vehicle-car-moto-outline',
  'vehicle-car-moto-solid',
  'vehicle-car-outline-back',
  'vehicle-car-outline-cockpit',
  'vehicle-car-outline-color',
  'vehicle-car-outline-front',
  'vehicle-car-outline-signal',
  'vehicle-car-outline',
  'vehicle-car-sad-color',
  'vehicle-car-sad-outline',
  'vehicle-car-sad-solid',
  'vehicle-car-search-color',
  'vehicle-car-search-outline',
  'vehicle-car-search-solid',
  'vehicle-car-setting-solid',
  'vehicle-car-setting-color',
  'vehicle-car-setting-outline-color',
  'vehicle-car-setting-outline',
  'vehicle-car-setting',
  'vehicle-car-side-outline',
  'vehicle-car-side-tire-behind-color',
  'vehicle-car-side-tire-behind-oultine-color',
  'vehicle-car-side-tire-behind-oultine',
  'vehicle-car-side-tire-behind',
  'vehicle-car-side-tire-front-color',
  'vehicle-car-side-tire-front-outline-color',
  'vehicle-car-side-tire-front-outline',
  'vehicle-car-side-tire-front-solid',
  'vehicle-car-side-tire-front',
  'vehicle-car-side',
  'vehicle-car-solid',
  'vehicle-car-sound-color',
  'vehicle-car-sound-outline',
  'vehicle-car-sound-solid',
  'vehicle-car-unknow-color',
  'vehicle-car-unknow-outline',
  'vehicle-car-unknow-solid',
  'vehicle-car-unknow',
  'vehicle-car-unknowoutline-color',
  'vehicle-car-unselected-color',
  'vehicle-car-unselected-outline-color',
  'vehicle-car-unselected-outline',
  'vehicle-car-unselected-solid',
  'vehicle-car-unselected',
  'vehicle-car',
  'vehicle-caravan-color',
  'vehicle-caravan-outline',
  'vehicle-caravan-solid',
  'vehicle-carport-color',
  'vehicle-carport-outline',
  'vehicle-carport',
  'vehicle-collector-color',
  'vehicle-collector-outline',
  'vehicle-collector-solid',
  'vehicle-delivery-fast-color',
  'vehicle-delivery-fast-outline-color',
  'vehicle-delivery-fast-solid',
  'vehicle-delivery-fast-outline',
  'vehicle-delivery-fast',
  'vehicle-delivery-free-return-color',
  'vehicle-delivery-free-return-outline',
  'vehicle-delivery-free-return-solid',
  'vehicle-delivery-meeting-color',
  'vehicle-delivery-meeting-outline-color',
  'vehicle-delivery-meeting-outline',
  'vehicle-delivery-meeting-solid',
  'vehicle-delivery-meeting',
  'vehicle-delevery-outline',
  'vehicle-delevery-van',
  'vehicle-delevery',
  'vehicle-delivery-color',
  'vehicle-delivery-outline',
  'vehicle-delivery-solid',
  'vehicle-distance-color',
  'vehicle-distance-outline',
  'vehicle-drive-carport-color',
  'vehicle-drive-carport-outline',
  'vehicle-drive-color',
  'vehicle-drive-outline',
  'vehicle-drive',
  'vehicle-micro-color',
  'vehicle-micro-outline',
  'vehicle-micro-solid',
  'vehicle-minivan-color',
  'vehicle-minivan-outline',
  'vehicle-minivan-solid',
  'vehicle-moto-bike-outline-color',
  'vehicle-moto-bike-outline',
  'vehicle-moto-bike',
  'vehicle-moto-color',
  'vehicle-moto-outline-color',
  'vehicle-moto-outline',
  'vehicle-moto-solid',
  'vehicle-mower-color',
  'vehicle-mower-outline',
  'vehicle-mower-solid',
  'vehicle-no-license',
  'vehicle-pickup-van-color',
  'vehicle-pickup-van-outline',
  'vehicle-pickup-van-solid',
  'vehicle-pickup-color',
  'vehicle-pickup-outline',
  'vehicle-pickup-solid',
  'vehicle-quad-color',
  'vehicle-quad-outline',
  'vehicle-quad-solid',
  'vehicle-rally',
  'vehicle-rent-bike-electric-color',
  'vehicle-rent-bike-electric-outline',
  'vehicle-rent-bike-electric-solid',
  'vehicle-rent-car-micro-color',
  'vehicle-rent-car-micro-outline',
  'vehicle-rent-car-micro-solid',
  'vehicle-rent-car-no-license',
  'vehicle-rent-car-color',
  'vehicle-rent-car-outline',
  'vehicle-rent-car-solid',
  'vehicle-rent-trailer-color',
  'vehicle-rent-trailer-outline',
  'vehicle-rent-trailer-solid',
  'vehicle-repair-car-color',
  'vehicle-repair-car-outline',
  'vehicle-repair-car-solid',
  'vehicle-repair-moto-color',
  'vehicle-repair-moto-outline',
  'vehicle-repair-moto-solid',
  'vehicle-repair-pickup-van-color',
  'vehicle-repair-pickup-van-outline',
  'vehicle-repair-pickup-van-solid',
  'vehicle-repair-van-color',
  'vehicle-repair-van-outline',
  'vehicle-repair-van-solid',
  'vehicle-road-grip-color',
  'vehicle-road-grip-outline',
  'vehicle-road-grip-solid',
  'vehicle-roadster-color',
  'vehicle-roadster-outline',
  'vehicle-roadster-solid',
  'vehicle-roof-box-outline-color',
  'vehicle-roof-box-outline',
  'vehicle-roof-box',
  'vehicle-roofrack-color',
  'vehicle-roofrack-outline',
  'vehicle-roofrack-solid',
  'vehicle-scooter-kick-color',
  'vehicle-scooter-kick-electric-color',
  'vehicle-scooter-kick-electric',
  'vehicle-scooter-kick',
  'vehicle-scooter-color',
  'vehicle-scooter-outline-color',
  'vehicle-scooter-outline',
  'vehicle-scooter-solid',
  'vehicle-sedan-color',
  'vehicle-sedan-outline',
  'vehicle-sedan-solid',
  'vehicle-sport-color',
  'vehicle-sport-outline',
  'vehicle-sport-solid',
  'vehicle-station-wagon-color',
  'vehicle-station-wagon-outline',
  'vehicle-station-wagon-solid',
  'vehicle-suv-color',
  'vehicle-suv-outline',
  'vehicle-suv-solid',
  'vehicle-tire-invert-outline-color',
  'vehicle-tire-invert-outline',
  'vehicle-tire-invert',
  'vehicle-tires-all-color',
  'vehicle-tires-back-color',
  'vehicle-tires-back-left-color',
  'vehicle-tires-back-right-color',
  'vehicle-tires-front-color',
  'vehicle-tires-front-left-color',
  'vehicle-tires-front-right-color',
  'vehicle-tires',
  'vehicle-tractor-color',
  'vehicle-tractor-outline',
  'vehicle-tractor-solid',
  'vehicle-trailer-color',
  'vehicle-trailer-outline',
  'vehicle-trailer-solid',
  'vehicle-truck-color',
  'vehicle-truck-outline',
  'vehicle-truck-solid',
  'vehicle-van-color',
  'vehicle-van-outline',
  'vehicle-van-solid',
  'vehicle-workshop-color',
  'vehicle-workshop-outline-color',
  'vehicle-workshop-outline',
  'vehicle-workshop-solid',
  'ventilator-color',
  'ventilator',
  'videocam-color',
  'videocam-outline',
  'videocam-solid',
  'videocam',
  'view-3d-360-color',
  'view-3d-360-disabled-color',
  'view-3d-360-disabled',
  'view-3d-360',
  'virus-color',
  'virus',
  'visibility-off-outline-color',
  'visibility-off-outline',
  'visibility-off',
  'visibility-outline-color',
  'visibility-outline',
  'visibility',
  'warehouse-color',
  'warehouse',
  'weather-rain-color',
  'weather-rain-outline-color',
  'weather-rain-outline',
  'weather-rain-solid',
  'weather-rain',
  'weather-snow-color',
  'weather-snow',
  'weather-sun-color',
  'weather-sun-outline-color',
  'weather-sun-outline',
  'weather-sun-snow-color',
  'weather-sun-snow-outline-color',
  'weather-sun-snow-outline',
  'weather-sun-snow',
  'weather-sun',
  'wheel-bearings-color',
  'wheel-bearings',
  'wheel-hub-cap-color',
  'wheel-hub-cap-outline',
  'wheel-hub-cap',
  'wheel-rim-color',
  'wheel-rim-double-spoke-outline-color',
  'wheel-rim-double-spoke-outline',
  'wheel-rim-double-spoke',
  'wheel-rim-honeycomb-outline-color',
  'wheel-rim-honeycomb-outline',
  'wheel-rim-honeycomb',
  'wheel-rim-multi-spoke-outline-color',
  'wheel-rim-multi-spoke-outline',
  'wheel-rim-multi-spoke',
  'wheel-rim-outline',
  'wheel-rim',
  'wifi',
  'windscreen-back-fluid-color',
  'windscreen-back-fluid',
  'windscreen-back-frost-color',
  'windscreen-back-frost',
  'windscreen-back-wiper-color',
  'windscreen-back-wiper',
  'windscreen-color',
  'windscreen-fluid-color',
  'windscreen-fluid',
  'windscreen-frost-color',
  'windscreen-frost',
  'windscreen-wiper-color',
  'windscreen-wiper',
  'windscreen-wipers-color',
  'windscreen-wipers-left-color',
  'windscreen-wipers-left',
  'windscreen-wipers-right-color',
  'windscreen-wipers-right',
  'windscreen-wipers',
  'windscreen',
  'windshield-washer-fluid',
  'work-order-awaiting-approval-color',
  'work-order-awaiting-approval-outline',
  'work-order-awaiting-approval',
  'work-order-check-outline-color',
  'work-order-check-outline',
  'work-order-check',
  'work-order-diagnostic-outline-color',
  'work-order-diagnostic-outline',
  'work-order-diagnostic',
  'work-order-pass-maintain-color',
  'work-order-pass-maintain',
  'work-order-view-outline-color',
  'work-order-view-outline',
  'work-order-view',
  'work-order-wait-check-outline-color',
  'work-order-wait-check-outline',
  'work-order-wait-check',
  'world-color',
  'world',
  'wrench-dual-outline',
  'wrench-dual',
  'wrench-hand-color',
  'wrench-hand-dual-color',
  'wrench-hand-dual',
  'wrench-hand',
  'wrench-outline-color',
  'wrench-outline',
  'wrench-tools-outline-color',
  'wrench-tools-outline',
  'wrench-tools',
  'wrench-twin',
  'wrench',
  'zoom-in-color',
  'zoom-in',
  'zoom-out-color',
  'zoom-out',
  'zoom-less-color',
  'zoom-less-outline'
];