# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.33.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.32.5...3.33.0) (2025-03-13)

### Features

* **DES-562:** phone number error message ([e83ff34](https://gitlab.com/mobivia-design/roadtrip/components/commit/e83ff343633c0222190898a9806841bc253e4a1f))
* **font:** add weight font ([7e3313a](https://gitlab.com/mobivia-design/roadtrip/components/commit/7e3313a71d3360615ad9fc3199d5010a128d8258))
* **drawer:** add prop remove-padding ([eae0bff](https://gitlab.com/mobivia-design/roadtrip/components/commit/eae0bff4253f6a43ceda6225ff3178df32b6345c))

### Bug Fixes

* **dropdown:** update zindex ([9aa1888](https://gitlab.com/mobivia-design/roadtrip/components/commit/9aa188833a45c0f209d8ea6dba8e6fd4b816fffe))
* **type:** fix type for react ([b128686](https://gitlab.com/mobivia-design/roadtrip/components/commit/b1286861ebd2a7be3176419afd3aa80ab95b5614))
* **drawer:** update padding mobile device ([0dc2b3f](https://gitlab.com/mobivia-design/roadtrip/components/commit/0dc2b3f9fb04bd68f21e326f8aa5c9d521ebca41))


### [3.32.5](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.32.4...3.32.5) (2025-02-28)

### Bug Fixes

* **storybook:** fix display storybook and description ([3633e1b](https://gitlab.com/mobivia-design/roadtrip/components/commit/3633e1b1f696c90a152c298d11b626548d5a7bd5))
* **drawer:** fix display footer drawer ([c40b6b4](https://gitlab.com/mobivia-design/roadtrip/components/commit/c40b6b46c6e62e47eded93e4644e5a3aa012883d))


### [3.32.4](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.32.3...3.32.4) (2025-02-26)

### Bug Fixes

* **package:** script build-storybook ([f53cc52](https://gitlab.com/mobivia-design/roadtrip/components/commit/f53cc52fec43a5daefdfc80010fc3a621c25666b))


### [3.32.3](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.32.2...3.32.3) (2025-02-26)

### Bug Fixes

* **icons:** update storybook version and fix display icons checkbox and counter ([f0a3316](https://gitlab.com/mobivia-design/roadtrip/components/commit/f0a331675ae525d85e8e2b75764f9f243d83dde4))


### [3.32.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.32.1...3.32.2) (2025-02-21)

### Bug Fixes

* **storybook:** version storybook ci ([5ee3c65](https://gitlab.com/mobivia-design/roadtrip/components/commit/5ee3c65ea63f6c5fcf34fb8b0c269f13469b0392))


### [3.32.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.32.0...v3.32.1) (2025-02-14)

* **pipeline:** update gitlabci node version ([0e86431](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/0e86431c7335296923424a9a5180fca761df5a46))


## [3.32.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.31.0...3.32.0) (2025-02-14)

### Features

* **storybook:** update version ([9011ca3](https://gitlab.com/mobivia-design/roadtrip/components/commit/9011ca3cce44ef9a30e5979d76dacf904df33284))
* **linter:** add linter css ([30f6fb5](https://gitlab.com/mobivia-design/roadtrip/components/commit/30f6fb57861c8b43159f3f6de1481acf976f6565))
* **input:** add prop blockDecimal ([1c15c6d](https://gitlab.com/mobivia-design/roadtrip/components/commit/1c15c6d4a00c6e17ab196c3bff1f6a743cce4160))
* **stylelint:** add value allowed ([f53e0c0](https://gitlab.com/mobivia-design/roadtrip/components/commit/f53e0c0eb66e1148e4c63cbd75f8ae23fb40aa87))
* **stylint:** update readme ([e012d6a](https://gitlab.com/mobivia-design/roadtrip/components/commit/e012d6ad57fc40983d872008870a5f1a75477e1a))
* **icon:** add icon neo3 ([f576d84](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/f576d84fd06e8d76e51d85de094cdef3f3b615d5))


### Fixes

* **icon:** rotate 180 ([3b04837](https://gitlab.com/mobivia-design/roadtrip/components/commit/3b04837475845be244fef0b22a084d96a41a77cc))
* **tooltip:** fix hover mobile ([2aae60b](https://gitlab.com/mobivia-design/roadtrip/components/commit/2aae60b4569b57540b906e2243dc2666c250424b))
* **tabs:** remove border tab ([ed59aa9](https://gitlab.com/mobivia-design/roadtrip/components/commit/ed59aa96167cd94319af573f46d295e728e769a0))


## [3.31.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.30.0...3.31.0) (2025-01-24)

### Features

* **global-nav-v2:** add new global nav ([2f497ed](https://gitlab.com/mobivia-design/roadtrip/components/commit/2f497edf71754b259dacd3ef3792f54670f8ffad))
* **global-nav-v2:** refactor logo link and tooltip nav compact ([af45545](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/af455458ec3209dec713aa05facebefa35ec78be))
* **build:** add fichier d.ts for react ([798de5e](https://gitlab.com/mobivia-design/roadtrip/components/commit/798de5e18e05af45765a8cd5f7724d88be909080))

## Bug Fixes

* **variables:** fix variable midas ([1277938](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/1277938cb11c0b0cf507c16eed89ed3ac3f9845c))
* **variables:** refactor midas variable header ([22d9fae](https://gitlab.com/mobivia-design/roadtrip/components/commit/22d9fae29a20fb51d03a1be81c7850a5a31f02a3))
* **drawer:** fix padding and background footer ([f0d76ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/f0d76ea359f57f26bf869f1aa2092660a034100b))


## [3.30.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.29.0...3.30.0) (2024-12-24)

### Features

* **utilites:** add media queries spacing and flex ([873587d](https://gitlab.com/mobivia-design/roadtrip/components/commit/873587dcdc6cf57da56885f21db54f3e86e9e376))


### Bug Fixes

* **tooltip:** fix focus ([ed42ef1](https://gitlab.com/mobivia-design/roadtrip/components/commit/ed42ef13fe605445d604fa6839df0e9866f5d88a))


## [3.29.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.28.0...3.29.0) (2024-12-19)

### Features

* **button:** add variant destructive ([09065eb](https://gitlab.com/mobivia-design/roadtrip/components/commit/09065ebdf1893f5238b96d0386139bec13df2c9a))

### Bug Fixes

* **toggle:** update cursor when disabled ([07029c3](https://gitlab.com/mobivia-design/roadtrip/components/commit/07029c31350243a5f6f1f2192ed4702ad036ed3e))
* **input:** add regex for type number ([5f94697](https://gitlab.com/mobivia-design/roadtrip/components/commit/5f94697a29d0f5b7d68f25614a3665d08af342c1))
* **phone-number:** add type tel ([2e6b269](https://gitlab.com/mobivia-design/roadtrip/components/commit/2e6b2698a9335af4a8973876a1a27883e6dd291e))
* **button:** update variable css ghost button ([ea67fef](https://gitlab.com/mobivia-design/roadtrip/components/commit/ea67fefc07a2317c2408e26ae3dad362c5f25d63))
* **tabs:** add border for button ([14bf067](https://gitlab.com/mobivia-design/roadtrip/components/commit/14bf06790bacbd52e74259dec5d7b490afd7b769))
* **drawer:** add padding media queries min 768px ([a6aa5ce](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/a6aa5cec2772ec06263aaf7360c2a80dbbb4398f))


## [3.28.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.27.0...3.28.0) (2024-12-05)

### Features

* **tabs:** add prop disabled ([174ce47](https://gitlab.com/mobivia-design/roadtrip/components/commit/174ce4756a4cb3c1ac162d02113b49ce88feed30))
* **input-group:** add prop disabled ([fe62f3d](https://gitlab.com/mobivia-design/roadtrip/components/commit/fe62f3d283728f82a62dd1461aed6f49b1dbb7be))

### Bug Fixes

* **input:** fix when value is null ([a1c9336](https://gitlab.com/mobivia-design/roadtrip/components/commit/a1c93365b5db0d259fdd186a67b5bb9b0d62cc9c))
* **input-group:** focus all group ([71a3622](https://gitlab.com/mobivia-design/roadtrip/components/commit/71a36226d98f2dac8b36abf90af4d1ccd63ac336))


## [3.27.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.26.0...3.27.0) (2024-11-22)


### Features

* **checkbox-radio:** add secondary label props ([84becee](https://gitlab.com/mobivia-design/roadtrip/components/commit/84becee88c2f370c85e05e484326fe12861ade24))
* **progress-indicator-horizontal:** add option in progress ([e7a12ab](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7a12ab049cd55696469a50f6535351ea7fe4938))


### Bug Fixes

* **navbar:** fix focus selected value ([6367788](https://gitlab.com/mobivia-design/roadtrip/components/commit/6367788d73017809aef72e4a58f5c51338683923))
* **textarea:** with label focus ([cc20f19](https://gitlab.com/mobivia-design/roadtrip/components/commit/cc20f19a4d8820f92744d520c3a76bfb24be31ce))
* **select-filter:** add event key escape to close ([645b62c](https://gitlab.com/mobivia-design/roadtrip/components/commit/645b62cb9e3f6cdb57aaf00bd85514ecefa0a122))
* **tab-button:** add aria-selected to false ([90effa0](https://gitlab.com/mobivia-design/roadtrip/components/commit/90effa01ddcfdfda70f245036082bdcee3886718))


## [3.26.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.25.0...3.26.0) (2024-11-05)

### Features

* **badge:** add props inverse ([bb8b5ee](https://gitlab.com/mobivia-design/roadtrip/components/commit/bb8b5eefd78eac90153fe6b782f75a3b8db5e4cc))
* **rating:** add option extra-small ([45a64be](https://gitlab.com/mobivia-design/roadtrip/components/commit/45a64bedae51206dc990a680a7fda2ce4567862c))
* **chip:** add color inverse ([f0198ee](https://gitlab.com/mobivia-design/roadtrip/components/commit/f0198eef99cc41998b7816b8fc9d92825d68d78d))

### Refactor

* **accessibility:** update token outline ([ebbde8a](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/ebbde8a2227ac76a258b49c1ea2884884795cfbd))


## [3.25.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.24.0...3.25.0) (2024-10-30)

### Features

* **accordion:** add css custom propertie --header-padding-vertical ([67c63cb](https://gitlab.com/mobivia-design/roadtrip/components/commit/67c63cbb55e3a054f336a17740c3ce1bacef4d99))

### Bug Fixes

* **accessibilite:** add outline focus ([d28f5c7](https://gitlab.com/mobivia-design/roadtrip/components/commit/d28f5c7bfad1124307c380424eba915943dd3552))
* **tooltip:** fix width tootltip ([a9b0e89](https://gitlab.com/mobivia-design/roadtrip/components/commit/a9b0e8935084416f7a158361cb479b3cca37447e))
* **accessibility:** update token on-surfaceweak - extra-weak, outline and outline-weak ([a315959](https://gitlab.com/mobivia-design/roadtrip/components/commit/a31595984dde5d2f39656971bcd6f9a467a99e31))

### Refactor

* **tabs:** add troncate label ([1bdd827](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/1bdd827c55e31177914f049a68dd095adf40437e))


## [3.24.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.23.1...3.24.0) (2024-10-17)

### Features

* **asset:** add new component for assets ([ced8884](https://gitlab.com/mobivia-design/roadtrip/components/commit/ced888492c54c95aa161a37b6eb8fbc5d2d698e6))

### Bug Fixes

* **counter:** touchend hover mobile ([73f2150](https://gitlab.com/mobivia-design/roadtrip/components/commit/73f2150bbddd8f67039385fa8879938b21673bf9))
* **accessibility:** update token and add slot aria label ([6303a41](https://gitlab.com/mobivia-design/roadtrip/components/commit/6303a4179b3f5df55dd9ecb503dba0c74fbbb6fc))


### [3.23.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.23.0...3.23.1) (2024-10-01)

### Bug Fixes

* **select-filter:** update zindex ([0a5c0aa](https://gitlab.com/mobivia-design/roadtrip/components/commit/0a5c0aad013691ada16a5a599a293f10900bac5c))
* **accordion:** remove margin bottom ([ba9ff43](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/ba9ff43f5f299dfd678fe3cc9b77a6f0b28a5533))
* **button:** remove bg outline primary disabled ([838f03d](https://gitlab.com/mobivia-design/roadtrip/components/commit/838f03d8ef9afc242b3dc3b0f4445fb8a557aa68))


## [3.23.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.22.0...3.23.0) (2024-09-24)

### Features

* **tooltip:** update width and add new position ([1f35a9d](https://gitlab.com/mobivia-design/roadtrip/components/commit/1f35a9d6cb2d8a5cf2bcc69e9d28df1ae8db7c7b))

### Bug Fixes

* **icons:** fix color token icon ([e80b42a](https://gitlab.com/mobivia-design/roadtrip/components/commit/e80b42a3be4b2d952df72726734052c417b24a5e))
* **tooltip:** max-width 100% ([622fa45](https://gitlab.com/mobivia-design/roadtrip/components/commit/622fa45ff296a1e081264d4f07f3e6e78041ac89))
* **select-filter:** add zindex ([6ac2fac](https://gitlab.com/mobivia-design/roadtrip/components/commit/6ac2fac9ae40f50bc4ff38fcc07a8623d1f2bb2d))


## [3.22.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.21.0...3.22.0) (2024-09-05)

### Features

* **drawer:** add prop aria-label ([d8c4c4c](https://gitlab.com/mobivia-design/roadtrip/components/commit/d8c4c4c29f5fefff23434ca57270b5484b0530d7))
* **range:** add disabled prop ([99ae673](https://gitlab.com/mobivia-design/roadtrip/components/commit/99ae67325428c1287220a82bb8e2519596b5bcbd))
* **item:** add css custum properties --border-color and fix bg ([54ad8de](https://gitlab.com/mobivia-design/roadtrip/components/commit/54ad8dea2f24094ffc510d022e737e48675748fe))

### Bug Fixes

* **range:** when start value is different of 0 + add disabled props ([74d580e](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/74d580ee738e7a98b321e172278aa223e2511d02))

### Refactor

* **fab:** update color fab button midas ([582bc61](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/582bc610505b4d1dafa58ca67d93e89c91118396))


## [3.21.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.20.4...3.21.0) (2024-08-13)

### Features

* **drawer:** add option footer ([762eff8](https://gitlab.com/mobivia-design/roadtrip/components/commit/762eff8e9f8a4840fe0ee214b3fd766ab711432a))

### Bug Fixes

* **input:** color input disabled ([60d0533](https://gitlab.com/mobivia-design/roadtrip/components/commit/60d053368fafb8ed6162118438f61a1c8137d68e))
* **select:** reset value null ([9f8a1a8](https://gitlab.com/mobivia-design/roadtrip/components/commit/9f8a1a8cf1331c9aeb94f783ffbd5e4b3f17c1ad))


### [3.20.4](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.20.3...3.20.4) (2024-08-01)

### Refactor

* **modal:** update padding bottom ([c7983a5](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/c7983a58561cbd5fa67ddd017ed2fc776c5bdd7b))
* **accordion:** update spacing is-light ([eaab51f](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/eaab51f583183e66b4622984fbf1dee418257f7d))


### [3.20.3](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.20.2...3.20.3) (2024-07-26)

### Bug Fixes

* **dropdown:** position top right ([5b8af41](https://gitlab.com/mobivia-design/roadtrip/components/commit/5b8af41dc16a8a38c7303eb30ca04349efaea1fa))

### Refactor

* **icons:** add vehicle-5-doors-hatchback-color.svg ([4680f0e](https://gitlab.com/mobivia-design/roadtrip/components/commit/4680f0e7146d95b654f37f908b0d8c8909e28816))
* **accordion:** remove bg is-light prop ([a7df07c](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/a7df07c8a31e09b6e5dfb642f2df15db541202e6))


### [3.20.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.20.1...3.20.2) (2024-07-11)

### Bug Fixe

* **tabs:** fix selected tab ([7007345](https://gitlab.com/mobivia-design/roadtrip/components/commit/70073450afebe2b42237191e5a7a0e89c4b2bea2))


### [3.20.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.20.0...3.20.1) (2024-07-10)

### Bug Fixes

* **input:** debounce ([e535fdf](https://gitlab.com/mobivia-design/roadtrip/components/commit/e535fdf3b4e7241c180177d03948bc01ed986819))
* **tab:** selected tab ([a7c73b9](https://gitlab.com/mobivia-design/roadtrip/components/commit/a7c73b903535635d46804d8680c64955745d22e8))
* **debounce:** add lodash dependencies ([d8a7887](https://gitlab.com/mobivia-design/roadtrip/components/commit/d8a7887241105ce79b2bbd5de0735dbca145383c))
* **debounce:** fix debounce ([b221ec1](https://gitlab.com/mobivia-design/roadtrip/components/commit/b221ec14b92133d282fc1b65ed9da2c9c9795c07))
* **debounce:** remove console log and double event ([4665270](https://gitlab.com/mobivia-design/roadtrip/components/commit/4665270b7bec8c6f2953b532451c35d8e2e7c73e))
* **debounce:** double event ([171671b](https://gitlab.com/mobivia-design/roadtrip/components/commit/171671b6dc4f980bcdfdb6769ddc723140bd485c))
* **tab:** expand tab with ([d650678](https://gitlab.com/mobivia-design/roadtrip/components/commit/d6506783c51d16ffc9586fec4e9380746ee22f78))

### Refactor

* **input:** update docs value input ([0feedf5](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/0feedf5964ebe66bd24d3e72114a2cd940ef614c))


## [3.20.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.19.0...3.20.0) (2024-06-26)

### Features

* **icons:** add meeting-shipping-outline.svg ([263df93](https://gitlab.com/mobivia-design/roadtrip/components/commit/263df934dd7abc685a2882912d5641bb9d793a9e))
* **icons:** add meeting-shipping-color.svg ([0393818](https://gitlab.com/mobivia-design/roadtrip/components/commit/0393818ec9ca27c6b6bc934f2410aec59b68b3c8))
* **icons:** add people-car-fleet-solid.svg ([49a48e5](https://gitlab.com/mobivia-design/roadtrip/components/commit/49a48e5d3595eab313d16b7278e313e95c9f1788))
* **icons:** add vehicle-car-all-authorize-color.svg ([9f27066](https://gitlab.com/mobivia-design/roadtrip/components/commit/9f27066bdfbd96211f683bee69e7a2b6e89522b6))
* **icons:** add garage-solid.svg ([146ae17](https://gitlab.com/mobivia-design/roadtrip/components/commit/146ae1705e896022127e0e62895f8bcc2bfe4458))
* **icons:** add call-center-solid.svg ([709ecb5](https://gitlab.com/mobivia-design/roadtrip/components/commit/709ecb56b7e5a63b4ebd0c4598483b03133f6c78))

### Bug Fixes

* **phone-number:** refactor block character entry ([9cb35d9](https://gitlab.com/mobivia-design/roadtrip/components/commit/9cb35d9b04d32bb38212697064b7ec97ca233a61))
* **icons:** clean svg ([915d3e3](https://gitlab.com/mobivia-design/roadtrip/components/commit/915d3e37e9121c634bacbaf2d78a8e59ab0bf9d0))
* **icons:** clean svg whatsapp-badge ([d1fe540](https://gitlab.com/mobivia-design/roadtrip/components/commit/d1fe540057c3cd0e10f891ab435182d562d3b6bd))
* **alert:** update token link ([9dde765](https://gitlab.com/mobivia-design/roadtrip/components/commit/9dde76518e52a08d02606cfb359be4bba227fc00))
* **icon:** rotate alignment ([5418485](https://gitlab.com/mobivia-design/roadtrip/components/commit/541848566a3e98bca20e0af9974c1462cb58b6b5))
* **input:** debounce ([c383903](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/c3839034fe09a359f41b615b0951810bd3ec5bf6))


## [3.19.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.18.0...3.19.0) (2024-06-13)

### Features

* **brand:** add bythjul brand ([749bd59](https://gitlab.com/mobivia-design/roadtrip/components/commit/749bd59dc0e69d062c13c7f874431e2a0debf144))
* **brand:** add skruvat brand ([1ec76a2](https://gitlab.com/mobivia-design/roadtrip/components/commit/1ec76a251810b017b63e94060ba13a6bd900fd12))

### Fixes

* **icons:** clean svg ([30f09a5](https://gitlab.com/mobivia-design/roadtrip/components/commit/30f09a5a2550cc14ac6434275c7b4f2ec7c76c1c))
* **icons:** clean index.js ([1fcecc8](https://gitlab.com/mobivia-design/roadtrip/components/commit/1fcecc8f784a5bfeee2331a5e34606bc3053860b))

### Refactor

* **modal:** update padding ([cad51a8](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/cad51a8f8e52bbda6b39769e486e7322943e012a))


## [3.18.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.17.0...3.18.0) (2024-06-06)

### Features

* **phone-number:** only accepts numbers and + ([26c141d](https://gitlab.com/mobivia-design/roadtrip/components/commit/26c141dc24d1d7eeb3f964d818a4855567a34092))

### Refactor

* **accessibility:** refactor accessibility ([4e9cb3e](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/4e9cb3ea10514898a02751a79679c6409a32ddc0))
* **carousel:** token accessibilite ([b8e03a8](https://gitlab.com/mobivia-design/roadtrip/components/commit/b8e03a8b2ff33f25af2c5d8a6816b56c63757595))


## [3.17.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.16.1...3.17.0) (2024-04-30)

### Features

* **icons:** add vehicle-towbar-box-color.svg ([e1279a1](https://gitlab.com/mobivia-design/roadtrip/components/commit/e1279a1b6a43af087795b52e5113b5f44e9540dc))
* **icons:** add vehicle-towbar-box-outline.svg ([1375b71](https://gitlab.com/mobivia-design/roadtrip/components/commit/1375b713f9c57019cfae31a585f5bbe923efccf9))
* **icons:** add vehicle-towbar-box-solid.svg ([cbb10dd](https://gitlab.com/mobivia-design/roadtrip/components/commit/cbb10dd95c8f0fdc043a72b927146f4ccdd077ba))


### [3.16.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.16.0...3.16.1) (2024-04-22)


### Bug Fixes

* **item:** fix item dropdown ([97251ba](https://gitlab.com/mobivia-design/roadtrip/components/commit/97251baa953c2b57f3ae3bc06cf0be1585487b3f))

### Refactor

* **tabs:** color border ([75d941a](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/75d941a75566dea5b0a05e4abe5e11abb67d9224))


## [3.16.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.15.0...3.16.0) (2024-04-11)


### Features

* **icons:** add check-list-solid.svg ([3f04fe4](https://gitlab.com/mobivia-design/roadtrip/components/commit/3f04fe444046c851f9931f8afe90db9e0109b23b))
* **icons:** add world-solid.svg ([2823e06](https://gitlab.com/mobivia-design/roadtrip/components/commit/2823e061d8101a592df31e1342b9fe97809dca79))
* **dropdown:** opening and closing one dropdown ([debcdbd](https://gitlab.com/mobivia-design/roadtrip/components/commit/debcdbd0e48233b6172535dea5fe6ba598f18248))
* **input:** roadblur add value ([0c520cd](https://gitlab.com/mobivia-design/roadtrip/components/commit/0c520cd9f0291dee2269b3eb24563d44db80a1c9))
* **item:** add option layout + refacto ([aba0077](https://gitlab.com/mobivia-design/roadtrip/components/commit/aba0077cbd44d6bc2fc1c1d8550f0bbc9532812e))


## [3.15.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.14.3...3.15.0) (2024-03-26)

### Features

* **icons:** add vehicle-car-search-color.svg ([4e3e1ef](https://gitlab.com/mobivia-design/roadtrip/components/commit/4e3e1ef2cf2925381a64bd7bbe8df559a6cac329))
* **input:** check min max keyut ([5f39d9e](https://gitlab.com/mobivia-design/roadtrip/components/commit/5f39d9e7fc7568acc9ee4065a636bb7edb4ede22))

### Refactor

* **token:** change token auto5 fab-button ([91e7fcf](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/91e7fcf245e5e94a9793bba2c37edf0af0eee619))
* **toogle:** remove color props ([9ea5cb6](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/9ea5cb64c1546872d1d6f4606415c0a3138918f7))
* **carousel:** update img ([78fc8ce](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/78fc8cea91b5b103e02a0896406f4f505f93dfd7))


### [3.14.3](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.14.2...3.14.3) (2024-03-08)

### Bug Fixes

* **card:**  remove spacing 5 mobile temp ([286ce8f](https://gitlab.com/mobivia-design/roadtrip/components/commit/286ce8f32a506a9db763ec9d1e8539fa1a18dd88))
* **card/dropdown:** fix spacing and zindex ([4a72e25](https://gitlab.com/mobivia-design/roadtrip/components/commit/4a72e25efd5a00b9c41d575d793060632be71dd5))


### [3.14.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.14.1...3.14.2) (2024-03-01)

### Bug Fixes

* **card:** attribut button and padding ([e9eb48f](https://gitlab.com/mobivia-design/roadtrip/components/commit/e9eb48f3d2f07a7bd60c90a78b9e59133f6acd8d))


### [3.14.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.14.0...3.14.1) (2024-02-20)

### Bug Fixes

* **dropdown:** position & zindex ([abb4e62](https://gitlab.com/mobivia-design/roadtrip/components/commit/abb4e62566941c9b5ba1fe6ce9059d6afc39cb78))
* **variables:** fix token blur ([847b0dc](https://gitlab.com/mobivia-design/roadtrip/components/commit/847b0dcf0b5cd98daf51dca4622e3f3226a7ebf1))


## [3.14.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.13.3...3.14.0) (2024-02-16)

### Features

* **content-card:** add new component ([feea31e](https://gitlab.com/mobivia-design/roadtrip/components/commit/feea31ea5a8c84a02ef361621e3da7e89982a725))
* **utilities:** color token and add -12 spacing ([23795b1](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/23795b155eff0e58817d6e6a3f79dea40bc1c26e))
* **token:** add new token fab button ([3bc66e2](https://gitlab.com/mobivia-design/roadtrip/components/commit/3bc66e20dc8179aea9bbfbc10957becb939fbafc))

### Bug Fixes

* **phone-number:** arrow select ([e95c4fc](https://gitlab.com/mobivia-design/roadtrip/components/commit/e95c4fca3a92703752dece3c560ff79ca9f51985))
* **dropdown:** position absolute ([74bdca0](https://gitlab.com/mobivia-design/roadtrip/components/commit/74bdca01deec0b0645910336f0bb9c6c464744cc))    
* **dropdown:** description prop is-medium ([1c5b6f5](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/1c5b6f58eb414b6eea6097b15616e6e967bc5447))


### [3.13.3](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.13.2...3.13.3) (2024-02-02)

### Bug Fixes

* **select:** selected value option ([5e0c8ad](https://gitlab.com/mobivia-design/roadtrip/components/commit/5e0c8adb8d5b036330b97107af38663ee39dc9bd))


### [3.13.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.13.1...3.13.2) (2024-02-01)

### Bug Fixes

* **select:** fix value ([f9de7e4](https://gitlab.com/mobivia-design/roadtrip/components/commit/f9de7e45c33710e3cf6f0588048ceed228cdd364))
* **icon:** add funding-refused-outline ([c0045a8](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/c0045a8321228b5078f4261ba03cd73e760928d5))
* **icon:** add funding-refused-color ([1b83f05](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/1b83f051c37a449e2738cce2e95531ca1cd485af))
* **icon:** add funding-refused-solid ([80fc45d](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/80fc45d6fb66ff55982635362328a610e6e26518))



### [3.13.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.13.1...3.13.2) (2024-02-01)

### Bug Fixes

* **select:** fix value ([f9de7e4](https://gitlab.com/mobivia-design/roadtrip/components/commit/f9de7e45c33710e3cf6f0588048ceed228cdd364))
* **icon:** add funding-refused-outline ([c0045a8](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/c0045a8321228b5078f4261ba03cd73e760928d5))
* **icon:** add funding-refused-color ([1b83f05](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/1b83f051c37a449e2738cce2e95531ca1cd485af))
* **icon:** add funding-refused-solid ([80fc45d](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/80fc45d6fb66ff55982635362328a610e6e26518))



### [3.13.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.13.0...3.13.1) (2024-01-18)

### Refactor

* **icon:** windscreen-frost-winter-color.svg ([8010806](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/8010806961c6d267cce09bcbf4614f7cd836ed08))
* **icon:** windscreen-frost-winter-outline.svg ([47c831a](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/47c831ae0e038a0548fb6658c6f1fb0d2441084f))


## [3.13.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.12.2...3.13.0) (2024-01-17)

### Features

* **card:** add elevation option ([0623a88](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/63e7e49713c58bf574fcd902019e4959c1c86ec9))
* **counter:** add option readonly ([ea6ae80](https://gitlab.com/mobivia-design/roadtrip/components/commit/ea6ae80e7b1ee89fa223fcc8f37905dc726d9b82))
* **icons:** add tire-front-color.svg ([4e4f00e](https://gitlab.com/mobivia-design/roadtrip/components/commit/4e4f00e55e8b9db2fb31c8df80a649d033194b9d))
* **icons:** add tire-front-outline.svg ([fc6d2b3](https://gitlab.com/mobivia-design/roadtrip/components/commit/fc6d2b32bcd6f31fc66f8f6acb521da520aceabb))
* **icons:** add tire-front-solid.svg ([33d74f4](https://gitlab.com/mobivia-design/roadtrip/components/commit/33d74f40a423ce616117a31b584709913aaf8e2c))
* **icons:** add windscreen-frost-winter-color.svg ([217a596](https://gitlab.com/mobivia-design/roadtrip/components/commit/217a5963807de0658184ca3850433522cc7afdf9))
* **icons:** add windscreen-frost-winter-outline.svg ([35ba414](https://gitlab.com/mobivia-design/roadtrip/components/commit/35ba4145dc43e94fc43304c48aab55495e17bb19))
* **icons:** add alert-notification-off-color.svg ([fd0110c](https://gitlab.com/mobivia-design/roadtrip/components/commit/fd0110c072567b5d34f5a2dab412795460e16dae))
* **icons:** add alert-notification-off-outline.svg ([600f12b](https://gitlab.com/mobivia-design/roadtrip/components/commit/600f12b851a97f9728ed31965ea67d4566e76383))
* **icons:** add alert-notification-off-solid.svg ([8d29b8f](https://gitlab.com/mobivia-design/roadtrip/components/commit/8d29b8f85df958da19db2ecf35fd1c09cb9e2877))
* **icons:** add air-conditioning-color.svg ([3ac0379](https://gitlab.com/mobivia-design/roadtrip/components/commit/3ac0379bf0b154d3866e0c4c4af1afce82b63a78))
* **icons:** add air-conditioning-outline.svg ([9068aa3](https://gitlab.com/mobivia-design/roadtrip/components/commit/9068aa36918a6dcf91fc13fa22793b448e247c21))
* **icons:** add vehicle-damage-solid.svg ([8118fbd](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/8118fbd5a8b60d3ec986dcd1a43aa140bac7cbf2))
* **icons:** add vehicle-clean-solid.svg ([77fb38d](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/77fb38d37d308c5c6041cf349329bc7171311547))
* **icons:** add add vehicle-clean-color.svg ([81d013e](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/81d013e762db7d01161b9b28c01e0720d17084fc))
* **icons:** add vehicle-clean-outline.svg ([f0b0416](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/f0b0416be82c5b2741f31c2f08ea3e00eef62f7e))

### Bug Fixes

* **variable:** fix spacing 5 and 6 ([bf79ec3](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf79ec32fa5a72cd8c1d330ca4deb85d6f9bc917))
* **fab:** fix display position center ([efb01db](https://gitlab.com/mobivia-design/roadtrip/components/commit/efb01db234ed02c8c342d42db169dac5438a1362))


### [3.12.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.12.1...3.12.2) (2024-01-05)

### Bug Fixes

* **navbar-item:** fix disabled ([4536265](https://gitlab.com/mobivia-design/roadtrip/components/commit/453626571a4524a238aed962e87aba3063ff3f24))
* **navbar-item:** optim css disabled ([fc4d394](https://gitlab.com/mobivia-design/roadtrip/components/commit/fc4d394993b48f5db84812024284d7d2359d2a69))
* **navbar-item:** optim css disabled ([fc4d394](https://gitlab.com/mobivia-design/roadtrip/components/commit/fc4d394993b48f5db84812024284d7d2359d2a69))


### [3.12.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.12.0...3.12.1) (2023-12-22)

### Bug Fixes

* **select:** selected option ([9b68ba5](https://gitlab.com/mobivia-design/roadtrip/components/commit/9b68ba57f0fa580e82708094bd799582fbf81927))
* **button:** fix disabled outline ([d4a43e8](https://gitlab.com/mobivia-design/roadtrip/components/commit/d4a43e877f293b284399bff5f1f01f078ef835dc))
* **token:** tag exclusivity yellow-gold-50 ([e400814](https://gitlab.com/mobivia-design/roadtrip/components/commit/e400814c92f7fe8fc467a6a76c259b4b5d885b51))
* **phone-number-DES-461:** phone input country auto select ([1f8ca97](https://gitlab.com/mobivia-design/roadtrip/components/commit/1f8ca971bb53e7fd16c0f81caff65cfb9ceec11b))


## [3.12.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.11.2...3.12.0) (2023-12-01)

### Features

* **progress-horizontal:** add option number step ([42d3e7d](https://gitlab.com/mobivia-design/roadtrip/components/commit/42d3e7dec05721614df957e568205bb756b42756))
* **token:** add token blur ([d2b5478](https://gitlab.com/mobivia-design/roadtrip/components/commit/d2b547858576798dafa965c0f8350b742871ff6d))
* **rating:** add option readOnly ([42c8b2c](https://gitlab.com/mobivia-design/roadtrip/components/commit/42c8b2c2b66ba66f0a6a6035fb26ec468ed9ba6d))


### Bug Fixes

* **icon:** display icon vehicle-car-search-color ([19c2d4a](https://gitlab.com/mobivia-design/roadtrip/components/commit/19c2d4af87f13aadc8dae6df63b3e725e7af0824))
* **icon:** clean svg ([f33f3b0](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/f33f3b0ed97c133c992abc760d7be67d70695b32))
* **global-nav:** maj storie padding ([3965722](https://gitlab.com/mobivia-design/roadtrip/components/commit/39657227f3b787057667b8035cc705a8310a1175))
* **phone number input:** reactivity on user input ([230f51f](https://gitlab.com/mobivia-design/roadtrip/components/commit/230f51f1c0af5aebb9c8c156e480326b89f98e7f))
* **phone-number:** remove log ([8fafb3b](https://gitlab.com/mobivia-design/roadtrip/components/commit/8fafb3b75668d2a26468dd36ade75b8acbcad90b))


### Refactor

refactor(select): select value ([e5046da](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/e5046dac64b2aaa1e421695b1ba4f45dbe2fd3cd))


### [3.11.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.11.1...3.11.2) (2023-11-21)

### Bug Fixes

* **ssr:** remove setMode ([363b6db](https://gitlab.com/mobivia-design/roadtrip/components/commit/363b6dbbff196cb1593105de771254ade8587d9a))


### [3.11.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.11.0...3.11.1) (2023-11-20)

### Bug Fixes

* **phone-number:** refacto ([f637c8c](https://gitlab.com/mobivia-design/roadtrip/components/commit/f637c8c7f88d82d64df8359f9fd605e0bb4aaeb4))
* **drawer:** remove class ([0eb4e28](https://gitlab.com/mobivia-design/roadtrip/components/commit/0eb4e287a7d3370cafcd2bae634d09ce4a9ad5b0))
* **icons:** fix icon vehicle-car-search-color ([5627ddb](https://gitlab.com/mobivia-design/roadtrip/components/commit/5627ddb78bc17a4780662f896618e2d5ebf78354))
* **Stencil:** update stencil version ([2c78d2b](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/2c78d2b5bf5208694b54b10f3d2103f8574f7271))


## [3.11.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.10.1...3.11.0) (2023-11-10)

### Features

* **icons:** add social-whatsapp-badge.svg ([dc477f0](https://gitlab.com/mobivia-design/roadtrip/components/commit/dc477f067b377d198db36b270c800cbea3829bbb))
* **icons:** add social-whatsapp-color.svg ([887ac7c](https://gitlab.com/mobivia-design/roadtrip/components/commit/887ac7c8a48fc662d9100ea57bee757a5e470bd6))
* **icons:** add social-whatsapp-solid.svg ([7d485b2](https://gitlab.com/mobivia-design/roadtrip/components/commit/7d485b22fc473690b3fb65e9fcbfbea83014fe4b))
* **select:** add option md ([3a7b4c8](https://gitlab.com/mobivia-design/roadtrip/components/commit/3a7b4c883babebef4f21f513be685f8836b4e89d))


### Bug Fixes

* **drawer:** add bold title ([c27fb7d](https://gitlab.com/mobivia-design/roadtrip/components/commit/c27fb7d6be8fd94825f0a139d2d02a0c271e39f2))
* **icons:** remove clip path icon tire-diameter ([7043539](https://gitlab.com/mobivia-design/roadtrip/components/commit/7043539abeff9507ba870f7ca5ecf52bd91fcd1e))
* **phone-number:** optim component ([280ceeb](https://gitlab.com/mobivia-design/roadtrip/components/commit/280ceeb764d7ebdccd070851e295d3c1412a78b1))
* **phone-number:** display event storie ([207143c](https://gitlab.com/mobivia-design/roadtrip/components/commit/207143c6d4f18902e134e0f86fa58e819dc4bdd3))
* **icons:** fix display location-pin-outline-color ([a23ff61](https://gitlab.com/mobivia-design/roadtrip/components/commit/a23ff612fa1849cf2a359d13e53b45227a8ee5ad))
* **phone-number-input:** documentation ([851a070](https://gitlab.com/mobivia-design/roadtrip/components/commit/851a070e79b31ff72fbd2cd51ef1c6705c9c2fc9))


### [3.10.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.10.0...3.10.1) (2023-10-31)

### Bug Fixes

* **button:** add option onlyIcon ([d70d20b](https://gitlab.com/mobivia-design/roadtrip/components/commit/d70d20bcfee0d93d08d3af59bd4ee126e17a310f))
* **illustration:** fix component illustration ([f67a3d1](https://gitlab.com/mobivia-design/roadtrip/components/commit/f67a3d135aa78a7958579797b67d0fe929376fc0))
* **phone-number-imput:** refactor component ([946aa04](https://gitlab.com/mobivia-design/roadtrip/components/commit/946aa04a0409b0e0864be27ed50a82dc6439c48c))



## [3.10.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.9.0...3.10.0) (2023-10-17)

### Features

* **phone-number:** add new component ([5655b56](https://gitlab.com/mobivia-design/roadtrip/components/commit/5655b56d35ebb6a7bb52c0d9fff8431c51df9b34))
* **illustrations:** add new component ([7b01bc9](https://gitlab.com/mobivia-design/roadtrip/components/commit/7b01bc9bf8fe387afc057194482ed5ca0975b7eb))
* **illustation:** add new component ([11f6ea2](https://gitlab.com/mobivia-design/roadtrip/components/commit/11f6ea26d9fe2b31fbcf1055c287f5dd15001b2b))
* **icons:** add social-x-solid.svg ([b6eb5d9](https://gitlab.com/mobivia-design/roadtrip/components/commit/b6eb5d9e947c5b7aae243a27ef23137dc9396f5d))
* **icons:** add social-x-solid.svg ([117f2d0](https://gitlab.com/mobivia-design/roadtrip/components/commit/117f2d0e49e94cf901b9ef452580a84787f463db))
* **icons:** add smiley-happy-color.svg ([9640aa9](https://gitlab.com/mobivia-design/roadtrip/components/commit/9640aa9b4ed5c72304cfce0b47c437fa0f63260a))
* **icons:** add smiley-happy-outline.svg ([b202a4e](https://gitlab.com/mobivia-design/roadtrip/components/commit/b202a4eeb07738e224f57a69b668c03c92a45b5e))
* **icons:** add smiley-happy-solid.svg ([c3cc417](https://gitlab.com/mobivia-design/roadtrip/components/commit/c3cc41717c1f9190234ab7c1ea9279188b7ce212))
* **icons:** add smiley-neutral-color.svg ([737a5f7](https://gitlab.com/mobivia-design/roadtrip/components/commit/737a5f758c1b9c127ce5baca6daf552e19735d1a))
* **icons:** add smiley-neutral-outline.svg ([45bbfb3](https://gitlab.com/mobivia-design/roadtrip/components/commit/45bbfb320a85249b08b7a7c32c5510e617826390))
* **icons:** add smiley-neutral-solid.svg ([ca6d49f](https://gitlab.com/mobivia-design/roadtrip/components/commit/ca6d49f3a471975b46be472247a5bd18eef919c2))
* **icons:** add smiley-sad-color.svg ([d919393](https://gitlab.com/mobivia-design/roadtrip/components/commit/d919393aa5622fe30d0f8b825600eb63250a3741))
* **icons:** add smiley-sad-outline.svg ([0ded431](https://gitlab.com/mobivia-design/roadtrip/components/commit/0ded43162d1c36575e4d21c7a532433d1f7479b5))
* **icons:** add smiley-sad-solid.svg ([cefcb52](https://gitlab.com/mobivia-design/roadtrip/components/commit/cefcb52a62ebf5c0ca39e60cf8ee16b69c5583bf))
* **stencil:** update stenciljs version ([0308664](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/03086646ae1ce0561411f5c07f6059bb66894b6a))


### Bug Fixes

* **dialog:** fix overflow description ([34dbf3c](https://gitlab.com/mobivia-design/roadtrip/components/commit/34dbf3c7b3feab7ccfbedc2be41a689777c60a59))
* **progress-tracker:** remove padding ul ([6c63164](https://gitlab.com/mobivia-design/roadtrip/components/commit/6c631644f0c0dccf844f075b51ba579749225d84))
* **variable:** fix token outilne weak ([431e8df](https://gitlab.com/mobivia-design/roadtrip/components/commit/431e8df3a3c487570e841fdc95e0e305e44111e4))
* **banner:** refacto token ([39e9f6b](https://gitlab.com/mobivia-design/roadtrip/components/commit/39e9f6bcba00123c53cdc173cf11b8b0ab8e064d))
* **auto5:** refactor token header-badge ([32ade99](https://gitlab.com/mobivia-design/roadtrip/components/commit/32ade99e7a792714c7fc00088a5b0350b9045c23))


## [3.9.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.8.0...3.9.0) (2023-09-01)

### Features

* **flap:** add option filled ([3e7d65e](https://gitlab.com/mobivia-design/roadtrip/components/commit/3e7d65ec35547ae464e36627c33a6ded45da5f6a))
* **alert:** add option button ([eb2a919](https://gitlab.com/mobivia-design/roadtrip/components/commit/eb2a919fca2a214b4e949279323d7bdeeb8101aa))
* **icons:** add script package json ([f60421e](https://gitlab.com/mobivia-design/roadtrip/components/commit/f60421eec3afb7a57ba9502576332505c969e598))

### Bug Fixes

* **range:** update icon solid less and more ([1252218](https://gitlab.com/mobivia-design/roadtrip/components/commit/1252218b802813335572a48518aecffd3d095d49))

## [3.8.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.7.0...3.8.0) (2023-08-18)

### Features

* **component:** ssr ([30a8f8b](https://gitlab.com/mobivia-design/roadtrip/components/commit/30a8f8bef57b92a657f17a5a08aa8f6ae8459f55))
* **rating:** add option showreviews ([a5e6b81](https://gitlab.com/mobivia-design/roadtrip/components/commit/a5e6b81f71b5f81124a11a68d4b73c0beb3253fa))
* **tokens:** add spacing tokens ([5c97550](https://gitlab.com/mobivia-design/roadtrip/components/commit/5c97550803958259991489ddfa34eaeedcc7d5d2))
* **icons:** add icons outline player and cookie ([3654398](https://gitlab.com/mobivia-design/roadtrip/components/commit/3654398f74670d3d987818ae97340f944afdde33))

### Bug Fixes

* **input:** fix autofocus ([9b96bb9](https://gitlab.com/mobivia-design/roadtrip/components/commit/9b96bb9f0aefdfa04d85d70cdd32f798951a0e73))

### Refactor

* **breadcrumb:** replace arrow by slash ([b6595e5](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/b6595e5871a6d661099c217f8de32cff5728fd9a))

## [3.7.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.6.0...3.7.0) (2023-07-07)

### Features

* **progress:** add color rating ([929eab6](https://gitlab.com/mobivia-design/roadtrip/components/commit/929eab68ac27446c71d470b81ac47d976288162c))
* **tag:** add contrast option ([40844f9](https://gitlab.com/mobivia-design/roadtrip/components/commit/40844f96382abc379149f98dfa9bc085cf1be4e2))
* **textarea:** add option resize ([41a6924](https://gitlab.com/mobivia-design/roadtrip/components/commit/41a6924cb73053370fd378e4b107bba2f80d030a))
* **icon:** add icon saison swap ([2524a97](https://gitlab.com/mobivia-design/roadtrip/components/commit/2524a97965532cd2d91ac01746a08a01ffbbe446))


### Bug Fixes

* **tag:** fix color tag ([0f02f98](https://gitlab.com/mobivia-design/roadtrip/components/commit/0f02f98d282ea080a719f32a52c1263e065405fb))
* **icons:** fix name season ([19aa5d5](https://gitlab.com/mobivia-design/roadtrip/components/commit/19aa5d51e38a890fcb1be30f3fe5ad78db811917))

### Refactor

* **global-nav:** update size navbar label ([85a536a](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/85a536a3ca12f48443938e94d98c630bb101194e))


### Features

* **accordion:** add icon customisation ([0623a88](https://gitlab.com/mobivia-design/roadtrip/components/commit/0623a88e89b9cebbed270c44ca3aabaa89314d36))
* **accordion:** add icon left option ([73fa326](https://gitlab.com/mobivia-design/roadtrip/components/commit/73fa326bc3d9c953e6ed025f88890e6641c19231))
* **accordion:** add opening animation ([4c5969b](https://gitlab.com/mobivia-design/roadtrip/components/commit/4c5969b02e402eaa4b0563549a1b32aa234a5ba8))
* **accordion:** add small and separator version ([68ec74f](https://gitlab.com/mobivia-design/roadtrip/components/commit/68ec74fab2a14bd67956637e7d776a41299ace51))
* **accordion:** move margin to the host element ([3820cef](https://gitlab.com/mobivia-design/roadtrip/components/commit/3820cef8c41fd7e771b1a187134223c536f703f6))
* add accordion noborder component ([1fe6c05](https://gitlab.com/mobivia-design/roadtrip/components/commit/1fe6c054d574bd666b30101744370bd69929bbe2))
* add banner component ([a036770](https://gitlab.com/mobivia-design/roadtrip/components/commit/a036770aaa10d894a1a1511b2b07d2daccc85d38))
* add brand color ([3315976](https://gitlab.com/mobivia-design/roadtrip/components/commit/3315976cca18ad4b67f5c1cd2faa5fe113f5e1bb))
* add breadcrumb pattern ([03101f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/03101f209725cf3624c9c231f40bdf621d4b5c3f))
* add chip component ([ef03729](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef03729261876e4753c0cb3a6d6d76601ed285b8))
* add CSS variables for accordion and item customisation ([8924ba7](https://gitlab.com/mobivia-design/roadtrip/components/commit/8924ba7739330e142c7bb59cd556ced0b916ded4))
* add dropdown component ([bf37106](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf3710652578eb555b24105bcd732604cebf9a5a))
* add duration component ([c7d8349](https://gitlab.com/mobivia-design/roadtrip/components/commit/c7d8349b8edea3dd8f60b623287dd0cf8398deb2))
* add helper checkbox component ([628f3f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/628f3f2bb7aa6d5544abc5126030440406d2c62d))
* add helper input component ([25f16a4](https://gitlab.com/mobivia-design/roadtrip/components/commit/25f16a4a5a59a406c65a52d9d07baa6c9ae6dfed))
* add helper radio component ([908f67a](https://gitlab.com/mobivia-design/roadtrip/components/commit/908f67a79cdd29fec6d47277c5091b652bf426f7))
* add helper textarea component ([b8d9713](https://gitlab.com/mobivia-design/roadtrip/components/commit/b8d97130e939871512dfb40724012352fbb96704))
* add link component ([60af3d3](https://gitlab.com/mobivia-design/roadtrip/components/commit/60af3d34d1c328af2e8b06aad2a6cdfddb7ba39c))
* add lowercase events ([f395d4d](https://gitlab.com/mobivia-design/roadtrip/components/commit/f395d4d90898d4dbba79217e6aa81121e6fa8c20))
* add plate number component ([8981668](https://gitlab.com/mobivia-design/roadtrip/components/commit/89816682304159e78ed191deb50219521b2ad2dc))
* add range component ([ad4189f](https://gitlab.com/mobivia-design/roadtrip/components/commit/ad4189fe077f3653e1ca45c5b3f0fd38aa8a0d4e))
* add select-filter component ([27c1e2f](https://gitlab.com/mobivia-design/roadtrip/components/commit/27c1e2f72df590fc4375ce614f835c1d742729ae))
* Add SVG sprite of icons ([06b85cc](https://gitlab.com/mobivia-design/roadtrip/components/commit/06b85ccc7cdaca7e3e6d9c42812e79dac7883769))
* add toggle component ([c6f796c](https://gitlab.com/mobivia-design/roadtrip/components/commit/c6f796cc9116d589e779b90122596afb0890ccbc))
* **alert:** add title and link ([46cd96b](https://gitlab.com/mobivia-design/roadtrip/components/commit/46cd96b557a4bdbd50ec1d1dd6a69a94e2567fa7))
* **alert:** update colors and text size ([160d132](https://gitlab.com/mobivia-design/roadtrip/components/commit/160d13253a15fa889a04c3468c7c389076ac425e))
* **alert:** update icon color ([a630457](https://gitlab.com/mobivia-design/roadtrip/components/commit/a6304576995d3bcd5a7d0b29a7586cdbd0bbb747))
* **avatar:** add new component ([fd8b37d](https://gitlab.com/mobivia-design/roadtrip/components/commit/fd8b37dfb1cfd276487034e9a8976eb1c1d46cee))
* **badge:** update colors and padding ([96b5aaa](https://gitlab.com/mobivia-design/roadtrip/components/commit/96b5aaa8099b4d8ff463797decf0ab2bb1ea5034))
* **breakpoint:** add breakpoint 1200px for desktop users ([af1b10c](https://gitlab.com/mobivia-design/roadtrip/components/commit/af1b10c11405f294544da00c2da5ae002e8b1b18))
* **button-floating:** add new component ([4a82710](https://gitlab.com/mobivia-design/roadtrip/components/commit/4a8271020d4bdd8be318ac2c93c82e3e2b6a6035))
* **button:** add download attribute ([c6c75f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/c6c75f280467f295a87499caa3979a41200aaafc))
* **button:** add ghost color ([cb288e3](https://gitlab.com/mobivia-design/roadtrip/components/commit/cb288e3fd3c4bf58dfdc6b3e40c487bef482d685))
* **button:** add xl size ([f49f489](https://gitlab.com/mobivia-design/roadtrip/components/commit/f49f489cb4e214ce380c886177e7d6dd4a1a371c))
* **button:** update colors ([0b64cf1](https://gitlab.com/mobivia-design/roadtrip/components/commit/0b64cf185c4b90165f705eb1ade538b8a3af3765))
* **carousel:** update color pagers ([af4e49c](https://gitlab.com/mobivia-design/roadtrip/components/commit/af4e49c5baa3db2401958f16a1426e6a3cbc7dca))
* **checkbox:** add indeterminate state ([9fae5aa](https://gitlab.com/mobivia-design/roadtrip/components/commit/9fae5aa2c756469dbbb5ca591d7f2aba859a3de2))
* **checkbox:** add left label and spaced props ([987176d](https://gitlab.com/mobivia-design/roadtrip/components/commit/987176deab49bbb43fa48bacd78d9a07d31082fa))
* **checkbox:** add more contrast in disabled state ([2f37c32](https://gitlab.com/mobivia-design/roadtrip/components/commit/2f37c32388600f6300da7142798390985b3a8a49))
* **checkbox:** Add props to inverse radio and label order ([aa7bf0c](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa7bf0cf889c1f9a5531f163be576b3c7d63caed))
* **checkbox:** update colors and font-size ([1449f5d](https://gitlab.com/mobivia-design/roadtrip/components/commit/1449f5d2f991fd985f7de114937d487d9ae3bf33))
* **chip:** update colors and paddings ([65bcd91](https://gitlab.com/mobivia-design/roadtrip/components/commit/65bcd9149e67db60bc63ad053d6cd2f92d0717cd))
* **chip:** update height ([6cfe08f](https://gitlab.com/mobivia-design/roadtrip/components/commit/6cfe08f8c4e7ea0b4be16cffc714ccf912eb756a))
* **collapse:** add button customisation ([d39bd0d](https://gitlab.com/mobivia-design/roadtrip/components/commit/d39bd0db53e72668f85cf9ef79fa830ce9aadfe3))
* **color:** add variable color for header ([4497788](https://gitlab.com/mobivia-design/roadtrip/components/commit/4497788fa4815b52785aa64220d2bb78793236d1))
* **color:** add variables badge header ([dbca90a](https://gitlab.com/mobivia-design/roadtrip/components/commit/dbca90aa4a90e8545b0047542f5f1ee0c291dbe6))
* **counter:** add data-cy ([e412f04](https://gitlab.com/mobivia-design/roadtrip/components/commit/e412f04e674f11114c660a5a31b20ee32e0e5259))
* **counter:** Add events when clicking more or less buttons ([3d91968](https://gitlab.com/mobivia-design/roadtrip/components/commit/3d919689fd771f6c30c1e33123531924a4af3e61))
* **counter:** add option icon delete ([1e50ad8](https://gitlab.com/mobivia-design/roadtrip/components/commit/1e50ad8a7ffbeea94e6a3ec0d0ac18ffb2e8807c))
* **counter:** implement the other sizes ([64c605a](https://gitlab.com/mobivia-design/roadtrip/components/commit/64c605a27fdd000c3ec5a32a1772c52fe62f438e))
* **counter:** increase size for minus and more ([63eebea](https://gitlab.com/mobivia-design/roadtrip/components/commit/63eebea3ea75532838443980119c0bbb1d023ddd))
* **counter:** update colors and font-size ([42e83b4](https://gitlab.com/mobivia-design/roadtrip/components/commit/42e83b459df473ce3f3343900f4ffac7c9483fee))
* **dialog:** make icon customizable ([152e3d7](https://gitlab.com/mobivia-design/roadtrip/components/commit/152e3d77dbb7c0049c3822119e1624ac283c08ba))
* **dialog:** make icon customizable ([52f6e38](https://gitlab.com/mobivia-design/roadtrip/components/commit/52f6e38269afc1add015e3c868f7a1a9ec1a7378))
* **dialog:** update colors and paddings ([7a96b8a](https://gitlab.com/mobivia-design/roadtrip/components/commit/7a96b8ac6c7ec439d73cedc21ac48f20de5130a5))
* **dialog:** update icon to use outline version ([a440a28](https://gitlab.com/mobivia-design/roadtrip/components/commit/a440a2882d1d3f0de47b965a3fc5bb8d376d1e8c))
* **docs:** fix documentation title ([e34d656](https://gitlab.com/mobivia-design/roadtrip/components/commit/e34d65612b46f405ee1427e60def2013a741e392))
* **docs:** update license fil format ([008e6a3](https://gitlab.com/mobivia-design/roadtrip/components/commit/008e6a3b1e99c38555c3ff7a69045ef32059b170))
* **drawer, item:** add icon customisation ([5ec8895](https://gitlab.com/mobivia-design/roadtrip/components/commit/5ec8895b18c9abfc417639cbc01baf44ca778954))
* **drawer:** add css variable to customize header color ([d987bf2](https://gitlab.com/mobivia-design/roadtrip/components/commit/d987bf2b747896e8a4a193dab3c2844e1929866a))
* **drawer:** add delimiter css variable ([c1a72ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/c1a72eaee7c84584bcb927f7e43dfa5790d73f0f))
* **drawer:** add title customisation ([838b4c8](https://gitlab.com/mobivia-design/roadtrip/components/commit/838b4c88e2ce1f91d155c646edf79979ef3f46b5))
* **drawer:** scroll to top after closing ([d89068d](https://gitlab.com/mobivia-design/roadtrip/components/commit/d89068d41fdd2eb4f56f9f0e0f1b5411412157d3))
* drop IE support ([208a8b5](https://gitlab.com/mobivia-design/roadtrip/components/commit/208a8b5306fcf227a23b8c53df81871773d50acd))
* **dropdown:** add hover button ([0d1c69b](https://gitlab.com/mobivia-design/roadtrip/components/commit/0d1c69b1efcddc1b424bcac1412faaccdfcb9efa))
* **dropdown:** add medium size option ([0849c69](https://gitlab.com/mobivia-design/roadtrip/components/commit/0849c69d950b2708698b794ae14e25ded9bb803a))
* **dropdown:** update items ([de1eef4](https://gitlab.com/mobivia-design/roadtrip/components/commit/de1eef48614e12b8f3d2cca70e88d10a63d42abb))
* **dropdown:** update margin stories ([deacbac](https://gitlab.com/mobivia-design/roadtrip/components/commit/deacbac8c09377976da05a8701b554fde2f213ce))
* **dropwdown:** add position and direction ([6f02d40](https://gitlab.com/mobivia-design/roadtrip/components/commit/6f02d40a88a71819c5407b3ebed37d219e9e25da))
* **flag:** update design and add new size ([985e36d](https://gitlab.com/mobivia-design/roadtrip/components/commit/985e36d77db5bf5d106bd5f07d328127cbe50a3d))
* **flap:** add color black for blackfriday ([00e503c](https://gitlab.com/mobivia-design/roadtrip/components/commit/00e503c6efb3c63a6682aa4c6a32e6a239030a6d))
* **flap:** add ecology color ([8661cfa](https://gitlab.com/mobivia-design/roadtrip/components/commit/8661cfaaacd780b4d5f8fe17c14db4bdadd7878e))
* **flap:** update colors ([e7fc667](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7fc6670daa453f43cbb7dccc0420b41dedd5bce))
* **floating-button:** add new componant ([a4dce5a](https://gitlab.com/mobivia-design/roadtrip/components/commit/a4dce5a212870b1339a60f6dc5d214dfe4ccb4e3))
* **form:** add required for input and select ([aa145ec](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa145ec5f633806e8c3c13f2e699e6c288b63cca))
* **forms:** add hover state ([71dcce0](https://gitlab.com/mobivia-design/roadtrip/components/commit/71dcce013ddc7c1db2cdf9bc2c1d806b1f172cef))
* **form:** update font size for input and select ([eddffbf](https://gitlab.com/mobivia-design/roadtrip/components/commit/eddffbf664fc8ea7b644d282d0a4d82f00beeb82))
* **form:** update font size for input and select ([43bbbd0](https://gitlab.com/mobivia-design/roadtrip/components/commit/43bbbd0b0f507fc1a71c8cbb038e29053f8c0bf8))
* **globalNavigation:** add new component ([6b7ebdf](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b7ebdf2347c90f482cc1f1a338b1d290b6441e3))
* **grid:** update margin ([59d1fea](https://gitlab.com/mobivia-design/roadtrip/components/commit/59d1feae7004b2a420e2b603ce6994cca7fe79e6))
* **icon:** add and improve timer and lock icons ([9877c03](https://gitlab.com/mobivia-design/roadtrip/components/commit/9877c0350dfc77a9bb342ee5007463953e3870b8))
* **icon:** add and update icons ([cc61738](https://gitlab.com/mobivia-design/roadtrip/components/commit/cc61738c32f804c6ea24b3c6f2f9e473e76eecf4))
* **icon:** add and update icons ([20f43c8](https://gitlab.com/mobivia-design/roadtrip/components/commit/20f43c856a73c2c52c21244dac29e3dd736752ba))
* **icon:** add and update icons ([d5a2d97](https://gitlab.com/mobivia-design/roadtrip/components/commit/d5a2d97a7f99a5280d27c839eeabcf04829e0bb0))
* **icon:** add and update icons ([4644b1e](https://gitlab.com/mobivia-design/roadtrip/components/commit/4644b1e28755049a20d318912b268574599726c7))
* **icon:** add and update icons ([1aeb659](https://gitlab.com/mobivia-design/roadtrip/components/commit/1aeb659482be39bfbedd8040acbd97fccb391c18))
* **icon:** add and update icons ([0329660](https://gitlab.com/mobivia-design/roadtrip/components/commit/0329660c391df12801f2feb8f21c6a8e971dbcab))
* **icon:** add and update icons ([e8a4170](https://gitlab.com/mobivia-design/roadtrip/components/commit/e8a41705e9becbd74fe7342ab3ce3e2c0f465ae9))
* **icon:** add and update icons ([01d8751](https://gitlab.com/mobivia-design/roadtrip/components/commit/01d875122af951df4c4b8eb48146e5d6d9f1238e))
* **icon:** add and update icons ([e676c91](https://gitlab.com/mobivia-design/roadtrip/components/commit/e676c914f3e97c690f642b49fe72ce7816e0d7d3))
* **icon:** add and update icons ([22bfba1](https://gitlab.com/mobivia-design/roadtrip/components/commit/22bfba1be9c7014a7dfa73e497e9b323d9375d39))
* **icon:** add and update icons ([62fff5b](https://gitlab.com/mobivia-design/roadtrip/components/commit/62fff5b790876245a1b17a071119e2fb6e33d7a8))
* **icon:** add and update icons ([b434a35](https://gitlab.com/mobivia-design/roadtrip/components/commit/b434a354839ee8a95e46b45df3e5f877e1061336))
* **icon:** add and update icons ([fee5305](https://gitlab.com/mobivia-design/roadtrip/components/commit/fee5305aff7b71ec4c986306d0dd917ae58eb386))
* **icon:** add and update icons ([8aee992](https://gitlab.com/mobivia-design/roadtrip/components/commit/8aee992978f2d29cf83e6c230fd307e33a0752cd))
* **icon:** add and update icons ([66fe56c](https://gitlab.com/mobivia-design/roadtrip/components/commit/66fe56c52934fdda2cdd6b26d001c49134ee1ff7))
* **icon:** add and update icons ([4fa3537](https://gitlab.com/mobivia-design/roadtrip/components/commit/4fa3537cef5eb7c29c1d86f962e393e536867b5a))
* **icon:** add and update icons ([b67cc1a](https://gitlab.com/mobivia-design/roadtrip/components/commit/b67cc1a3cf2a1781406d978fbc2d4844f2fb3604))
* **icon:** add and update icons ([83b6bc6](https://gitlab.com/mobivia-design/roadtrip/components/commit/83b6bc67a149bb27c2614a1952f8b139a82a9c5c))
* **icon:** add and update icons ([007f1b0](https://gitlab.com/mobivia-design/roadtrip/components/commit/007f1b07e7ec1ce9db6d05fb393277aa9eda9ee7))
* **icon:** add and update icons ([9d097c1](https://gitlab.com/mobivia-design/roadtrip/components/commit/9d097c1562f65b10a122d2e15fa57a93bad5847b))
* **icon:** add and update icons ([7d43f1c](https://gitlab.com/mobivia-design/roadtrip/components/commit/7d43f1cd7d4177b945195b7329b338fbeb1f3ae8))
* **icon:** add and update icons ([74067f0](https://gitlab.com/mobivia-design/roadtrip/components/commit/74067f041d4719261779eef949e5e64ffd1c202a))
* **icon:** add and update icons ([2abe584](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abe584dff33451dc0bf9d49fe0222dd73bf58ac))
* **icon:** add icon saison swap ([2524a97](https://gitlab.com/mobivia-design/roadtrip/components/commit/2524a97965532cd2d91ac01746a08a01ffbbe446))
* **icon:** add icons ([8867503](https://gitlab.com/mobivia-design/roadtrip/components/commit/8867503c60dd10b97d560ba286a8f32bc25d04ca))
* **icon:** add icons ([0da9999](https://gitlab.com/mobivia-design/roadtrip/components/commit/0da999997438c9ef5bab9f4c36be3eec43888ff8))
* **icon:** add icons ([e787344](https://gitlab.com/mobivia-design/roadtrip/components/commit/e787344c4fe472ed3203d8d792a3a93c0dd98b57))
* **icon:** add people-customer-review ([4a543e5](https://gitlab.com/mobivia-design/roadtrip/components/commit/4a543e5cc52b0fb4041cb1c9b0616dde3fada69c))
* **icon:** add start and stop icon ([aa7881c](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa7881c947194a87ef120e62e4e74fd69eedebe1))
* **icon:** add vehicle doors color icon variants ([c81664d](https://gitlab.com/mobivia-design/roadtrip/components/commit/c81664d88dfa075b79989bbda6843a6411e724d1))
* **icon:** add vehicle tires color variant for each tire ([97d2e8a](https://gitlab.com/mobivia-design/roadtrip/components/commit/97d2e8a48422d3db7d094ed683c8303eefcfc5af))
* **icon:** add world, saveDisk and meetingOff icons ([ad15d75](https://gitlab.com/mobivia-design/roadtrip/components/commit/ad15d7520cf8af0dcaca4a8251b9e42c0ab40851))
* **icon:** generate icons variables ([a7f7526](https://gitlab.com/mobivia-design/roadtrip/components/commit/a7f7526b5b0877416c63685691802ba83f97dc0c))
* **icon:** reduce size of flag icons ([78a199f](https://gitlab.com/mobivia-design/roadtrip/components/commit/78a199feec29845ea3e29f1d67147bf470107320))
* **icons:** add icon ([076bbb6](https://gitlab.com/mobivia-design/roadtrip/components/commit/076bbb659198499dfa3bb3329976ea0796dedb24))
* **icons:** add icons ([aa22ad8](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa22ad8f40b3717a9fa6d0cf4376cf7b870a15ec))
* **icons:** add icons ([ebbe0d3](https://gitlab.com/mobivia-design/roadtrip/components/commit/ebbe0d3318b12c3eedb58b67ac95b7f6f0ae9fb4))
* **icons:** add icons ([a114deb](https://gitlab.com/mobivia-design/roadtrip/components/commit/a114deb5072e5fefb7b03434d76f5b10d84fb741))
* **icons:** add icons ([ef86222](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef86222c5cf1ff3da90b2ca38445a927f013ce2e))
* **icons:** add icons ([dc474c2](https://gitlab.com/mobivia-design/roadtrip/components/commit/dc474c234ec4a1fa975ff2f806490dc2071df733))
* **icons:** add icons ([8d50d18](https://gitlab.com/mobivia-design/roadtrip/components/commit/8d50d18406d2cffe81f487b244c413c2cb1fa3e0))
* **icons:** add icons ([d38d42a](https://gitlab.com/mobivia-design/roadtrip/components/commit/d38d42af5c3cd115d15f01f954e9d75bee0f5351))
* **icons:** add icons ([2c49cf1](https://gitlab.com/mobivia-design/roadtrip/components/commit/2c49cf1eed87078d3e55af72448bd16a6ced89ca))
* **icons:** add icons ([eb329b2](https://gitlab.com/mobivia-design/roadtrip/components/commit/eb329b22626089cd4019abcfe29d1b542da14e1e))
* **icons:** add icons flag uk ecologic driver ([d3d3eec](https://gitlab.com/mobivia-design/roadtrip/components/commit/d3d3eec1002109d2693ace01a4dca889193aaafb))
* **icons:** add icons van life + atu ([2cb9bf5](https://gitlab.com/mobivia-design/roadtrip/components/commit/2cb9bf52e0cc240f481900b664d80fd61b47e084))
* **icons:** add update icons ([2200d6e](https://gitlab.com/mobivia-design/roadtrip/components/commit/2200d6eaef9ddfe457eb2e7c9657f32e35c6decc))
* **icons:** add variants and icons ([77a0766](https://gitlab.com/mobivia-design/roadtrip/components/commit/77a0766a1763f9d934755cedbcdf9bff6bc27372))
* **icons:** optimisation fill ([f46cb96](https://gitlab.com/mobivia-design/roadtrip/components/commit/f46cb96aeab7c168c5e568519537ef68763d4caa))
* **icons:** update icon color ([34c00a2](https://gitlab.com/mobivia-design/roadtrip/components/commit/34c00a284562025acdf8baa720255a7183728fe4))
* **icons:** update icons figma ([0a25a6c](https://gitlab.com/mobivia-design/roadtrip/components/commit/0a25a6c8f4c0bbf3ae53c341c399d9bbb21d7ceb))
* **icon:** update color icon token ([57b688a](https://gitlab.com/mobivia-design/roadtrip/components/commit/57b688a14cb49579d6c9b257369f2eacb348c380))
* **icon:** update icon with fallback color ([56a455d](https://gitlab.com/mobivia-design/roadtrip/components/commit/56a455d084bbf998d31b01d43f1a161c1c971e18))
* **icon:** update icons ([55ecc4c](https://gitlab.com/mobivia-design/roadtrip/components/commit/55ecc4ce70916c3b71b487c56ab72e43404b9425))
* **input-group:** update to handle input size xl ([0b79af9](https://gitlab.com/mobivia-design/roadtrip/components/commit/0b79af9028f3b4ed09a1ada2ac9c1cf48ad1fd06))
* **input:** add password ([0de7c54](https://gitlab.com/mobivia-design/roadtrip/components/commit/0de7c540696ec53298706003892359e57b4559c8))
* **input:** add type time ([73a284d](https://gitlab.com/mobivia-design/roadtrip/components/commit/73a284ddbc3fcbe63ed3e33c9393b86b1eccf0e7))
* **input:** update colors and font-size ([2abeace](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abeaceb0521f914b26f1c6ca8c5bb2e76c97212))
* **link:** add variant white ([8b95191](https://gitlab.com/mobivia-design/roadtrip/components/commit/8b9519194800f596e52a90d6172297cab9eb58c5))
* **modals:** mover layer color value to the CSS variable ([12f5fa9](https://gitlab.com/mobivia-design/roadtrip/components/commit/12f5fa95b61e83808dd42e588536da183f4d7069))
* move fonts declaration in its own file ([481f0a9](https://gitlab.com/mobivia-design/roadtrip/components/commit/481f0a9e2c45a6334c3ea762c982bf34d8ed7c8a))
* **navbar-item:** update colors and label text size ([05ba3b4](https://gitlab.com/mobivia-design/roadtrip/components/commit/05ba3b422a6bc9fa56257f35a0443db7a554125b))
* **navbar:** add drawer ([cae1433](https://gitlab.com/mobivia-design/roadtrip/components/commit/cae1433339f9445820e123cf6f4a541de3800200))
* **plate-number:** add Austrian and german format ([023c568](https://gitlab.com/mobivia-design/roadtrip/components/commit/023c568a204fabe06fa1231f407a07ac758ef591))
* **PlateNumber:** add disabled and readonly state ([4b9cafc](https://gitlab.com/mobivia-design/roadtrip/components/commit/4b9cafc88e8b4ce7faa590286ea51974f6976866))
* **plateNumber:** add event bindings ([e68afc3](https://gitlab.com/mobivia-design/roadtrip/components/commit/e68afc3c849c8b15b7350711d492da017f203b5c))
* **plateNumber:** add events ([4c004ac](https://gitlab.com/mobivia-design/roadtrip/components/commit/4c004acc1473c7fca84d64967076fe67e6d2ecb8))
* **PlateNumber:** add motorbike plate ([8151618](https://gitlab.com/mobivia-design/roadtrip/components/commit/81516189439daea79e8511204d1be186d90dd4f3))
* **plateNumber:** Add placeholder attribute ([a37e00d](https://gitlab.com/mobivia-design/roadtrip/components/commit/a37e00dabbca242a233a4a808ce273729a6a1f51))
* **plateNumber:** update Belgian plate to be closer to real one ([2731ddf](https://gitlab.com/mobivia-design/roadtrip/components/commit/2731ddfe4a931a10bdbf13604ea9298bcc270a0b))
* **plateNumber:** update colors and font-size ([2799edf](https://gitlab.com/mobivia-design/roadtrip/components/commit/2799edfe19a538748482b8e41baedcff371f9d59))
* **plateNumber:** update colors nad margins ([d625dc4](https://gitlab.com/mobivia-design/roadtrip/components/commit/d625dc4ad84dcafaa5ff55518296d54803b5e6d8))
* **plateNumber:** update country placeholders ([015f276](https://gitlab.com/mobivia-design/roadtrip/components/commit/015f2769a1a54628e8d730276416340be428710c))
* **profil-dropdown:** add new component ([bf95bb3](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf95bb386a62ea65236be8254b8633a5a16eae85))
* **progress-tracker:** add new composent ([573bd13](https://gitlab.com/mobivia-design/roadtrip/components/commit/573bd1390b1efba4101860eba5782312a1f59a50))
* **progress:** add color rating ([929eab6](https://gitlab.com/mobivia-design/roadtrip/components/commit/929eab68ac27446c71d470b81ac47d976288162c))
* **progress:** add label ([a203656](https://gitlab.com/mobivia-design/roadtrip/components/commit/a20365622e2c918c746c778fecc76172d27b8a37))
* **progress:** add light background option ([23a4dab](https://gitlab.com/mobivia-design/roadtrip/components/commit/23a4dab47f026f6d8e96dc6ec9bd9b24e91d5ef5))
* **progress:** add option step ([326ff7b](https://gitlab.com/mobivia-design/roadtrip/components/commit/326ff7b4e9c3e2b3bad898a7d5de5480dc44d291))
* **progress:** add primary and secondary colors ([e095689](https://gitlab.com/mobivia-design/roadtrip/components/commit/e0956892cd45e0ac4f5c2844a1b85822bebe28a9))
* **progress:** update colors ([5746ecd](https://gitlab.com/mobivia-design/roadtrip/components/commit/5746ecdeb10fe3d50d5c1648dacb77b5d1c4d994))
* **radio:** add more contrast in disabled state ([3c89f45](https://gitlab.com/mobivia-design/roadtrip/components/commit/3c89f45e171fc4930afe64582e63acf8f0110741))
* **radio:** Add props to inverse radio and label order ([86956cd](https://gitlab.com/mobivia-design/roadtrip/components/commit/86956cdf3733e8e1162a3a3539491187aba99a6d))
* **RadioGroup:** Required field missing ([a359bdd](https://gitlab.com/mobivia-design/roadtrip/components/commit/a359bdd05dcc30ad43bbedc6f305aeecab4d1d72))
* **radio:** update colors and font-size ([8ef807d](https://gitlab.com/mobivia-design/roadtrip/components/commit/8ef807dbac414605eed91240b5d16122d01ca546))
* **radio:** update event listener to handle Vue3 ([4e2bf51](https://gitlab.com/mobivia-design/roadtrip/components/commit/4e2bf51fad1db9f941b8bf5adca3fc8c499ac2f3))
* **range:** add labels ([5139410](https://gitlab.com/mobivia-design/roadtrip/components/commit/5139410f196d9d0062cd4dd13da15732471d16b9))
* **range:** add tick and color focus active ([b9bf04c](https://gitlab.com/mobivia-design/roadtrip/components/commit/b9bf04c93f0e73e0076a6be76aace09e60fd993b))
* **rating:** add small version ([376bc0a](https://gitlab.com/mobivia-design/roadtrip/components/commit/376bc0af9d97928c32af2eaa0e659f5adf46baa1))
* **rating:** update colors and font size ([fe07841](https://gitlab.com/mobivia-design/roadtrip/components/commit/fe07841119c1ca3833e33d17be0f5dac4c2f6870))
* remove tap highlight on mobile ([d9b7375](https://gitlab.com/mobivia-design/roadtrip/components/commit/d9b737524370bc4218028ddc7eec6f59378c945f))
* **search:** add  button ([5b4573d](https://gitlab.com/mobivia-design/roadtrip/components/commit/5b4573d59e1184e703f3027bd90012f16703c83f))
* **segementedbuttons:** add new component ([624a51e](https://gitlab.com/mobivia-design/roadtrip/components/commit/624a51ed95c1acf11a29e9a383357287a42d4a07))
* **segmented-buttons:** add size sm ([38db72a](https://gitlab.com/mobivia-design/roadtrip/components/commit/38db72ab4bfe5df65071912a2d1791280b2081da))
* **select-filter:** add Blur and active ([f0b6c57](https://gitlab.com/mobivia-design/roadtrip/components/commit/f0b6c57552f179b49ad53b4e53525a0877594a4d))
* **select-filter:** add fuzzysearch ([9ab929a](https://gitlab.com/mobivia-design/roadtrip/components/commit/9ab929a8d073a80821f607dd1b9b2d91d3c663b9))
* **select-filter:** add phone number ([0961914](https://gitlab.com/mobivia-design/roadtrip/components/commit/096191401236e6b61edb28d18e6ade43c999ef2a))
* **select-filter:** pass html as options ([c8a549b](https://gitlab.com/mobivia-design/roadtrip/components/commit/c8a549b9aef02727af36dd534337da063e04dbd1))
* **select-filter:** update colors and font-size ([02e732d](https://gitlab.com/mobivia-design/roadtrip/components/commit/02e732d019aa685e93960dbda05e190327a5bf15))
* **select:** update colors and font-size ([53aded0](https://gitlab.com/mobivia-design/roadtrip/components/commit/53aded002f5340a4f1c3e93f81723112eb22caa8))
* **skeleton:** update color and proportion ([72f8cb3](https://gitlab.com/mobivia-design/roadtrip/components/commit/72f8cb3086e32c8597e6c1c14c45d35546da4610))
* **skeleton:** update grey values ([e30e127](https://gitlab.com/mobivia-design/roadtrip/components/commit/e30e1276b4d1fdee941ee091d12bb5d207d2096b))
* **skeleton:** update margin and height ([1e222ad](https://gitlab.com/mobivia-design/roadtrip/components/commit/1e222add8ccca7a30f08313a9ae46383509be9d6))
* **spacing:** add 4px padding and margin utilities ([203ccc1](https://gitlab.com/mobivia-design/roadtrip/components/commit/203ccc14b56dd297c261b89d902a9a340878d5e3))
* **spinner:** add xl size and color ([2abf306](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abf306e0f86cbc04f796f7744c95c8e353d1af2))
* **stepper:** add component stepper ([5f9b636](https://gitlab.com/mobivia-design/roadtrip/components/commit/5f9b636c030283e9f1d9c9db8ba7705a2862b7e2))
* **storybook:** add utility stories ([a9db6fb](https://gitlab.com/mobivia-design/roadtrip/components/commit/a9db6fb5d3bb388009f54782784da4dacca0eb0d))
* **switch:** customize width of the lever ([f1c1918](https://gitlab.com/mobivia-design/roadtrip/components/commit/f1c19180956579c6821cad2b00e561de2560ac02))
* **tag:** add contrast option ([40844f9](https://gitlab.com/mobivia-design/roadtrip/components/commit/40844f96382abc379149f98dfa9bc085cf1be4e2))
* **tag:** add new component ([01e6d86](https://gitlab.com/mobivia-design/roadtrip/components/commit/01e6d86e9975125a6418e603215b7862c443ef7d))
* **textarea:** add option resize ([41a6924](https://gitlab.com/mobivia-design/roadtrip/components/commit/41a6924cb73053370fd378e4b107bba2f80d030a))
* **textarea:** update colors and font-size ([e9b0673](https://gitlab.com/mobivia-design/roadtrip/components/commit/e9b0673f7823e7fcfb516e29353690850224bd2b))
* **theme:** add Auto5 theme ([38006d6](https://gitlab.com/mobivia-design/roadtrip/components/commit/38006d6e0960f6ca36c099236fb65aed56b42cec))
* **theme:** add mobivia theme ([2ba3e77](https://gitlab.com/mobivia-design/roadtrip/components/commit/2ba3e77b0c06e71b095bb58d2a89297edf26b7c1))
* **theme:** update ATU and Midas themes ([4f101c6](https://gitlab.com/mobivia-design/roadtrip/components/commit/4f101c68d2cc467a57a8e52144f8f7dbe9fb747a))
* **theme:** update ATU theme ([202cad4](https://gitlab.com/mobivia-design/roadtrip/components/commit/202cad4896705b459104f4c350a72da583b8c843))
* **toast:** add progress bar in the toast ([e277094](https://gitlab.com/mobivia-design/roadtrip/components/commit/e277094ed5f7a393e72f6f6d1678aff610202d15))
* **toast:** update icon color for warning state ([3720a20](https://gitlab.com/mobivia-design/roadtrip/components/commit/3720a2023c47f5486fbbdeb8dac3384d89b70ee5))
* **toggle:** update colors and font-size ([be84a52](https://gitlab.com/mobivia-design/roadtrip/components/commit/be84a52ebab43c7484cc1963bffbf613b094a6d1))
* **tokens:** add decorative tokens ([b0c94b3](https://gitlab.com/mobivia-design/roadtrip/components/commit/b0c94b39c3ba90fd41481643baee6b5394dcb911))
* **tokens:** add neutral grey ([450069c](https://gitlab.com/mobivia-design/roadtrip/components/commit/450069cc1276b01cff8a505bd1e4c713377bb0ee))
* **tokens:** add tag - banner- rating tokens ([d9cf316](https://gitlab.com/mobivia-design/roadtrip/components/commit/d9cf316811b33a180dd6b6805e07c8ad6aa82527))
* **tokens:** add tokens header disabled ([c02f1e8](https://gitlab.com/mobivia-design/roadtrip/components/commit/c02f1e864c89870f3d398e6fc39eb1378f524a27))
* **tokens:** add tokens on components ([f6f171b](https://gitlab.com/mobivia-design/roadtrip/components/commit/f6f171b2270188e771be63fd429a859826ac4d96))
* **tokens:** add tokens violet ([f5fb7cb](https://gitlab.com/mobivia-design/roadtrip/components/commit/f5fb7cbc35601c335c0ffbc134686d8ec7a64aad))
* **toolbar:** add header navigation ([083181f](https://gitlab.com/mobivia-design/roadtrip/components/commit/083181fb52bded4c0d88d19041424d63cf40cec8))
* **tooltip:** add line break support ([af4e874](https://gitlab.com/mobivia-design/roadtrip/components/commit/af4e874e546278b4b31985e1e8918948932ce893))
* **tooltip:** update size text and position ([2d2bf72](https://gitlab.com/mobivia-design/roadtrip/components/commit/2d2bf72b0e2e730a3a6c6bce0a06bddc7a4edcc7))
* **typography:** add color title ([8a5c2c2](https://gitlab.com/mobivia-design/roadtrip/components/commit/8a5c2c2c91f5a9e124a247689dff51c4ba1efdbb))
* **typography:** add color title and class text ([bf5a3e3](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf5a3e3e40ae9440c61819139f4911ae1b8c5aca))
* **typography:** add underline ([0637492](https://gitlab.com/mobivia-design/roadtrip/components/commit/06374922ce8ba3aafe95ecbf0738efdc17772d87))
* **typography:** update font-weight for h7, h8, h9 ([cbe2312](https://gitlab.com/mobivia-design/roadtrip/components/commit/cbe23128550569f8976e22671146f06029db3b7c))
* update ATU theme to reflect brand changes ([bd0da7b](https://gitlab.com/mobivia-design/roadtrip/components/commit/bd0da7b3e2e2eca27577a020358fca8843a180a6))
* update Auto5 theme ([a6379f6](https://gitlab.com/mobivia-design/roadtrip/components/commit/a6379f66d5a78921be582f14551541abd67e059a))
* update color system ([62c2f38](https://gitlab.com/mobivia-design/roadtrip/components/commit/62c2f38bcb2d205390f99e798dda974ebcd0f63f))
* update components colors ([47e781f](https://gitlab.com/mobivia-design/roadtrip/components/commit/47e781fc4eb682021ccbe9a1daa8dba9297d3c67))
* update font-size tabs ([954aa2b](https://gitlab.com/mobivia-design/roadtrip/components/commit/954aa2bd8bd86104a31b45fe714118dde60598dc))
* update Midas theme ([034ef65](https://gitlab.com/mobivia-design/roadtrip/components/commit/034ef653f40f996f7223e2057d2902baa38015a6))
* update swiper to 7.0.3 ([e002ac2](https://gitlab.com/mobivia-design/roadtrip/components/commit/e002ac24b36cd8c5e5e40ec99fa4f9b2b5c869ae))
* update theme architecture ([bda995a](https://gitlab.com/mobivia-design/roadtrip/components/commit/bda995a432cde172136813721959592cb9d61000))
* **variables:** add hover colors ([8a893ee](https://gitlab.com/mobivia-design/roadtrip/components/commit/8a893ee6c5267f8ba74d7651e9b02a36a7259a66))
* **variables:** add hover colors for themes ([4771fe6](https://gitlab.com/mobivia-design/roadtrip/components/commit/4771fe6459936025cdaec252904e87034b432ca1))
* **variables:** update component colors ([760b903](https://gitlab.com/mobivia-design/roadtrip/components/commit/760b9036f655514d0737bcc938e61e44c15285c3))
* **variables:** update icon colors ([99d9cf4](https://gitlab.com/mobivia-design/roadtrip/components/commit/99d9cf4defd4dc6b2f438a4b90576079a62b2eaf))
* **vertical-stepper:** add new component ([7e2c132](https://gitlab.com/mobivia-design/roadtrip/components/commit/7e2c132da444700f137144cffb19fa6a67af728f))


### Bug Fixes

* **accordion:**  max-height none ([78320bb](https://gitlab.com/mobivia-design/roadtrip/components/commit/78320bba80167303cf765b2418bc6049b61da39a))
* **accordion:** remove max-height ([cffbd25](https://gitlab.com/mobivia-design/roadtrip/components/commit/cffbd2596e419380f889ad111c333cb509cdfa00))
* **alert:** fix color text alert ([1ccc00f](https://gitlab.com/mobivia-design/roadtrip/components/commit/1ccc00f2ecc6f3359cbeaf70101b933183d8c6e3))
* **banner:** change color text in the banner ([b53fc73](https://gitlab.com/mobivia-design/roadtrip/components/commit/b53fc73a66766c9dafe51a96f119682547c65bb0))
* **build:** icons ([9b8d5d0](https://gitlab.com/mobivia-design/roadtrip/components/commit/9b8d5d0647871eb1f40d6cb206a48644eb9072ee))
* **build:** stencil ([f1d0d28](https://gitlab.com/mobivia-design/roadtrip/components/commit/f1d0d28b280d61c9c44433926284130af06eb2f3))
* **button:** add webkit for ios ([2c7f24b](https://gitlab.com/mobivia-design/roadtrip/components/commit/2c7f24be23554fc75edd410b6234331102ad1212))
* **button:** click disabled and counter size icon ([28a48b0](https://gitlab.com/mobivia-design/roadtrip/components/commit/28a48b0a03ea462f03fabfd474f4a4b3c3c97767))
* **button:** fix color disabled ([758e842](https://gitlab.com/mobivia-design/roadtrip/components/commit/758e842c30c1cf31fa2bb5fb493bbb36287ba4f7))
* **button:** icons ([e7ca331](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7ca331e82ac949b900ff220a475a47d80f46801))
* **buttons:** add outline default ([9db068f](https://gitlab.com/mobivia-design/roadtrip/components/commit/9db068f89ff3851fbdc55874d463fc10e8ad7065))
* **carousel:** remove unecessay files for swiper ([4fba7b1](https://gitlab.com/mobivia-design/roadtrip/components/commit/4fba7b1d8a71292b0147b0578c6174476d94df5d))
* **carousel:** test fix init ([f4e39b3](https://gitlab.com/mobivia-design/roadtrip/components/commit/f4e39b392bb0169ff522f020b08482b97276738a))
* **carousel:** test fix init ([d95d83f](https://gitlab.com/mobivia-design/roadtrip/components/commit/d95d83f945a5c717f7d947f7f83e9bd740fc23cb))
* ci build error ([124ed72](https://gitlab.com/mobivia-design/roadtrip/components/commit/124ed7226a34d80dd0cc18c4eff19668b881849d))
* ci build error ([b0c3be7](https://gitlab.com/mobivia-design/roadtrip/components/commit/b0c3be7160245eadf9f5de6c606a19db2bc83c35))
* ci build error ([d671a52](https://gitlab.com/mobivia-design/roadtrip/components/commit/d671a52777edcdbc8b2a57aa966415e0c4c60a60))
* ci build error ([b76c121](https://gitlab.com/mobivia-design/roadtrip/components/commit/b76c121a14b4f391e6d0b5d6dd53b7987f2cb9f2))
* **collapse:** increase max-height ([da2ba37](https://gitlab.com/mobivia-design/roadtrip/components/commit/da2ba37823e2d9578b2bce5b8a3fc9a410ba3c97))
* color icons weather ([e035eb6](https://gitlab.com/mobivia-design/roadtrip/components/commit/e035eb623af6583b1508d53c962188a630c6d8dd))
* **color:** fix color surface-header norauto ([67a1b76](https://gitlab.com/mobivia-design/roadtrip/components/commit/67a1b76fc56daa6804d711f7ac25e1065981ef78))
* **colors:** map old background value to new one ([8542d35](https://gitlab.com/mobivia-design/roadtrip/components/commit/8542d351195f3576576c70d3f7d6d1710d6d5a1e))
* **color:** update variable surface header ([e14bc39](https://gitlab.com/mobivia-design/roadtrip/components/commit/e14bc397dbf840e6a1867acc1450e58957b3b128))
* **counter:** add z-index ([66551c7](https://gitlab.com/mobivia-design/roadtrip/components/commit/66551c74cef8ae52a8438aaf9d851299d92f9708))
* **counter:** change element ([4303a36](https://gitlab.com/mobivia-design/roadtrip/components/commit/4303a3683c20da159127e0824bcb2fef42087c51))
* **counter:** finish implementation ([7d633b1](https://gitlab.com/mobivia-design/roadtrip/components/commit/7d633b1be22069130c640baae715275d3c351ca9))
* **counter:** finish tests ([c4056bd](https://gitlab.com/mobivia-design/roadtrip/components/commit/c4056bddd855258c4f61cd476bde758c8fbb0495))
* **counter:** fix null value and size ([7e286ee](https://gitlab.com/mobivia-design/roadtrip/components/commit/7e286eed67f90898937606691c8132fa03c4f45e))
* **counter:** start tests ([dc20f25](https://gitlab.com/mobivia-design/roadtrip/components/commit/dc20f253ce7126e611e3ba5b1241ea816bf14b1b))
* **dialog:** empty height header ([9d7fee8](https://gitlab.com/mobivia-design/roadtrip/components/commit/9d7fee8932319e152c4f85c73de0ee1400e6a01e))
* **dialog:** Layer closing the dialog even with close button hidden ([94e7741](https://gitlab.com/mobivia-design/roadtrip/components/commit/94e7741feaaa73335109a81067349ab7c5ef0c80))
* **dialog:** send close event after the close animation is finished. ([605ad5f](https://gitlab.com/mobivia-design/roadtrip/components/commit/605ad5ff9d04bfe553d1bbaeb72b0d890949dfed))
* **drawer:** add max height CSS variable ([c0aab78](https://gitlab.com/mobivia-design/roadtrip/components/commit/c0aab78cbe6ae276c6b62c93f6ca5a9f04238c50))
* **drawer:** fix height for bottom position ([d1dcf82](https://gitlab.com/mobivia-design/roadtrip/components/commit/d1dcf82a6079327b4e1eb0f7839807a0ee333326))
* **drawer:** send close event after the close animation is finished. ([96ac0d6](https://gitlab.com/mobivia-design/roadtrip/components/commit/96ac0d663cb5f3cb03f2d75d8423debd38787da1))
* fix immutable props ([f914a80](https://gitlab.com/mobivia-design/roadtrip/components/commit/f914a80fbab119adbfc23f444ebeb85a0632a1ee))
* **global-navigation:** add zindex ([ea40bad](https://gitlab.com/mobivia-design/roadtrip/components/commit/ea40bad32a4c31d968e38d81a16c5e1a4e014aab))
* **global-navigation:** fix position - zindex ([89f7ff7](https://gitlab.com/mobivia-design/roadtrip/components/commit/89f7ff70a5c357e5e5d35dc30107da6b5d3e8366))
* **icon:** color primary ([e56fbac](https://gitlab.com/mobivia-design/roadtrip/components/commit/e56fbacc50b6b76942cafa0c13b2ab3a6eb633b7))
* **icon:** fix flag icons ([ff2f8dc](https://gitlab.com/mobivia-design/roadtrip/components/commit/ff2f8dc46b3c30ebbda0cad293502063b3cf31f9))
* **icon:** fix path to icon for Stencil apps ([d69f656](https://gitlab.com/mobivia-design/roadtrip/components/commit/d69f65697ea6e98838b4cd6bcb471c5324b88bf5))
* **icon:** fix vehicle-car-bodywork naming icon ([e7c893f](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7c893fbada18685331579f529572c60fadb33e6))
* **icon:** fix vehicle-car-light-* light part invisible ([b0848f0](https://gitlab.com/mobivia-design/roadtrip/components/commit/b0848f05b21cb7d79e37b72341620ddd43375935))
* **icon:** remove #clip-path) causing problem for icon import ([a0cca13](https://gitlab.com/mobivia-design/roadtrip/components/commit/a0cca13e4d199ac1174b65d031c29103c43c1c7c))
* **icon:** remove unused css variable for wiper icons ([f032629](https://gitlab.com/mobivia-design/roadtrip/components/commit/f032629a470a30596911579ebc3577bf4eb175fb))
* **icons:** add new keep in repair ([4badeb4](https://gitlab.com/mobivia-design/roadtrip/components/commit/4badeb4df978b17efa36d1dadb26d3811a9a1cec))
* **icons:** add viewbox ([8dd3dde](https://gitlab.com/mobivia-design/roadtrip/components/commit/8dd3dde3bbf1591debf4ab9f50de33e9a06ab4e6))
* **icons:** add viewbox ([3ae8d52](https://gitlab.com/mobivia-design/roadtrip/components/commit/3ae8d52f3b05816373325feafb48387cbb12ab30))
* **icons:** black color flag ([9bfcdc9](https://gitlab.com/mobivia-design/roadtrip/components/commit/9bfcdc98a0c3ac0e6412706446f63d41e0d32e85))
* **icons:** build ([5c36334](https://gitlab.com/mobivia-design/roadtrip/components/commit/5c363343c5dc236aad10d95b80e96a7392f8f768))
* **icons:** build ([e4db3dd](https://gitlab.com/mobivia-design/roadtrip/components/commit/e4db3dda7437b48be55fe0014bf71633771a25ee))
* **icons:** color currentcolor ([b53e5b2](https://gitlab.com/mobivia-design/roadtrip/components/commit/b53e5b2c573deebea600dd133a47d0fd5ddbf3e1))
* **icons:** color icons alert ([8b561f1](https://gitlab.com/mobivia-design/roadtrip/components/commit/8b561f1dd1ddd6b759405f31682ef7cd4603936f))
* **icons:** color icons navbar / flag belgium ([a70bd58](https://gitlab.com/mobivia-design/roadtrip/components/commit/a70bd5846c2bf8ee3c5debe259e8ee82f6955041))
* **icons:** fix build ([4474197](https://gitlab.com/mobivia-design/roadtrip/components/commit/447419793bc81f4906b00e3f9bf7dc0bdb85a220))
* **icons:** fix build ([d71782d](https://gitlab.com/mobivia-design/roadtrip/components/commit/d71782d5c3a228ba10c1b25c0b9218d40ee6fe0f))
* **icons:** fix build ([792c601](https://gitlab.com/mobivia-design/roadtrip/components/commit/792c60140524627359563155f763bbccd67b0afc))
* **icons:** fix build ([e545327](https://gitlab.com/mobivia-design/roadtrip/components/commit/e545327d1ccc781ac5eb5e7808146a544bfdf446))
* **icons:** fix build ([6b7ad94](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b7ad94ef80d0e0a1a922c066aaa0be97e8c6ac6))
* **icons:** fix build ([24b30a9](https://gitlab.com/mobivia-design/roadtrip/components/commit/24b30a9e71175730f2725bb922f480d3b97b2f82))
* **icons:** fix clip path ([52bfcf7](https://gitlab.com/mobivia-design/roadtrip/components/commit/52bfcf7e38345bc3e89c65a852c05d3a308dc20d))
* **icons:** fix fill icons ([548281e](https://gitlab.com/mobivia-design/roadtrip/components/commit/548281e2d51f0bfd27d524423116ae8372e70e3d))
* **icons:** fix flag icons ([97ace0e](https://gitlab.com/mobivia-design/roadtrip/components/commit/97ace0e65b5cd63547761e0186032c5624f5225a))
* **icons:** fix icons svg ([75f7876](https://gitlab.com/mobivia-design/roadtrip/components/commit/75f7876b81156cc330f939b1c5903b6fcc097f61))
* **icons:** fix icons vehicle tires and 5 doors ([5f65b38](https://gitlab.com/mobivia-design/roadtrip/components/commit/5f65b38c99a8c2aabf65d8601165bcddb626844d))
* **icons:** fix icons.svg ([c0bec74](https://gitlab.com/mobivia-design/roadtrip/components/commit/c0bec74133d915b2c7c13fc626bcc5c61004d85d))
* **icons:** fix name season ([19aa5d5](https://gitlab.com/mobivia-design/roadtrip/components/commit/19aa5d51e38a890fcb1be30f3fe5ad78db811917))
* **icons:** flag - weather ([08b84ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/08b84ea25802595b673cded8973b4d7f8f7f86f9))
* **icons:** keep old name ([178310f](https://gitlab.com/mobivia-design/roadtrip/components/commit/178310fff2159d5bd45e66a6634b26ca901be981))
* **icons:** name icons ([eb97f15](https://gitlab.com/mobivia-design/roadtrip/components/commit/eb97f155a56fced5648c21d7a2d1b201b65e3ecb))
* **icons:** name icons ([d218998](https://gitlab.com/mobivia-design/roadtrip/components/commit/d218998f849bd55a187779e95708972bdf27201b))
* **icons:** name season ([c32a115](https://gitlab.com/mobivia-design/roadtrip/components/commit/c32a115670690ce01e0778a3c079d29eaba0a019))
* **icons:** remove fill path svg ([278ff44](https://gitlab.com/mobivia-design/roadtrip/components/commit/278ff440834780134055b3c9996174eb85f39d0a))
* **icons:** remove hexa for rbg ([5681eee](https://gitlab.com/mobivia-design/roadtrip/components/commit/5681eeebb092dfce98bc44dc91003824f1b03136))
* **icons:** stocktaking ([86ed1d8](https://gitlab.com/mobivia-design/roadtrip/components/commit/86ed1d8277b803de01ba5bfb852dca63f0913f66))
* **icons:** update color success ([01696e4](https://gitlab.com/mobivia-design/roadtrip/components/commit/01696e41c56d4291e0280addac03bb3fc9eb0f25))
* **input:** add data-cy ([3b0cb57](https://gitlab.com/mobivia-design/roadtrip/components/commit/3b0cb5768b45a332e34859d6cc6151ab09d84da7))
* **input:** duplicating characters ([394253d](https://gitlab.com/mobivia-design/roadtrip/components/commit/394253dd1597bfe33f1037674f09ac0c1674af63))
* **input:** fix opacity for Safari iOS 15+ ([27c7dc3](https://gitlab.com/mobivia-design/roadtrip/components/commit/27c7dc30148c424177aa1e5c1227ebd3428a1ab7))
* **input:** fix placeholder opacity for firefox ([8647025](https://gitlab.com/mobivia-design/roadtrip/components/commit/864702505d05ba89b9d8a4eb581c79003bc19e76))
* **inputGroup:** fix button background hover ([6b0f4d9](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b0f4d912c57d8b0a82805e59e82ac04cbde6ae9))
* **inputGroup:** fix styles for disabled and error states ([32e78f5](https://gitlab.com/mobivia-design/roadtrip/components/commit/32e78f5542d95f1fd8016bd72ec04b20fe85a080))
* **item:** fix detail icon when used in frameworks ([fe613c0](https://gitlab.com/mobivia-design/roadtrip/components/commit/fe613c0fe6610e0c158e0b0d8e39526a2dd6cc88))
* **loader:** fix path esm ([9aaa29b](https://gitlab.com/mobivia-design/roadtrip/components/commit/9aaa29b5122b78c3f06faca30afcbb3c8f112684))
* **modal:** send close event after the close animation is finished. ([863860a](https://gitlab.com/mobivia-design/roadtrip/components/commit/863860a68b677a716374355c58178c954e212bbf))
* **navbar:** add tooltip ([162de95](https://gitlab.com/mobivia-design/roadtrip/components/commit/162de95b10a3606ae271bb4309214af3ddd81224))
* **navbar:** add tooltip navbar item ([25fb008](https://gitlab.com/mobivia-design/roadtrip/components/commit/25fb0085123f705a2b643350d64ae45c36672c9d))
* **plate-number:** Duplicating characters ([86d24f7](https://gitlab.com/mobivia-design/roadtrip/components/commit/86d24f789729a891cb213168897df1df8138499e))
* **plate-number:** Duplicating characters ([5bc18ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/5bc18eac34059f4a044b18e7062cab2e7bee31cd))
* **plateNumber:** fix icon import ([bd73768](https://gitlab.com/mobivia-design/roadtrip/components/commit/bd73768ef9c4a43175a75cc6ed8ef6fc1c8c7e77))
* **profil-dropdown:** add slot name email ([a641ca1](https://gitlab.com/mobivia-design/roadtrip/components/commit/a641ca147cd20f9a77d2c6e9a33bc242ffeb6410))
* **profil-dropdown:** add z-index ([33ab966](https://gitlab.com/mobivia-design/roadtrip/components/commit/33ab966ea9e987ff926a245e69415652675bde21))
* **profil-dropdow:** width size long name ([72ccad9](https://gitlab.com/mobivia-design/roadtrip/components/commit/72ccad933cb67f984f58543c5cf697c3278d52f1))
* **progress:** add padding full width ([5123f3a](https://gitlab.com/mobivia-design/roadtrip/components/commit/5123f3a5cd3596efe530ec74e47becec22378ccd))
* **progress:** remove console log ([97356e2](https://gitlab.com/mobivia-design/roadtrip/components/commit/97356e23f855bc4d01768644642766d84cccf95e))
* **radio:** fix alignment ([2f201a3](https://gitlab.com/mobivia-design/roadtrip/components/commit/2f201a3f1e170a2093382e7b00b1c474c73c4c36))
* **radio:** fix disabled ([31fec72](https://gitlab.com/mobivia-design/roadtrip/components/commit/31fec72982a61549b38b18c24710512e09123661))
* **rating:** fix half rate ([2a98ab8](https://gitlab.com/mobivia-design/roadtrip/components/commit/2a98ab8654da43ca70d2e21ed565444452d4b9dd))
* **rating:** fix half rate ([fd977d9](https://gitlab.com/mobivia-design/roadtrip/components/commit/fd977d909d273cc4d5f5e87082098ddd9e269c9a))
* **rating:** Remove space when reaviewText is empty ([801c423](https://gitlab.com/mobivia-design/roadtrip/components/commit/801c4237894cb7f579073e780acf6b21fd3f8cf6))
* **road-button:** remove z-index ([5ffc853](https://gitlab.com/mobivia-design/roadtrip/components/commit/5ffc853e6e2a949cbb4da8dd178b0f913a9c95fe))
* **select-filter:** fix console name ([3c79c43](https://gitlab.com/mobivia-design/roadtrip/components/commit/3c79c43a81d7b5881024eea1a7e7949807e28346))
* **select-filter:** fix select value ([076dbd6](https://gitlab.com/mobivia-design/roadtrip/components/commit/076dbd624c323b074dadb782b8d1a38201f01019))
* **select-filter:** remove value only if not part of the options ([c868779](https://gitlab.com/mobivia-design/roadtrip/components/commit/c8687792ba2758734546148a8a2ad742afa44123))
* **select:** selected value in options should be optional ([f8bb721](https://gitlab.com/mobivia-design/roadtrip/components/commit/f8bb72100a2b32af65761b83dbc71c8de2f569e5))
* **skeleton:** fix animation ([e650437](https://gitlab.com/mobivia-design/roadtrip/components/commit/e65043770b7ea38a1cc5d89f755e897f4629e119))
* **spinner:** add size spinner ([c10637d](https://gitlab.com/mobivia-design/roadtrip/components/commit/c10637ddc0cdb666bd26cfce49b6c77514521247))
* **stepper:** add option link step ([2553945](https://gitlab.com/mobivia-design/roadtrip/components/commit/2553945ee8ef257f9aac5882bce829f667d24ad6))
* **storybook:** fix stories examples ([54677a6](https://gitlab.com/mobivia-design/roadtrip/components/commit/54677a609016cb41de95530240ecf5df5850bf82))
* **switch:** prevent lever from shrinking with long label ([ef000d1](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef000d1d5defc7c85ad7f283541d3695eedbf564))
* **tabs:** add 2 lines display in mobile ([323b2df](https://gitlab.com/mobivia-design/roadtrip/components/commit/323b2dfbcbc8fb4c2f700da660239a028ee8646e))
* **tag:** fix color tag ([0f02f98](https://gitlab.com/mobivia-design/roadtrip/components/commit/0f02f98d282ea080a719f32a52c1263e065405fb))
* **themes:** fix themes with new color system ([df67ffc](https://gitlab.com/mobivia-design/roadtrip/components/commit/df67ffc8302aa447039a7bac2416a05696e8090a))
* **toast:** add pointer event ([6b616ce](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b616ce1e34f3109cc78cc02f7409d88eaf99a87))
* **toast:** fix position mobile ([3586980](https://gitlab.com/mobivia-design/roadtrip/components/commit/358698073381926b52980e0bdfa50300f2819912))
* **tokens:** color on header badge mobivia ([f1ada78](https://gitlab.com/mobivia-design/roadtrip/components/commit/f1ada78006e03e1893ba3a0dec33f05930162266))
* **tokens:** update name header badge ([9a053bb](https://gitlab.com/mobivia-design/roadtrip/components/commit/9a053bb2651cf3258f556cd201950309f1da16a2))
* **token:** update color token ([e281289](https://gitlab.com/mobivia-design/roadtrip/components/commit/e2812896aac516e51569c93c5746c05c0dbdcc7a))
* **toolbar:** fix button back ([d1e8420](https://gitlab.com/mobivia-design/roadtrip/components/commit/d1e842003f5f8bacc7b0d1251aaa3bdec469d660))
* **toolbar:** size title ([9d5828f](https://gitlab.com/mobivia-design/roadtrip/components/commit/9d5828f825df03efa0bf31727fd2c3960f26311b))
* **utilities:** bg light ([1a92e5b](https://gitlab.com/mobivia-design/roadtrip/components/commit/1a92e5b620c207d90611d523c2b012d2c50d61a3))
* **variable:** missing one variable ([bc3a73a](https://gitlab.com/mobivia-design/roadtrip/components/commit/bc3a73a7cf57d7ad8fac51ed2c6566ae0414360b))

## [3.6.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.5.0...3.6.0) (2023-06-08)

### Features

* **tag:** add new component ([01e6d86](https://gitlab.com/mobivia-design/roadtrip/components/commit/01e6d86e9975125a6418e603215b7862c443ef7d))
* **icons:** add icons van life + atu ([2cb9bf5](https://gitlab.com/mobivia-design/roadtrip/components/commit/2cb9bf52e0cc240f481900b664d80fd61b47e084))

### Bug Fixes

* **toast:** add pointer event ([6b616ce](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b616ce1e34f3109cc78cc02f7409d88eaf99a87))

### Refactor

* **progress-indicator-vertical:** refacto stepper-vertical by progress-indicator-vertical ([327cbd7](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/327cbd7f769190f01e43d27bee32a5f7dcaeb541
))

## [3.5.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.4.0...3.5.0) (2023-06-01)

### Features

* **tokens:** add tokens violet ([f5fb7cb](https://gitlab.com/mobivia-design/roadtrip/components/commit/f5fb7cbc35601c335c0ffbc134686d8ec7a64aad))
* **tokens:** add neutral grey ([450069c](https://gitlab.com/mobivia-design/roadtrip/components/commit/450069cc1276b01cff8a505bd1e4c713377bb0ee))
* **icons:** add icons ([aa22ad8](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa22ad8f40b3717a9fa6d0cf4376cf7b870a15ec))

### Bug Fixes

* **button:** fix color disabled ([758e842](https://gitlab.com/mobivia-design/roadtrip/components/commit/758e842c30c1cf31fa2bb5fb493bbb36287ba4f7))

### Refactor

* **progress-indicator-horizontal:** token ([be68a05](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/be68a05419e579914b139e65cbc0ba43ce63e625))
* **progress-indicator-horizontal:** color svg ([174e412](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/174e4125ffa3329475dc6ae4efa49c0bc6b9b037))


## [3.4.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.3.0...3.4.0) (2023-05-12)

### Features

* **floating-button:** add new componant ([a4dce5a](https://gitlab.com/mobivia-design/roadtrip/components/commit/a4dce5a212870b1339a60f6dc5d214dfe4ccb4e3))
* **icons:** add icons ([ebbe0d3](https://gitlab.com/mobivia-design/roadtrip/components/commit/ebbe0d3318b12c3eedb58b67ac95b7f6f0ae9fb4))

### Bug Fixes

* **alert:** fix color text alert ([1ccc00f](https://gitlab.com/mobivia-design/roadtrip/components/commit/1ccc00f2ecc6f3359cbeaf70101b933183d8c6e3))
* **road-button:** remove z-index ([5ffc853](https://gitlab.com/mobivia-design/roadtrip/components/commit/5ffc853e6e2a949cbb4da8dd178b0f913a9c95fe))

## [3.3.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.2.1...3.3.0) (2023-04-24)

### Features

* **progress-tracker:** add new composent ([573bd13](https://gitlab.com/mobivia-design/roadtrip/components/commit/573bd1390b1efba4101860eba5782312a1f59a50))
* **tokens:** add tokens header disabled ([c02f1e8](https://gitlab.com/mobivia-design/roadtrip/components/commit/c02f1e864c89870f3d398e6fc39eb1378f524a27))

### Refactor
* **stepper:** name composent and update color ([9428c65](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/9428c65f3787d9789403bbc0c7afdd6395c3bd2e))

### Bug Fixes

* **icons:** color currentcolor ([b53e5b2](https://gitlab.com/mobivia-design/roadtrip/components/commit/b53e5b2c573deebea600dd133a47d0fd5ddbf3e1))
* **global-navigation:** add zindex ([ea40bad](https://gitlab.com/mobivia-design/roadtrip/components/commit/ea40bad32a4c31d968e38d81a16c5e1a4e014aab))
* **icons:** color icons alert ([8b561f1](https://gitlab.com/mobivia-design/roadtrip/components/commit/8b561f1dd1ddd6b759405f31682ef7cd4603936f))
* **tokens:** color on header badge mobivia ([f1ada78](https://gitlab.com/mobivia-design/roadtrip/components/commit/f1ada78006e03e1893ba3a0dec33f05930162266))

### [3.2.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.2.0...3.2.1) (2023-04-13)

### Bug Fixes

* **icons:** color icons navbar / flag belgium ([a70bd58](https://gitlab.com/mobivia-design/roadtrip/components/commit/a70bd5846c2bf8ee3c5debe259e8ee82f6955041))
* **profil-dropdow:** width size long name ([72ccad9](https://gitlab.com/mobivia-design/roadtrip/components/commit/72ccad933cb67f984f58543c5cf697c3278d52f1))
* **icons:** add new keep in repair ([4badeb4](https://gitlab.com/mobivia-design/roadtrip/components/commit/4badeb4df978b17efa36d1dadb26d3811a9a1cec))
* **icon:** color primary ([e56fbac](https://gitlab.com/mobivia-design/roadtrip/components/commit/e56fbacc50b6b76942cafa0c13b2ab3a6eb633b7))
* **icons:** flag - weather ([08b84ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/08b84ea25802595b673cded8973b4d7f8f7f86f9))


## [3.2.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.1.3...3.2.0) (2023-04-04)

### Features

* **tokens:** add decorative tokens ([b0c94b3](https://gitlab.com/mobivia-design/roadtrip/components/commit/b0c94b39c3ba90fd41481643baee6b5394dcb911))
* **tokens:** add tag - banner- rating tokens ([d9cf316](https://gitlab.com/mobivia-design/roadtrip/components/commit/d9cf316811b33a180dd6b6805e07c8ad6aa82527))
* **dropdown:** add medium size option ([0849c69](https://gitlab.com/mobivia-design/roadtrip/components/commit/0849c69d950b2708698b794ae14e25ded9bb803a))

### Bug Fixes

* **utilities:** bg light ([1a92e5b](https://gitlab.com/mobivia-design/roadtrip/components/commit/1a92e5b620c207d90611d523c2b012d2c50d61a3))
* **icon:** fix flag icons ([ff2f8dc](https://gitlab.com/mobivia-design/roadtrip/components/commit/ff2f8dc46b3c30ebbda0cad293502063b3cf31f9))
* **global-navigation:** fix position - zindex ([89f7ff7](https://gitlab.com/mobivia-design/roadtrip/components/commit/89f7ff70a5c357e5e5d35dc30107da6b5d3e8366))
* **dialog:** empty height header ([9d7fee8](https://gitlab.com/mobivia-design/roadtrip/components/commit/9d7fee8932319e152c4f85c73de0ee1400e6a01e))
* **stepper:** add option link step ([2553945](https://gitlab.com/mobivia-design/roadtrip/components/commit/2553945ee8ef257f9aac5882bce829f667d24ad6))
* **counter:** add z-index ([66551c7](https://gitlab.com/mobivia-design/roadtrip/components/commit/66551c74cef8ae52a8438aaf9d851299d92f9708))

### Refactor

* **flap:** add new decorative token ([edb83c2](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/edb83c22df010fbab0c95d383d1becbb89c6ff02))
* **plate-number:** bg - cursor readonly ([268bc90](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/268bc90a36e0cccaada91b385c8693996a8e6900))


### [3.1.3](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.1.2...3.1.3) (2023-03-23)

### Bug Fixes

* **icons:** fix icons svg ([75f7876](https://gitlab.com/mobivia-design/roadtrip/components/commit/75f7876b81156cc330f939b1c5903b6fcc097f61))

### [3.1.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.1.1...3.1.2) (2023-03-23)

### Bug Fixes

* **icons:** fix icons.svg ([c0bec74](https://gitlab.com/mobivia-design/roadtrip/components/commit/c0bec74133d915b2c7c13fc626bcc5c61004d85d))

### [3.1.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.0.1...3.1.1) (2023-03-23)

### Features

* **toast:** add progress bar in the toast ([e277094](https://gitlab.com/mobivia-design/roadtrip/components/commit/e277094ed5f7a393e72f6f6d1678aff610202d15))
* **progress:** add light background option ([23a4dab](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/23a4dab47f026f6d8e96dc6ec9bd9b24e91d5ef5))
* **utilities:** add new class css ([23a4dab](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/34fd68505487196927c52a41d98c267e00db83c5))
* **icons:** add icons ([a114deb](https://gitlab.com/mobivia-design/roadtrip/components/commit/a114deb5072e5fefb7b03434d76f5b10d84fb741))

### Refactor

* **toast:** update toast position ([0ef1f92](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/0ef1f927d5d2f397446c0815e6080504732a0bc8))

### Bug Fixes

* **token:** update color token ([e281289](https://gitlab.com/mobivia-design/roadtrip/components/commit/e2812896aac516e51569c93c5746c05c0dbdcc7a))
* **radio:** fix disabled ([31fec72](https://gitlab.com/mobivia-design/roadtrip/components/commit/31fec72982a61549b38b18c24710512e09123661))
* **icons:** fix clip path ([52bfcf7](https://gitlab.com/mobivia-design/roadtrip/components/commit/52bfcf7e38345bc3e89c65a852c05d3a308dc20d))
* **toast:** fix position mobile ([3586980](https://gitlab.com/mobivia-design/roadtrip/components/commit/358698073381926b52980e0bdfa50300f2819912))


### [3.0.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/3.0.0...3.0.1) (2023-03-03)

### Bug Fixes

* **profil-dropdown:** add z-index ([33ab966](https://gitlab.com/mobivia-design/roadtrip/components/commit/33ab966ea9e987ff926a245e69415652675bde21))
* **loader:** fix path esm ([9aaa29b](https://gitlab.com/mobivia-design/roadtrip/components/commit/9aaa29b5122b78c3f06faca30afcbb3c8f112684))
* **navbar:** add tooltip navbar item ([25fb008](https://gitlab.com/mobivia-design/roadtrip/components/commit/25fb0085123f705a2b643350d64ae45c36672c9d))
* **navbar:** add tooltip ([162de95](https://gitlab.com/mobivia-design/roadtrip/components/commit/162de95b10a3606ae271bb4309214af3ddd81224))

## [3.0.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.51.1...3.0.0) (2023-02-27)

### Features

* **tokens:** add new token on the components ([4faaf2c](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/4faaf2ca322351dbb58355d68225b5cb5ca7e7f0))

### [2.51.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.51.0...2.51.1) (2023-02-16)

### Bug Fixes

* **profil-dropdown:** add slot name email ([a641ca1](https://gitlab.com/mobivia-design/roadtrip/components/commit/a641ca147cd20f9a77d2c6e9a33bc242ffeb6410))
* **progress:** add padding full width ([5123f3a](https://gitlab.com/mobivia-design/roadtrip/components/commit/5123f3a5cd3596efe530ec74e47becec22378ccd))
* **skeleton:** fix animation ([e650437](https://gitlab.com/mobivia-design/roadtrip/components/commit/e65043770b7ea38a1cc5d89f755e897f4629e119))


## [2.51.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.50.0...2.51.0) (2023-02-08)

### Features

* **toolbar:** size title ([9d5828f](https://gitlab.com/mobivia-design/roadtrip/components/commit/9d5828f825df03efa0bf31727fd2c3960f26311b))
* **icon:** update color icon token ([57b688a](https://gitlab.com/mobivia-design/roadtrip/components/commit/57b688a14cb49579d6c9b257369f2eacb348c380))


## [2.50.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.49.1...2.50.0) (2023-01-25)

### Features

* **globalNavigation:** add new component ([6b7ebdf](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b7ebdf2347c90f482cc1f1a338b1d290b6441e3))
* **toolbar:** add header navigation ([083181f](https://gitlab.com/mobivia-design/roadtrip/components/commit/083181fb52bded4c0d88d19041424d63cf40cec8))
* **navbar:** add drawer ([cae1433](https://gitlab.com/mobivia-design/roadtrip/components/commit/cae1433339f9445820e123cf6f4a541de3800200))

### Bug Fixes

* **toolbar:** fix button back ([d1e8420](https://gitlab.com/mobivia-design/roadtrip/components/commit/d1e842003f5f8bacc7b0d1251aaa3bdec469d660))
* **radio:** fix alignment ([2f201a3](https://gitlab.com/mobivia-design/roadtrip/components/commit/2f201a3f1e170a2093382e7b00b1c474c73c4c36))

### [2.49.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.49.0...2.49.1) (2023-01-11)

### Bug Fixes

* color icons weather ([e035eb6](https://gitlab.com/mobivia-design/roadtrip/components/commit/e035eb623af6583b1508d53c962188a630c6d8dd))

## [2.49.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.48.0...v2.49.0) (2022-12-23)

### Features

* **avatar:** add new component ([fd8b37d](https://gitlab.com/mobivia-design/roadtrip/components/commit/fd8b37dfb1cfd276487034e9a8976eb1c1d46cee))
* **profil-dropdown:** add new component ([bf95bb3](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf95bb386a62ea65236be8254b8633a5a16eae85))

### Bug Fixes

* **color:** fix color surface-header norauto ([67a1b76](https://gitlab.com/mobivia-design/roadtrip/components/commit/67a1b76fc56daa6804d711f7ac25e1065981ef78))
* **color:** update variable surface header ([e14bc39](https://gitlab.com/mobivia-design/roadtrip/components/commit/e14bc397dbf840e6a1867acc1450e58957b3b128))


## [2.48.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.47.0...2.48.0) (2022-12-14)

### Features

* **vertical-stepper:** add new component ([7e2c132](https://gitlab.com/mobivia-design/roadtrip/components/commit/7e2c132da444700f137144cffb19fa6a67af728f))
* **segmented-buttons:** add size sm ([38db72a](https://gitlab.com/mobivia-design/roadtrip/components/commit/38db72ab4bfe5df65071912a2d1791280b2081da))
* **color:** add variables badge header ([dbca90a](https://gitlab.com/mobivia-design/roadtrip/components/commit/dbca90aa4a90e8545b0047542f5f1ee0c291dbe6))
* **icons:** add icons ([ef86222](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef86222c5cf1ff3da90b2ca38445a927f013ce2e))

### Bug Fixes

* **button:** add webkit for ios ([2c7f24b](https://gitlab.com/mobivia-design/roadtrip/components/commit/2c7f24be23554fc75edd410b6234331102ad1212))
* **input:** refactor(input): add data-cy ([6e5e9fa](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/6e5e9fae74be85ebdb1fc4bb0ad2d05779ba6e71))
* **counter:** refactor(counter): update color ([643da88](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/643da889223f7de86ef15552ecfd1ab5956714f1))
* **progress:** remove console log ([97356e2](https://gitlab.com/mobivia-design/roadtrip/components/commit/97356e23f855bc4d01768644642766d84cccf95e))

## [2.47.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.46.0...2.47.0) (2022-11-30)

### Features

* **button:** add ghost color ([cb288e3](https://gitlab.com/mobivia-design/roadtrip/components/commit/cb288e3fd3c4bf58dfdc6b3e40c487bef482d685))
* **progress:** add option step ([326ff7b](https://gitlab.com/mobivia-design/roadtrip/components/commit/326ff7b4e9c3e2b3bad898a7d5de5480dc44d291))
* **icons:** add icon ([076bbb6](https://gitlab.com/mobivia-design/roadtrip/components/commit/076bbb659198499dfa3bb3329976ea0796dedb24))
* **counter:** add data-cy ([e412f04](https://gitlab.com/mobivia-design/roadtrip/components/commit/e412f04e674f11114c660a5a31b20ee32e0e5259))

### Bug Fixes

* **icons:** fix icons vehicle tires and 5 doors ([5f65b38](https://gitlab.com/mobivia-design/roadtrip/components/commit/5f65b38c99a8c2aabf65d8601165bcddb626844d))
* **icons:** fix build ([4474197](https://gitlab.com/mobivia-design/roadtrip/components/commit/447419793bc81f4906b00e3f9bf7dc0bdb85a220))
* **buttons:** add outline default ([9db068f](https://gitlab.com/mobivia-design/roadtrip/components/commit/9db068f89ff3851fbdc55874d463fc10e8ad7065))


## [2.46.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.45.1...2.46.0) (2022-11-14)

### Features

* **color:** add variable color for header ([4497788](https://gitlab.com/mobivia-design/roadtrip/components/commit/4497788fa4815b52785aa64220d2bb78793236d1))
* **flap:** add color black for blackfriday ([00e503c](https://gitlab.com/mobivia-design/roadtrip/components/commit/00e503c6efb3c63a6682aa4c6a32e6a239030a6d))
* **icons:** add icons ([dc474c2](https://gitlab.com/mobivia-design/roadtrip/components/commit/dc474c234ec4a1fa975ff2f806490dc2071df733))
* **segementedbuttons:** add new component ([624a51e](https://gitlab.com/mobivia-design/roadtrip/components/commit/624a51ed95c1acf11a29e9a383357287a42d4a07))


## [2.45.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.44.0...2.45.0) (2022-11-04)

### Bug Fixes

* **icons:** name icons ([d218998](https://gitlab.com/mobivia-design/roadtrip/components/commit/d218998f849bd55a187779e95708972bdf27201b))
* **counter:** reactivity-changes

### Features

* **range:** add labels ([5139410](https://gitlab.com/mobivia-design/roadtrip/components/commit/5139410f196d9d0062cd4dd13da15732471d16b9))
* **carousel:** update color pagers ([af4e49c](https://gitlab.com/mobivia-design/roadtrip/components/commit/af4e49c5baa3db2401958f16a1426e6a3cbc7dca))
* **link:** add variant white ([8b95191](https://gitlab.com/mobivia-design/roadtrip/components/commit/8b9519194800f596e52a90d6172297cab9eb58c5))
* **icons:** add icons flag uk ecologic driver ([d3d3eec](https://gitlab.com/mobivia-design/roadtrip/components/commit/d3d3eec1002109d2693ace01a4dca889193aaafb))
* **accordion:** add icon left option ([73fa326](https://gitlab.com/mobivia-design/roadtrip/components/commit/73fa326bc3d9c953e6ed025f88890e6641c19231))


## [2.44.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.43.0...2.44.0) (2022-10-13)

### Features
* **icons:** add icons ([8d50d18](https://gitlab.com/mobivia-design/roadtrip/components/commit/8d50d18406d2cffe81f487b244c413c2cb1fa3e0))

### Bug Fixes
* **icons:** black color flag ([9bfcdc9](https://gitlab.com/mobivia-design/roadtrip/components/commit/9bfcdc98a0c3ac0e6412706446f63d41e0d32e85))
* **carousel:** remove unecessay files for swiper ([4fba7b1](https://gitlab.com/mobivia-design/roadtrip/components/commit/4fba7b1d8a71292b0147b0578c6174476d94df5d))
* **counter:** change element ([4303a36](https://gitlab.com/mobivia-design/roadtrip/components/commit/4303a3683c20da159127e0824bcb2fef42087c51))
* **counter:** finish implementation ([7d633b1](https://gitlab.com/mobivia-design/roadtrip/components/commit/7d633b1be22069130c640baae715275d3c351ca9))
* **counter:** start tests ([dc20f25](https://gitlab.com/mobivia-design/roadtrip/components/commit/dc20f253ce7126e611e3ba5b1241ea816bf14b1b))
* **counter:** finish tests ([c4056bd](https://gitlab.com/mobivia-design/roadtrip/components/commit/c4056bddd855258c4f61cd476bde758c8fbb0495))
* **counter:** fix null value and size ([7e286ee](https://gitlab.com/mobivia-design/roadtrip/components/commit/7e286eed67f90898937606691c8132fa03c4f45e))
* **icons:** build ([5c36334](https://gitlab.com/mobivia-design/roadtrip/components/commit/5c363343c5dc236aad10d95b80e96a7392f8f768))
* **icons:** build ([e4db3dd](https://gitlab.com/mobivia-design/roadtrip/components/commit/e4db3dda7437b48be55fe0014bf71633771a25ee))

## [2.43.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.42.0-beta...2.43.0) (2022-10-06)

### Features

* **icons:** add variants and icons ([77a0766](https://gitlab.com/mobivia-design/roadtrip/components/commit/77a0766a1763f9d934755cedbcdf9bff6bc27372))
* **accordion:** add small and separator version ([68ec74f](https://gitlab.com/mobivia-design/roadtrip/components/commit/68ec74fab2a14bd67956637e7d776a41299ace51))
* **build:** stencil ([f1d0d28](https://gitlab.com/mobivia-design/roadtrip/components/commit/f1d0d28b280d61c9c44433926284130af06eb2f3))

### Bug Fixes

* **button:** click disabled and counter size icon ([28a48b0](https://gitlab.com/mobivia-design/roadtrip/components/commit/28a48b0a03ea462f03fabfd474f4a4b3c3c97767))
* **toast:** update color success ([01696e4](https://gitlab.com/mobivia-design/roadtrip/components/commit/01696e41c56d4291e0280addac03bb3fc9eb0f25))


## [2.42.0-beta](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.42.0...2.42.0-beta) (2022-09-28)
### Bug Fixes

* **carousel:** test fix init ([f4e39b3](https://gitlab.com/mobivia-design/roadtrip/components/commit/f4e39b392bb0169ff522f020b08482b97276738a))

## [2.42.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.41.0...2.42.0) (2022-09-14)

### Features

* **icons:** add icons ([d38d42a](https://gitlab.com/mobivia-design/roadtrip/components/commit/d38d42af5c3cd115d15f01f954e9d75bee0f5351))

### Bug Fixes

* **icons:** fix fill icons ([548281e](https://gitlab.com/mobivia-design/roadtrip/components/commit/548281e2d51f0bfd27d524423116ae8372e70e3d))
* **accordion:**  max-height none ([78320bb](https://gitlab.com/mobivia-design/roadtrip/components/commit/78320bba80167303cf765b2418bc6049b61da39a))


## [2.41.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.40.1...2.41.0) (2022-08-30)

### Features

* **input:** add type time ([73a284d](https://gitlab.com/mobivia-design/roadtrip/components/commit/73a284ddbc3fcbe63ed3e33c9393b86b1eccf0e7))
* **counter:** add option icon delete ([1e50ad8](https://gitlab.com/mobivia-design/roadtrip/components/commit/1e50ad8a7ffbeea94e6a3ec0d0ac18ffb2e8807c))

### Bug Fixes

* **banner:** change color text in the banner ([b53fc73](https://gitlab.com/mobivia-design/roadtrip/components/commit/b53fc73a66766c9dafe51a96f119682547c65bb0))
* **carousel:** test fix init ([d95d83f](https://gitlab.com/mobivia-design/roadtrip/components/commit/d95d83f945a5c717f7d947f7f83e9bd740fc23cb))
* **icons:**  add color for suns and fix flag


### [2.40.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.40.0...2.40.1) (2022-08-18)

### Bug Fixes

* **icons:** remove hexa for rbg ([5681eee](https://gitlab.com/mobivia-design/roadtrip/components/commit/5681eeebb092dfce98bc44dc91003824f1b03136))

### Features

* **accordion:** add icon customisation ([0623a88](https://gitlab.com/mobivia-design/roadtrip/components/commit/0623a88e89b9cebbed270c44ca3aabaa89314d36))
* **accordion:** add opening animation ([4c5969b](https://gitlab.com/mobivia-design/roadtrip/components/commit/4c5969b02e402eaa4b0563549a1b32aa234a5ba8))
* **accordion:** move margin to the host element ([3820cef](https://gitlab.com/mobivia-design/roadtrip/components/commit/3820cef8c41fd7e771b1a187134223c536f703f6))
* add accordion noborder component ([1fe6c05](https://gitlab.com/mobivia-design/roadtrip/components/commit/1fe6c054d574bd666b30101744370bd69929bbe2))
* add banner component ([a036770](https://gitlab.com/mobivia-design/roadtrip/components/commit/a036770aaa10d894a1a1511b2b07d2daccc85d38))
* add brand color ([3315976](https://gitlab.com/mobivia-design/roadtrip/components/commit/3315976cca18ad4b67f5c1cd2faa5fe113f5e1bb))
* add breadcrumb pattern ([03101f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/03101f209725cf3624c9c231f40bdf621d4b5c3f))
* add chip component ([ef03729](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef03729261876e4753c0cb3a6d6d76601ed285b8))
* add CSS variables for accordion and item customisation ([8924ba7](https://gitlab.com/mobivia-design/roadtrip/components/commit/8924ba7739330e142c7bb59cd556ced0b916ded4))
* add dropdown component ([bf37106](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf3710652578eb555b24105bcd732604cebf9a5a))
* add duration component ([c7d8349](https://gitlab.com/mobivia-design/roadtrip/components/commit/c7d8349b8edea3dd8f60b623287dd0cf8398deb2))
* add helper checkbox component ([628f3f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/628f3f2bb7aa6d5544abc5126030440406d2c62d))
* add helper input component ([25f16a4](https://gitlab.com/mobivia-design/roadtrip/components/commit/25f16a4a5a59a406c65a52d9d07baa6c9ae6dfed))
* add helper radio component ([908f67a](https://gitlab.com/mobivia-design/roadtrip/components/commit/908f67a79cdd29fec6d47277c5091b652bf426f7))
* add helper textarea component ([b8d9713](https://gitlab.com/mobivia-design/roadtrip/components/commit/b8d97130e939871512dfb40724012352fbb96704))
* add link component ([60af3d3](https://gitlab.com/mobivia-design/roadtrip/components/commit/60af3d34d1c328af2e8b06aad2a6cdfddb7ba39c))
* add lowercase events ([f395d4d](https://gitlab.com/mobivia-design/roadtrip/components/commit/f395d4d90898d4dbba79217e6aa81121e6fa8c20))
* add plate number component ([8981668](https://gitlab.com/mobivia-design/roadtrip/components/commit/89816682304159e78ed191deb50219521b2ad2dc))
* add range component ([ad4189f](https://gitlab.com/mobivia-design/roadtrip/components/commit/ad4189fe077f3653e1ca45c5b3f0fd38aa8a0d4e))
* add select-filter component ([27c1e2f](https://gitlab.com/mobivia-design/roadtrip/components/commit/27c1e2f72df590fc4375ce614f835c1d742729ae))
* Add SVG sprite of icons ([06b85cc](https://gitlab.com/mobivia-design/roadtrip/components/commit/06b85ccc7cdaca7e3e6d9c42812e79dac7883769))
* add toggle component ([c6f796c](https://gitlab.com/mobivia-design/roadtrip/components/commit/c6f796cc9116d589e779b90122596afb0890ccbc))
* **alert:** add title and link ([46cd96b](https://gitlab.com/mobivia-design/roadtrip/components/commit/46cd96b557a4bdbd50ec1d1dd6a69a94e2567fa7))
* **alert:** update colors and text size ([160d132](https://gitlab.com/mobivia-design/roadtrip/components/commit/160d13253a15fa889a04c3468c7c389076ac425e))
* **alert:** update icon color ([a630457](https://gitlab.com/mobivia-design/roadtrip/components/commit/a6304576995d3bcd5a7d0b29a7586cdbd0bbb747))
* **badge:** update colors and padding ([96b5aaa](https://gitlab.com/mobivia-design/roadtrip/components/commit/96b5aaa8099b4d8ff463797decf0ab2bb1ea5034))
* **breakpoint:** add breakpoint 1200px for desktop users ([af1b10c](https://gitlab.com/mobivia-design/roadtrip/components/commit/af1b10c11405f294544da00c2da5ae002e8b1b18))
* **button:** add download attribute ([c6c75f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/c6c75f280467f295a87499caa3979a41200aaafc))
* **button:** add xl size ([f49f489](https://gitlab.com/mobivia-design/roadtrip/components/commit/f49f489cb4e214ce380c886177e7d6dd4a1a371c))
* **button:** update colors ([0b64cf1](https://gitlab.com/mobivia-design/roadtrip/components/commit/0b64cf185c4b90165f705eb1ade538b8a3af3765))
* **checkbox:** add indeterminate state ([9fae5aa](https://gitlab.com/mobivia-design/roadtrip/components/commit/9fae5aa2c756469dbbb5ca591d7f2aba859a3de2))
* **checkbox:** add left label and spaced props ([987176d](https://gitlab.com/mobivia-design/roadtrip/components/commit/987176deab49bbb43fa48bacd78d9a07d31082fa))
* **checkbox:** add more contrast in disabled state ([2f37c32](https://gitlab.com/mobivia-design/roadtrip/components/commit/2f37c32388600f6300da7142798390985b3a8a49))
* **checkbox:** Add props to inverse radio and label order ([aa7bf0c](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa7bf0cf889c1f9a5531f163be576b3c7d63caed))
* **checkbox:** update colors and font-size ([1449f5d](https://gitlab.com/mobivia-design/roadtrip/components/commit/1449f5d2f991fd985f7de114937d487d9ae3bf33))
* **chip:** update colors and paddings ([65bcd91](https://gitlab.com/mobivia-design/roadtrip/components/commit/65bcd9149e67db60bc63ad053d6cd2f92d0717cd))
* **chip:** update height ([6cfe08f](https://gitlab.com/mobivia-design/roadtrip/components/commit/6cfe08f8c4e7ea0b4be16cffc714ccf912eb756a))
* **collapse:** add button customisation ([d39bd0d](https://gitlab.com/mobivia-design/roadtrip/components/commit/d39bd0db53e72668f85cf9ef79fa830ce9aadfe3))
* **counter:** Add events when clicking more or less buttons ([3d91968](https://gitlab.com/mobivia-design/roadtrip/components/commit/3d919689fd771f6c30c1e33123531924a4af3e61))
* **counter:** implement the other sizes ([64c605a](https://gitlab.com/mobivia-design/roadtrip/components/commit/64c605a27fdd000c3ec5a32a1772c52fe62f438e))
* **counter:** increase size for minus and more ([63eebea](https://gitlab.com/mobivia-design/roadtrip/components/commit/63eebea3ea75532838443980119c0bbb1d023ddd))
* **counter:** update colors and font-size ([42e83b4](https://gitlab.com/mobivia-design/roadtrip/components/commit/42e83b459df473ce3f3343900f4ffac7c9483fee))
* **dialog:** make icon customizable ([152e3d7](https://gitlab.com/mobivia-design/roadtrip/components/commit/152e3d77dbb7c0049c3822119e1624ac283c08ba))
* **dialog:** make icon customizable ([52f6e38](https://gitlab.com/mobivia-design/roadtrip/components/commit/52f6e38269afc1add015e3c868f7a1a9ec1a7378))
* **dialog:** update colors and paddings ([7a96b8a](https://gitlab.com/mobivia-design/roadtrip/components/commit/7a96b8ac6c7ec439d73cedc21ac48f20de5130a5))
* **dialog:** update icon to use outline version ([a440a28](https://gitlab.com/mobivia-design/roadtrip/components/commit/a440a2882d1d3f0de47b965a3fc5bb8d376d1e8c))
* **docs:** fix documentation title ([e34d656](https://gitlab.com/mobivia-design/roadtrip/components/commit/e34d65612b46f405ee1427e60def2013a741e392))
* **docs:** update license fil format ([008e6a3](https://gitlab.com/mobivia-design/roadtrip/components/commit/008e6a3b1e99c38555c3ff7a69045ef32059b170))
* **drawer, item:** add icon customisation ([5ec8895](https://gitlab.com/mobivia-design/roadtrip/components/commit/5ec8895b18c9abfc417639cbc01baf44ca778954))
* **drawer:** add css variable to customize header color ([d987bf2](https://gitlab.com/mobivia-design/roadtrip/components/commit/d987bf2b747896e8a4a193dab3c2844e1929866a))
* **drawer:** add delimiter css variable ([c1a72ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/c1a72eaee7c84584bcb927f7e43dfa5790d73f0f))
* **drawer:** add title customisation ([838b4c8](https://gitlab.com/mobivia-design/roadtrip/components/commit/838b4c88e2ce1f91d155c646edf79979ef3f46b5))
* **drawer:** scroll to top after closing ([d89068d](https://gitlab.com/mobivia-design/roadtrip/components/commit/d89068d41fdd2eb4f56f9f0e0f1b5411412157d3))
* drop IE support ([208a8b5](https://gitlab.com/mobivia-design/roadtrip/components/commit/208a8b5306fcf227a23b8c53df81871773d50acd))
* **dropdown:** add hover button ([0d1c69b](https://gitlab.com/mobivia-design/roadtrip/components/commit/0d1c69b1efcddc1b424bcac1412faaccdfcb9efa))
* **dropdown:** update items ([de1eef4](https://gitlab.com/mobivia-design/roadtrip/components/commit/de1eef48614e12b8f3d2cca70e88d10a63d42abb))
* **dropdown:** update margin stories ([deacbac](https://gitlab.com/mobivia-design/roadtrip/components/commit/deacbac8c09377976da05a8701b554fde2f213ce))
* **dropwdown:** add position and direction ([6f02d40](https://gitlab.com/mobivia-design/roadtrip/components/commit/6f02d40a88a71819c5407b3ebed37d219e9e25da))
* **flag:** update design and add new size ([985e36d](https://gitlab.com/mobivia-design/roadtrip/components/commit/985e36d77db5bf5d106bd5f07d328127cbe50a3d))
* **flap:** add ecology color ([8661cfa](https://gitlab.com/mobivia-design/roadtrip/components/commit/8661cfaaacd780b4d5f8fe17c14db4bdadd7878e))
* **flap:** update colors ([e7fc667](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7fc6670daa453f43cbb7dccc0420b41dedd5bce))
* **form:** add required for input and select ([aa145ec](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa145ec5f633806e8c3c13f2e699e6c288b63cca))
* **forms:** add hover state ([71dcce0](https://gitlab.com/mobivia-design/roadtrip/components/commit/71dcce013ddc7c1db2cdf9bc2c1d806b1f172cef))
* **form:** update font size for input and select ([eddffbf](https://gitlab.com/mobivia-design/roadtrip/components/commit/eddffbf664fc8ea7b644d282d0a4d82f00beeb82))
* **form:** update font size for input and select ([43bbbd0](https://gitlab.com/mobivia-design/roadtrip/components/commit/43bbbd0b0f507fc1a71c8cbb038e29053f8c0bf8))
* **icon:** add and improve timer and lock icons ([9877c03](https://gitlab.com/mobivia-design/roadtrip/components/commit/9877c0350dfc77a9bb342ee5007463953e3870b8))
* **icon:** add and update icons ([cc61738](https://gitlab.com/mobivia-design/roadtrip/components/commit/cc61738c32f804c6ea24b3c6f2f9e473e76eecf4))
* **icon:** add and update icons ([20f43c8](https://gitlab.com/mobivia-design/roadtrip/components/commit/20f43c856a73c2c52c21244dac29e3dd736752ba))
* **icon:** add and update icons ([d5a2d97](https://gitlab.com/mobivia-design/roadtrip/components/commit/d5a2d97a7f99a5280d27c839eeabcf04829e0bb0))
* **icon:** add and update icons ([4644b1e](https://gitlab.com/mobivia-design/roadtrip/components/commit/4644b1e28755049a20d318912b268574599726c7))
* **icon:** add and update icons ([1aeb659](https://gitlab.com/mobivia-design/roadtrip/components/commit/1aeb659482be39bfbedd8040acbd97fccb391c18))
* **icon:** add and update icons ([0329660](https://gitlab.com/mobivia-design/roadtrip/components/commit/0329660c391df12801f2feb8f21c6a8e971dbcab))
* **icon:** add and update icons ([e8a4170](https://gitlab.com/mobivia-design/roadtrip/components/commit/e8a41705e9becbd74fe7342ab3ce3e2c0f465ae9))
* **icon:** add and update icons ([01d8751](https://gitlab.com/mobivia-design/roadtrip/components/commit/01d875122af951df4c4b8eb48146e5d6d9f1238e))
* **icon:** add and update icons ([e676c91](https://gitlab.com/mobivia-design/roadtrip/components/commit/e676c914f3e97c690f642b49fe72ce7816e0d7d3))
* **icon:** add and update icons ([22bfba1](https://gitlab.com/mobivia-design/roadtrip/components/commit/22bfba1be9c7014a7dfa73e497e9b323d9375d39))
* **icon:** add and update icons ([62fff5b](https://gitlab.com/mobivia-design/roadtrip/components/commit/62fff5b790876245a1b17a071119e2fb6e33d7a8))
* **icon:** add and update icons ([b434a35](https://gitlab.com/mobivia-design/roadtrip/components/commit/b434a354839ee8a95e46b45df3e5f877e1061336))
* **icon:** add and update icons ([fee5305](https://gitlab.com/mobivia-design/roadtrip/components/commit/fee5305aff7b71ec4c986306d0dd917ae58eb386))
* **icon:** add and update icons ([8aee992](https://gitlab.com/mobivia-design/roadtrip/components/commit/8aee992978f2d29cf83e6c230fd307e33a0752cd))
* **icon:** add and update icons ([66fe56c](https://gitlab.com/mobivia-design/roadtrip/components/commit/66fe56c52934fdda2cdd6b26d001c49134ee1ff7))
* **icon:** add and update icons ([4fa3537](https://gitlab.com/mobivia-design/roadtrip/components/commit/4fa3537cef5eb7c29c1d86f962e393e536867b5a))
* **icon:** add and update icons ([b67cc1a](https://gitlab.com/mobivia-design/roadtrip/components/commit/b67cc1a3cf2a1781406d978fbc2d4844f2fb3604))
* **icon:** add and update icons ([83b6bc6](https://gitlab.com/mobivia-design/roadtrip/components/commit/83b6bc67a149bb27c2614a1952f8b139a82a9c5c))
* **icon:** add and update icons ([007f1b0](https://gitlab.com/mobivia-design/roadtrip/components/commit/007f1b07e7ec1ce9db6d05fb393277aa9eda9ee7))
* **icon:** add and update icons ([9d097c1](https://gitlab.com/mobivia-design/roadtrip/components/commit/9d097c1562f65b10a122d2e15fa57a93bad5847b))
* **icon:** add and update icons ([7d43f1c](https://gitlab.com/mobivia-design/roadtrip/components/commit/7d43f1cd7d4177b945195b7329b338fbeb1f3ae8))
* **icon:** add and update icons ([74067f0](https://gitlab.com/mobivia-design/roadtrip/components/commit/74067f041d4719261779eef949e5e64ffd1c202a))
* **icon:** add and update icons ([2abe584](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abe584dff33451dc0bf9d49fe0222dd73bf58ac))
* **icon:** add icons ([8867503](https://gitlab.com/mobivia-design/roadtrip/components/commit/8867503c60dd10b97d560ba286a8f32bc25d04ca))
* **icon:** add icons ([0da9999](https://gitlab.com/mobivia-design/roadtrip/components/commit/0da999997438c9ef5bab9f4c36be3eec43888ff8))
* **icon:** add icons ([e787344](https://gitlab.com/mobivia-design/roadtrip/components/commit/e787344c4fe472ed3203d8d792a3a93c0dd98b57))
* **icon:** add people-customer-review ([4a543e5](https://gitlab.com/mobivia-design/roadtrip/components/commit/4a543e5cc52b0fb4041cb1c9b0616dde3fada69c))
* **icon:** add start and stop icon ([aa7881c](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa7881c947194a87ef120e62e4e74fd69eedebe1))
* **icon:** add vehicle doors color icon variants ([c81664d](https://gitlab.com/mobivia-design/roadtrip/components/commit/c81664d88dfa075b79989bbda6843a6411e724d1))
* **icon:** add vehicle tires color variant for each tire ([97d2e8a](https://gitlab.com/mobivia-design/roadtrip/components/commit/97d2e8a48422d3db7d094ed683c8303eefcfc5af))
* **icon:** add world, saveDisk and meetingOff icons ([ad15d75](https://gitlab.com/mobivia-design/roadtrip/components/commit/ad15d7520cf8af0dcaca4a8251b9e42c0ab40851))
* **icon:** generate icons variables ([a7f7526](https://gitlab.com/mobivia-design/roadtrip/components/commit/a7f7526b5b0877416c63685691802ba83f97dc0c))
* **icon:** reduce size of flag icons ([78a199f](https://gitlab.com/mobivia-design/roadtrip/components/commit/78a199feec29845ea3e29f1d67147bf470107320))
* **icons:** add icons ([2c49cf1](https://gitlab.com/mobivia-design/roadtrip/components/commit/2c49cf1eed87078d3e55af72448bd16a6ced89ca))
* **icons:** add icons ([eb329b2](https://gitlab.com/mobivia-design/roadtrip/components/commit/eb329b22626089cd4019abcfe29d1b542da14e1e))
* **icons:** add update icons ([2200d6e](https://gitlab.com/mobivia-design/roadtrip/components/commit/2200d6eaef9ddfe457eb2e7c9657f32e35c6decc))
* **icons:** optimisation fill ([f46cb96](https://gitlab.com/mobivia-design/roadtrip/components/commit/f46cb96aeab7c168c5e568519537ef68763d4caa))
* **icons:** update icon color ([34c00a2](https://gitlab.com/mobivia-design/roadtrip/components/commit/34c00a284562025acdf8baa720255a7183728fe4))
* **icons:** update icons figma ([0a25a6c](https://gitlab.com/mobivia-design/roadtrip/components/commit/0a25a6c8f4c0bbf3ae53c341c399d9bbb21d7ceb))
* **icon:** update icon with fallback color ([56a455d](https://gitlab.com/mobivia-design/roadtrip/components/commit/56a455d084bbf998d31b01d43f1a161c1c971e18))
* **icon:** update icons ([55ecc4c](https://gitlab.com/mobivia-design/roadtrip/components/commit/55ecc4ce70916c3b71b487c56ab72e43404b9425))
* **input-group:** update to handle input size xl ([0b79af9](https://gitlab.com/mobivia-design/roadtrip/components/commit/0b79af9028f3b4ed09a1ada2ac9c1cf48ad1fd06))
* **input:** add password ([0de7c54](https://gitlab.com/mobivia-design/roadtrip/components/commit/0de7c540696ec53298706003892359e57b4559c8))
* **input:** update colors and font-size ([2abeace](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abeaceb0521f914b26f1c6ca8c5bb2e76c97212))
* **modals:** mover layer color value to the CSS variable ([12f5fa9](https://gitlab.com/mobivia-design/roadtrip/components/commit/12f5fa95b61e83808dd42e588536da183f4d7069))
* move fonts declaration in its own file ([481f0a9](https://gitlab.com/mobivia-design/roadtrip/components/commit/481f0a9e2c45a6334c3ea762c982bf34d8ed7c8a))
* **navbar-item:** update colors and label text size ([05ba3b4](https://gitlab.com/mobivia-design/roadtrip/components/commit/05ba3b422a6bc9fa56257f35a0443db7a554125b))
* **plate-number:** add Austrian and german format ([023c568](https://gitlab.com/mobivia-design/roadtrip/components/commit/023c568a204fabe06fa1231f407a07ac758ef591))
* **PlateNumber:** add disabled and readonly state ([4b9cafc](https://gitlab.com/mobivia-design/roadtrip/components/commit/4b9cafc88e8b4ce7faa590286ea51974f6976866))
* **plateNumber:** add event bindings ([e68afc3](https://gitlab.com/mobivia-design/roadtrip/components/commit/e68afc3c849c8b15b7350711d492da017f203b5c))
* **plateNumber:** add events ([4c004ac](https://gitlab.com/mobivia-design/roadtrip/components/commit/4c004acc1473c7fca84d64967076fe67e6d2ecb8))
* **PlateNumber:** add motorbike plate ([8151618](https://gitlab.com/mobivia-design/roadtrip/components/commit/81516189439daea79e8511204d1be186d90dd4f3))
* **plateNumber:** Add placeholder attribute ([a37e00d](https://gitlab.com/mobivia-design/roadtrip/components/commit/a37e00dabbca242a233a4a808ce273729a6a1f51))
* **plateNumber:** update Belgian plate to be closer to real one ([2731ddf](https://gitlab.com/mobivia-design/roadtrip/components/commit/2731ddfe4a931a10bdbf13604ea9298bcc270a0b))
* **plateNumber:** update colors and font-size ([2799edf](https://gitlab.com/mobivia-design/roadtrip/components/commit/2799edfe19a538748482b8e41baedcff371f9d59))
* **plateNumber:** update colors nad margins ([d625dc4](https://gitlab.com/mobivia-design/roadtrip/components/commit/d625dc4ad84dcafaa5ff55518296d54803b5e6d8))
* **plateNumber:** update country placeholders ([015f276](https://gitlab.com/mobivia-design/roadtrip/components/commit/015f2769a1a54628e8d730276416340be428710c))
* **progress:** add label ([a203656](https://gitlab.com/mobivia-design/roadtrip/components/commit/a20365622e2c918c746c778fecc76172d27b8a37))
* **progress:** add primary and secondary colors ([e095689](https://gitlab.com/mobivia-design/roadtrip/components/commit/e0956892cd45e0ac4f5c2844a1b85822bebe28a9))
* **progress:** update colors ([5746ecd](https://gitlab.com/mobivia-design/roadtrip/components/commit/5746ecdeb10fe3d50d5c1648dacb77b5d1c4d994))
* **radio:** add more contrast in disabled state ([3c89f45](https://gitlab.com/mobivia-design/roadtrip/components/commit/3c89f45e171fc4930afe64582e63acf8f0110741))
* **radio:** Add props to inverse radio and label order ([86956cd](https://gitlab.com/mobivia-design/roadtrip/components/commit/86956cdf3733e8e1162a3a3539491187aba99a6d))
* **RadioGroup:** Required field missing ([a359bdd](https://gitlab.com/mobivia-design/roadtrip/components/commit/a359bdd05dcc30ad43bbedc6f305aeecab4d1d72))
* **radio:** update colors and font-size ([8ef807d](https://gitlab.com/mobivia-design/roadtrip/components/commit/8ef807dbac414605eed91240b5d16122d01ca546))
* **radio:** update event listener to handle Vue3 ([4e2bf51](https://gitlab.com/mobivia-design/roadtrip/components/commit/4e2bf51fad1db9f941b8bf5adca3fc8c499ac2f3))
* **range:** add tick and color focus active ([b9bf04c](https://gitlab.com/mobivia-design/roadtrip/components/commit/b9bf04c93f0e73e0076a6be76aace09e60fd993b))
* **rating:** add small version ([376bc0a](https://gitlab.com/mobivia-design/roadtrip/components/commit/376bc0af9d97928c32af2eaa0e659f5adf46baa1))
* **rating:** update colors and font size ([fe07841](https://gitlab.com/mobivia-design/roadtrip/components/commit/fe07841119c1ca3833e33d17be0f5dac4c2f6870))
* remove tap highlight on mobile ([d9b7375](https://gitlab.com/mobivia-design/roadtrip/components/commit/d9b737524370bc4218028ddc7eec6f59378c945f))
* **search:** add  button ([5b4573d](https://gitlab.com/mobivia-design/roadtrip/components/commit/5b4573d59e1184e703f3027bd90012f16703c83f))
* **select-filter:** add Blur and active ([f0b6c57](https://gitlab.com/mobivia-design/roadtrip/components/commit/f0b6c57552f179b49ad53b4e53525a0877594a4d))
* **select-filter:** add fuzzysearch ([9ab929a](https://gitlab.com/mobivia-design/roadtrip/components/commit/9ab929a8d073a80821f607dd1b9b2d91d3c663b9))
* **select-filter:** add phone number ([0961914](https://gitlab.com/mobivia-design/roadtrip/components/commit/096191401236e6b61edb28d18e6ade43c999ef2a))
* **select-filter:** pass html as options ([c8a549b](https://gitlab.com/mobivia-design/roadtrip/components/commit/c8a549b9aef02727af36dd534337da063e04dbd1))
* **select-filter:** update colors and font-size ([02e732d](https://gitlab.com/mobivia-design/roadtrip/components/commit/02e732d019aa685e93960dbda05e190327a5bf15))
* **select:** update colors and font-size ([53aded0](https://gitlab.com/mobivia-design/roadtrip/components/commit/53aded002f5340a4f1c3e93f81723112eb22caa8))
* **skeleton:** update color and proportion ([72f8cb3](https://gitlab.com/mobivia-design/roadtrip/components/commit/72f8cb3086e32c8597e6c1c14c45d35546da4610))
* **skeleton:** update grey values ([e30e127](https://gitlab.com/mobivia-design/roadtrip/components/commit/e30e1276b4d1fdee941ee091d12bb5d207d2096b))
* **skeleton:** update margin and height ([1e222ad](https://gitlab.com/mobivia-design/roadtrip/components/commit/1e222add8ccca7a30f08313a9ae46383509be9d6))
* **spacing:** add 4px padding and margin utilities ([203ccc1](https://gitlab.com/mobivia-design/roadtrip/components/commit/203ccc14b56dd297c261b89d902a9a340878d5e3))
* **spinner:** add xl size and color ([2abf306](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abf306e0f86cbc04f796f7744c95c8e353d1af2))
* **stepper:** add component stepper ([5f9b636](https://gitlab.com/mobivia-design/roadtrip/components/commit/5f9b636c030283e9f1d9c9db8ba7705a2862b7e2))
* **storybook:** add utility stories ([a9db6fb](https://gitlab.com/mobivia-design/roadtrip/components/commit/a9db6fb5d3bb388009f54782784da4dacca0eb0d))
* **switch:** customize width of the lever ([f1c1918](https://gitlab.com/mobivia-design/roadtrip/components/commit/f1c19180956579c6821cad2b00e561de2560ac02))
* **textarea:** update colors and font-size ([e9b0673](https://gitlab.com/mobivia-design/roadtrip/components/commit/e9b0673f7823e7fcfb516e29353690850224bd2b))
* **theme:** add Auto5 theme ([38006d6](https://gitlab.com/mobivia-design/roadtrip/components/commit/38006d6e0960f6ca36c099236fb65aed56b42cec))
* **theme:** add mobivia theme ([2ba3e77](https://gitlab.com/mobivia-design/roadtrip/components/commit/2ba3e77b0c06e71b095bb58d2a89297edf26b7c1))
* **theme:** update ATU and Midas themes ([4f101c6](https://gitlab.com/mobivia-design/roadtrip/components/commit/4f101c68d2cc467a57a8e52144f8f7dbe9fb747a))
* **theme:** update ATU theme ([202cad4](https://gitlab.com/mobivia-design/roadtrip/components/commit/202cad4896705b459104f4c350a72da583b8c843))
* **toast:** update icon color for warning state ([3720a20](https://gitlab.com/mobivia-design/roadtrip/components/commit/3720a2023c47f5486fbbdeb8dac3384d89b70ee5))
* **toggle:** update colors and font-size ([be84a52](https://gitlab.com/mobivia-design/roadtrip/components/commit/be84a52ebab43c7484cc1963bffbf613b094a6d1))
* **tooltip:** add line break support ([af4e874](https://gitlab.com/mobivia-design/roadtrip/components/commit/af4e874e546278b4b31985e1e8918948932ce893))
* **tooltip:** update size text and position ([2d2bf72](https://gitlab.com/mobivia-design/roadtrip/components/commit/2d2bf72b0e2e730a3a6c6bce0a06bddc7a4edcc7))
* **typography:** add color title ([8a5c2c2](https://gitlab.com/mobivia-design/roadtrip/components/commit/8a5c2c2c91f5a9e124a247689dff51c4ba1efdbb))
* **typography:** add color title and class text ([bf5a3e3](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf5a3e3e40ae9440c61819139f4911ae1b8c5aca))
* **typography:** add underline ([0637492](https://gitlab.com/mobivia-design/roadtrip/components/commit/06374922ce8ba3aafe95ecbf0738efdc17772d87))
* **typography:** update font-weight for h7, h8, h9 ([cbe2312](https://gitlab.com/mobivia-design/roadtrip/components/commit/cbe23128550569f8976e22671146f06029db3b7c))
* update ATU theme to reflect brand changes ([bd0da7b](https://gitlab.com/mobivia-design/roadtrip/components/commit/bd0da7b3e2e2eca27577a020358fca8843a180a6))
* update Auto5 theme ([a6379f6](https://gitlab.com/mobivia-design/roadtrip/components/commit/a6379f66d5a78921be582f14551541abd67e059a))
* update color system ([62c2f38](https://gitlab.com/mobivia-design/roadtrip/components/commit/62c2f38bcb2d205390f99e798dda974ebcd0f63f))
* update components colors ([47e781f](https://gitlab.com/mobivia-design/roadtrip/components/commit/47e781fc4eb682021ccbe9a1daa8dba9297d3c67))
* update font-size tabs ([954aa2b](https://gitlab.com/mobivia-design/roadtrip/components/commit/954aa2bd8bd86104a31b45fe714118dde60598dc))
* update Midas theme ([034ef65](https://gitlab.com/mobivia-design/roadtrip/components/commit/034ef653f40f996f7223e2057d2902baa38015a6))
* update swiper to 7.0.3 ([e002ac2](https://gitlab.com/mobivia-design/roadtrip/components/commit/e002ac24b36cd8c5e5e40ec99fa4f9b2b5c869ae))
* update theme architecture ([bda995a](https://gitlab.com/mobivia-design/roadtrip/components/commit/bda995a432cde172136813721959592cb9d61000))
* **variables:** add hover colors ([8a893ee](https://gitlab.com/mobivia-design/roadtrip/components/commit/8a893ee6c5267f8ba74d7651e9b02a36a7259a66))
* **variables:** add hover colors for themes ([4771fe6](https://gitlab.com/mobivia-design/roadtrip/components/commit/4771fe6459936025cdaec252904e87034b432ca1))
* **variables:** update component colors ([760b903](https://gitlab.com/mobivia-design/roadtrip/components/commit/760b9036f655514d0737bcc938e61e44c15285c3))
* **variables:** update icon colors ([99d9cf4](https://gitlab.com/mobivia-design/roadtrip/components/commit/99d9cf4defd4dc6b2f438a4b90576079a62b2eaf))


### Bug Fixes

* **accordion:** remove max-height ([cffbd25](https://gitlab.com/mobivia-design/roadtrip/components/commit/cffbd2596e419380f889ad111c333cb509cdfa00))
* ci build error ([124ed72](https://gitlab.com/mobivia-design/roadtrip/components/commit/124ed7226a34d80dd0cc18c4eff19668b881849d))
* ci build error ([b0c3be7](https://gitlab.com/mobivia-design/roadtrip/components/commit/b0c3be7160245eadf9f5de6c606a19db2bc83c35))
* ci build error ([d671a52](https://gitlab.com/mobivia-design/roadtrip/components/commit/d671a52777edcdbc8b2a57aa966415e0c4c60a60))
* ci build error ([b76c121](https://gitlab.com/mobivia-design/roadtrip/components/commit/b76c121a14b4f391e6d0b5d6dd53b7987f2cb9f2))
* **collapse:** increase max-height ([da2ba37](https://gitlab.com/mobivia-design/roadtrip/components/commit/da2ba37823e2d9578b2bce5b8a3fc9a410ba3c97))
* **colors:** map old background value to new one ([8542d35](https://gitlab.com/mobivia-design/roadtrip/components/commit/8542d351195f3576576c70d3f7d6d1710d6d5a1e))
* **dialog:** Layer closing the dialog even with close button hidden ([94e7741](https://gitlab.com/mobivia-design/roadtrip/components/commit/94e7741feaaa73335109a81067349ab7c5ef0c80))
* **dialog:** send close event after the close animation is finished. ([605ad5f](https://gitlab.com/mobivia-design/roadtrip/components/commit/605ad5ff9d04bfe553d1bbaeb72b0d890949dfed))
* **drawer:** add max height CSS variable ([c0aab78](https://gitlab.com/mobivia-design/roadtrip/components/commit/c0aab78cbe6ae276c6b62c93f6ca5a9f04238c50))
* **drawer:** fix height for bottom position ([d1dcf82](https://gitlab.com/mobivia-design/roadtrip/components/commit/d1dcf82a6079327b4e1eb0f7839807a0ee333326))
* **drawer:** send close event after the close animation is finished. ([96ac0d6](https://gitlab.com/mobivia-design/roadtrip/components/commit/96ac0d663cb5f3cb03f2d75d8423debd38787da1))
* fix immutable props ([f914a80](https://gitlab.com/mobivia-design/roadtrip/components/commit/f914a80fbab119adbfc23f444ebeb85a0632a1ee))
* **icon:** fix flag icons ([ff2f8dc](https://gitlab.com/mobivia-design/roadtrip/components/commit/ff2f8dc46b3c30ebbda0cad293502063b3cf31f9))
* **icon:** fix path to icon for Stencil apps ([d69f656](https://gitlab.com/mobivia-design/roadtrip/components/commit/d69f65697ea6e98838b4cd6bcb471c5324b88bf5))
* **icon:** fix vehicle-car-bodywork naming icon ([e7c893f](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7c893fbada18685331579f529572c60fadb33e6))
* **icon:** fix vehicle-car-light-* light part invisible ([b0848f0](https://gitlab.com/mobivia-design/roadtrip/components/commit/b0848f05b21cb7d79e37b72341620ddd43375935))
* **icon:** remove #clip-path) causing problem for icon import ([a0cca13](https://gitlab.com/mobivia-design/roadtrip/components/commit/a0cca13e4d199ac1174b65d031c29103c43c1c7c))
* **icon:** remove unused css variable for wiper icons ([f032629](https://gitlab.com/mobivia-design/roadtrip/components/commit/f032629a470a30596911579ebc3577bf4eb175fb))
* **icons:** fix build ([6b7ad94](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b7ad94ef80d0e0a1a922c066aaa0be97e8c6ac6))
* **icons:** fix build ([24b30a9](https://gitlab.com/mobivia-design/roadtrip/components/commit/24b30a9e71175730f2725bb922f480d3b97b2f82))
* **icons:** keep old name ([178310f](https://gitlab.com/mobivia-design/roadtrip/components/commit/178310fff2159d5bd45e66a6634b26ca901be981))
* **icons:** remove fill path svg ([278ff44](https://gitlab.com/mobivia-design/roadtrip/components/commit/278ff440834780134055b3c9996174eb85f39d0a))
* **icons:** remove hexa for rbg ([5681eee](https://gitlab.com/mobivia-design/roadtrip/components/commit/5681eeebb092dfce98bc44dc91003824f1b03136))
* **input:** duplicating characters ([394253d](https://gitlab.com/mobivia-design/roadtrip/components/commit/394253dd1597bfe33f1037674f09ac0c1674af63))
* **input:** fix opacity for Safari iOS 15+ ([27c7dc3](https://gitlab.com/mobivia-design/roadtrip/components/commit/27c7dc30148c424177aa1e5c1227ebd3428a1ab7))
* **input:** fix placeholder opacity for firefox ([8647025](https://gitlab.com/mobivia-design/roadtrip/components/commit/864702505d05ba89b9d8a4eb581c79003bc19e76))
* **inputGroup:** fix button background hover ([6b0f4d9](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b0f4d912c57d8b0a82805e59e82ac04cbde6ae9))
* **inputGroup:** fix styles for disabled and error states ([32e78f5](https://gitlab.com/mobivia-design/roadtrip/components/commit/32e78f5542d95f1fd8016bd72ec04b20fe85a080))
* **item:** fix detail icon when used in frameworks ([fe613c0](https://gitlab.com/mobivia-design/roadtrip/components/commit/fe613c0fe6610e0c158e0b0d8e39526a2dd6cc88))
* **modal:** send close event after the close animation is finished. ([863860a](https://gitlab.com/mobivia-design/roadtrip/components/commit/863860a68b677a716374355c58178c954e212bbf))
* **plate-number:** Duplicating characters ([86d24f7](https://gitlab.com/mobivia-design/roadtrip/components/commit/86d24f789729a891cb213168897df1df8138499e))
* **plate-number:** Duplicating characters ([5bc18ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/5bc18eac34059f4a044b18e7062cab2e7bee31cd))
* **plateNumber:** fix icon import ([bd73768](https://gitlab.com/mobivia-design/roadtrip/components/commit/bd73768ef9c4a43175a75cc6ed8ef6fc1c8c7e77))
* **rating:** fix half rate ([2a98ab8](https://gitlab.com/mobivia-design/roadtrip/components/commit/2a98ab8654da43ca70d2e21ed565444452d4b9dd))
* **rating:** fix half rate ([fd977d9](https://gitlab.com/mobivia-design/roadtrip/components/commit/fd977d909d273cc4d5f5e87082098ddd9e269c9a))
* **rating:** Remove space when reaviewText is empty ([801c423](https://gitlab.com/mobivia-design/roadtrip/components/commit/801c4237894cb7f579073e780acf6b21fd3f8cf6))
* **select-filter:** fix select value ([076dbd6](https://gitlab.com/mobivia-design/roadtrip/components/commit/076dbd624c323b074dadb782b8d1a38201f01019))
* **select-filter:** remove value only if not part of the options ([c868779](https://gitlab.com/mobivia-design/roadtrip/components/commit/c8687792ba2758734546148a8a2ad742afa44123))
* **select:** selected value in options should be optional ([f8bb721](https://gitlab.com/mobivia-design/roadtrip/components/commit/f8bb72100a2b32af65761b83dbc71c8de2f569e5))
* **spinner:** add size spinner ([c10637d](https://gitlab.com/mobivia-design/roadtrip/components/commit/c10637ddc0cdb666bd26cfce49b6c77514521247))
* **storybook:** fix stories examples ([54677a6](https://gitlab.com/mobivia-design/roadtrip/components/commit/54677a609016cb41de95530240ecf5df5850bf82))
* **switch:** prevent lever from shrinking with long label ([ef000d1](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef000d1d5defc7c85ad7f283541d3695eedbf564))
* **tabs:** add 2 lines display in mobile ([323b2df](https://gitlab.com/mobivia-design/roadtrip/components/commit/323b2dfbcbc8fb4c2f700da660239a028ee8646e))
* **themes:** fix themes with new color system ([df67ffc](https://gitlab.com/mobivia-design/roadtrip/components/commit/df67ffc8302aa447039a7bac2416a05696e8090a))
* **variable:** missing one variable ([bc3a73a](https://gitlab.com/mobivia-design/roadtrip/components/commit/bc3a73a7cf57d7ad8fac51ed2c6566ae0414360b))

## [2.40.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.39.0...2.40.0) (2022-08-11)

### Features

* **icons:** fix build ([24b30a9](https://gitlab.com/mobivia-design/roadtrip/components/commit/24b30a9e71175730f2725bb922f480d3b97b2f82))
* **icons:** add icons ([2c49cf1](https://gitlab.com/mobivia-design/roadtrip/components/commit/2c49cf1eed87078d3e55af72448bd16a6ced89ca))
* **icons:** fix build ([6b7ad94](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b7ad94ef80d0e0a1a922c066aaa0be97e8c6ac6))
* **icons:** keep old name ([178310f](https://gitlab.com/mobivia-design/roadtrip/components/commit/178310fff2159d5bd45e66a6634b26ca901be981))

### Features

* **accordion:** add icon customisation ([0623a88](https://gitlab.com/mobivia-design/roadtrip/components/commit/0623a88e89b9cebbed270c44ca3aabaa89314d36))
* **accordion:** add opening animation ([4c5969b](https://gitlab.com/mobivia-design/roadtrip/components/commit/4c5969b02e402eaa4b0563549a1b32aa234a5ba8))
* **accordion:** move margin to the host element ([3820cef](https://gitlab.com/mobivia-design/roadtrip/components/commit/3820cef8c41fd7e771b1a187134223c536f703f6))
* add accordion noborder component ([1fe6c05](https://gitlab.com/mobivia-design/roadtrip/components/commit/1fe6c054d574bd666b30101744370bd69929bbe2))
* add banner component ([a036770](https://gitlab.com/mobivia-design/roadtrip/components/commit/a036770aaa10d894a1a1511b2b07d2daccc85d38))
* add brand color ([3315976](https://gitlab.com/mobivia-design/roadtrip/components/commit/3315976cca18ad4b67f5c1cd2faa5fe113f5e1bb))
* add breadcrumb pattern ([03101f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/03101f209725cf3624c9c231f40bdf621d4b5c3f))
* add chip component ([ef03729](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef03729261876e4753c0cb3a6d6d76601ed285b8))
* add CSS variables for accordion and item customisation ([8924ba7](https://gitlab.com/mobivia-design/roadtrip/components/commit/8924ba7739330e142c7bb59cd556ced0b916ded4))
* add dropdown component ([bf37106](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf3710652578eb555b24105bcd732604cebf9a5a))
* add duration component ([c7d8349](https://gitlab.com/mobivia-design/roadtrip/components/commit/c7d8349b8edea3dd8f60b623287dd0cf8398deb2))
* add helper checkbox component ([628f3f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/628f3f2bb7aa6d5544abc5126030440406d2c62d))
* add helper input component ([25f16a4](https://gitlab.com/mobivia-design/roadtrip/components/commit/25f16a4a5a59a406c65a52d9d07baa6c9ae6dfed))
* add helper radio component ([908f67a](https://gitlab.com/mobivia-design/roadtrip/components/commit/908f67a79cdd29fec6d47277c5091b652bf426f7))
* add helper textarea component ([b8d9713](https://gitlab.com/mobivia-design/roadtrip/components/commit/b8d97130e939871512dfb40724012352fbb96704))
* add link component ([60af3d3](https://gitlab.com/mobivia-design/roadtrip/components/commit/60af3d34d1c328af2e8b06aad2a6cdfddb7ba39c))
* add lowercase events ([f395d4d](https://gitlab.com/mobivia-design/roadtrip/components/commit/f395d4d90898d4dbba79217e6aa81121e6fa8c20))
* add plate number component ([8981668](https://gitlab.com/mobivia-design/roadtrip/components/commit/89816682304159e78ed191deb50219521b2ad2dc))
* add range component ([ad4189f](https://gitlab.com/mobivia-design/roadtrip/components/commit/ad4189fe077f3653e1ca45c5b3f0fd38aa8a0d4e))
* add select-filter component ([27c1e2f](https://gitlab.com/mobivia-design/roadtrip/components/commit/27c1e2f72df590fc4375ce614f835c1d742729ae))
* Add SVG sprite of icons ([06b85cc](https://gitlab.com/mobivia-design/roadtrip/components/commit/06b85ccc7cdaca7e3e6d9c42812e79dac7883769))
* add toggle component ([c6f796c](https://gitlab.com/mobivia-design/roadtrip/components/commit/c6f796cc9116d589e779b90122596afb0890ccbc))
* **alert:** add title and link ([46cd96b](https://gitlab.com/mobivia-design/roadtrip/components/commit/46cd96b557a4bdbd50ec1d1dd6a69a94e2567fa7))
* **alert:** update colors and text size ([160d132](https://gitlab.com/mobivia-design/roadtrip/components/commit/160d13253a15fa889a04c3468c7c389076ac425e))
* **alert:** update icon color ([a630457](https://gitlab.com/mobivia-design/roadtrip/components/commit/a6304576995d3bcd5a7d0b29a7586cdbd0bbb747))
* **badge:** update colors and padding ([96b5aaa](https://gitlab.com/mobivia-design/roadtrip/components/commit/96b5aaa8099b4d8ff463797decf0ab2bb1ea5034))
* **breakpoint:** add breakpoint 1200px for desktop users ([af1b10c](https://gitlab.com/mobivia-design/roadtrip/components/commit/af1b10c11405f294544da00c2da5ae002e8b1b18))
* **button:** add download attribute ([c6c75f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/c6c75f280467f295a87499caa3979a41200aaafc))
* **button:** add xl size ([f49f489](https://gitlab.com/mobivia-design/roadtrip/components/commit/f49f489cb4e214ce380c886177e7d6dd4a1a371c))
* **button:** update colors ([0b64cf1](https://gitlab.com/mobivia-design/roadtrip/components/commit/0b64cf185c4b90165f705eb1ade538b8a3af3765))
* **checkbox:** add indeterminate state ([9fae5aa](https://gitlab.com/mobivia-design/roadtrip/components/commit/9fae5aa2c756469dbbb5ca591d7f2aba859a3de2))
* **checkbox:** add left label and spaced props ([987176d](https://gitlab.com/mobivia-design/roadtrip/components/commit/987176deab49bbb43fa48bacd78d9a07d31082fa))
* **checkbox:** add more contrast in disabled state ([2f37c32](https://gitlab.com/mobivia-design/roadtrip/components/commit/2f37c32388600f6300da7142798390985b3a8a49))
* **checkbox:** Add props to inverse radio and label order ([aa7bf0c](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa7bf0cf889c1f9a5531f163be576b3c7d63caed))
* **checkbox:** update colors and font-size ([1449f5d](https://gitlab.com/mobivia-design/roadtrip/components/commit/1449f5d2f991fd985f7de114937d487d9ae3bf33))
* **chip:** update colors and paddings ([65bcd91](https://gitlab.com/mobivia-design/roadtrip/components/commit/65bcd9149e67db60bc63ad053d6cd2f92d0717cd))
* **chip:** update height ([6cfe08f](https://gitlab.com/mobivia-design/roadtrip/components/commit/6cfe08f8c4e7ea0b4be16cffc714ccf912eb756a))
* **collapse:** add button customisation ([d39bd0d](https://gitlab.com/mobivia-design/roadtrip/components/commit/d39bd0db53e72668f85cf9ef79fa830ce9aadfe3))
* **counter:** Add events when clicking more or less buttons ([3d91968](https://gitlab.com/mobivia-design/roadtrip/components/commit/3d919689fd771f6c30c1e33123531924a4af3e61))
* **counter:** implement the other sizes ([64c605a](https://gitlab.com/mobivia-design/roadtrip/components/commit/64c605a27fdd000c3ec5a32a1772c52fe62f438e))
* **counter:** increase size for minus and more ([63eebea](https://gitlab.com/mobivia-design/roadtrip/components/commit/63eebea3ea75532838443980119c0bbb1d023ddd))
* **counter:** update colors and font-size ([42e83b4](https://gitlab.com/mobivia-design/roadtrip/components/commit/42e83b459df473ce3f3343900f4ffac7c9483fee))
* **dialog:** make icon customizable ([152e3d7](https://gitlab.com/mobivia-design/roadtrip/components/commit/152e3d77dbb7c0049c3822119e1624ac283c08ba))
* **dialog:** make icon customizable ([52f6e38](https://gitlab.com/mobivia-design/roadtrip/components/commit/52f6e38269afc1add015e3c868f7a1a9ec1a7378))
* **dialog:** update colors and paddings ([7a96b8a](https://gitlab.com/mobivia-design/roadtrip/components/commit/7a96b8ac6c7ec439d73cedc21ac48f20de5130a5))
* **dialog:** update icon to use outline version ([a440a28](https://gitlab.com/mobivia-design/roadtrip/components/commit/a440a2882d1d3f0de47b965a3fc5bb8d376d1e8c))
* **docs:** fix documentation title ([e34d656](https://gitlab.com/mobivia-design/roadtrip/components/commit/e34d65612b46f405ee1427e60def2013a741e392))
* **docs:** update license fil format ([008e6a3](https://gitlab.com/mobivia-design/roadtrip/components/commit/008e6a3b1e99c38555c3ff7a69045ef32059b170))
* **drawer, item:** add icon customisation ([5ec8895](https://gitlab.com/mobivia-design/roadtrip/components/commit/5ec8895b18c9abfc417639cbc01baf44ca778954))
* **drawer:** add css variable to customize header color ([d987bf2](https://gitlab.com/mobivia-design/roadtrip/components/commit/d987bf2b747896e8a4a193dab3c2844e1929866a))
* **drawer:** add delimiter css variable ([c1a72ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/c1a72eaee7c84584bcb927f7e43dfa5790d73f0f))
* **drawer:** add title customisation ([838b4c8](https://gitlab.com/mobivia-design/roadtrip/components/commit/838b4c88e2ce1f91d155c646edf79979ef3f46b5))
* **drawer:** scroll to top after closing ([d89068d](https://gitlab.com/mobivia-design/roadtrip/components/commit/d89068d41fdd2eb4f56f9f0e0f1b5411412157d3))
* drop IE support ([208a8b5](https://gitlab.com/mobivia-design/roadtrip/components/commit/208a8b5306fcf227a23b8c53df81871773d50acd))
* **dropdown:** add hover button ([0d1c69b](https://gitlab.com/mobivia-design/roadtrip/components/commit/0d1c69b1efcddc1b424bcac1412faaccdfcb9efa))
* **dropdown:** update items ([de1eef4](https://gitlab.com/mobivia-design/roadtrip/components/commit/de1eef48614e12b8f3d2cca70e88d10a63d42abb))
* **dropdown:** update margin stories ([deacbac](https://gitlab.com/mobivia-design/roadtrip/components/commit/deacbac8c09377976da05a8701b554fde2f213ce))
* **dropwdown:** add position and direction ([6f02d40](https://gitlab.com/mobivia-design/roadtrip/components/commit/6f02d40a88a71819c5407b3ebed37d219e9e25da))
* **flag:** update design and add new size ([985e36d](https://gitlab.com/mobivia-design/roadtrip/components/commit/985e36d77db5bf5d106bd5f07d328127cbe50a3d))
* **flap:** add ecology color ([8661cfa](https://gitlab.com/mobivia-design/roadtrip/components/commit/8661cfaaacd780b4d5f8fe17c14db4bdadd7878e))
* **flap:** update colors ([e7fc667](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7fc6670daa453f43cbb7dccc0420b41dedd5bce))
* **form:** add required for input and select ([aa145ec](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa145ec5f633806e8c3c13f2e699e6c288b63cca))
* **forms:** add hover state ([71dcce0](https://gitlab.com/mobivia-design/roadtrip/components/commit/71dcce013ddc7c1db2cdf9bc2c1d806b1f172cef))
* **form:** update font size for input and select ([eddffbf](https://gitlab.com/mobivia-design/roadtrip/components/commit/eddffbf664fc8ea7b644d282d0a4d82f00beeb82))
* **form:** update font size for input and select ([43bbbd0](https://gitlab.com/mobivia-design/roadtrip/components/commit/43bbbd0b0f507fc1a71c8cbb038e29053f8c0bf8))
* **icon:** add and improve timer and lock icons ([9877c03](https://gitlab.com/mobivia-design/roadtrip/components/commit/9877c0350dfc77a9bb342ee5007463953e3870b8))
* **icon:** add and update icons ([cc61738](https://gitlab.com/mobivia-design/roadtrip/components/commit/cc61738c32f804c6ea24b3c6f2f9e473e76eecf4))
* **icon:** add and update icons ([20f43c8](https://gitlab.com/mobivia-design/roadtrip/components/commit/20f43c856a73c2c52c21244dac29e3dd736752ba))
* **icon:** add and update icons ([d5a2d97](https://gitlab.com/mobivia-design/roadtrip/components/commit/d5a2d97a7f99a5280d27c839eeabcf04829e0bb0))
* **icon:** add and update icons ([4644b1e](https://gitlab.com/mobivia-design/roadtrip/components/commit/4644b1e28755049a20d318912b268574599726c7))
* **icon:** add and update icons ([1aeb659](https://gitlab.com/mobivia-design/roadtrip/components/commit/1aeb659482be39bfbedd8040acbd97fccb391c18))
* **icon:** add and update icons ([0329660](https://gitlab.com/mobivia-design/roadtrip/components/commit/0329660c391df12801f2feb8f21c6a8e971dbcab))
* **icon:** add and update icons ([e8a4170](https://gitlab.com/mobivia-design/roadtrip/components/commit/e8a41705e9becbd74fe7342ab3ce3e2c0f465ae9))
* **icon:** add and update icons ([01d8751](https://gitlab.com/mobivia-design/roadtrip/components/commit/01d875122af951df4c4b8eb48146e5d6d9f1238e))
* **icon:** add and update icons ([e676c91](https://gitlab.com/mobivia-design/roadtrip/components/commit/e676c914f3e97c690f642b49fe72ce7816e0d7d3))
* **icon:** add and update icons ([22bfba1](https://gitlab.com/mobivia-design/roadtrip/components/commit/22bfba1be9c7014a7dfa73e497e9b323d9375d39))
* **icon:** add and update icons ([62fff5b](https://gitlab.com/mobivia-design/roadtrip/components/commit/62fff5b790876245a1b17a071119e2fb6e33d7a8))
* **icon:** add and update icons ([b434a35](https://gitlab.com/mobivia-design/roadtrip/components/commit/b434a354839ee8a95e46b45df3e5f877e1061336))
* **icon:** add and update icons ([fee5305](https://gitlab.com/mobivia-design/roadtrip/components/commit/fee5305aff7b71ec4c986306d0dd917ae58eb386))
* **icon:** add and update icons ([8aee992](https://gitlab.com/mobivia-design/roadtrip/components/commit/8aee992978f2d29cf83e6c230fd307e33a0752cd))
* **icon:** add and update icons ([66fe56c](https://gitlab.com/mobivia-design/roadtrip/components/commit/66fe56c52934fdda2cdd6b26d001c49134ee1ff7))
* **icon:** add and update icons ([4fa3537](https://gitlab.com/mobivia-design/roadtrip/components/commit/4fa3537cef5eb7c29c1d86f962e393e536867b5a))
* **icon:** add and update icons ([b67cc1a](https://gitlab.com/mobivia-design/roadtrip/components/commit/b67cc1a3cf2a1781406d978fbc2d4844f2fb3604))
* **icon:** add and update icons ([83b6bc6](https://gitlab.com/mobivia-design/roadtrip/components/commit/83b6bc67a149bb27c2614a1952f8b139a82a9c5c))
* **icon:** add and update icons ([007f1b0](https://gitlab.com/mobivia-design/roadtrip/components/commit/007f1b07e7ec1ce9db6d05fb393277aa9eda9ee7))
* **icon:** add and update icons ([9d097c1](https://gitlab.com/mobivia-design/roadtrip/components/commit/9d097c1562f65b10a122d2e15fa57a93bad5847b))
* **icon:** add and update icons ([7d43f1c](https://gitlab.com/mobivia-design/roadtrip/components/commit/7d43f1cd7d4177b945195b7329b338fbeb1f3ae8))
* **icon:** add and update icons ([74067f0](https://gitlab.com/mobivia-design/roadtrip/components/commit/74067f041d4719261779eef949e5e64ffd1c202a))
* **icon:** add and update icons ([2abe584](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abe584dff33451dc0bf9d49fe0222dd73bf58ac))
* **icon:** add icons ([8867503](https://gitlab.com/mobivia-design/roadtrip/components/commit/8867503c60dd10b97d560ba286a8f32bc25d04ca))
* **icon:** add icons ([0da9999](https://gitlab.com/mobivia-design/roadtrip/components/commit/0da999997438c9ef5bab9f4c36be3eec43888ff8))
* **icon:** add icons ([e787344](https://gitlab.com/mobivia-design/roadtrip/components/commit/e787344c4fe472ed3203d8d792a3a93c0dd98b57))
* **icon:** add people-customer-review ([4a543e5](https://gitlab.com/mobivia-design/roadtrip/components/commit/4a543e5cc52b0fb4041cb1c9b0616dde3fada69c))
* **icon:** add start and stop icon ([aa7881c](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa7881c947194a87ef120e62e4e74fd69eedebe1))
* **icon:** add vehicle doors color icon variants ([c81664d](https://gitlab.com/mobivia-design/roadtrip/components/commit/c81664d88dfa075b79989bbda6843a6411e724d1))
* **icon:** add vehicle tires color variant for each tire ([97d2e8a](https://gitlab.com/mobivia-design/roadtrip/components/commit/97d2e8a48422d3db7d094ed683c8303eefcfc5af))
* **icon:** add world, saveDisk and meetingOff icons ([ad15d75](https://gitlab.com/mobivia-design/roadtrip/components/commit/ad15d7520cf8af0dcaca4a8251b9e42c0ab40851))
* **icon:** generate icons variables ([a7f7526](https://gitlab.com/mobivia-design/roadtrip/components/commit/a7f7526b5b0877416c63685691802ba83f97dc0c))
* **icon:** reduce size of flag icons ([78a199f](https://gitlab.com/mobivia-design/roadtrip/components/commit/78a199feec29845ea3e29f1d67147bf470107320))
* **icons:** add icons ([2c49cf1](https://gitlab.com/mobivia-design/roadtrip/components/commit/2c49cf1eed87078d3e55af72448bd16a6ced89ca))
* **icons:** add icons ([eb329b2](https://gitlab.com/mobivia-design/roadtrip/components/commit/eb329b22626089cd4019abcfe29d1b542da14e1e))
* **icons:** add update icons ([2200d6e](https://gitlab.com/mobivia-design/roadtrip/components/commit/2200d6eaef9ddfe457eb2e7c9657f32e35c6decc))
* **icons:** optimisation fill ([f46cb96](https://gitlab.com/mobivia-design/roadtrip/components/commit/f46cb96aeab7c168c5e568519537ef68763d4caa))
* **icons:** update icon color ([34c00a2](https://gitlab.com/mobivia-design/roadtrip/components/commit/34c00a284562025acdf8baa720255a7183728fe4))
* **icons:** update icons figma ([0a25a6c](https://gitlab.com/mobivia-design/roadtrip/components/commit/0a25a6c8f4c0bbf3ae53c341c399d9bbb21d7ceb))
* **icon:** update icon with fallback color ([56a455d](https://gitlab.com/mobivia-design/roadtrip/components/commit/56a455d084bbf998d31b01d43f1a161c1c971e18))
* **icon:** update icons ([55ecc4c](https://gitlab.com/mobivia-design/roadtrip/components/commit/55ecc4ce70916c3b71b487c56ab72e43404b9425))
* **input-group:** update to handle input size xl ([0b79af9](https://gitlab.com/mobivia-design/roadtrip/components/commit/0b79af9028f3b4ed09a1ada2ac9c1cf48ad1fd06))
* **input:** add password ([0de7c54](https://gitlab.com/mobivia-design/roadtrip/components/commit/0de7c540696ec53298706003892359e57b4559c8))
* **input:** update colors and font-size ([2abeace](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abeaceb0521f914b26f1c6ca8c5bb2e76c97212))
* **modals:** mover layer color value to the CSS variable ([12f5fa9](https://gitlab.com/mobivia-design/roadtrip/components/commit/12f5fa95b61e83808dd42e588536da183f4d7069))
* move fonts declaration in its own file ([481f0a9](https://gitlab.com/mobivia-design/roadtrip/components/commit/481f0a9e2c45a6334c3ea762c982bf34d8ed7c8a))
* **navbar-item:** update colors and label text size ([05ba3b4](https://gitlab.com/mobivia-design/roadtrip/components/commit/05ba3b422a6bc9fa56257f35a0443db7a554125b))
* **plate-number:** add Austrian and german format ([023c568](https://gitlab.com/mobivia-design/roadtrip/components/commit/023c568a204fabe06fa1231f407a07ac758ef591))
* **PlateNumber:** add disabled and readonly state ([4b9cafc](https://gitlab.com/mobivia-design/roadtrip/components/commit/4b9cafc88e8b4ce7faa590286ea51974f6976866))
* **plateNumber:** add event bindings ([e68afc3](https://gitlab.com/mobivia-design/roadtrip/components/commit/e68afc3c849c8b15b7350711d492da017f203b5c))
* **plateNumber:** add events ([4c004ac](https://gitlab.com/mobivia-design/roadtrip/components/commit/4c004acc1473c7fca84d64967076fe67e6d2ecb8))
* **PlateNumber:** add motorbike plate ([8151618](https://gitlab.com/mobivia-design/roadtrip/components/commit/81516189439daea79e8511204d1be186d90dd4f3))
* **plateNumber:** Add placeholder attribute ([a37e00d](https://gitlab.com/mobivia-design/roadtrip/components/commit/a37e00dabbca242a233a4a808ce273729a6a1f51))
* **plateNumber:** update Belgian plate to be closer to real one ([2731ddf](https://gitlab.com/mobivia-design/roadtrip/components/commit/2731ddfe4a931a10bdbf13604ea9298bcc270a0b))
* **plateNumber:** update colors and font-size ([2799edf](https://gitlab.com/mobivia-design/roadtrip/components/commit/2799edfe19a538748482b8e41baedcff371f9d59))
* **plateNumber:** update colors nad margins ([d625dc4](https://gitlab.com/mobivia-design/roadtrip/components/commit/d625dc4ad84dcafaa5ff55518296d54803b5e6d8))
* **plateNumber:** update country placeholders ([015f276](https://gitlab.com/mobivia-design/roadtrip/components/commit/015f2769a1a54628e8d730276416340be428710c))
* **progress:** add label ([a203656](https://gitlab.com/mobivia-design/roadtrip/components/commit/a20365622e2c918c746c778fecc76172d27b8a37))
* **progress:** add primary and secondary colors ([e095689](https://gitlab.com/mobivia-design/roadtrip/components/commit/e0956892cd45e0ac4f5c2844a1b85822bebe28a9))
* **progress:** update colors ([5746ecd](https://gitlab.com/mobivia-design/roadtrip/components/commit/5746ecdeb10fe3d50d5c1648dacb77b5d1c4d994))
* **radio:** add more contrast in disabled state ([3c89f45](https://gitlab.com/mobivia-design/roadtrip/components/commit/3c89f45e171fc4930afe64582e63acf8f0110741))
* **radio:** Add props to inverse radio and label order ([86956cd](https://gitlab.com/mobivia-design/roadtrip/components/commit/86956cdf3733e8e1162a3a3539491187aba99a6d))
* **RadioGroup:** Required field missing ([a359bdd](https://gitlab.com/mobivia-design/roadtrip/components/commit/a359bdd05dcc30ad43bbedc6f305aeecab4d1d72))
* **radio:** update colors and font-size ([8ef807d](https://gitlab.com/mobivia-design/roadtrip/components/commit/8ef807dbac414605eed91240b5d16122d01ca546))
* **radio:** update event listener to handle Vue3 ([4e2bf51](https://gitlab.com/mobivia-design/roadtrip/components/commit/4e2bf51fad1db9f941b8bf5adca3fc8c499ac2f3))
* **range:** add tick and color focus active ([b9bf04c](https://gitlab.com/mobivia-design/roadtrip/components/commit/b9bf04c93f0e73e0076a6be76aace09e60fd993b))
* **rating:** add small version ([376bc0a](https://gitlab.com/mobivia-design/roadtrip/components/commit/376bc0af9d97928c32af2eaa0e659f5adf46baa1))
* **rating:** update colors and font size ([fe07841](https://gitlab.com/mobivia-design/roadtrip/components/commit/fe07841119c1ca3833e33d17be0f5dac4c2f6870))
* remove tap highlight on mobile ([d9b7375](https://gitlab.com/mobivia-design/roadtrip/components/commit/d9b737524370bc4218028ddc7eec6f59378c945f))
* **search:** add  button ([5b4573d](https://gitlab.com/mobivia-design/roadtrip/components/commit/5b4573d59e1184e703f3027bd90012f16703c83f))
* **select-filter:** add Blur and active ([f0b6c57](https://gitlab.com/mobivia-design/roadtrip/components/commit/f0b6c57552f179b49ad53b4e53525a0877594a4d))
* **select-filter:** add fuzzysearch ([9ab929a](https://gitlab.com/mobivia-design/roadtrip/components/commit/9ab929a8d073a80821f607dd1b9b2d91d3c663b9))
* **select-filter:** add phone number ([0961914](https://gitlab.com/mobivia-design/roadtrip/components/commit/096191401236e6b61edb28d18e6ade43c999ef2a))
* **select-filter:** pass html as options ([c8a549b](https://gitlab.com/mobivia-design/roadtrip/components/commit/c8a549b9aef02727af36dd534337da063e04dbd1))
* **select-filter:** update colors and font-size ([02e732d](https://gitlab.com/mobivia-design/roadtrip/components/commit/02e732d019aa685e93960dbda05e190327a5bf15))
* **select:** update colors and font-size ([53aded0](https://gitlab.com/mobivia-design/roadtrip/components/commit/53aded002f5340a4f1c3e93f81723112eb22caa8))
* **skeleton:** update color and proportion ([72f8cb3](https://gitlab.com/mobivia-design/roadtrip/components/commit/72f8cb3086e32c8597e6c1c14c45d35546da4610))
* **skeleton:** update grey values ([e30e127](https://gitlab.com/mobivia-design/roadtrip/components/commit/e30e1276b4d1fdee941ee091d12bb5d207d2096b))
* **skeleton:** update margin and height ([1e222ad](https://gitlab.com/mobivia-design/roadtrip/components/commit/1e222add8ccca7a30f08313a9ae46383509be9d6))
* **spacing:** add 4px padding and margin utilities ([203ccc1](https://gitlab.com/mobivia-design/roadtrip/components/commit/203ccc14b56dd297c261b89d902a9a340878d5e3))
* **spinner:** add xl size and color ([2abf306](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abf306e0f86cbc04f796f7744c95c8e353d1af2))
* **stepper:** add component stepper ([5f9b636](https://gitlab.com/mobivia-design/roadtrip/components/commit/5f9b636c030283e9f1d9c9db8ba7705a2862b7e2))
* **storybook:** add utility stories ([a9db6fb](https://gitlab.com/mobivia-design/roadtrip/components/commit/a9db6fb5d3bb388009f54782784da4dacca0eb0d))
* **switch:** customize width of the lever ([f1c1918](https://gitlab.com/mobivia-design/roadtrip/components/commit/f1c19180956579c6821cad2b00e561de2560ac02))
* **textarea:** update colors and font-size ([e9b0673](https://gitlab.com/mobivia-design/roadtrip/components/commit/e9b0673f7823e7fcfb516e29353690850224bd2b))
* **theme:** add Auto5 theme ([38006d6](https://gitlab.com/mobivia-design/roadtrip/components/commit/38006d6e0960f6ca36c099236fb65aed56b42cec))
* **theme:** add mobivia theme ([2ba3e77](https://gitlab.com/mobivia-design/roadtrip/components/commit/2ba3e77b0c06e71b095bb58d2a89297edf26b7c1))
* **theme:** update ATU and Midas themes ([4f101c6](https://gitlab.com/mobivia-design/roadtrip/components/commit/4f101c68d2cc467a57a8e52144f8f7dbe9fb747a))
* **theme:** update ATU theme ([202cad4](https://gitlab.com/mobivia-design/roadtrip/components/commit/202cad4896705b459104f4c350a72da583b8c843))
* **toast:** update icon color for warning state ([3720a20](https://gitlab.com/mobivia-design/roadtrip/components/commit/3720a2023c47f5486fbbdeb8dac3384d89b70ee5))
* **toggle:** update colors and font-size ([be84a52](https://gitlab.com/mobivia-design/roadtrip/components/commit/be84a52ebab43c7484cc1963bffbf613b094a6d1))
* **tooltip:** add line break support ([af4e874](https://gitlab.com/mobivia-design/roadtrip/components/commit/af4e874e546278b4b31985e1e8918948932ce893))
* **tooltip:** update size text and position ([2d2bf72](https://gitlab.com/mobivia-design/roadtrip/components/commit/2d2bf72b0e2e730a3a6c6bce0a06bddc7a4edcc7))
* **typography:** add color title ([8a5c2c2](https://gitlab.com/mobivia-design/roadtrip/components/commit/8a5c2c2c91f5a9e124a247689dff51c4ba1efdbb))
* **typography:** add color title and class text ([bf5a3e3](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf5a3e3e40ae9440c61819139f4911ae1b8c5aca))
* **typography:** add underline ([0637492](https://gitlab.com/mobivia-design/roadtrip/components/commit/06374922ce8ba3aafe95ecbf0738efdc17772d87))
* **typography:** update font-weight for h7, h8, h9 ([cbe2312](https://gitlab.com/mobivia-design/roadtrip/components/commit/cbe23128550569f8976e22671146f06029db3b7c))
* update ATU theme to reflect brand changes ([bd0da7b](https://gitlab.com/mobivia-design/roadtrip/components/commit/bd0da7b3e2e2eca27577a020358fca8843a180a6))
* update Auto5 theme ([a6379f6](https://gitlab.com/mobivia-design/roadtrip/components/commit/a6379f66d5a78921be582f14551541abd67e059a))
* update color system ([62c2f38](https://gitlab.com/mobivia-design/roadtrip/components/commit/62c2f38bcb2d205390f99e798dda974ebcd0f63f))
* update components colors ([47e781f](https://gitlab.com/mobivia-design/roadtrip/components/commit/47e781fc4eb682021ccbe9a1daa8dba9297d3c67))
* update font-size tabs ([954aa2b](https://gitlab.com/mobivia-design/roadtrip/components/commit/954aa2bd8bd86104a31b45fe714118dde60598dc))
* update Midas theme ([034ef65](https://gitlab.com/mobivia-design/roadtrip/components/commit/034ef653f40f996f7223e2057d2902baa38015a6))
* update swiper to 7.0.3 ([e002ac2](https://gitlab.com/mobivia-design/roadtrip/components/commit/e002ac24b36cd8c5e5e40ec99fa4f9b2b5c869ae))
* update theme architecture ([bda995a](https://gitlab.com/mobivia-design/roadtrip/components/commit/bda995a432cde172136813721959592cb9d61000))
* **variables:** add hover colors ([8a893ee](https://gitlab.com/mobivia-design/roadtrip/components/commit/8a893ee6c5267f8ba74d7651e9b02a36a7259a66))
* **variables:** add hover colors for themes ([4771fe6](https://gitlab.com/mobivia-design/roadtrip/components/commit/4771fe6459936025cdaec252904e87034b432ca1))
* **variables:** update component colors ([760b903](https://gitlab.com/mobivia-design/roadtrip/components/commit/760b9036f655514d0737bcc938e61e44c15285c3))
* **variables:** update icon colors ([99d9cf4](https://gitlab.com/mobivia-design/roadtrip/components/commit/99d9cf4defd4dc6b2f438a4b90576079a62b2eaf))


### Bug Fixes

* **accordion:** remove max-height ([cffbd25](https://gitlab.com/mobivia-design/roadtrip/components/commit/cffbd2596e419380f889ad111c333cb509cdfa00))
* ci build error ([124ed72](https://gitlab.com/mobivia-design/roadtrip/components/commit/124ed7226a34d80dd0cc18c4eff19668b881849d))
* ci build error ([b0c3be7](https://gitlab.com/mobivia-design/roadtrip/components/commit/b0c3be7160245eadf9f5de6c606a19db2bc83c35))
* ci build error ([d671a52](https://gitlab.com/mobivia-design/roadtrip/components/commit/d671a52777edcdbc8b2a57aa966415e0c4c60a60))
* ci build error ([b76c121](https://gitlab.com/mobivia-design/roadtrip/components/commit/b76c121a14b4f391e6d0b5d6dd53b7987f2cb9f2))
* **collapse:** increase max-height ([da2ba37](https://gitlab.com/mobivia-design/roadtrip/components/commit/da2ba37823e2d9578b2bce5b8a3fc9a410ba3c97))
* **colors:** map old background value to new one ([8542d35](https://gitlab.com/mobivia-design/roadtrip/components/commit/8542d351195f3576576c70d3f7d6d1710d6d5a1e))
* **dialog:** Layer closing the dialog even with close button hidden ([94e7741](https://gitlab.com/mobivia-design/roadtrip/components/commit/94e7741feaaa73335109a81067349ab7c5ef0c80))
* **dialog:** send close event after the close animation is finished. ([605ad5f](https://gitlab.com/mobivia-design/roadtrip/components/commit/605ad5ff9d04bfe553d1bbaeb72b0d890949dfed))
* **drawer:** add max height CSS variable ([c0aab78](https://gitlab.com/mobivia-design/roadtrip/components/commit/c0aab78cbe6ae276c6b62c93f6ca5a9f04238c50))
* **drawer:** fix height for bottom position ([d1dcf82](https://gitlab.com/mobivia-design/roadtrip/components/commit/d1dcf82a6079327b4e1eb0f7839807a0ee333326))
* **drawer:** send close event after the close animation is finished. ([96ac0d6](https://gitlab.com/mobivia-design/roadtrip/components/commit/96ac0d663cb5f3cb03f2d75d8423debd38787da1))
* fix immutable props ([f914a80](https://gitlab.com/mobivia-design/roadtrip/components/commit/f914a80fbab119adbfc23f444ebeb85a0632a1ee))
* **icon:** fix flag icons ([ff2f8dc](https://gitlab.com/mobivia-design/roadtrip/components/commit/ff2f8dc46b3c30ebbda0cad293502063b3cf31f9))
* **icon:** fix path to icon for Stencil apps ([d69f656](https://gitlab.com/mobivia-design/roadtrip/components/commit/d69f65697ea6e98838b4cd6bcb471c5324b88bf5))
* **icon:** fix vehicle-car-bodywork naming icon ([e7c893f](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7c893fbada18685331579f529572c60fadb33e6))
* **icon:** fix vehicle-car-light-* light part invisible ([b0848f0](https://gitlab.com/mobivia-design/roadtrip/components/commit/b0848f05b21cb7d79e37b72341620ddd43375935))
* **icon:** remove #clip-path) causing problem for icon import ([a0cca13](https://gitlab.com/mobivia-design/roadtrip/components/commit/a0cca13e4d199ac1174b65d031c29103c43c1c7c))
* **icon:** remove unused css variable for wiper icons ([f032629](https://gitlab.com/mobivia-design/roadtrip/components/commit/f032629a470a30596911579ebc3577bf4eb175fb))
* **icons:** fix build ([6b7ad94](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b7ad94ef80d0e0a1a922c066aaa0be97e8c6ac6))
* **icons:** fix build ([24b30a9](https://gitlab.com/mobivia-design/roadtrip/components/commit/24b30a9e71175730f2725bb922f480d3b97b2f82))
* **icons:** keep old name ([178310f](https://gitlab.com/mobivia-design/roadtrip/components/commit/178310fff2159d5bd45e66a6634b26ca901be981))
* **icons:** remove fill path svg ([278ff44](https://gitlab.com/mobivia-design/roadtrip/components/commit/278ff440834780134055b3c9996174eb85f39d0a))
* **input:** duplicating characters ([394253d](https://gitlab.com/mobivia-design/roadtrip/components/commit/394253dd1597bfe33f1037674f09ac0c1674af63))
* **input:** fix opacity for Safari iOS 15+ ([27c7dc3](https://gitlab.com/mobivia-design/roadtrip/components/commit/27c7dc30148c424177aa1e5c1227ebd3428a1ab7))
* **input:** fix placeholder opacity for firefox ([8647025](https://gitlab.com/mobivia-design/roadtrip/components/commit/864702505d05ba89b9d8a4eb581c79003bc19e76))
* **inputGroup:** fix button background hover ([6b0f4d9](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b0f4d912c57d8b0a82805e59e82ac04cbde6ae9))
* **inputGroup:** fix styles for disabled and error states ([32e78f5](https://gitlab.com/mobivia-design/roadtrip/components/commit/32e78f5542d95f1fd8016bd72ec04b20fe85a080))
* **item:** fix detail icon when used in frameworks ([fe613c0](https://gitlab.com/mobivia-design/roadtrip/components/commit/fe613c0fe6610e0c158e0b0d8e39526a2dd6cc88))
* **modal:** send close event after the close animation is finished. ([863860a](https://gitlab.com/mobivia-design/roadtrip/components/commit/863860a68b677a716374355c58178c954e212bbf))
* **plate-number:** Duplicating characters ([86d24f7](https://gitlab.com/mobivia-design/roadtrip/components/commit/86d24f789729a891cb213168897df1df8138499e))
* **plate-number:** Duplicating characters ([5bc18ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/5bc18eac34059f4a044b18e7062cab2e7bee31cd))
* **plateNumber:** fix icon import ([bd73768](https://gitlab.com/mobivia-design/roadtrip/components/commit/bd73768ef9c4a43175a75cc6ed8ef6fc1c8c7e77))
* **rating:** fix half rate ([2a98ab8](https://gitlab.com/mobivia-design/roadtrip/components/commit/2a98ab8654da43ca70d2e21ed565444452d4b9dd))
* **rating:** fix half rate ([fd977d9](https://gitlab.com/mobivia-design/roadtrip/components/commit/fd977d909d273cc4d5f5e87082098ddd9e269c9a))
* **rating:** Remove space when reaviewText is empty ([801c423](https://gitlab.com/mobivia-design/roadtrip/components/commit/801c4237894cb7f579073e780acf6b21fd3f8cf6))
* **select-filter:** fix select value ([076dbd6](https://gitlab.com/mobivia-design/roadtrip/components/commit/076dbd624c323b074dadb782b8d1a38201f01019))
* **select-filter:** remove value only if not part of the options ([c868779](https://gitlab.com/mobivia-design/roadtrip/components/commit/c8687792ba2758734546148a8a2ad742afa44123))
* **select:** selected value in options should be optional ([f8bb721](https://gitlab.com/mobivia-design/roadtrip/components/commit/f8bb72100a2b32af65761b83dbc71c8de2f569e5))
* **spinner:** add size spinner ([c10637d](https://gitlab.com/mobivia-design/roadtrip/components/commit/c10637ddc0cdb666bd26cfce49b6c77514521247))
* **storybook:** fix stories examples ([54677a6](https://gitlab.com/mobivia-design/roadtrip/components/commit/54677a609016cb41de95530240ecf5df5850bf82))
* **switch:** prevent lever from shrinking with long label ([ef000d1](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef000d1d5defc7c85ad7f283541d3695eedbf564))
* **tabs:** add 2 lines display in mobile ([323b2df](https://gitlab.com/mobivia-design/roadtrip/components/commit/323b2dfbcbc8fb4c2f700da660239a028ee8646e))
* **themes:** fix themes with new color system ([df67ffc](https://gitlab.com/mobivia-design/roadtrip/components/commit/df67ffc8302aa447039a7bac2416a05696e8090a))
* **variable:** missing one variable ([bc3a73a](https://gitlab.com/mobivia-design/roadtrip/components/commit/bc3a73a7cf57d7ad8fac51ed2c6566ae0414360b))

## [2.39.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.38.0...2.39.0) (2022-08-02)

### Features

* **stepper:** add component stepper ([5f9b636](https://gitlab.com/mobivia-design/roadtrip/components/commit/5f9b636c030283e9f1d9c9db8ba7705a2862b7e2))
* **alert:** add title and link ([46cd96b](https://gitlab.com/mobivia-design/roadtrip/components/commit/46cd96b557a4bdbd50ec1d1dd6a69a94e2567fa7))

## [2.38.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.37.1...2.38.0) (2022-07-22)


### Features

* **input:** add password ([0de7c54](https://gitlab.com/mobivia-design/roadtrip/components/commit/0de7c540696ec53298706003892359e57b4559c8))
* **progress:** add label ([a203656](https://gitlab.com/mobivia-design/roadtrip/components/commit/a20365622e2c918c746c778fecc76172d27b8a37))
* **counter:** increase size for minus and more ([63eebea](https://gitlab.com/mobivia-design/roadtrip/components/commit/63eebea3ea75532838443980119c0bbb1d023ddd))
* **rating:** add small version ([376bc0a](https://gitlab.com/mobivia-design/roadtrip/components/commit/376bc0af9d97928c32af2eaa0e659f5adf46baa1))


## [2.37.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.36.0...2.37.0) (2022-07-07)


### Features

* **search:** add  button ([5b4573d](https://gitlab.com/mobivia-design/roadtrip/components/commit/5b4573d59e1184e703f3027bd90012f16703c83f))
* **icons:** add icons ([eb329b2](https://gitlab.com/mobivia-design/roadtrip/components/commit/eb329b22626089cd4019abcfe29d1b542da14e1e))

### Bug Fixes

* **tabs:** add 2 lines display in mobile ([323b2df](https://gitlab.com/mobivia-design/roadtrip/components/commit/323b2dfbcbc8fb4c2f700da660239a028ee8646e))


## [2.36.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.35.0...2.36.0) (2022-06-23)


### Features

* **tooltip:** update size text and position ([2d2bf72](https://gitlab.com/mobivia-design/roadtrip/components/commit/2d2bf72b0e2e730a3a6c6bce0a06bddc7a4edcc7))
* **skeleton:** update color and proportion ([72f8cb3](https://gitlab.com/mobivia-design/roadtrip/components/commit/72f8cb3086e32c8597e6c1c14c45d35546da4610))
* **skeleton:** update margin and height ([1e222ad](https://gitlab.com/mobivia-design/roadtrip/components/commit/1e222add8ccca7a30f08313a9ae46383509be9d6))
* **spinner:** add xl size and color ([2abf306](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abf306e0f86cbc04f796f7744c95c8e353d1af2))
* **select-filter:** add phone number ([0961914](https://gitlab.com/mobivia-design/roadtrip/components/commit/096191401236e6b61edb28d18e6ade43c999ef2a))


### Bug Fixes

* **input:** duplicating characters ([394253d](https://gitlab.com/mobivia-design/roadtrip/components/commit/394253dd1597bfe33f1037674f09ac0c1674af63))


## [2.35.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.34.0...2.35.0) (2022-06-15)

### Features

* **form:** add required for input and select ([aa145ec](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa145ec5f633806e8c3c13f2e699e6c288b63cca))
* **dropwdown:** add position and direction ([6f02d40](https://gitlab.com/mobivia-design/roadtrip/components/commit/6f02d40a88a71819c5407b3ebed37d219e9e25da))
* **dropdown:** update margin stories ([deacbac](https://gitlab.com/mobivia-design/roadtrip/components/commit/deacbac8c09377976da05a8701b554fde2f213ce))
* **dropdown:** add hover button ([0d1c69b](https://gitlab.com/mobivia-design/roadtrip/components/commit/0d1c69b1efcddc1b424bcac1412faaccdfcb9efa))
* **dropdown:** update items ([de1eef4](https://gitlab.com/mobivia-design/roadtrip/components/commit/de1eef48614e12b8f3d2cca70e88d10a63d42abb))
* **plate-number:** add Austrian and german format ([023c568](https://gitlab.com/mobivia-design/roadtrip/components/commit/023c568a204fabe06fa1231f407a07ac758ef591))

### Bug Fixes

* **accordion:** remove max-height ([cffbd25](https://gitlab.com/mobivia-design/roadtrip/components/commit/cffbd2596e419380f889ad111c333cb509cdfa00))
* **icons:** remove fill path svg ([278ff44](https://gitlab.com/mobivia-design/roadtrip/components/commit/278ff440834780134055b3c9996174eb85f39d0a))
* **spinner:** add size spinner ([c10637d](https://gitlab.com/mobivia-design/roadtrip/components/commit/c10637ddc0cdb666bd26cfce49b6c77514521247))


## [2.34.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.33.0...2.34.0) (2022-06-08)


### Refactor

* **accordion:** update bold header ([53b7759](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/53b77598f5ffb05813c08a310b51aa92b04bc4e8))
* **chip:** update color close ([09d7abf](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/09d7abf246cdd6f4448fccfa7f368fa608133e20))
* **typography:** update size h5 ([54deb64](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/54deb64c6b18f375c289c93633921febe0e4b512))
* **form:** update color border([a441c98](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/a441c98dd20292b15668578aaf02f52cf451b025))
* **progress-bar** update color secondary([a7c1c07](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/a7c1c0732723e3f7e3385ac7bae9e7cabc5edd36))
* **tabs** update color + secondary([44c5633](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/44c5633971b98ab58ad092b737f277215e5bb5f8))
* **flap** update color([fe5c7b5](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/fe5c7b59bff3956aa9eeace1b904ba7b55b552fd))

## [2.33.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.32.0...2.33.0) (2022-06-01)

### Features

* **typography:** add color title ([8a5c2c2](https://gitlab.com/mobivia-design/roadtrip/components/commit/8a5c2c2c91f5a9e124a247689dff51c4ba1efdbb))
* **typography:** add color title and class text ([bf5a3e3](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf5a3e3e40ae9440c61819139f4911ae1b8c5aca))
* **toast:** update position and color ([9f853cd](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/9f853cdfb7a858e56c121582de9dfd2c27ba653a))

## [2.32.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.31.1...2.32.0) (2022-05-24)

### Features

* **icons:** optimisation fill ([f46cb96](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/f46cb96aeab7c168c5e568519537ef68763d4caa))
* **icons:** update icons figma ([0a25a6c](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/0a25a6c8f4c0bbf3ae53c341c399d9bbb21d7ceb))

### [2.31.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.31.0...2.31.1) (2022-05-23)

### Bug Fixes

* **select-filter:** fix select value ([076dbd6](https://gitlab.com/mobivia-design/roadtrip/components/commit/076dbd624c323b074dadb782b8d1a38201f01019))

## [2.31.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.30.2...2.31.0) (2022-05-20)

### Features

* **icon:** add people-customer-review ([4a543e5](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/4a543e5cc52b0fb4041cb1c9b0616dde3fada69c))


### [2.30.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.30.1...2.30.2) (2022-05-19)

### Bug Fixes

* **plate-number:** Duplicating characters ([86d24f7](https://gitlab.com/mobivia-design/roadtrip/components/commit/86d24f789729a891cb213168897df1df8138499e))


### [2.30.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.30.0...2.30.1) (2022-05-19)

### Bug Fixes

* **plate-number:** Duplicating characters ([5bc18ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/5bc18eac34059f4a044b18e7062cab2e7bee31cd))

## [2.30.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.29.1...2.30.0) (2022-05-19)


### Features

* **select-filter:** add Blur and active ([f0b6c57](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/f0b6c57552f179b49ad53b4e53525a0877594a4d))
* **range:** add tick and color focus active ([b9bf04c](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/b9bf04c93f0e73e0076a6be76aace09e60fd993b))


### [2.29.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.29.0...v2.29.1) (2022-05-13)


### Features

* **typography:** add underline heading title ([0637492](https://gitlab.com/mobivia-design/roadtrip/components/-/commit/06374922ce8ba3aafe95ecbf0738efdc17772d87))

## [2.29.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.28.0...2.29.0) (2022-05-05)


### Features

* add brand color ([3315976](https://gitlab.com/mobivia-design/roadtrip/components/commit/3315976cca18ad4b67f5c1cd2faa5fe113f5e1bb))
* add banner component ([a036770](https://gitlab.com/mobivia-design/roadtrip/components/commit/a036770aaa10d894a1a1511b2b07d2daccc85d38))
* **checkbox:** add helper checkbox component ([628f3f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/628f3f2bb7aa6d5544abc5126030440406d2c62d))
* **input:** add helper input component ([25f16a4](https://gitlab.com/mobivia-design/roadtrip/components/commit/25f16a4a5a59a406c65a52d9d07baa6c9ae6dfed))
* **radio:** add helper radio component ([908f67a](https://gitlab.com/mobivia-design/roadtrip/components/commit/908f67a79cdd29fec6d47277c5091b652bf426f7))
* **textarea:** add helper textarea component ([b8d9713](https://gitlab.com/mobivia-design/roadtrip/components/commit/b8d97130e939871512dfb40724012352fbb96704))
* **accordion:** add accordion noborder component ([1fe6c05](https://gitlab.com/mobivia-design/roadtrip/components/commit/1fe6c054d574bd666b30101744370bd69929bbe2))


### Bug Fixes

* **themes:** fix themes with new color system ([df67ffc](https://gitlab.com/mobivia-design/roadtrip/components/commit/df67ffc8302aa447039a7bac2416a05696e8090a))
* **colors:** map old background value to new one ([8542d35](https://gitlab.com/mobivia-design/roadtrip/components/commit/8542d351195f3576576c70d3f7d6d1710d6d5a1e))


## [2.28.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.27.1...2.28.0) (2022-04-20)


### Features

* update color system ([62c2f38](https://gitlab.com/mobivia-design/roadtrip/components/commit/62c2f38bcb2d205390f99e798dda974ebcd0f63f))
* update components colors ([47e781f](https://gitlab.com/mobivia-design/roadtrip/components/commit/47e781fc4eb682021ccbe9a1daa8dba9297d3c67))
* add link component ([60af3d3](https://gitlab.com/mobivia-design/roadtrip/components/commit/60af3d34d1c328af2e8b06aad2a6cdfddb7ba39c))
* add breadcrumb pattern ([03101f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/03101f209725cf3624c9c231f40bdf621d4b5c3f))
* add toggle component ([c6f796c](https://gitlab.com/mobivia-design/roadtrip/components/commit/c6f796cc9116d589e779b90122596afb0890ccbc))
* **theme:** update ATU theme ([202cad4](https://gitlab.com/mobivia-design/roadtrip/components/commit/202cad4896705b459104f4c350a72da583b8c843))
* **theme:** add mobivia theme ([2ba3e77](https://gitlab.com/mobivia-design/roadtrip/components/commit/2ba3e77b0c06e71b095bb58d2a89297edf26b7c1))
* **colors** update color utilities([24af24d](https://gitlab.com/mobivia-design/roadtrip/components/commit/24af24de7493911ef811adc62319d2261e046138))
* **alert:** update colors and text size ([160d132](https://gitlab.com/mobivia-design/roadtrip/components/commit/160d13253a15fa889a04c3468c7c389076ac425e))
* **badge:** update colors and padding ([96b5aaa](https://gitlab.com/mobivia-design/roadtrip/components/commit/96b5aaa8099b4d8ff463797decf0ab2bb1ea5034))
* **button:** update colors ([0b64cf1](https://gitlab.com/mobivia-design/roadtrip/components/commit/0b64cf185c4b90165f705eb1ade538b8a3af3765))
* **chip:** update colors and paddings ([65bcd91](https://gitlab.com/mobivia-design/roadtrip/components/commit/65bcd9149e67db60bc63ad053d6cd2f92d0717cd))
* **dialog:** update colors and paddings ([7a96b8a](https://gitlab.com/mobivia-design/roadtrip/components/commit/7a96b8ac6c7ec439d73cedc21ac48f20de5130a5))
* **flap:** update colors ([e7fc667](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7fc6670daa453f43cbb7dccc0420b41dedd5bce))
* **navbar-item:** update colors and label text size ([05ba3b4](https://gitlab.com/mobivia-design/roadtrip/components/commit/05ba3b422a6bc9fa56257f35a0443db7a554125b))
* **progress:** add primary and secondary colors ([e095689](https://gitlab.com/mobivia-design/roadtrip/components/commit/e0956892cd45e0ac4f5c2844a1b85822bebe28a9))
* **progress:** update colors ([5746ecd](https://gitlab.com/mobivia-design/roadtrip/components/commit/5746ecdeb10fe3d50d5c1648dacb77b5d1c4d994))
* **rating:** update colors and font size ([fe07841](https://gitlab.com/mobivia-design/roadtrip/components/commit/fe07841119c1ca3833e33d17be0f5dac4c2f6870))
* **checkbox:** update colors and font-size ([1449f5d](https://gitlab.com/mobivia-design/roadtrip/components/commit/1449f5d2f991fd985f7de114937d487d9ae3bf33))
* **radio:** update colors and font-size ([8ef807d](https://gitlab.com/mobivia-design/roadtrip/components/commit/8ef807dbac414605eed91240b5d16122d01ca546))
* **toggle:** update colors and font-size ([be84a52](https://gitlab.com/mobivia-design/roadtrip/components/commit/be84a52ebab43c7484cc1963bffbf613b094a6d1))
* **input:** update colors and font-size ([2abeace](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abeaceb0521f914b26f1c6ca8c5bb2e76c97212))
* **textarea:** update colors and font-size ([e9b0673](https://gitlab.com/mobivia-design/roadtrip/components/commit/e9b0673f7823e7fcfb516e29353690850224bd2b))
* **select:** update colors and font-size ([53aded0](https://gitlab.com/mobivia-design/roadtrip/components/commit/53aded002f5340a4f1c3e93f81723112eb22caa8))
* **input-group:** update to handle input size xl ([0b79af9](https://gitlab.com/mobivia-design/roadtrip/components/commit/0b79af9028f3b4ed09a1ada2ac9c1cf48ad1fd06))
* **select-filter:** update colors and font-size ([02e732d](https://gitlab.com/mobivia-design/roadtrip/components/commit/02e732d019aa685e93960dbda05e190327a5bf15))
* **counter:** update colors and font-size ([42e83b4](https://gitlab.com/mobivia-design/roadtrip/components/commit/42e83b459df473ce3f3343900f4ffac7c9483fee))
* **plateNumber:** update colors and font-size ([2799edf](https://gitlab.com/mobivia-design/roadtrip/components/commit/2799edfe19a538748482b8e41baedcff371f9d59))
* **plateNumber:** update colors and margins ([d625dc4](https://gitlab.com/mobivia-design/roadtrip/components/commit/d625dc4ad84dcafaa5ff55518296d54803b5e6d8))
* **button:** add xl size ([f49f489](https://gitlab.com/mobivia-design/roadtrip/components/commit/f49f489cb4e214ce380c886177e7d6dd4a1a371c))
* **checkbox:** add indeterminate state ([9fae5aa](https://gitlab.com/mobivia-design/roadtrip/components/commit/9fae5aa2c756469dbbb5ca591d7f2aba859a3de2))
* **chip:** update height ([6cfe08f](https://gitlab.com/mobivia-design/roadtrip/components/commit/6cfe08f8c4e7ea0b4be16cffc714ccf912eb756a))
* **plateNumber:** update Belgian plate to be closer to real one ([2731ddf](https://gitlab.com/mobivia-design/roadtrip/components/commit/2731ddfe4a931a10bdbf13604ea9298bcc270a0b))
* **checkbox:** add more contrast in disabled state ([2f37c32](https://gitlab.com/mobivia-design/roadtrip/components/commit/2f37c32388600f6300da7142798390985b3a8a49))
* **radio:** add more contrast in disabled state ([3c89f45](https://gitlab.com/mobivia-design/roadtrip/components/commit/3c89f45e171fc4930afe64582e63acf8f0110741))
* **forms:** add hover state ([71dcce0](https://gitlab.com/mobivia-design/roadtrip/components/commit/71dcce013ddc7c1db2cdf9bc2c1d806b1f172cef))
* **skeleton:** update grey values ([e30e127](https://gitlab.com/mobivia-design/roadtrip/components/commit/e30e1276b4d1fdee941ee091d12bb5d207d2096b))


### [2.27.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.27.0...2.27.1) (2022-03-18)


### Features

* **PlateNumber:** add disabled and readonly state ([4b9cafc](https://gitlab.com/mobivia-design/roadtrip/components/commit/4b9cafc88e8b4ce7faa590286ea51974f6976866))

## [2.27.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.26.0...2.27.0) (2022-02-08)


### Features

* add duration component ([c7d8349](https://gitlab.com/mobivia-design/roadtrip/components/commit/c7d8349b8edea3dd8f60b623287dd0cf8398deb2))
* **icon:** add and update icons ([20f43c8](https://gitlab.com/mobivia-design/roadtrip/components/commit/20f43c856a73c2c52c21244dac29e3dd736752ba))
* **icon:** add and update icons ([cc61738](https://gitlab.com/mobivia-design/roadtrip/components/commit/cc61738c32f804c6ea24b3c6f2f9e473e76eecf4))


## [2.26.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.25.1...2.26.0) (2022-01-20)


### Features

* add lowercase events ([f395d4d](https://gitlab.com/mobivia-design/roadtrip/components/commit/f395d4d90898d4dbba79217e6aa81121e6fa8c20))
* move fonts declaration in its own file ([481f0a9](https://gitlab.com/mobivia-design/roadtrip/components/commit/481f0a9e2c45a6334c3ea762c982bf34d8ed7c8a))
* update theme architecture ([bda995a](https://gitlab.com/mobivia-design/roadtrip/components/commit/bda995a432cde172136813721959592cb9d61000))

### [2.25.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.25.0...2.25.1) (2022-01-12)


### Features

* **icon:** add icons ([8867503](https://gitlab.com/mobivia-design/roadtrip/components/commit/8867503c60dd10b97d560ba286a8f32bc25d04ca))


## [2.25.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.24.1...2.25.0) (2021-12-21)


### Features

* **select-filter:** add fuzzysearch ([9ab929a](https://gitlab.com/mobivia-design/roadtrip/components/commit/9ab929a8d073a80821f607dd1b9b2d91d3c663b9))
* Update loader path (old one still present for backbard compatibility) ([144ce9](https://gitlab.com/mobivia-design/roadtrip/components/commit/144ce9b5d4164bde9bd24a82f7e46560c723a2f9))

### Bug Fixes

* **select-filter:** remove value only if not part of the options ([c868779](https://gitlab.com/mobivia-design/roadtrip/components/commit/c8687792ba2758734546148a8a2ad742afa44123))
* **icon:** fix vehicle-car-bodywork naming icon ([e7c893f](https://gitlab.com/mobivia-design/roadtrip/components/commit/e7c893fbada18685331579f529572c60fadb33e6))


### [2.24.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.24.0...2.24.1) (2021-12-09)


### Features

* **icon:** add and update icons ([4644b1e](https://gitlab.com/mobivia-design/roadtrip/components/commit/4644b1e28755049a20d318912b268574599726c7))
* **icon:** add and update icons ([d5a2d97](https://gitlab.com/mobivia-design/roadtrip/components/commit/d5a2d97a7f99a5280d27c839eeabcf04829e0bb0))


### Bug Fixes

* **drawer:** fix height for bottom position ([d1dcf82](https://gitlab.com/mobivia-design/roadtrip/components/commit/d1dcf82a6079327b4e1eb0f7839807a0ee333326))
* **drawer:** add max height CSS variable ([c0aab78](https://gitlab.com/mobivia-design/roadtrip/components/commit/c0aab78cbe6ae276c6b62c93f6ca5a9f04238c50))
* **inputGroup:** fix button background hover ([6b0f4d9](https://gitlab.com/mobivia-design/roadtrip/components/commit/6b0f4d912c57d8b0a82805e59e82ac04cbde6ae9))


## [2.24.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.23.0...2.24.0) (2021-12-02)


### Features

* add dropdown component ([bf37106](https://gitlab.com/mobivia-design/roadtrip/components/commit/bf3710652578eb555b24105bcd732604cebf9a5a))
* **PlateNumber:** add motorbike plate ([8151618](https://gitlab.com/mobivia-design/roadtrip/components/commit/81516189439daea79e8511204d1be186d90dd4f3))
* **drawer:** add delimiter css variable ([c1a72ea](https://gitlab.com/mobivia-design/roadtrip/components/commit/c1a72eaee7c84584bcb927f7e43dfa5790d73f0f))
* **select-filter:** pass html as options ([c8a549b](https://gitlab.com/mobivia-design/roadtrip/components/commit/c8a549b9aef02727af36dd534337da063e04dbd1))


## [2.23.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.22.1...2.23.0) (2021-11-18)


### Features

* add select-filter component ([27c1e2f](https://gitlab.com/mobivia-design/roadtrip/components/commit/27c1e2f72df590fc4375ce614f835c1d742729ae))
* update ATU theme to reflect brand changes ([bd0da7b](https://gitlab.com/mobivia-design/roadtrip/components/commit/bd0da7b3e2e2eca27577a020358fca8843a180a6))
* **drawer:** add title customisation ([838b4c8](https://gitlab.com/mobivia-design/roadtrip/components/commit/838b4c88e2ce1f91d155c646edf79979ef3f46b5))
* **accordion:** add opening animation ([4c5969b](https://gitlab.com/mobivia-design/roadtrip/components/commit/4c5969b02e402eaa4b0563549a1b32aa234a5ba8))
* **flap:** add ecology color ([8661cfa](https://gitlab.com/mobivia-design/roadtrip/components/commit/8661cfaaacd780b4d5f8fe17c14db4bdadd7878e))
* drop IE support ([208a8b5](https://gitlab.com/mobivia-design/roadtrip/components/commit/208a8b5306fcf227a23b8c53df81871773d50acd))
* remove tap highlight on mobile ([d9b7375](https://gitlab.com/mobivia-design/roadtrip/components/commit/d9b737524370bc4218028ddc7eec6f59378c945f))
* **icon:** add and update icons ([1aeb659](https://gitlab.com/mobivia-design/roadtrip/components/commit/1aeb659482be39bfbedd8040acbd97fccb391c18))

### Bug Fixes

* **collapse:** increase max-height ([da2ba37](https://gitlab.com/mobivia-design/roadtrip/components/commit/da2ba37823e2d9578b2bce5b8a3fc9a410ba3c97))
* **input:** fix opacity for Safari iOS 15+ ([27c7dc3](https://gitlab.com/mobivia-design/roadtrip/components/commit/27c7dc30148c424177aa1e5c1227ebd3428a1ab7))
* **inputGroup:** fix styles for disabled and error states ([32e78f5](https://gitlab.com/mobivia-design/roadtrip/components/commit/32e78f5542d95f1fd8016bd72ec04b20fe85a080))



### [2.22.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.22.0...2.22.1) (2021-10-15)


### Features

* **icon:** add and update icons ([0329660](https://gitlab.com/mobivia-design/roadtrip/components/commit/0329660c391df12801f2feb8f21c6a8e971dbcab))



## [2.22.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.21.0...2.22.0) (2021-10-11)


### Features

* **counter:** implement the other sizes ([64c605a](https://gitlab.com/mobivia-design/roadtrip/components/commit/64c605a27fdd000c3ec5a32a1772c52fe62f438e))



## [2.21.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.20.2...2.21.0) (2021-10-01)


### Features

* **dialog:** make icon customizable ([52f6e38](https://gitlab.com/mobivia-design/roadtrip/components/commit/52f6e38269afc1add015e3c868f7a1a9ec1a7378))
* **RadioGroup:** Required field missing ([a359bdd](https://gitlab.com/mobivia-design/roadtrip/components/commit/a359bdd05dcc30ad43bbedc6f305aeecab4d1d72))
* **icon:** add world, saveDisk and meetingOff icons ([ad15d75](https://gitlab.com/mobivia-design/roadtrip/components/commit/ad15d7520cf8af0dcaca4a8251b9e42c0ab40851))
* **icon:** add and update icons ([e8a4170](https://gitlab.com/mobivia-design/roadtrip/components/commit/e8a41705e9becbd74fe7342ab3ce3e2c0f465ae9))
* **radio:** update event listener to handle Vue3 ([4e2bf51](https://gitlab.com/mobivia-design/roadtrip/components/commit/4e2bf51fad1db9f941b8bf5adca3fc8c499ac2f3))
* **carousel:** update swiper to 7.0.3 ([e002ac2](https://gitlab.com/mobivia-design/roadtrip/components/commit/e002ac24b36cd8c5e5e40ec99fa4f9b2b5c869ae))

### Bug Fixes

* **switch:** prevent lever from shrinking with long label ([ef000d1](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef000d1d5defc7c85ad7f283541d3695eedbf564))



### [2.20.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.19.0...2.20.2) (2021-08-25)


### Features

* Add SVG sprite of icons ([06b85cc](https://gitlab.com/mobivia-design/roadtrip/components/commit/06b85ccc7cdaca7e3e6d9c42812e79dac7883769))
* **icon:** reduce size of flag icons ([78a199f](https://gitlab.com/mobivia-design/roadtrip/components/commit/78a199feec29845ea3e29f1d67147bf470107320))
* **counter:** Add events when clicking more or less buttons ([3d91968](https://gitlab.com/mobivia-design/roadtrip/components/commit/3d919689fd771f6c30c1e33123531924a4af3e61))
* **plateNumber:** Add placeholder attribute ([a37e00d](https://gitlab.com/mobivia-design/roadtrip/components/commit/a37e00dabbca242a233a4a808ce273729a6a1f51))
* **drawer:** scroll to top after closing ([d89068d](https://gitlab.com/mobivia-design/roadtrip/components/commit/d89068d41fdd2eb4f56f9f0e0f1b5411412157d3))

### Bug Fixes

* **dialog:** Layer closing the dialog even with close button hidden ([94e7741](https://gitlab.com/mobivia-design/roadtrip/components/commit/94e7741feaaa73335109a81067349ab7c5ef0c80))
* **rating:** Remove space when reaviewText is empty ([801c423](https://gitlab.com/mobivia-design/roadtrip/components/commit/801c4237894cb7f579073e780acf6b21fd3f8cf6))
* **plateNumber:** fix icon import ([bd73768](https://gitlab.com/mobivia-design/roadtrip/components/commit/bd73768ef9c4a43175a75cc6ed8ef6fc1c8c7e77))



### [2.19.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.18.3...2.19.0) (2021-07-23)


### Features

* **theme:** add Auto5 theme ([38006d6](https://gitlab.com/mobivia-design/roadtrip/components/commit/38006d6e0960f6ca36c099236fb65aed56b42cec))
* **theme:** update ATU and Midas themes ([4f101c6](https://gitlab.com/mobivia-design/roadtrip/components/commit/4f101c68d2cc467a57a8e52144f8f7dbe9fb747a))



### [2.18.3](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.18.2...2.18.3) (2021-07-12)


### Features

* **icon:** add and update icons ([e676c91](https://gitlab.com/mobivia-design/roadtrip/components/commit/e676c914f3e97c690f642b49fe72ce7816e0d7d3))



### [2.18.1](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.18.0...2.18.1) (2021-07-07)


### Features

* **drawer:** add css variable to customize header color ([d987bf2](https://gitlab.com/mobivia-design/roadtrip/components/commit/d987bf2b747896e8a4a193dab3c2844e1929866a))
* **icon:** add and update icons ([22bfba1](https://gitlab.com/mobivia-design/roadtrip/components/commit/22bfba1be9c7014a7dfa73e497e9b323d9375d39))



### [2.18.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.17.6...2.18.0) (2021-06-30)


### Features

* add range component ([ad4189f](https://gitlab.com/mobivia-design/roadtrip/components/commit/ad4189fe077f3653e1ca45c5b3f0fd38aa8a0d4e))
* add plate number component ([8981668](https://gitlab.com/mobivia-design/roadtrip/components/commit/89816682304159e78ed191deb50219521b2ad2dc))
* **button:** add download attribute ([c6c75f2](https://gitlab.com/mobivia-design/roadtrip/components/commit/c6c75f280467f295a87499caa3979a41200aaafc))
* **collapse:** add button customisation ([d39bd0d](https://gitlab.com/mobivia-design/roadtrip/components/commit/d39bd0db53e72668f85cf9ef79fa830ce9aadfe3))

### Bug Fixes

* **select:** selected value in options should be optional ([f8bb721](https://gitlab.com/mobivia-design/roadtrip/components/commit/f8bb72100a2b32af65761b83dbc71c8de2f569e5))



### [2.17.6](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.17.5...2.17.6) (2021-06-10)


### Features

* **icon:** add and update icons ([4fa3537](https://gitlab.com/mobivia-design/roadtrip/components/commit/4fa3537cef5eb7c29c1d86f962e393e536867b5a))
* **icon:** add and update icons ([66fe56c](https://gitlab.com/mobivia-design/roadtrip/components/commit/66fe56c52934fdda2cdd6b26d001c49134ee1ff7))
* **icon:** add and update icons ([8aee992](https://gitlab.com/mobivia-design/roadtrip/components/commit/8aee992978f2d29cf83e6c230fd307e33a0752cd))

### Bug Fixes

* **item:** fix detail icon when used in frameworks ([fe613c0](https://gitlab.com/mobivia-design/roadtrip/components/commit/fe613c0fe6610e0c158e0b0d8e39526a2dd6cc88))
* **icon:** fix flag icons ([ff2f8dc](https://gitlab.com/mobivia-design/roadtrip/components/commit/ff2f8dc46b3c30ebbda0cad293502063b3cf31f9))



### [2.17.5](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.17.4...2.17.5) (2021-05-06)


### Features

* **variables:** add hover colors ([8a893ee](https://gitlab.com/mobivia-design/roadtrip/components/commit/8a893ee6c5267f8ba74d7651e9b02a36a7259a66))
* **variables:** add hover colors for themes ([4771fe6](https://gitlab.com/mobivia-design/roadtrip/components/commit/4771fe6459936025cdaec252904e87034b432ca1))
* **variables:** update icon colors ([99d9cf4](https://gitlab.com/mobivia-design/roadtrip/components/commit/99d9cf4defd4dc6b2f438a4b90576079a62b2eaf))
* **variables:** update component colors ([760b903](https://gitlab.com/mobivia-design/roadtrip/components/commit/760b9036f655514d0737bcc938e61e44c15285c3))

### Bug Fixes

* **input:** fix placeholder opacity for firefox ([8647025](https://gitlab.com/mobivia-design/roadtrip/components/commit/864702505d05ba89b9d8a4eb581c79003bc19e76))



### [2.17.4](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.17.3...2.17.4) (2021-04-15)


### Features

* **icon:** add and update icons ([83b6bc6](https://gitlab.com/mobivia-design/roadtrip/components/commit/83b6bc67a149bb27c2614a1952f8b139a82a9c5c))
* **icon:** add and update icons ([b67cc1a](https://gitlab.com/mobivia-design/roadtrip/components/commit/b67cc1a3cf2a1781406d978fbc2d4844f2fb3604))

### Bug Fixes

* **dialog:** send close event after the close animation is finished. ([605ad5f](https://gitlab.com/mobivia-design/roadtrip/components/commit/605ad5ff9d04bfe553d1bbaeb72b0d890949dfed))
* **drawer:** send close event after the close animation is finished. ([96ac0d6](https://gitlab.com/mobivia-design/roadtrip/components/commit/96ac0d663cb5f3cb03f2d75d8423debd38787da1))
* **modal:** send close event after the close animation is finished. ([863860a](https://gitlab.com/mobivia-design/roadtrip/components/commit/863860a68b677a716374355c58178c954e212bbf))



### [2.17.3](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.17.2...2.17.3) (2021-03-31)


### Features

* **icon:** add vehicle tires color variant for each tire ([97d2e8a](https://gitlab.com/mobivia-design/roadtrip/components/commit/97d2e8a48422d3db7d094ed683c8303eefcfc5af))
* **icon:** add vehicle doors color icon variants ([c81664d](https://gitlab.com/mobivia-design/roadtrip/components/commit/c81664d88dfa075b79989bbda6843a6411e724d1))
* **alert:** update icon color ([a630457](https://gitlab.com/mobivia-design/roadtrip/components/commit/a6304576995d3bcd5a7d0b29a7586cdbd0bbb747))
* **toast:** update icon color for warning state ([3720a20](https://gitlab.com/mobivia-design/roadtrip/components/commit/3720a2023c47f5486fbbdeb8dac3384d89b70ee5))
* **icon:** add and update icons ([9d097c1](https://gitlab.com/mobivia-design/roadtrip/components/commit/9d097c1562f65b10a122d2e15fa57a93bad5847b))
* **icon:** add and update icons ([007f1b0](https://gitlab.com/mobivia-design/roadtrip/components/commit/007f1b07e7ec1ce9db6d05fb393277aa9eda9ee7))



### [2.17.2](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.17.1...2.17.2) (2021-03-25)


### Features

* **icon:** add and update icons ([7d43f1c](https://gitlab.com/mobivia-design/roadtrip/components/commit/7d43f1cd7d4177b945195b7329b338fbeb1f3ae8))
* **icon:** add icons ([0da9999](https://gitlab.com/mobivia-design/roadtrip/components/commit/0da999997438c9ef5bab9f4c36be3eec43888ff8))



### [2.17.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.16.0...2.17.0) (2021-03-24)


### Features

* **dialog:** update icon to use outline version ([a440a28](https://gitlab.com/mobivia-design/roadtrip/components/commit/a440a2882d1d3f0de47b965a3fc5bb8d376d1e8c))
* **icon:** add and update icons ([2abe584](https://gitlab.com/mobivia-design/roadtrip/components/commit/2abe584dff33451dc0bf9d49fe0222dd73bf58ac))
* **icon:** add and update icons ([74067f0](https://gitlab.com/mobivia-design/roadtrip/components/commit/74067f041d4719261779eef949e5e64ffd1c202a))



### [2.16.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.15.0...2.16.0) (2021-03-03)


### Features

* add chip component ([ef03729](https://gitlab.com/mobivia-design/roadtrip/components/commit/ef03729261876e4753c0cb3a6d6d76601ed285b8))
* **accordion:** add icon customisation ([0623a88](https://gitlab.com/mobivia-design/roadtrip/components/commit/0623a88e89b9cebbed270c44ca3aabaa89314d36))
* **tooltip:** add line break support ([af4e874](https://gitlab.com/mobivia-design/roadtrip/components/commit/af4e874e546278b4b31985e1e8918948932ce893))
* **checkbox:** Add props to inverse radio and label order ([aa7bf0c](https://gitlab.com/mobivia-design/roadtrip/components/commit/aa7bf0cf889c1f9a5531f163be576b3c7d63caed))
* **radio:** Add props to inverse radio and label order ([86956cd](https://gitlab.com/mobivia-design/roadtrip/components/commit/86956cdf3733e8e1162a3a3539491187aba99a6d))
* add CSS variables for accordion and item customisation ([8924ba7](https://gitlab.com/mobivia-design/roadtrip/components/commit/8924ba7739330e142c7bb59cd556ced0b916ded4))



### [2.15.0](https://gitlab.com/mobivia-design/roadtrip/components/compare/2.14.3...2.15.0) (2021-02-24)


### Features

* **typography:** update font-weight for h7, h8, h9 ([cbe2312](https://gitlab.com/mobivia-design/roadtrip/components/commit/cbe23128550569f8976e22671146f06029db3b7c))
* **drawer, item:** add icon customisation ([5ec8895](https://gitlab.com/mobivia-design/roadtrip/components/commit/5ec8895b18c9abfc417639cbc01baf44ca778954))
* **switch:** customize width of the lever ([f1c1918](https://gitlab.com/mobivia-design/roadtrip/components/commit/f1c19180956579c6821cad2b00e561de2560ac02))
* **breakpoint:** add breakpoint 1200px for desktop users ([af1b10c](https://gitlab.com/mobivia-design/roadtrip/components/commit/af1b10c11405f294544da00c2da5ae002e8b1b18))
* **spacing:** add 4px padding and margin utilities ([203ccc1](https://gitlab.com/mobivia-design/roadtrip/components/commit/203ccc14b56dd297c261b89d902a9a340878d5e3))
* **checkbox:** add left label and spaced props ([987176d](https://gitlab.com/mobivia-design/roadtrip/components/commit/987176deab49bbb43fa48bacd78d9a07d31082fa))
* **accordion:** move margin to the host element ([3820cef](https://gitlab.com/mobivia-design/roadtrip/components/commit/3820cef8c41fd7e771b1a187134223c536f703f6))
